//***********************************************************************************************
//* File: 'Main.cpp'
//* THIS IS A GENERATED FILE: DO NOT EDIT. Please edit the Perfect Developer source file instead!
//*
//* Generated from: 'C:\Users\user\Dropbox\Academico\Pesquisa\pacemaker-project\pacemaker-pd\Main.pd'
//* by Perfect Developer version 6.00.00.03 at 13:45:29 UTC on Thursday April 10th 2014
//* Using command line options:
//* -z1 -el=3 -em=100 -gl=EmbeddedC++ -gs=1 -gv=ISO -gw=100 -gdp=1 -gdo=0 -gdc=3 -gda=1 -gdA=0 -gdl=0 -gdr=0 -gdt=0 -gdi=1 -st=4 -sb=C:\Program Files\Escher Technologies\Verification Studio 6\Bin\builtin.pd -sr=C:\Program Files\Escher Technologies\Verification Studio 6\Bin\rubric.pd -q=0 -gk=Pacemaker -eM=0 -@=C:\Users\user\AppData\Local\Temp\etf402C.tmp
//***********************************************************************************************

#include "Ertsys.hpp"

// File inclusions for forward declarations

#include "Main_0.hpp"

// File inclusions for full declarations

#include "Main_1.hpp"

// File inclusions for inline code

#include "Classes_2.hpp"
#include "Main_2.hpp"


static const _eMDH _aobjLoaderNode (_amoduleData, NULL, NULL, NULL);

namespace Pacemaker
{
    _eInt PulseGenerator :: _nz_PulseWidth (const _eInt x) const
    {
        _mFunction (PulseWidth);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre ((_mInteger (0) < x), "32,15");
        #endif
        return _onDiv (_mInteger (60000), x);
    }
    _eInt PulseGenerator :: _nz_PulsePerMinute (const _eInt x) const
    {
        _mFunction (PulsePerMinute);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre ((_mInteger (0) < x), "36,15");
        #endif
        return _onDiv (_mInteger (60000), x);
    }
    _eInt PulseGenerator :: countAP (const _eInt _rmin, const _eInt _rmax) const
    {
        _mFunction (countAP);
        _eSeq < _n5_quin < MarkerABB :: _eEnum _mComma _eInt _mComma _eInt _mComma _eInt _mComma
            _eInt > > _vThose_40_13 = _eSeq < _n5_quin < MarkerABB :: _eEnum _mComma _eInt _mComma
            _eInt _mComma _eInt _mComma _eInt > > ();
        {
            const _eInt _vCaptureCount_i_40_38 = c_EventMarkers.atrial_marker._oHash ();
            _eInt _vLoopCounter_40_20 = _mInteger (0);
            for (;;)
            {
                if ((_vLoopCounter_40_20 == _vCaptureCount_i_40_38)) break;
                if (((((c_EventMarkers.atrial_marker [_vLoopCounter_40_20].x == MarkerABB :: AP) &&
                    (_mInteger (0) < c_EventMarkers.atrial_marker [_vLoopCounter_40_20].y)) && (
                    _rmin <= _nz_PulsePerMinute (c_EventMarkers.atrial_marker [_vLoopCounter_40_20].
                    y))) && (_nz_PulsePerMinute (c_EventMarkers.atrial_marker [_vLoopCounter_40_20].
                    y) <= _rmax)))
                {
                    _vThose_40_13 = _vThose_40_13.append (c_EventMarkers.atrial_marker [
                        _vLoopCounter_40_20]);
                }
                else
                {
                }
                _vLoopCounter_40_20 = _onSucc (_vLoopCounter_40_20);
            }
        }
        return _vThose_40_13._oHash ();
    }
    _eInt PulseGenerator :: countAS (const _eInt _rmin, const _eInt _rmax) const
    {
        _mFunction (countAS);
        _eSeq < _n5_quin < MarkerABB :: _eEnum _mComma _eInt _mComma _eInt _mComma _eInt _mComma
            _eInt > > _vThose_43_13 = _eSeq < _n5_quin < MarkerABB :: _eEnum _mComma _eInt _mComma
            _eInt _mComma _eInt _mComma _eInt > > ();
        {
            const _eInt _vCaptureCount_i_43_38 = c_EventMarkers.atrial_marker._oHash ();
            _eInt _vLoopCounter_43_20 = _mInteger (0);
            for (;;)
            {
                if ((_vLoopCounter_43_20 == _vCaptureCount_i_43_38)) break;
                if (((((c_EventMarkers.atrial_marker [_vLoopCounter_43_20].x == MarkerABB :: AS) &&
                    (_mInteger (0) < c_EventMarkers.atrial_marker [_vLoopCounter_43_20].y)) && (
                    _rmin <= _nz_PulsePerMinute (c_EventMarkers.atrial_marker [_vLoopCounter_43_20].
                    y))) && (_nz_PulsePerMinute (c_EventMarkers.atrial_marker [_vLoopCounter_43_20].
                    y) <= _rmax)))
                {
                    _vThose_43_13 = _vThose_43_13.append (c_EventMarkers.atrial_marker [
                        _vLoopCounter_43_20]);
                }
                else
                {
                }
                _vLoopCounter_43_20 = _onSucc (_vLoopCounter_43_20);
            }
        }
        return _vThose_43_13._oHash ();
    }
    _eInt PulseGenerator :: countVP (const _eInt _rmin, const _eInt _rmax) const
    {
        _mFunction (countVP);
        _eSeq < _n5_quin < MarkerABB :: _eEnum _mComma _eInt _mComma _eInt _mComma _eInt _mComma
            _eInt > > _vThose_46_13 = _eSeq < _n5_quin < MarkerABB :: _eEnum _mComma _eInt _mComma
            _eInt _mComma _eInt _mComma _eInt > > ();
        {
            const _eInt _vCaptureCount_i_46_38 = c_EventMarkers.ventricular_marker._oHash ();
            _eInt _vLoopCounter_46_20 = _mInteger (0);
            for (;;)
            {
                if ((_vLoopCounter_46_20 == _vCaptureCount_i_46_38)) break;
                if (((((c_EventMarkers.ventricular_marker [_vLoopCounter_46_20].x == MarkerABB :: VP)
                    && (_mInteger (0) < c_EventMarkers.ventricular_marker [_vLoopCounter_46_20].y))
                    && (_rmin <= _nz_PulsePerMinute (c_EventMarkers.ventricular_marker [
                    _vLoopCounter_46_20].y))) && (_nz_PulsePerMinute (c_EventMarkers.
                    ventricular_marker [_vLoopCounter_46_20].y) <= _rmax)))
                {
                    _vThose_46_13 = _vThose_46_13.append (c_EventMarkers.ventricular_marker [
                        _vLoopCounter_46_20]);
                }
                else
                {
                }
                _vLoopCounter_46_20 = _onSucc (_vLoopCounter_46_20);
            }
        }
        return _vThose_46_13._oHash ();
    }
    _eInt PulseGenerator :: countVS (const _eInt _rmin, const _eInt _rmax) const
    {
        _mFunction (countVS);
        _eSeq < _n5_quin < MarkerABB :: _eEnum _mComma _eInt _mComma _eInt _mComma _eInt _mComma
            _eInt > > _vThose_49_13 = _eSeq < _n5_quin < MarkerABB :: _eEnum _mComma _eInt _mComma
            _eInt _mComma _eInt _mComma _eInt > > ();
        {
            const _eInt _vCaptureCount_i_49_38 = c_EventMarkers.ventricular_marker._oHash ();
            _eInt _vLoopCounter_49_20 = _mInteger (0);
            for (;;)
            {
                if ((_vLoopCounter_49_20 == _vCaptureCount_i_49_38)) break;
                if (((((c_EventMarkers.ventricular_marker [_vLoopCounter_49_20].x == MarkerABB :: VS)
                    && (_mInteger (0) < c_EventMarkers.ventricular_marker [_vLoopCounter_49_20].y))
                    && (_rmin <= _nz_PulsePerMinute (c_EventMarkers.ventricular_marker [
                    _vLoopCounter_49_20].y))) && (_nz_PulsePerMinute (c_EventMarkers.
                    ventricular_marker [_vLoopCounter_49_20].y) <= _rmax)))
                {
                    _vThose_49_13 = _vThose_49_13.append (c_EventMarkers.ventricular_marker [
                        _vLoopCounter_49_20]);
                }
                else
                {
                }
                _vLoopCounter_49_20 = _onSucc (_vLoopCounter_49_20);
            }
        }
        return _vThose_49_13._oHash ();
    }
    _eInt PulseGenerator :: countPVC () const
    {
        _mFunction (countPVC);
        _eSeq < _n5_quin < MarkerABB :: _eEnum _mComma _eInt _mComma _eInt _mComma _eInt _mComma
            _eInt > > _vThose_52_13 = _eSeq < _n5_quin < MarkerABB :: _eEnum _mComma _eInt _mComma
            _eInt _mComma _eInt _mComma _eInt > > ();
        {
            const _eInt _vCaptureCount_i_52_38 = c_EventMarkers.ventricular_marker._oHash ();
            _eInt _vLoopCounter_52_20 = _mInteger (0);
            for (;;)
            {
                if ((_vLoopCounter_52_20 == _vCaptureCount_i_52_38)) break;
                if ((c_EventMarkers.ventricular_marker [_vLoopCounter_52_20].x == MarkerABB :: PVC))
                {
                    _vThose_52_13 = _vThose_52_13.append (c_EventMarkers.ventricular_marker [
                        _vLoopCounter_52_20]);
                }
                else
                {
                }
                _vLoopCounter_52_20 = _onSucc (_vLoopCounter_52_20);
            }
        }
        return _vThose_52_13._oHash ();
    }
    _eInt PulseGenerator :: countAT () const
    {
        _mFunction (countAT);
        _eSeq < _n5_quin < MarkerABB :: _eEnum _mComma _eInt _mComma _eInt _mComma _eInt _mComma
            _eInt > > _vThose_54_13 = _eSeq < _n5_quin < MarkerABB :: _eEnum _mComma _eInt _mComma
            _eInt _mComma _eInt _mComma _eInt > > ();
        {
            const _eInt _vCaptureCount_i_54_38 = c_EventMarkers.atrial_marker._oHash ();
            _eInt _vLoopCounter_54_20 = _mInteger (0);
            for (;;)
            {
                if ((_vLoopCounter_54_20 == _vCaptureCount_i_54_38)) break;
                if ((c_EventMarkers.atrial_marker [_vLoopCounter_54_20].x == MarkerABB :: AT))
                {
                    _vThose_54_13 = _vThose_54_13.append (c_EventMarkers.atrial_marker [
                        _vLoopCounter_54_20]);
                }
                else
                {
                }
                _vLoopCounter_54_20 = _onSucc (_vLoopCounter_54_20);
            }
        }
        return _vThose_54_13._oHash ();
    }
    PulseGenerator :: PulseGenerator (const ProgrammableParameters _vc_ProgrammableParameters, const
        MeasuredParameters _vc_MeasuredParameters, const TimeSt _vc_TimeSt, const BO_MODE
        _vc_BO_MODE, const SensingPulse _vc_SensingPulse, const PacingPulse _vc_PacingPulse, const
        BatteryStatus _vc_BatteryStatus, const EventMarkers _vc_EventMarkers, const NoiseDetection
        _vc_NoiseDetection) : _eAny (), c_ProgrammableParameters (_vc_ProgrammableParameters),
        c_MeasuredParameters (_vc_MeasuredParameters), c_TimeSt (_vc_TimeSt), c_SensingPulse (
        _vc_SensingPulse), c_PacingPulse (_vc_PacingPulse), c_BatteryStatus (_vc_BatteryStatus),
        c_EventMarkers (_vc_EventMarkers), c_NoiseDetection (_vc_NoiseDetection), c_BO_MODE (
        _vc_BO_MODE)
    {
        _mBuild;
    }
    _eBool PulseGenerator :: preSetVOO () const
    {
        _mFunction (preSetVOO);
        return (((((((true == c_BO_MODE.isVOO ()) && (_mInteger (0) < c_ProgrammableParameters.c_URL
            .upper_rate_limit)) && (_mInteger (0) < c_ProgrammableParameters.c_LRL.lower_rate_limit))
            && ((((BATT_STATUS_LEVEL :: BOL == c_BatteryStatus.batt_status_level) || (
            BATT_STATUS_LEVEL :: ERN == c_BatteryStatus.batt_status_level)) || (BATT_STATUS_LEVEL ::
            ERT == c_BatteryStatus.batt_status_level)) || (BATT_STATUS_LEVEL :: ERP ==
            c_BatteryStatus.batt_status_level))) && ((c_PacingPulse.last_p_v_pulse + _nz_PulseWidth
            (c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time)) && (_nz_PulseWidth
            (c_ProgrammableParameters.c_LRL.lower_rate_limit) <= (c_TimeSt.time - c_PacingPulse.
            last_p_v_pulse))) && (c_EventMarkers.ventricular_marker.empty () || (c_EventMarkers.
            ventricular_marker [(c_EventMarkers.ventricular_marker._oHash () - _mInteger (1))].m <
            c_TimeSt.time)));
    }
    void PulseGenerator :: _nz_SetVOO ()
    {
        _mSchema (_nz_SetVOO);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (preSetVOO (), "77,9");
        #endif
        c_PacingPulse.chg_paced_ventricular (c_ProgrammableParameters.c_VPulseAmpRegulated.
            v_pulse_amp_regulated.y, c_ProgrammableParameters.c_VPulseWidth.v_pulse_width, c_TimeSt.
            time);
        c_EventMarkers.chg_ventricular_marker (MarkerABB :: VP, c_ProgrammableParameters.
            c_VPulseAmpRegulated.v_pulse_amp_regulated.y, c_ProgrammableParameters.c_VPulseWidth.
            v_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
    }
    _eBool PulseGenerator :: preSetAOO () const
    {
        _mFunction (preSetAOO);
        return (((((((true == c_BO_MODE.isAOO ()) && (_mInteger (0) < c_ProgrammableParameters.c_URL
            .upper_rate_limit)) && (_mInteger (0) < c_ProgrammableParameters.c_LRL.lower_rate_limit))
            && ((((BATT_STATUS_LEVEL :: BOL == c_BatteryStatus.batt_status_level) || (
            BATT_STATUS_LEVEL :: ERN == c_BatteryStatus.batt_status_level)) || (BATT_STATUS_LEVEL ::
            ERT == c_BatteryStatus.batt_status_level)) || (BATT_STATUS_LEVEL :: ERP ==
            c_BatteryStatus.batt_status_level))) && ((c_PacingPulse.last_p_a_pulse + _nz_PulseWidth
            (c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time)) && (_nz_PulseWidth
            (c_ProgrammableParameters.c_LRL.lower_rate_limit) <= (c_TimeSt.time - c_PacingPulse.
            last_p_a_pulse))) && (c_EventMarkers.atrial_marker.empty () || (c_EventMarkers.
            atrial_marker [(c_EventMarkers.atrial_marker._oHash () - _mInteger (1))].m < c_TimeSt.
            time)));
    }
    void PulseGenerator :: _nz_SetAOO ()
    {
        _mSchema (_nz_SetAOO);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (preSetAOO (), "97,9");
        #endif
        c_PacingPulse.chg_paced_atrial (c_ProgrammableParameters.c_APulseAmpRegulated.
            a_pulse_amp_regulated.y, c_ProgrammableParameters.c_APulseWidth.a_pulse_width, c_TimeSt.
            time);
        c_EventMarkers.chg_atrial_marker (MarkerABB :: AP, c_ProgrammableParameters.
            c_APulseAmpRegulated.a_pulse_amp_regulated.y, c_ProgrammableParameters.c_APulseWidth.
            a_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
    }
    _eBool PulseGenerator :: preSetDOO () const
    {
        _mFunction (preSetDOO);
        return (((((((true == c_BO_MODE.isDOO ()) && (_mInteger (0) < c_ProgrammableParameters.c_URL
            .upper_rate_limit)) && (_mInteger (0) < c_ProgrammableParameters.c_LRL.lower_rate_limit))
            && ((BATT_STATUS_LEVEL :: BOL == c_BatteryStatus.batt_status_level) || (
            BATT_STATUS_LEVEL :: ERN == c_BatteryStatus.batt_status_level))) && (((c_PacingPulse.
            last_p_a_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)) ==
            c_TimeSt.time) && (_nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit) <= (
            c_TimeSt.time - c_PacingPulse.last_p_a_pulse)))) && (c_EventMarkers.ventricular_marker.
            empty () || (c_EventMarkers.ventricular_marker [(c_EventMarkers.ventricular_marker.
            _oHash () - _mInteger (1))].m < c_TimeSt.time))) && (c_EventMarkers.atrial_marker.empty
            () || (c_EventMarkers.atrial_marker [(c_EventMarkers.atrial_marker._oHash () - _mInteger
            (1))].m < c_TimeSt.time)));
    }
    void PulseGenerator :: _nz_SetDOO ()
    {
        _mSchema (_nz_SetDOO);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (preSetDOO (), "117,9");
        #endif
        c_PacingPulse.chg_paced_dual (c_ProgrammableParameters.c_APulseWidth.a_pulse_width,
            c_ProgrammableParameters.c_APulseAmpRegulated.a_pulse_amp_regulated.y,
            c_ProgrammableParameters.c_VPulseWidth.v_pulse_width, c_ProgrammableParameters.
            c_VPulseAmpRegulated.v_pulse_amp_regulated.y, c_TimeSt.time);
        c_EventMarkers.chg_dual_marker (MarkerABB :: AP, c_ProgrammableParameters.
            c_APulseAmpRegulated.a_pulse_amp_regulated.y, c_ProgrammableParameters.c_APulseWidth.
            a_pulse_width, MarkerABB :: VP, c_ProgrammableParameters.c_VPulseAmpRegulated.
            v_pulse_amp_regulated.y, c_ProgrammableParameters.c_VPulseWidth.v_pulse_width,
            c_NoiseDetection.noise, c_TimeSt.time);
    }
    _eBool PulseGenerator :: preSetDDI () const
    {
        _mFunction (preSetDDI);
        return (((((true == c_BO_MODE.isDDI ()) && ((BATT_STATUS_LEVEL :: BOL == c_BatteryStatus.
            batt_status_level) || (BATT_STATUS_LEVEL :: ERN == c_BatteryStatus.batt_status_level)))
            && (((((((_nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit) -
            c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) + c_PacingPulse.last_p_v_pulse)
            == (c_TimeSt.time - c_PacingPulse.last_p_v_pulse)) || (((c_PacingPulse.last_p_v_pulse +
            c_ProgrammableParameters.c_PVARP.pvarp) < c_SensingPulse.last_s_a_pulse) && (
            c_SensingPulse.last_s_a_pulse < ((_nz_PulseWidth (c_ProgrammableParameters.c_URL.
            upper_rate_limit) - c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) +
            c_PacingPulse.last_p_v_pulse)))) || ((((c_PacingPulse.last_p_v_pulse +
            c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) == c_PacingPulse.last_p_a_pulse)
            && (c_PacingPulse.last_p_v_pulse < c_SensingPulse.last_s_v_pulse)) && (c_SensingPulse.
            last_s_v_pulse < (c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (
            c_ProgrammableParameters.c_URL.upper_rate_limit))))) || (((((_nz_PulseWidth (
            c_ProgrammableParameters.c_URL.upper_rate_limit) - c_ProgrammableParameters.
            c_FixedAVDelay.fixed_av_delay) + c_PacingPulse.last_p_v_pulse) == c_PacingPulse.
            last_p_a_pulse) && (c_SensingPulse.last_s_v_pulse < c_PacingPulse.last_p_v_pulse)) && ((
            c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.
            upper_rate_limit)) == c_TimeSt.time))) || (((((((c_PacingPulse.last_p_v_pulse +
            c_ProgrammableParameters.c_PVARP.pvarp) < c_SensingPulse.last_s_a_pulse) && (
            c_SensingPulse.last_s_a_pulse < ((_nz_PulseWidth (c_ProgrammableParameters.c_URL.
            upper_rate_limit) - c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) +
            c_PacingPulse.last_p_v_pulse))) && (c_PacingPulse.last_p_v_pulse < c_SensingPulse.
            last_s_v_pulse)) && (c_SensingPulse.last_s_v_pulse < (c_PacingPulse.last_p_v_pulse +
            _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)))) && (_mInteger (0) <
            c_ProgrammableParameters.c_URL.upper_rate_limit)) && (_mInteger (0) <
            c_ProgrammableParameters.c_LRL.lower_rate_limit)))) && (c_EventMarkers.
            ventricular_marker.empty () || (c_EventMarkers.ventricular_marker [(c_EventMarkers.
            ventricular_marker._oHash () - _mInteger (1))].m < c_TimeSt.time))) && (c_EventMarkers.
            atrial_marker.empty () || (c_EventMarkers.atrial_marker [(c_EventMarkers.atrial_marker.
            _oHash () - _mInteger (1))].m < c_TimeSt.time)));
    }
    void PulseGenerator :: _nz_SetDDI ()
    {
        _mSchema (_nz_SetDDI);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (preSetDDI (), "171,9");
        #endif
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre ((_mInteger (0) < c_ProgrammableParameters.c_URL.upper_rate_limit),
            "171,69");
        #endif
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre ((_mInteger (0) < c_ProgrammableParameters.c_LRL.lower_rate_limit),
            "171,124");
        #endif
        if ((((_nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit) -
            c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) + c_PacingPulse.last_p_v_pulse)
            == (c_TimeSt.time - c_PacingPulse.last_p_v_pulse)))
        {
            c_PacingPulse.chg_paced_atrial (c_ProgrammableParameters.c_APulseWidth.a_pulse_width,
                c_ProgrammableParameters.c_APulseAmpRegulated.a_pulse_amp_regulated.y, c_TimeSt.time)
                ;
            c_EventMarkers.chg_atrial_marker (MarkerABB :: AP, c_ProgrammableParameters.
                c_APulseAmpRegulated.a_pulse_amp_regulated.y, c_ProgrammableParameters.c_APulseWidth
                .a_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
        }
        else if (((((c_PacingPulse.last_p_v_pulse + c_ProgrammableParameters.c_PVARP.pvarp) <
            c_SensingPulse.last_s_a_pulse) && (c_SensingPulse.last_s_a_pulse < ((_nz_PulseWidth (
            c_ProgrammableParameters.c_URL.upper_rate_limit) - c_ProgrammableParameters.
            c_FixedAVDelay.fixed_av_delay) + c_PacingPulse.last_p_v_pulse))) && (_mInteger (0) <
            c_ProgrammableParameters.c_URL.upper_rate_limit)))
        {
            c_EventMarkers.unchg_atrial_marker ();
        }
        else if ((((((c_PacingPulse.last_p_v_pulse + c_ProgrammableParameters.c_FixedAVDelay.
            fixed_av_delay) == c_PacingPulse.last_p_a_pulse) && (c_PacingPulse.last_p_v_pulse <
            c_SensingPulse.last_s_v_pulse)) && (c_SensingPulse.last_s_v_pulse < (c_PacingPulse.
            last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)))) &&
            (_mInteger (0) < c_ProgrammableParameters.c_URL.upper_rate_limit)))
        {
            c_EventMarkers.unchg_ventricular_marker ();
        }
        else if ((((((((c_PacingPulse.last_p_v_pulse + c_ProgrammableParameters.c_PVARP.pvarp) <
            c_SensingPulse.last_s_a_pulse) && (c_SensingPulse.last_s_a_pulse < ((_nz_PulseWidth (
            c_ProgrammableParameters.c_URL.upper_rate_limit) - c_ProgrammableParameters.
            c_FixedAVDelay.fixed_av_delay) + c_PacingPulse.last_p_v_pulse))) && (c_PacingPulse.
            last_p_v_pulse < c_SensingPulse.last_s_v_pulse)) && (_mInteger (0) <
            c_ProgrammableParameters.c_URL.upper_rate_limit)) && (_mInteger (0) <
            c_ProgrammableParameters.c_LRL.lower_rate_limit)) && (c_SensingPulse.last_s_v_pulse < (
            c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.
            upper_rate_limit)))))
        {
            c_EventMarkers.unchg_ventricular_marker ();
        }
        else if (((((((_nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit) -
            c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) + c_PacingPulse.last_p_v_pulse)
            == c_PacingPulse.last_p_a_pulse) && (c_SensingPulse.last_s_v_pulse < c_PacingPulse.
            last_p_v_pulse)) && ((c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (
            c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time)) && (_mInteger (0) <
            c_ProgrammableParameters.c_URL.upper_rate_limit)))
        {
            c_PacingPulse.chg_paced_ventricular (c_ProgrammableParameters.c_VPulseWidth.
                v_pulse_width, c_ProgrammableParameters.c_VPulseAmpRegulated.v_pulse_amp_regulated.y,
                c_TimeSt.time);
            c_EventMarkers.chg_ventricular_marker (MarkerABB :: VP, c_ProgrammableParameters.
                c_VPulseAmpRegulated.v_pulse_amp_regulated.y, c_ProgrammableParameters.c_VPulseWidth
                .v_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
        }
        else
        {
        }
    }
    _eBool PulseGenerator :: preSetVDD () const
    {
        _mFunction (preSetVDD);
        return ((((((true == c_BO_MODE.isVDD ()) && (_mInteger (0) < c_ProgrammableParameters.c_URL.
            upper_rate_limit)) && ((BATT_STATUS_LEVEL :: BOL == c_BatteryStatus.batt_status_level)
            || (BATT_STATUS_LEVEL :: ERN == c_BatteryStatus.batt_status_level))) && ((((((((
            c_SensingPulse.last_s_a_pulse < c_PacingPulse.last_p_v_pulse) && ((c_SensingPulse.
            last_s_a_pulse + c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) == c_TimeSt.
            time)) && (c_SensingPulse.last_s_v_pulse < c_TimeSt.time)) || (((c_SensingPulse.
            last_s_a_pulse < c_SensingPulse.last_s_v_pulse) && ((c_SensingPulse.last_s_a_pulse +
            c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) == c_TimeSt.time)) && (
            c_SensingPulse.last_s_v_pulse < c_TimeSt.time))) || ((c_SensingPulse.last_s_v_pulse <
            c_SensingPulse.last_s_a_pulse) && ((c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (
            c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time))) || ((c_PacingPulse
            .last_p_v_pulse < c_SensingPulse.last_s_a_pulse) && ((c_PacingPulse.last_p_v_pulse +
            _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time))) ||
            (((c_SensingPulse.last_s_a_pulse < c_PacingPulse.last_p_v_pulse) && ((c_PacingPulse.
            last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)) ==
            c_TimeSt.time)) && (c_TimeSt.time < (c_PacingPulse.last_p_v_pulse +
            c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay)))) || (((c_SensingPulse.
            last_s_a_pulse < c_SensingPulse.last_s_v_pulse) && ((c_PacingPulse.last_p_v_pulse +
            _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time)) &&
            (c_TimeSt.time < (c_PacingPulse.last_p_v_pulse + c_ProgrammableParameters.c_FixedAVDelay
            .fixed_av_delay))))) && (c_EventMarkers.ventricular_marker.empty () || (c_EventMarkers.
            ventricular_marker [(c_EventMarkers.ventricular_marker._oHash () - _mInteger (1))].m <
            c_TimeSt.time))) && (c_EventMarkers.atrial_marker.empty () || (c_EventMarkers.
            atrial_marker [(c_EventMarkers.atrial_marker._oHash () - _mInteger (1))].m < c_TimeSt.
            time)));
    }
    void PulseGenerator :: _nz_SetVDD ()
    {
        _mSchema (_nz_SetVDD);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (preSetVDD (), "270,9");
        #endif
        c_PacingPulse.chg_paced_ventricular (c_ProgrammableParameters.c_VPulseWidth.v_pulse_width,
            c_ProgrammableParameters.c_VPulseAmpRegulated.v_pulse_amp_regulated.y, c_TimeSt.time);
        c_EventMarkers.chg_ventricular_marker (MarkerABB :: VP, c_ProgrammableParameters.
            c_VPulseAmpRegulated.v_pulse_amp_regulated.y, c_ProgrammableParameters.c_VPulseWidth.
            v_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
    }
    _eBool PulseGenerator :: preSetVVI () const
    {
        _mFunction (preSetVVI);
        return ((((((true == c_BO_MODE.isVVI ()) && (_mInteger (0) < c_ProgrammableParameters.c_URL.
            upper_rate_limit)) && (_mInteger (0) < c_ProgrammableParameters.c_HysteresisRateLimit.
            hysteresis_rate_limit.y)) && ((BATT_STATUS_LEVEL :: BOL == c_BatteryStatus.
            batt_status_level) || (BATT_STATUS_LEVEL :: ERN == c_BatteryStatus.batt_status_level)))
            && ((((((c_TimeSt.time - c_PacingPulse.last_p_v_pulse) < (c_TimeSt.time - c_SensingPulse
            .last_s_v_pulse)) && ((c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (
            c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time)) && (
            c_ProgrammableParameters.c_VRefractoryPeriod.v_refract_period <= (c_PacingPulse.
            last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)))) &&
            (_nz_PulseWidth (c_ProgrammableParameters.c_LRL.lower_rate_limit) <= (c_TimeSt.time -
            c_PacingPulse.last_p_v_pulse))) || (((((c_TimeSt.time - c_SensingPulse.last_s_v_pulse) <
            (c_TimeSt.time - c_PacingPulse.last_p_v_pulse)) && ((c_SensingPulse.last_s_v_pulse +
            _nz_PulseWidth (c_ProgrammableParameters.c_HysteresisRateLimit.hysteresis_rate_limit.y))
            == c_TimeSt.time)) && (c_ProgrammableParameters.c_VRefractoryPeriod.v_refract_period <=
            (c_SensingPulse.last_s_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.
            c_HysteresisRateLimit.hysteresis_rate_limit.y)))) && (c_ProgrammableParameters.c_URL.
            upper_rate_limit <= (c_TimeSt.time - c_SensingPulse.last_s_v_pulse))))) && (
            c_EventMarkers.ventricular_marker.empty () || (c_EventMarkers.ventricular_marker [(
            c_EventMarkers.ventricular_marker._oHash () - _mInteger (1))].m < c_TimeSt.time)));
    }
    void PulseGenerator :: _nz_SetVVI ()
    {
        _mSchema (_nz_SetVVI);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (preSetVVI (), "307,9");
        #endif
        c_PacingPulse.chg_paced_ventricular (c_ProgrammableParameters.c_VPulseWidth.v_pulse_width,
            c_ProgrammableParameters.c_VPulseAmpRegulated.v_pulse_amp_regulated.y, c_TimeSt.time);
        c_EventMarkers.chg_ventricular_marker (MarkerABB :: VP, c_ProgrammableParameters.
            c_VPulseAmpRegulated.v_pulse_amp_regulated.y, c_ProgrammableParameters.c_VPulseWidth.
            v_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
    }
    _eBool PulseGenerator :: preSetAAI () const
    {
        _mFunction (preSetAAI);
        return (((((((true == c_BO_MODE.isAAI ()) && (_mInteger (0) < c_ProgrammableParameters.c_URL
            .upper_rate_limit)) && (_mInteger (0) < c_ProgrammableParameters.c_HysteresisRateLimit.
            hysteresis_rate_limit.y)) && ((((BATT_STATUS_LEVEL :: BOL == c_BatteryStatus.
            batt_status_level) || (BATT_STATUS_LEVEL :: ERN == c_BatteryStatus.batt_status_level))
            || (BATT_STATUS_LEVEL :: ERT == c_BatteryStatus.batt_status_level)) || (
            BATT_STATUS_LEVEL :: ERP == c_BatteryStatus.batt_status_level))) && ((((((c_TimeSt.time
            - c_PacingPulse.last_p_a_pulse) < (c_TimeSt.time - c_SensingPulse.last_s_a_pulse)) && ((
            c_PacingPulse.last_p_a_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.
            upper_rate_limit)) == c_TimeSt.time)) && (c_ProgrammableParameters.c_ARefractoryPeriod.
            a_refract_period <= (c_PacingPulse.last_p_a_pulse + _nz_PulseWidth (
            c_ProgrammableParameters.c_URL.upper_rate_limit)))) && (_nz_PulseWidth (
            c_ProgrammableParameters.c_LRL.lower_rate_limit) <= (c_TimeSt.time - c_PacingPulse.
            last_p_a_pulse))) || ((((c_TimeSt.time - c_SensingPulse.last_s_a_pulse) < (c_TimeSt.time
            - c_PacingPulse.last_p_a_pulse)) && ((c_SensingPulse.last_s_a_pulse + _nz_PulseWidth (
            c_ProgrammableParameters.c_HysteresisRateLimit.hysteresis_rate_limit.y)) == c_TimeSt.
            time)) && (c_ProgrammableParameters.c_ARefractoryPeriod.a_refract_period <= (
            c_PacingPulse.last_p_a_pulse + _nz_PulseWidth (c_ProgrammableParameters.
            c_HysteresisRateLimit.hysteresis_rate_limit.y)))))) && (SWITCH :: ON ==
            c_ProgrammableParameters.c_HysteresisRateLimit.hysteresis_rate_limit.x)) && (
            c_EventMarkers.atrial_marker.empty () || (c_EventMarkers.atrial_marker [(c_EventMarkers.
            atrial_marker._oHash () - _mInteger (1))].m < c_TimeSt.time)));
    }
    void PulseGenerator :: _nz_SetAAI ()
    {
        _mSchema (_nz_SetAAI);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (preSetAAI (), "338,9");
        #endif
        c_PacingPulse.chg_paced_atrial (c_ProgrammableParameters.c_APulseWidth.a_pulse_width,
            c_ProgrammableParameters.c_APulseAmpRegulated.a_pulse_amp_regulated.y, c_TimeSt.time);
        c_EventMarkers.chg_atrial_marker (MarkerABB :: AP, c_ProgrammableParameters.
            c_APulseAmpRegulated.a_pulse_amp_regulated.y, c_ProgrammableParameters.c_APulseWidth.
            a_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
    }
    _eBool PulseGenerator :: preSetVVT () const
    {
        _mFunction (preSetVVT);
        return (((((true == c_BO_MODE.isVVT ()) && (_mInteger (0) < c_ProgrammableParameters.c_URL.
            upper_rate_limit)) && ((((BATT_STATUS_LEVEL :: BOL == c_BatteryStatus.batt_status_level)
            || (BATT_STATUS_LEVEL :: ERN == c_BatteryStatus.batt_status_level)) || (
            BATT_STATUS_LEVEL :: ERT == c_BatteryStatus.batt_status_level)) || (BATT_STATUS_LEVEL ::
            ERP == c_BatteryStatus.batt_status_level))) && ((((((c_PacingPulse.last_p_v_pulse +
            _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time) && (
            c_ProgrammableParameters.c_VRefractoryPeriod.v_refract_period <= (c_PacingPulse.
            last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)))) &&
            (_nz_PulseWidth (c_ProgrammableParameters.c_LRL.lower_rate_limit) <= (c_TimeSt.time -
            c_PacingPulse.last_p_v_pulse))) || (((c_TimeSt.time < (c_SensingPulse.last_s_v_pulse +
            _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit))) && (
            c_ProgrammableParameters.c_VRefractoryPeriod.v_refract_period <= (c_SensingPulse.
            last_s_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)))) &&
            (_nz_PulseWidth (c_ProgrammableParameters.c_LRL.lower_rate_limit) <= (c_SensingPulse.
            last_s_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit))))) ||
            ((c_TimeSt.time == c_SensingPulse.last_s_v_pulse) && (c_ProgrammableParameters.
            c_VRefractoryPeriod.v_refract_period <= (c_SensingPulse.last_s_v_pulse + _nz_PulseWidth
            (c_ProgrammableParameters.c_URL.upper_rate_limit)))))) && (c_EventMarkers.
            ventricular_marker.empty () || (c_EventMarkers.ventricular_marker [(c_EventMarkers.
            ventricular_marker._oHash () - _mInteger (1))].m < c_TimeSt.time)));
    }
    void PulseGenerator :: _nz_SetVVT ()
    {
        _mSchema (_nz_SetVVT);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (preSetVVT (), "374,9");
        #endif
        c_PacingPulse.chg_paced_ventricular (c_ProgrammableParameters.c_VPulseAmpRegulated.
            v_pulse_amp_regulated.y, c_ProgrammableParameters.c_VPulseWidth.v_pulse_width, c_TimeSt.
            time);
        c_EventMarkers.chg_ventricular_marker (MarkerABB :: VP, c_ProgrammableParameters.
            c_VPulseAmpRegulated.v_pulse_amp_regulated.y, c_ProgrammableParameters.c_VPulseWidth.
            v_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
    }
    _eBool PulseGenerator :: preSetAAT () const
    {
        _mFunction (preSetAAT);
        return ((((((true == c_BO_MODE.isAAT ()) && (_mInteger (0) < c_ProgrammableParameters.c_URL.
            upper_rate_limit)) && (_mInteger (0) < c_ProgrammableParameters.c_LRL.lower_rate_limit))
            && ((((BATT_STATUS_LEVEL :: BOL == c_BatteryStatus.batt_status_level) || (
            BATT_STATUS_LEVEL :: ERN == c_BatteryStatus.batt_status_level)) || (BATT_STATUS_LEVEL ::
            ERT == c_BatteryStatus.batt_status_level)) || (BATT_STATUS_LEVEL :: ERP ==
            c_BatteryStatus.batt_status_level))) && ((((((c_PacingPulse.last_p_a_pulse +
            _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time) && (
            c_ProgrammableParameters.c_ARefractoryPeriod.a_refract_period <= (c_PacingPulse.
            last_p_a_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)))) &&
            (_nz_PulseWidth (c_ProgrammableParameters.c_LRL.lower_rate_limit) <= (c_PacingPulse.
            last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)))) ||
            ((c_TimeSt.time < (c_SensingPulse.last_s_a_pulse + _nz_PulseWidth (
            c_ProgrammableParameters.c_URL.upper_rate_limit))) && (c_ProgrammableParameters.
            c_ARefractoryPeriod.a_refract_period <= (c_SensingPulse.last_s_a_pulse + _nz_PulseWidth
            (c_ProgrammableParameters.c_URL.upper_rate_limit))))) || ((c_TimeSt.time ==
            c_SensingPulse.last_s_a_pulse) && (c_ProgrammableParameters.c_ARefractoryPeriod.
            a_refract_period <= (c_SensingPulse.last_s_a_pulse + _nz_PulseWidth (
            c_ProgrammableParameters.c_URL.upper_rate_limit)))))) && (c_EventMarkers.atrial_marker.
            empty () || (c_EventMarkers.atrial_marker [(c_EventMarkers.atrial_marker._oHash () -
            _mInteger (1))].m < c_TimeSt.time)));
    }
    void PulseGenerator :: _nz_SetAAT ()
    {
        _mSchema (_nz_SetAAT);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (preSetAAT (), "408,9");
        #endif
        c_PacingPulse.chg_paced_atrial (c_ProgrammableParameters.c_APulseWidth.a_pulse_width,
            c_ProgrammableParameters.c_APulseAmpRegulated.a_pulse_amp_regulated.y, c_TimeSt.time);
        c_EventMarkers.chg_atrial_marker (MarkerABB :: AP, c_ProgrammableParameters.
            c_APulseAmpRegulated.a_pulse_amp_regulated.y, c_ProgrammableParameters.c_APulseWidth.
            a_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
    }
    _eBool PulseGenerator :: preAtriumStartTime () const
    {
        _mFunction (preAtriumStartTime);
        return (((CHAMBERS :: C_DUAL == c_BO_MODE.bo_mode.chambers_sensed) || (CHAMBERS :: C_ATRIUM
            == c_BO_MODE.bo_mode.chambers_sensed)) && ((c_MeasuredParameters.c_PWave.p_wave <
            c_TimeSt.a_curr_measurement) && (_mInteger (0) == c_MeasuredParameters.c_PWave.p_wave)));
    }
    void PulseGenerator :: _nz_AtriumStartTime ()
    {
        _mSchema (_nz_AtriumStartTime);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (preAtriumStartTime (), "424,10");
        #endif
        c_MeasuredParameters.set_p_wave (c_TimeSt.a_curr_measurement);
        c_TimeSt.chg_a_start_time (c_TimeSt.time);
    }
    _eBool PulseGenerator :: preAtriumMax () const
    {
        _mFunction (preAtriumMax);
        return (((CHAMBERS :: C_DUAL == c_BO_MODE.bo_mode.chambers_sensed) || (CHAMBERS :: C_ATRIUM
            == c_BO_MODE.bo_mode.chambers_sensed)) && ((c_MeasuredParameters.c_PWave.p_wave <
            c_TimeSt.a_curr_measurement) && (_mInteger (0) < c_MeasuredParameters.c_PWave.p_wave)));
    }
    void PulseGenerator :: _nz_AtriumMax ()
    {
        _mSchema (_nz_AtriumMax);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (preAtriumMax (), "436,9");
        #endif
        c_MeasuredParameters.set_p_wave (c_TimeSt.a_curr_measurement);
        c_TimeSt.chg_a_max (c_TimeSt.a_curr_measurement);
    }
    _eBool PulseGenerator :: preAtriumEndTime () const
    {
        _mFunction (preAtriumEndTime);
        return (((CHAMBERS :: C_DUAL == c_BO_MODE.bo_mode.chambers_sensed) || (CHAMBERS :: C_ATRIUM
            == c_BO_MODE.bo_mode.chambers_sensed)) && ((_mInteger (0) == c_TimeSt.a_curr_measurement)
            && (_mInteger (0) < c_MeasuredParameters.c_PWave.p_wave)));
    }
    void PulseGenerator :: _nz_AtriumEndTime ()
    {
        _mSchema (_nz_AtriumEndTime);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (preAtriumEndTime (), "447,9");
        #endif
        c_MeasuredParameters.set_p_wave (_mInteger (0));
        c_TimeSt.chg_a_delay (c_TimeSt.time);
    }
    _eBool PulseGenerator :: preAtrialMeasurement () const
    {
        _mFunction (preAtrialMeasurement);
        return ((preAtriumStartTime () || preAtriumMax ()) || preAtriumEndTime ());
    }
    void PulseGenerator :: _nz_AtrialMeasurement ()
    {
        _mSchema (_nz_AtrialMeasurement);
        if (preAtriumStartTime ())
        {
            _nz_AtriumStartTime ();
        }
        else if (preAtriumMax ())
        {
            _nz_AtriumMax ();
        }
        else if (preAtriumEndTime ())
        {
            _nz_AtriumEndTime ();
        }
        else
        {
        }
    }
    _eBool PulseGenerator :: preVentricularStartTime () const
    {
        _mFunction (preVentricularStartTime);
        return (((CHAMBERS :: C_DUAL == c_BO_MODE.bo_mode.chambers_sensed) || (CHAMBERS ::
            C_VENTRICLE == c_BO_MODE.bo_mode.chambers_sensed)) && ((c_MeasuredParameters.c_RWave.
            r_wave < c_TimeSt.v_curr_measurement) && (_mInteger (0) == c_MeasuredParameters.c_RWave.
            r_wave)));
    }
    void PulseGenerator :: _nz_VentricularStartTime ()
    {
        _mSchema (_nz_VentricularStartTime);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (preVentricularStartTime (), "470,9");
        #endif
        c_MeasuredParameters.set_r_wave (c_TimeSt.v_curr_measurement);
        c_TimeSt.chg_v_start_time (c_TimeSt.time);
    }
    _eBool PulseGenerator :: preVentricularMax () const
    {
        _mFunction (preVentricularMax);
        return (((CHAMBERS :: C_DUAL == c_BO_MODE.bo_mode.chambers_sensed) || (CHAMBERS ::
            C_VENTRICLE == c_BO_MODE.bo_mode.chambers_sensed)) && ((c_MeasuredParameters.c_RWave.
            r_wave < c_TimeSt.v_curr_measurement) && (_mInteger (0) < c_MeasuredParameters.c_RWave.
            r_wave)));
    }
    void PulseGenerator :: _nz_VentricularMax ()
    {
        _mSchema (_nz_VentricularMax);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (preVentricularMax (), "482,9");
        #endif
        c_MeasuredParameters.set_r_wave (c_TimeSt.v_curr_measurement);
        c_TimeSt.chg_v_max (c_TimeSt.v_curr_measurement);
    }
    _eBool PulseGenerator :: preVentricularEndTime () const
    {
        _mFunction (preVentricularEndTime);
        return (((CHAMBERS :: C_DUAL == c_BO_MODE.bo_mode.chambers_sensed) || (CHAMBERS ::
            C_VENTRICLE == c_BO_MODE.bo_mode.chambers_sensed)) && ((_mInteger (0) == c_TimeSt.
            v_curr_measurement) && (_mInteger (0) < c_MeasuredParameters.c_RWave.r_wave)));
    }
    void PulseGenerator :: _nz_VentricularEndTime ()
    {
        _mSchema (_nz_VentricularEndTime);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (preVentricularEndTime (), "493,9");
        #endif
        c_MeasuredParameters.set_r_wave (_mInteger (0));
        c_TimeSt.chg_v_delay (c_TimeSt.time);
    }
    _eBool PulseGenerator :: preVentricularMeasurement () const
    {
        _mFunction (preVentricularMeasurement);
        return ((preVentricularStartTime () || preVentricularMax ()) || preVentricularEndTime ());
    }
    void PulseGenerator :: _nz_VentricularMeasurement ()
    {
        _mSchema (_nz_VentricularMeasurement);
        if (preVentricularStartTime ())
        {
            _nz_VentricularStartTime ();
        }
        else if (preVentricularMax ())
        {
            _nz_VentricularMax ();
        }
        else if (preVentricularEndTime ())
        {
            _nz_VentricularEndTime ();
        }
        else
        {
        }
    }
    _eBool PulseGenerator :: preSensingModule () const
    {
        _mFunction (preSensingModule);
        return (preAtrialMeasurement () || preVentricularMeasurement ());
    }
    _eBool PulseGenerator :: preAtrialSensedMarkerA () const
    {
        _mFunction (preAtrialSensedMarkerA);
        return (((_mInteger (1) < c_EventMarkers.atrial_marker._oHash ()) && (c_EventMarkers.
            atrial_marker [(c_EventMarkers.atrial_marker._oHash () - _mInteger (1))].m < c_TimeSt.
            time)) && ((c_TimeSt.a_start_time - c_EventMarkers.atrial_marker [(c_EventMarkers.
            atrial_marker._oHash () - _mInteger (1))].m) < _nz_PulseWidth (c_ProgrammableParameters.
            c_URL.upper_rate_limit)));
    }
    _eBool PulseGenerator :: preAtrialSensedMarkerB () const
    {
        _mFunction (preAtrialSensedMarkerB);
        return (((_mInteger (1) < c_EventMarkers.atrial_marker._oHash ()) && (c_EventMarkers.
            atrial_marker [(c_EventMarkers.atrial_marker._oHash () - _mInteger (1))].m < c_TimeSt.
            time)) && (_nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit) < (c_TimeSt.
            a_start_time - c_EventMarkers.atrial_marker [(c_EventMarkers.atrial_marker._oHash () -
            _mInteger (1))].m)));
    }
    _eBool PulseGenerator :: preAtrialSensedMarker () const
    {
        _mFunction (preAtrialSensedMarker);
        return (preAtrialSensedMarkerA () || preAtrialSensedMarkerB ());
    }
    void PulseGenerator :: _nz_AtrialSensedMarker ()
    {
        _mSchema (_nz_AtrialSensedMarker);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (((CHAMBERS :: C_ATRIUM == c_BO_MODE.bo_mode.chambers_sensed) || (
            CHAMBERS :: C_DUAL == c_BO_MODE.bo_mode.chambers_sensed)), "525,44");
        #endif
        if (c_EventMarkers.atrial_marker.empty ())
        {
            c_EventMarkers.chg_atrial_marker (MarkerABB :: AS, (c_TimeSt.a_delay - c_TimeSt.
                a_start_time), c_TimeSt.a_max, c_NoiseDetection.noise, c_TimeSt.time);
            c_SensingPulse.chg_sensed_atrial ((c_TimeSt.a_delay - c_TimeSt.a_start_time), c_TimeSt.
                a_max, c_TimeSt.time);
        }
        else if (preAtrialSensedMarkerA ())
        {
            c_EventMarkers.chg_atrial_marker (MarkerABB :: AS, (c_TimeSt.a_delay - c_TimeSt.
                a_start_time), c_TimeSt.a_max, c_NoiseDetection.noise, c_TimeSt.time);
            c_SensingPulse.chg_sensed_atrial ((c_TimeSt.a_delay - c_TimeSt.a_start_time), c_TimeSt.
                a_max, c_TimeSt.time);
        }
        else if (preAtrialSensedMarkerB ())
        {
            c_EventMarkers.chg_atrial_marker (MarkerABB :: AT, (c_TimeSt.a_delay - c_TimeSt.
                a_start_time), c_TimeSt.a_max, c_NoiseDetection.noise, c_TimeSt.time);
            c_SensingPulse.chg_sensed_atrial ((c_TimeSt.a_delay - c_TimeSt.a_start_time), c_TimeSt.
                a_max, c_TimeSt.time);
        }
        else
        {
        }
    }
    _eBool PulseGenerator :: preVentricularSensedMarker () const
    {
        _mFunction (preVentricularSensedMarker);
        return (((c_TimeSt.v_start_time <= c_TimeSt.v_delay) && (c_EventMarkers.ventricular_marker.
            empty () || (c_EventMarkers.ventricular_marker [(c_EventMarkers.ventricular_marker.
            _oHash () - _mInteger (1))].m < c_TimeSt.v_start_time))) && ((CHAMBERS :: C_VENTRICLE ==
            c_BO_MODE.bo_mode.chambers_sensed) || (CHAMBERS :: C_DUAL == c_BO_MODE.bo_mode.
            chambers_sensed)));
    }
    void PulseGenerator :: _nz_VentricularSensedMarker ()
    {
        _mSchema (_nz_VentricularSensedMarker);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (preVentricularSensedMarker (), "552,13");
        #endif
        if ((c_EventMarkers.ventricular_marker.empty () && c_EventMarkers.atrial_marker.empty ()))
        {
            c_SensingPulse.chg_sensed_ventricular ((c_TimeSt.v_delay - c_TimeSt.v_start_time),
                c_TimeSt.v_max, c_TimeSt.v_start_time, _mInteger (0));
            c_EventMarkers.chg_ventricular_marker (MarkerABB :: VS, c_TimeSt.v_max, (c_TimeSt.
                v_delay - c_TimeSt.v_start_time), c_NoiseDetection.noise, c_TimeSt.v_start_time);
        }
        else
        {
            _eBool _vCondResult_564_49;
            if (((_mInteger (1) < c_EventMarkers.ventricular_marker._oHash ()) && (!c_EventMarkers.
                atrial_marker.empty ())))
            {
                const _eInt _vCaptureCommonOperand_566_53 = c_EventMarkers.atrial_marker.last ().m;
                _vCondResult_564_49 = ((c_EventMarkers.ventricular_marker.last ().m <
                    _vCaptureCommonOperand_566_53) && (_vCaptureCommonOperand_566_53 < c_TimeSt.
                    v_start_time));
            }
            else
            {
                _vCondResult_564_49 = false;
            }
            if (_vCondResult_564_49)
            {
                c_SensingPulse.chg_sensed_ventricular ((c_TimeSt.v_delay - c_TimeSt.v_start_time),
                    c_TimeSt.v_max, c_TimeSt.v_start_time, (c_EventMarkers.ventricular_marker.last ()
                    .m - c_EventMarkers.ventricular_marker.front ().last ().m));
                c_EventMarkers.chg_ventricular_marker (MarkerABB :: VS, c_TimeSt.v_max, (c_TimeSt.
                    v_delay - c_TimeSt.v_start_time), c_NoiseDetection.noise, c_TimeSt.v_start_time);
            }
            else
            {
                _eBool _vCondResult_580_49;
                if ((((_mInteger (1) < c_EventMarkers.ventricular_marker._oHash ()) && (!
                    c_EventMarkers.ventricular_marker.empty ())) && (!c_EventMarkers.atrial_marker.
                    empty ())))
                {
                    const _eInt _vCaptureCommonOperand_582_53 = c_EventMarkers.atrial_marker.last ()
                        .m;
                    _vCondResult_580_49 = (!((c_EventMarkers.ventricular_marker.last ().m <
                        _vCaptureCommonOperand_582_53) && (_vCaptureCommonOperand_582_53 < c_TimeSt.
                        v_start_time)));
                }
                else
                {
                    _vCondResult_580_49 = false;
                }
                if (_vCondResult_580_49)
                {
                    c_SensingPulse.chg_sensed_ventricular ((c_TimeSt.v_delay - c_TimeSt.v_start_time)
                        , c_TimeSt.v_max, c_TimeSt.v_start_time, (c_EventMarkers.ventricular_marker.
                        last ().m - c_EventMarkers.ventricular_marker.front ().last ().m));
                    c_EventMarkers.chg_ventricular_marker (MarkerABB :: PVC, c_TimeSt.v_max, (
                        c_TimeSt.v_delay - c_TimeSt.v_start_time), c_NoiseDetection.noise, c_TimeSt.
                        v_start_time);
                }
                else
                {
                }
            }
        }
    }
    void PulseGenerator :: _nz_AugmentationMarker ()
    {
        _mSchema (_nz_AugmentationMarker);
        if ((((!c_EventMarkers.atrial_marker.empty ()) && (!c_EventMarkers.atrial_marker.front ().
            empty ())) && ((c_EventMarkers.atrial_marker.front ().last ().x == MarkerABB :: AS) && (
            c_EventMarkers.atrial_marker.last ().x == MarkerABB :: AT))))
        {
            c_EventMarkers.chg_augmentation_marker (MarkerABB :: ATR_Dur, c_EventMarkers.
                atrial_marker.last ().m);
        }
        else if ((((!c_EventMarkers.atrial_marker.empty ()) && (!c_EventMarkers.atrial_marker.front
            ().empty ())) && ((c_EventMarkers.atrial_marker.front ().last ().x == MarkerABB :: AT)
            && (c_EventMarkers.atrial_marker.last ().x == MarkerABB :: AT))))
        {
            c_EventMarkers.chg_augmentation_marker (MarkerABB :: ATR_FB, c_EventMarkers.
                atrial_marker.last ().m);
        }
        else if ((((!c_EventMarkers.atrial_marker.empty ()) && (!c_EventMarkers.atrial_marker.front
            ().empty ())) && ((c_EventMarkers.atrial_marker.front ().last ().x == MarkerABB :: AT)
            && (c_EventMarkers.atrial_marker.last ().x == MarkerABB :: AS))))
        {
            c_EventMarkers.chg_augmentation_marker (MarkerABB :: ATR_End, c_EventMarkers.
                atrial_marker.last ().m);
        }
        else if ((((!c_EventMarkers.ventricular_marker.empty ()) && (!c_EventMarkers.
            ventricular_marker.front ().empty ())) && ((c_EventMarkers.ventricular_marker.front ().
            last ().x == MarkerABB :: VS) && (c_EventMarkers.ventricular_marker.last ().x ==
            MarkerABB :: PVC))))
        {
            c_EventMarkers.chg_augmentation_marker (MarkerABB :: PVPext, c_EventMarkers.
                ventricular_marker.last ().m);
        }
        else
        {
        }
    }
    void PulseGenerator :: _nz_SetMode ()
    {
        _mSchema (_nz_SetMode);
        if (preSetVOO ())
        {
            _nz_SetVOO ();
        }
        else if (preSetAOO ())
        {
            _nz_SetAOO ();
        }
        else if (preSetDOO ())
        {
            _nz_SetDOO ();
        }
        else if (preSetDDI ())
        {
            _nz_SetDDI ();
        }
        else if (preSetVDD ())
        {
            _nz_SetVDD ();
        }
        else if (preSetVVI ())
        {
            _nz_SetVVI ();
        }
        else if (preSetAAI ())
        {
            _nz_SetAAI ();
        }
        else if (preSetVVT ())
        {
            _nz_SetVVT ();
        }
        else if (preSetAAT ())
        {
            _nz_SetAAT ();
        }
        else
        {
        }
    }
    _eBool PulseGenerator :: preSensingMarkers () const
    {
        _mFunction (preSensingMarkers);
        return (preAtrialSensedMarker () || preVentricularSensedMarker ());
    }
    void PulseGenerator :: _nz_SensingMarkers ()
    {
        _mSchema (_nz_SensingMarkers);
        if (preAtrialSensedMarker ())
        {
            _nz_AtrialSensedMarker ();
        }
        else if (preVentricularSensedMarker ())
        {
            _nz_VentricularSensedMarker ();
        }
        else if (true)
        {
            _nz_AugmentationMarker ();
        }
        else
        {
        }
    }
    void PulseGenerator :: _nz_SensingModule ()
    {
        _mSchema (_nz_SensingModule);
        if (preVentricularMeasurement ())
        {
            _nz_VentricularMeasurement ();
        }
        else if (preAtrialMeasurement ())
        {
            _nz_AtrialMeasurement ();
        }
        else
        {
        }
    }
    void PulseGenerator :: _nz_SetTimer ()
    {
        _mSchema (_nz_SetTimer);
        c_TimeSt.chg_time ((_mInteger (1) + c_TimeSt.time));
    }
    void PulseGenerator :: _nz_BradyTherapy ()
    {
        _mSchema (_nz_BradyTherapy);
        _nz_SetTimer ();
        _nz_SensingModule ();
        _nz_SensingMarkers ();
        _nz_SetMode ();
    }
    void PulseGenerator :: _nz_GetPWave (const _eInt device_p_wave)
    {
        _mSchema (_nz_GetPWave);
        c_MeasuredParameters.set_p_wave (device_p_wave);
    }
    void PulseGenerator :: _nz_GetRWave (const _eInt device_r_wave)
    {
        _mSchema (_nz_GetRWave);
        c_MeasuredParameters.set_r_wave (device_r_wave);
    }
    void PulseGenerator :: _nz_GetBatteryVoltage (const _eInt device_battery_voltage)
    {
        _mSchema (_nz_GetBatteryVoltage);
        c_MeasuredParameters.set_battery_voltage (device_battery_voltage);
    }
    void PulseGenerator :: _nz_GetLeadImpedance (const _eInt device_lead_impedance)
    {
        _mSchema (_nz_GetLeadImpedance);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (100), _mInteger (2500))._ovIn (
            device_lead_impedance), "672,31");
        #endif
        c_MeasuredParameters.set_lead_impedance (device_lead_impedance);
    }
    void PulseGenerator :: _nz_SetBradycardiaOperationMode (const BOM bradycardia_op_mode)
    {
        _mSchema (_nz_SetBradycardiaOperationMode);
        c_BO_MODE.chg_bo_mode (bradycardia_op_mode);
    }
    _eBool PulseGenerator :: operator == (const PulseGenerator & _vArg_9_9) const
    {
        _mOperator (=);
        return (((((((((_vArg_9_9.c_ProgrammableParameters == c_ProgrammableParameters) && (
            _vArg_9_9.c_MeasuredParameters == c_MeasuredParameters)) && (_vArg_9_9.c_TimeSt ==
            c_TimeSt)) && (_vArg_9_9.c_SensingPulse == c_SensingPulse)) && (_vArg_9_9.c_PacingPulse
            == c_PacingPulse)) && (_vArg_9_9.c_BatteryStatus == c_BatteryStatus)) && (_vArg_9_9.
            c_EventMarkers == c_EventMarkers)) && (_vArg_9_9.c_NoiseDetection == c_NoiseDetection))
            && (_vArg_9_9.c_BO_MODE == c_BO_MODE));
    }
    PulseGenerator :: PulseGenerator ()
    {
    }
    _eHndl < _eInstblTypeInfo > PulseGenerator :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 0));
        return ti;
    }
}


// End of file.
