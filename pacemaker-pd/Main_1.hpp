//***********************************************************************************************
//* File: 'Main_1.hpp'
//* THIS IS A GENERATED FILE: DO NOT EDIT. Please edit the Perfect Developer source file instead!
//*
//* Generated from: 'C:\Users\user\Dropbox\Academico\Pesquisa\pacemaker-project\pacemaker-pd\Main.pd'
//* by Perfect Developer version 6.00.00.03 at 13:45:29 UTC on Thursday April 10th 2014
//* Using command line options:
//* -z1 -el=3 -em=100 -gl=EmbeddedC++ -gs=1 -gv=ISO -gw=100 -gdp=1 -gdo=0 -gdc=3 -gda=1 -gdA=0 -gdl=0 -gdr=0 -gdt=0 -gdi=1 -st=4 -sb=C:\Program Files\Escher Technologies\Verification Studio 6\Bin\builtin.pd -sr=C:\Program Files\Escher Technologies\Verification Studio 6\Bin\rubric.pd -q=0 -gk=Pacemaker -eM=0 -@=C:\Users\user\AppData\Local\Temp\etf402C.tmp
//***********************************************************************************************


#if !defined(_hMain_Main)
#define _hMain_Main

// Full declarations

#include "Classes_1.hpp"


namespace Pacemaker
{
    class PulseGenerator : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (PulseGenerator)
        _mPerfectTypeInfoFwd
    public:
        ProgrammableParameters c_ProgrammableParameters;
    public:
        MeasuredParameters c_MeasuredParameters;
    public:
        TimeSt c_TimeSt;
    public:
        SensingPulse c_SensingPulse;
    public:
        PacingPulse c_PacingPulse;
    public:
        BatteryStatus c_BatteryStatus;
    public:
        EventMarkers c_EventMarkers;
    public:
        NoiseDetection c_NoiseDetection;
    public:
        BO_MODE c_BO_MODE;
    public:
        _eInt _nz_PulseWidth (const _eInt) const;
    public:
        _eInt _nz_PulsePerMinute (const _eInt) const;
    public:
        _eInt countAP (const _eInt, const _eInt) const;
    public:
        _eInt countAS (const _eInt, const _eInt) const;
    public:
        _eInt countVP (const _eInt, const _eInt) const;
    public:
        _eInt countVS (const _eInt, const _eInt) const;
    public:
        _eInt countPVC () const;
    public:
        _eInt countAT () const;
    public:
        PulseGenerator (const ProgrammableParameters, const MeasuredParameters, const TimeSt, const
            BO_MODE, const SensingPulse, const PacingPulse, const BatteryStatus, const EventMarkers,
            const NoiseDetection);
    public:
        _eBool preSetVOO () const;
    public:
        void _nz_SetVOO ();
    public:
        _eBool preSetAOO () const;
    public:
        void _nz_SetAOO ();
    public:
        _eBool preSetDOO () const;
    public:
        void _nz_SetDOO ();
    public:
        _eBool preSetDDI () const;
    public:
        void _nz_SetDDI ();
    public:
        _eBool preSetVDD () const;
    public:
        void _nz_SetVDD ();
    public:
        _eBool preSetVVI () const;
    public:
        void _nz_SetVVI ();
    public:
        _eBool preSetAAI () const;
    public:
        void _nz_SetAAI ();
    public:
        _eBool preSetVVT () const;
    public:
        void _nz_SetVVT ();
    public:
        _eBool preSetAAT () const;
    public:
        void _nz_SetAAT ();
    public:
        _eBool preAtriumStartTime () const;
    public:
        void _nz_AtriumStartTime ();
    public:
        _eBool preAtriumMax () const;
    public:
        void _nz_AtriumMax ();
    public:
        _eBool preAtriumEndTime () const;
    public:
        void _nz_AtriumEndTime ();
    public:
        _eBool preAtrialMeasurement () const;
    public:
        void _nz_AtrialMeasurement ();
    public:
        _eBool preVentricularStartTime () const;
    public:
        void _nz_VentricularStartTime ();
    public:
        _eBool preVentricularMax () const;
    public:
        void _nz_VentricularMax ();
    public:
        _eBool preVentricularEndTime () const;
    public:
        void _nz_VentricularEndTime ();
    public:
        _eBool preVentricularMeasurement () const;
    public:
        void _nz_VentricularMeasurement ();
    public:
        _eBool preSensingModule () const;
    public:
        _eBool preAtrialSensedMarkerA () const;
    public:
        _eBool preAtrialSensedMarkerB () const;
    public:
        _eBool preAtrialSensedMarker () const;
    public:
        void _nz_AtrialSensedMarker ();
    public:
        _eBool preVentricularSensedMarker () const;
    public:
        void _nz_VentricularSensedMarker ();
    public:
        void _nz_AugmentationMarker ();
    public:
        void _nz_SetMode ();
    public:
        _eBool preSensingMarkers () const;
    public:
        void _nz_SensingMarkers ();
    public:
        void _nz_SensingModule ();
    public:
        void _nz_SetTimer ();
    public:
        void _nz_BradyTherapy ();
    public:
        void _nz_GetPWave (const _eInt);
    public:
        void _nz_GetRWave (const _eInt);
    public:
        void _nz_GetBatteryVoltage (const _eInt);
    public:
        void _nz_GetLeadImpedance (const _eInt);
    public:
        void _nz_SetBradycardiaOperationMode (const BOM);
    public:
        _eBool operator == (const PulseGenerator &) const;
        PulseGenerator ();
    };
}

#endif

// End of file.
