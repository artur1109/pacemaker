//***********************************************************************************************
//* File: 'Classes.cpp'
//* THIS IS A GENERATED FILE: DO NOT EDIT. Please edit the Perfect Developer source file instead!
//*
//* Generated from: 'C:\Users\user\Dropbox\Academico\Pesquisa\pacemaker-project\pacemaker-pd\Classes.pd'
//* by Perfect Developer version 6.00.00.03 at 13:45:28 UTC on Thursday April 10th 2014
//* Using command line options:
//* -z1 -el=3 -em=100 -gl=EmbeddedC++ -gs=1 -gv=ISO -gw=100 -gdp=1 -gdo=0 -gdc=3 -gda=1 -gdA=0 -gdl=0 -gdr=0 -gdt=0 -gdi=1 -st=4 -sb=C:\Program Files\Escher Technologies\Verification Studio 6\Bin\builtin.pd -sr=C:\Program Files\Escher Technologies\Verification Studio 6\Bin\rubric.pd -q=0 -gk=Pacemaker -eM=0 -@=C:\Users\user\AppData\Local\Temp\etf402C.tmp
//***********************************************************************************************

#include "Ertsys.hpp"

// File inclusions for forward declarations

#include "Classes_0.hpp"

// File inclusions for full declarations

#include "Classes_1.hpp"

// File inclusions for inline code

#include "Classes_2.hpp"


static const _eMDH _aobjLoaderNode (_amoduleData, NULL, NULL, NULL);

namespace Pacemaker
{
    const _eMDH * _agetObjLoaderNode_Classes ()
    {
        return & _aobjLoaderNode;
    }
    _mEnumStorableSupportBdy (ACTIVITY_THRESHOLD, ACTIVITY_THRESHOLD)
    const _eNativeChar * ACTIVITY_THRESHOLD :: _lnaming [7] =
    {
        _mCstr("VERY_LOW"), _mCstr("LOW"), _mCstr("MED_LOW"), _mCstr("MED"), _mCstr("MED_HIGH"),
            _mCstr("HIGH"), _mCstr("VERY_HIGH")
    };
    _mEnumStorableSupportBdy (BATT_STATUS_LEVEL, BATT_STATUS_LEVEL)
    const _eNativeChar * BATT_STATUS_LEVEL :: _lnaming [4] =
    {
        _mCstr("BOL"), _mCstr("ERN"), _mCstr("ERT"), _mCstr("ERP")
    };
    _mEnumStorableSupportBdy (BRADYCARDIA_STATE, BRADYCARDIA_STATE)
    const _eNativeChar * BRADYCARDIA_STATE :: _lnaming [3] =
    {
        _mCstr("PERMANENT"), _mCstr("TEMPORARY"), _mCstr("PACE_NOW")
    };
    _mEnumStorableSupportBdy (LEAD_CHAMBER_TYPE, LEAD_CHAMBER_TYPE)
    const _eNativeChar * LEAD_CHAMBER_TYPE :: _lnaming [2] =
    {
        _mCstr("L_ATRIAL"), _mCstr("L_VENTRICULAR")
    };
    _mEnumStorableSupportBdy (POLARITY, POLARITY)
    const _eNativeChar * POLARITY :: _lnaming [2] =
    {
        _mCstr("UNIPOLAR"), _mCstr("BIPOLAR")
    };
    _mEnumStorableSupportBdy (MarkerABB, MarkerABB)
    const _eNativeChar * MarkerABB :: _lnaming [16] =
    {
        _mCstr("AS"), _mCstr("AP"), _mCstr("AT"), _mCstr("VS"), _mCstr("VP"), _mCstr("PVC"),
            _mCstr("TN"), _mCstr("SRef"), _mCstr("HyPc"), _mCstr("SRPc"), _mCstr("Sm_Up"),
            _mCstr("Sm_Down"), _mCstr("ATR_Dur"), _mCstr("ATR_FB"), _mCstr("ATR_End"),
            _mCstr("PVPext")
    };
    _mEnumStorableSupportBdy (SWITCH, SWITCH)
    const _eNativeChar * SWITCH :: _lnaming [2] =
    {
        _mCstr("ON"), _mCstr("OFF")
    };
    _mEnumStorableSupportBdy (CHAMBERS, CHAMBERS)
    const _eNativeChar * CHAMBERS :: _lnaming [4] =
    {
        _mCstr("C_NONE"), _mCstr("C_DUAL"), _mCstr("C_ATRIUM"), _mCstr("C_VENTRICLE")
    };
    _mEnumStorableSupportBdy (RESPONSE, RESPONSE)
    const _eNativeChar * RESPONSE :: _lnaming [4] =
    {
        _mCstr("R_NONE"), _mCstr("TRIGGERED"), _mCstr("INHIBITED"), _mCstr("TRACKED")
    };
    _eBool BOM :: inVOO () const
    {
        _mFunction (inVOO);
        return (((((SWITCH :: ON == _rswitch) && (CHAMBERS :: C_VENTRICLE == chambers_paced)) && (
            CHAMBERS :: C_NONE == chambers_sensed)) && (RESPONSE :: R_NONE == response_to_sensing))
            && (false == rate_modulation));
    }
    _eBool BOM :: inAOO () const
    {
        _mFunction (inAOO);
        return (((((SWITCH :: ON == _rswitch) && (CHAMBERS :: C_ATRIUM == chambers_paced)) && (
            CHAMBERS :: C_NONE == chambers_sensed)) && (RESPONSE :: R_NONE == response_to_sensing))
            && (false == rate_modulation));
    }
    _eBool BOM :: inDOO () const
    {
        _mFunction (inDOO);
        return (((((SWITCH :: ON == _rswitch) && (CHAMBERS :: C_DUAL == chambers_paced)) && (
            CHAMBERS :: C_NONE == chambers_sensed)) && (RESPONSE :: R_NONE == response_to_sensing))
            && (false == rate_modulation));
    }
    _eBool BOM :: inDDI () const
    {
        _mFunction (inDDI);
        return (((((SWITCH :: ON == _rswitch) && (CHAMBERS :: C_DUAL == chambers_paced)) && (
            CHAMBERS :: C_DUAL == chambers_sensed)) && (RESPONSE :: INHIBITED == response_to_sensing)
            ) && (false == rate_modulation));
    }
    _eBool BOM :: inVVI () const
    {
        _mFunction (inVVI);
        return (((((SWITCH :: ON == _rswitch) && (CHAMBERS :: C_VENTRICLE == chambers_paced)) && (
            CHAMBERS :: C_VENTRICLE == chambers_sensed)) && (RESPONSE :: INHIBITED ==
            response_to_sensing)) && (false == rate_modulation));
    }
    _eBool BOM :: inVDD () const
    {
        _mFunction (inVDD);
        return (((((SWITCH :: ON == _rswitch) && (CHAMBERS :: C_VENTRICLE == chambers_paced)) && (
            CHAMBERS :: C_DUAL == chambers_sensed)) && (RESPONSE :: TRACKED == response_to_sensing))
            && (false == rate_modulation));
    }
    _eBool BOM :: inAAI () const
    {
        _mFunction (inAAI);
        return (((((SWITCH :: ON == _rswitch) && (CHAMBERS :: C_ATRIUM == chambers_paced)) && (
            CHAMBERS :: C_ATRIUM == chambers_sensed)) && (RESPONSE :: INHIBITED ==
            response_to_sensing)) && (false == rate_modulation));
    }
    _eBool BOM :: inVVT () const
    {
        _mFunction (inVVT);
        return (((((SWITCH :: ON == _rswitch) && (CHAMBERS :: C_VENTRICLE == chambers_paced)) && (
            CHAMBERS :: C_VENTRICLE == chambers_sensed)) && (RESPONSE :: TRIGGERED ==
            response_to_sensing)) && (false == rate_modulation));
    }
    _eBool BOM :: inAAT () const
    {
        _mFunction (inAAT);
        return (((((SWITCH :: ON == _rswitch) && (CHAMBERS :: C_ATRIUM == chambers_paced)) && (
            CHAMBERS :: C_ATRIUM == chambers_sensed)) && (RESPONSE :: TRIGGERED ==
            response_to_sensing)) && (false == rate_modulation));
    }
    _eBool BOM :: inAXXX () const
    {
        _mFunction (inAXXX);
        return ((SWITCH :: ON == _rswitch) && (CHAMBERS :: C_ATRIUM == chambers_paced));
    }
    _eBool BOM :: inVXXX () const
    {
        _mFunction (inVXXX);
        return ((SWITCH :: ON == _rswitch) && (CHAMBERS :: C_VENTRICLE == chambers_paced));
    }
    _eBool BOM :: inDXXX () const
    {
        _mFunction (inDXXX);
        return ((SWITCH :: ON == _rswitch) && (CHAMBERS :: C_DUAL == chambers_paced));
    }
    _eBool BOM :: inOXO () const
    {
        _mFunction (inOXO);
        return (((SWITCH :: ON == _rswitch) && (CHAMBERS :: C_NONE == chambers_paced)) && (RESPONSE
            :: R_NONE == response_to_sensing));
    }
    BOM :: BOM (const SWITCH :: _eEnum _vswitch, const CHAMBERS :: _eEnum _vchambers_paced, const
        CHAMBERS :: _eEnum _vchambers_sensed, const RESPONSE :: _eEnum _vresponse_to_sensing, const
        _eBool _vrate_modulation) : _eAny (), _rswitch (_vswitch), chambers_paced (_vchambers_paced),
        chambers_sensed (_vchambers_sensed), response_to_sensing (_vresponse_to_sensing),
        rate_modulation (_vrate_modulation)
    {
        _mBuild;
    }
    _eBool BOM :: operator == (const BOM & _vArg_86_13) const
    {
        _mOperator (=);
        return (((((_vArg_86_13._rswitch == _rswitch) && (_vArg_86_13.chambers_paced ==
            chambers_paced)) && (_vArg_86_13.chambers_sensed == chambers_sensed)) && (_vArg_86_13.
            response_to_sensing == response_to_sensing)) && (_vArg_86_13.rate_modulation ==
            rate_modulation));
    }
    BOM :: BOM ()
    {
    }
    _eHndl < _eInstblTypeInfo > BOM :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 10));
        return ti;
    }
    _mBeginClassInvariant (TimeSt)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem ((_mInteger (0) <= time), "315,20");
    #endif
    _mEndClassInvariant
    TimeSt :: TimeSt (const _eInt _vtime, const _eInt _va_start_time, const _eInt _va_max, const
        _eInt _va_delay, const _eInt _vv_start_time, const _eInt _vv_max, const _eInt _vv_delay,
        const _eInt _va_curr_measurement, const _eInt _vv_curr_measurement) : _eAny (), time (_vtime)
        , a_start_time (_va_start_time), a_max (_va_max), a_delay (_va_delay), v_start_time (
        _vv_start_time), v_max (_vv_max), v_delay (_vv_delay), a_curr_measurement (
        _va_curr_measurement), v_curr_measurement (_vv_curr_measurement)
    {
        _mBuild;
        _mCheckClassInvariant ("322,5");
    }
    void TimeSt :: chg_time (const _eInt x)
    {
        _mSchema (chg_time);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre ((_mInteger (0) <= x), "333,14");
        #endif
        time = x;
        _mCheckClassInvariant ("334,14");
    }
    void TimeSt :: chg_a_start_time (const _eInt x)
    {
        _mSchema (chg_a_start_time);
        a_start_time = x;
        _mCheckClassInvariant ("338,14");
    }
    void TimeSt :: chg_a_max (const _eInt x)
    {
        _mSchema (chg_a_max);
        a_max = x;
        _mCheckClassInvariant ("342,14");
    }
    void TimeSt :: chg_a_delay (const _eInt x)
    {
        _mSchema (chg_a_delay);
        a_delay = x;
        _mCheckClassInvariant ("346,14");
    }
    void TimeSt :: chg_v_start_time (const _eInt x)
    {
        _mSchema (chg_v_start_time);
        v_start_time = x;
        _mCheckClassInvariant ("349,14");
    }
    void TimeSt :: chg_v_max (const _eInt x)
    {
        _mSchema (chg_v_max);
        v_max = x;
        _mCheckClassInvariant ("353,14");
    }
    void TimeSt :: chg_v_delay (const _eInt x)
    {
        _mSchema (chg_v_delay);
        v_delay = x;
        _mCheckClassInvariant ("357,14");
    }
    void TimeSt :: chg_v_curr_measurement (const _eInt x)
    {
        _mSchema (chg_v_curr_measurement);
        v_curr_measurement = x;
        _mCheckClassInvariant ("361,14");
    }
    void TimeSt :: chg_a_curr_measurement (const _eInt x)
    {
        _mSchema (chg_a_curr_measurement);
        a_curr_measurement = x;
        _mCheckClassInvariant ("365,14");
    }
    _eBool TimeSt :: operator == (const TimeSt & _vArg_305_13) const
    {
        _mOperator (=);
        return (((((((((_vArg_305_13.time == time) && (_vArg_305_13.a_start_time == a_start_time))
            && (_vArg_305_13.a_max == a_max)) && (_vArg_305_13.a_delay == a_delay)) && (_vArg_305_13
            .v_start_time == v_start_time)) && (_vArg_305_13.v_max == v_max)) && (_vArg_305_13.
            v_delay == v_delay)) && (_vArg_305_13.a_curr_measurement == a_curr_measurement)) && (
            _vArg_305_13.v_curr_measurement == v_curr_measurement));
    }
    TimeSt :: TimeSt ()
    {
    }
    _eHndl < _eInstblTypeInfo > TimeSt :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 11));
        return ti;
    }
    SensingPulse :: SensingPulse (const _eInt _vcardiac_cycles_length, const _eInt
        _va_sensing_threshold, const _eInt _vv_sensing_threshold, const _eInt _vsensed_a_pulse_width,
        const _eInt _vsensed_v_pulse_width, const _eInt _vsensed_a_pulse_amp, const _eInt
        _vsensed_v_pulse_amp, const _eInt _vlast_s_a_pulse, const _eInt _vlast_s_v_pulse, const
        _eInt _vcurrent_rate) : _eAny (), cardiac_cycles_length (_vcardiac_cycles_length),
        a_sensing_threshold (_va_sensing_threshold), v_sensing_threshold (_vv_sensing_threshold),
        sensed_a_pulse_width (_vsensed_a_pulse_width), sensed_v_pulse_width (_vsensed_v_pulse_width),
        sensed_a_pulse_amp (_vsensed_a_pulse_amp), sensed_v_pulse_amp (_vsensed_v_pulse_amp),
        last_s_a_pulse (_vlast_s_a_pulse), last_s_v_pulse (_vlast_s_v_pulse), current_rate (
        _vcurrent_rate)
    {
        _mBuild;
    }
    void SensingPulse :: chg_sensed_atrial (const _eInt x, const _eInt y, const _eInt z)
    {
        _mSchema (chg_sensed_atrial);
        sensed_a_pulse_width = x;
        sensed_a_pulse_amp = y;
        last_s_a_pulse = z;
    }
    void SensingPulse :: chg_sensed_ventricular (const _eInt x, const _eInt y, const _eInt k, const
        _eInt z)
    {
        _mSchema (chg_sensed_ventricular);
        sensed_v_pulse_width = x;
        sensed_v_pulse_amp = y;
        last_s_v_pulse = k;
        cardiac_cycles_length = z;
    }
    void SensingPulse :: chg_cardiac_cycles_length (const _eInt x)
    {
        _mSchema (chg_cardiac_cycles_length);
        cardiac_cycles_length = x;
    }
    void SensingPulse :: chg_a_sensing_threshold (const _eInt x)
    {
        _mSchema (chg_a_sensing_threshold);
        a_sensing_threshold = x;
    }
    void SensingPulse :: chg_v_sensing_threshold (const _eInt x)
    {
        _mSchema (chg_v_sensing_threshold);
        v_sensing_threshold = x;
    }
    void SensingPulse :: chg_sensed_a_pulse_width (const _eInt x)
    {
        _mSchema (chg_sensed_a_pulse_width);
        sensed_a_pulse_width = x;
    }
    void SensingPulse :: chg_sensed_v_pulse_width (const _eInt x)
    {
        _mSchema (chg_sensed_v_pulse_width);
        sensed_v_pulse_width = x;
    }
    void SensingPulse :: chg_sensed_a_pulse_amp (const _eInt x)
    {
        _mSchema (chg_sensed_a_pulse_amp);
        sensed_a_pulse_amp = x;
    }
    void SensingPulse :: chg_sensed_v_pulse_amp (const _eInt x)
    {
        _mSchema (chg_sensed_v_pulse_amp);
        sensed_v_pulse_amp = x;
    }
    void SensingPulse :: chg_last_s_a_pulse (const _eInt x)
    {
        _mSchema (chg_last_s_a_pulse);
        last_s_a_pulse = x;
    }
    void SensingPulse :: chg_last_s_v_pulse (const _eInt x)
    {
        _mSchema (chg_last_s_v_pulse);
        last_s_v_pulse = x;
    }
    void SensingPulse :: chg_current_rate (const _eInt x)
    {
        _mSchema (chg_current_rate);
        current_rate = x;
    }
    _eBool SensingPulse :: operator == (const SensingPulse & _vArg_371_13) const
    {
        _mOperator (=);
        return ((((((((((_vArg_371_13.cardiac_cycles_length == cardiac_cycles_length) && (
            _vArg_371_13.a_sensing_threshold == a_sensing_threshold)) && (_vArg_371_13.
            v_sensing_threshold == v_sensing_threshold)) && (_vArg_371_13.sensed_a_pulse_width ==
            sensed_a_pulse_width)) && (_vArg_371_13.sensed_v_pulse_width == sensed_v_pulse_width))
            && (_vArg_371_13.sensed_a_pulse_amp == sensed_a_pulse_amp)) && (_vArg_371_13.
            sensed_v_pulse_amp == sensed_v_pulse_amp)) && (_vArg_371_13.last_s_a_pulse ==
            last_s_a_pulse)) && (_vArg_371_13.last_s_v_pulse == last_s_v_pulse)) && (_vArg_371_13.
            current_rate == current_rate));
    }
    SensingPulse :: SensingPulse ()
    {
    }
    _eHndl < _eInstblTypeInfo > SensingPulse :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 12));
        return ti;
    }
    PacingPulse :: PacingPulse (const _eInt _vpaced_a_pulse_width, const _eInt _vpaced_v_pulse_width,
        const _eInt _vpaced_a_pulse_amp, const _eInt _vpaced_v_pulse_amp, const _eInt
        _vlast_p_a_pulse, const _eInt _vlast_p_v_pulse) : _eAny (), paced_a_pulse_width (
        _vpaced_a_pulse_width), paced_v_pulse_width (_vpaced_v_pulse_width), paced_a_pulse_amp (
        _vpaced_a_pulse_amp), paced_v_pulse_amp (_vpaced_v_pulse_amp), last_p_a_pulse (
        _vlast_p_a_pulse), last_p_v_pulse (_vlast_p_v_pulse)
    {
        _mBuild;
    }
    void PacingPulse :: chg_paced_atrial (const _eInt x, const _eInt y, const _eInt z)
    {
        _mSchema (chg_paced_atrial);
        paced_a_pulse_width = x;
        paced_a_pulse_amp = y;
        last_p_a_pulse = z;
    }
    void PacingPulse :: chg_paced_ventricular (const _eInt x, const _eInt y, const _eInt z)
    {
        _mSchema (chg_paced_ventricular);
        paced_v_pulse_width = x;
        paced_v_pulse_amp = y;
        last_p_v_pulse = x;
    }
    void PacingPulse :: chg_paced_dual (const _eInt x, const _eInt y, const _eInt z, const _eInt k,
        const _eInt m)
    {
        _mSchema (chg_paced_dual);
        paced_a_pulse_width = x;
        paced_a_pulse_amp = y;
        last_p_a_pulse = m;
        paced_v_pulse_width = z;
        paced_v_pulse_amp = k;
        last_p_v_pulse = m;
    }
    void PacingPulse :: chg_paced_a_pulse_width (const _eInt x)
    {
        _mSchema (chg_paced_a_pulse_width);
        paced_a_pulse_width = x;
    }
    void PacingPulse :: chg_paced_v_pulse_width (const _eInt x)
    {
        _mSchema (chg_paced_v_pulse_width);
        paced_v_pulse_width = x;
    }
    void PacingPulse :: chg_paced_a_pulse_amp (const _eInt x)
    {
        _mSchema (chg_paced_a_pulse_amp);
        paced_a_pulse_amp = x;
    }
    void PacingPulse :: chg_paced_v_pulse_amp (const _eInt x)
    {
        _mSchema (chg_paced_v_pulse_amp);
        paced_v_pulse_amp = x;
    }
    void PacingPulse :: chg_last_p_a_pulse (const _eInt x)
    {
        _mSchema (chg_last_p_a_pulse);
        last_p_a_pulse = x;
    }
    void PacingPulse :: chg_last_p_v_pulse (const _eInt x)
    {
        _mSchema (chg_last_p_v_pulse);
        last_p_v_pulse = x;
    }
    _eBool PacingPulse :: operator == (const PacingPulse & _vArg_449_13) const
    {
        _mOperator (=);
        return ((((((_vArg_449_13.paced_a_pulse_width == paced_a_pulse_width) && (_vArg_449_13.
            paced_v_pulse_width == paced_v_pulse_width)) && (_vArg_449_13.paced_a_pulse_amp ==
            paced_a_pulse_amp)) && (_vArg_449_13.paced_v_pulse_amp == paced_v_pulse_amp)) && (
            _vArg_449_13.last_p_a_pulse == last_p_a_pulse)) && (_vArg_449_13.last_p_v_pulse ==
            last_p_v_pulse));
    }
    PacingPulse :: PacingPulse ()
    {
    }
    _eHndl < _eInstblTypeInfo > PacingPulse :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 13));
        return ti;
    }
    _mBeginClassInvariant (BatteryStatus)
    _mEndClassInvariant
    BatteryStatus :: BatteryStatus (const BATT_STATUS_LEVEL :: _eEnum _vbatt_status_level) : _eAny ()
        , batt_status_level (_vbatt_status_level)
    {
        _mBuild;
        _mCheckClassInvariant ("516,5");
    }
    void BatteryStatus :: chg_batt_status_level (const BATT_STATUS_LEVEL :: _eEnum x)
    {
        _mSchema (chg_batt_status_level);
        batt_status_level = x;
        _mCheckClassInvariant ("519,14");
    }
    _eBool BatteryStatus :: operator == (const BatteryStatus & _vArg_508_13) const
    {
        _mOperator (=);
        return (_vArg_508_13.batt_status_level == batt_status_level);
    }
    BatteryStatus :: BatteryStatus ()
    {
    }
    _eHndl < _eInstblTypeInfo > BatteryStatus :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 14));
        return ti;
    }
    _mBeginClassInvariant (NoiseDetection)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem ((noise <= _mInteger (1)), "528,15");
    #endif
    _mEndClassInvariant
    NoiseDetection :: NoiseDetection (const _eInt _vnoise) : _eAny (), noise (_vnoise)
    {
        _mBuild;
        _mCheckClassInvariant ("533,5");
    }
    void NoiseDetection :: chg_noise (const _eInt x)
    {
        _mSchema (chg_noise);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre ((x <= _mInteger (1)), "536,15");
        #endif
        noise = x;
        _mCheckClassInvariant ("537,14");
    }
    _eBool NoiseDetection :: operator == (const NoiseDetection & _vArg_525_13) const
    {
        _mOperator (=);
        return (_vArg_525_13.noise == noise);
    }
    NoiseDetection :: NoiseDetection ()
    {
    }
    _eHndl < _eInstblTypeInfo > NoiseDetection :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 15));
        return ti;
    }
    _mBeginClassInvariant (EventMarkers)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem ((_mInteger (0) <= atrial_marker._oHash ()),
        "547,24");
    #endif
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem ((_mInteger (0) <= ventricular_marker._oHash
        ()), "548,29");
    #endif
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem ((_mInteger (0) <= augmentation_marker.
        _oHash ()), "549,30");
    #endif
    _mEndClassInvariant
    EventMarkers :: EventMarkers (const _eSeq < _n5_quin < MarkerABB :: _eEnum _mComma _eInt _mComma
        _eInt _mComma _eInt _mComma _eInt > > _vatrial_marker, const _eSeq < _n5_quin < MarkerABB ::
        _eEnum _mComma _eInt _mComma _eInt _mComma _eInt _mComma _eInt > > _vventricular_marker,
        const _eSeq < _ePair < MarkerABB :: _eEnum _mComma _eInt > > _vaugmentation_marker) : _eAny
        (), atrial_marker (_vatrial_marker), ventricular_marker (_vventricular_marker),
        augmentation_marker (_vaugmentation_marker)
    {
        _mBuild;
        _mCheckClassInvariant ("554,5");
    }
    void EventMarkers :: chg_atrial_marker (const MarkerABB :: _eEnum m, const _eInt pw, const _eInt
        pa, const _eInt no, const _eInt ti)
    {
        _mSchema (chg_atrial_marker);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre ((atrial_marker.empty () || (static_cast < const _eSeq < _n5_quin <
            MarkerABB :: _eEnum _mComma _eInt _mComma _eInt _mComma _eInt _mComma _eInt > > & > (
            atrial_marker) [(atrial_marker._oHash () - _mInteger (1))].m < ti)), "559,27");
        #endif
        atrial_marker = atrial_marker._oPlusPlus (_eSeq < _n5_quin < MarkerABB :: _eEnum _mComma
            _eInt _mComma _eInt _mComma _eInt _mComma _eInt > > (_n5_quin < MarkerABB :: _eEnum
            _mComma _eInt _mComma _eInt _mComma _eInt _mComma _eInt > (m, pw, pa, no, ti)));
        _mCheckClassInvariant ("561,14");
    }
    void EventMarkers :: chg_ventricular_marker (const MarkerABB :: _eEnum m, const _eInt pw, const
        _eInt pa, const _eInt no, const _eInt ti)
    {
        _mSchema (chg_ventricular_marker);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre ((ventricular_marker.empty () || (static_cast < const _eSeq < _n5_quin
            < MarkerABB :: _eEnum _mComma _eInt _mComma _eInt _mComma _eInt _mComma _eInt > > & > (
            ventricular_marker) [(ventricular_marker._oHash () - _mInteger (1))].m < ti)), "566,32");
        #endif
        ventricular_marker = ventricular_marker._oPlusPlus (_eSeq < _n5_quin < MarkerABB :: _eEnum
            _mComma _eInt _mComma _eInt _mComma _eInt _mComma _eInt > > (_n5_quin < MarkerABB ::
            _eEnum _mComma _eInt _mComma _eInt _mComma _eInt _mComma _eInt > (m, pw, pa, no, ti)));
        _mCheckClassInvariant ("568,14");
    }
    void EventMarkers :: chg_dual_marker (const MarkerABB :: _eEnum ma, const _eInt pwa, const _eInt
        paa, const MarkerABB :: _eEnum mv, const _eInt pwv, const _eInt pav, const _eInt no, const
        _eInt ti)
    {
        _mSchema (chg_dual_marker);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre ((atrial_marker.empty () || (static_cast < const _eSeq < _n5_quin <
            MarkerABB :: _eEnum _mComma _eInt _mComma _eInt _mComma _eInt _mComma _eInt > > & > (
            atrial_marker) [(atrial_marker._oHash () - _mInteger (1))].m < ti)), "573,27");
        #endif
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre ((ventricular_marker.empty () || (static_cast < const _eSeq < _n5_quin
            < MarkerABB :: _eEnum _mComma _eInt _mComma _eInt _mComma _eInt _mComma _eInt > > & > (
            ventricular_marker) [(ventricular_marker._oHash () - _mInteger (1))].m < ti)), "574,32");
        #endif
        atrial_marker = atrial_marker._oPlusPlus (_eSeq < _n5_quin < MarkerABB :: _eEnum _mComma
            _eInt _mComma _eInt _mComma _eInt _mComma _eInt > > (_n5_quin < MarkerABB :: _eEnum
            _mComma _eInt _mComma _eInt _mComma _eInt _mComma _eInt > (ma, pwa, paa, no, ti)));
        ventricular_marker = ventricular_marker._oPlusPlus (_eSeq < _n5_quin < MarkerABB :: _eEnum
            _mComma _eInt _mComma _eInt _mComma _eInt _mComma _eInt > > (_n5_quin < MarkerABB ::
            _eEnum _mComma _eInt _mComma _eInt _mComma _eInt _mComma _eInt > (mv, pwv, pav, no, ti)))
            ;
        _mCheckClassInvariant ("576,14");
    }
    void EventMarkers :: chg_augmentation_marker (const MarkerABB :: _eEnum m, const _eInt ti)
    {
        _mSchema (chg_augmentation_marker);
        augmentation_marker = augmentation_marker._oPlusPlus (_eSeq < _ePair < MarkerABB :: _eEnum
            _mComma _eInt > > (_ePair < MarkerABB :: _eEnum _mComma _eInt > (m, ti)));
        _mCheckClassInvariant ("585,14");
    }
    void EventMarkers :: unchg_dual_marker ()
    {
        _mSchema (unchg_dual_marker);
        atrial_marker = atrial_marker;
        ventricular_marker = ventricular_marker;
        _mCheckClassInvariant ("591,14");
    }
    void EventMarkers :: unchg_atrial_marker ()
    {
        _mSchema (unchg_atrial_marker);
        atrial_marker = atrial_marker;
        _mCheckClassInvariant ("596,14");
    }
    void EventMarkers :: unchg_ventricular_marker ()
    {
        _mSchema (unchg_ventricular_marker);
        ventricular_marker = ventricular_marker;
        _mCheckClassInvariant ("600,14");
    }
    void EventMarkers :: unchg_augmentation_marker ()
    {
        _mSchema (unchg_augmentation_marker);
        augmentation_marker = augmentation_marker;
        _mCheckClassInvariant ("604,14");
    }
    _eBool EventMarkers :: operator == (const EventMarkers & _vArg_543_13) const
    {
        _mOperator (=);
        return (((_vArg_543_13.atrial_marker == atrial_marker) && (_vArg_543_13.ventricular_marker
            == ventricular_marker)) && (_vArg_543_13.augmentation_marker == augmentation_marker));
    }
    EventMarkers :: EventMarkers ()
    {
    }
    _eHndl < _eInstblTypeInfo > EventMarkers :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 16));
        return ti;
    }
    _mBeginClassInvariant (Accelerometer)
    _mEndClassInvariant
    Accelerometer :: Accelerometer (const _eInt _vaccel) : _eAny (), accel (_vaccel)
    {
        _mBuild;
        _mCheckClassInvariant ("618,5");
    }
    void Accelerometer :: chg_accel (const _eInt x)
    {
        _mSchema (chg_accel);
        accel = x;
        _mCheckClassInvariant ("621,14");
    }
    _eBool Accelerometer :: operator == (const Accelerometer & _vArg_610_13) const
    {
        _mOperator (=);
        return (_vArg_610_13.accel == accel);
    }
    Accelerometer :: Accelerometer ()
    {
    }
    _eHndl < _eInstblTypeInfo > Accelerometer :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 17));
        return ti;
    }
    PWave :: PWave (const _eInt _vp_wave) : _eAny (), p_wave (_vp_wave)
    {
        _mBuild;
    }
    void PWave :: chg_p_wave (const _eInt x)
    {
        _mSchema (chg_p_wave);
        p_wave = x;
    }
    _eBool PWave :: operator == (const PWave & _vArg_627_13) const
    {
        _mOperator (=);
        return (_vArg_627_13.p_wave == p_wave);
    }
    PWave :: PWave ()
    {
    }
    _eHndl < _eInstblTypeInfo > PWave :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 18));
        return ti;
    }
    RWave :: RWave (const _eInt _vr_wave) : _eAny (), r_wave (_vr_wave)
    {
        _mBuild;
    }
    void RWave :: chg_r_wave (const _eInt x)
    {
        _mSchema (chg_r_wave);
        r_wave = x;
    }
    _eBool RWave :: operator == (const RWave & _vArg_640_13) const
    {
        _mOperator (=);
        return (_vArg_640_13.r_wave == r_wave);
    }
    RWave :: RWave ()
    {
    }
    _eHndl < _eInstblTypeInfo > RWave :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 19));
        return ti;
    }
    BatteryVoltage :: BatteryVoltage (const _eInt _vbattery_voltage) : _eAny (), battery_voltage (
        _vbattery_voltage)
    {
        _mBuild;
    }
    void BatteryVoltage :: chg_battery_voltage (const _eInt x)
    {
        _mSchema (chg_battery_voltage);
        battery_voltage = x;
    }
    _eBool BatteryVoltage :: operator == (const BatteryVoltage & _vArg_654_13) const
    {
        _mOperator (=);
        return (_vArg_654_13.battery_voltage == battery_voltage);
    }
    BatteryVoltage :: BatteryVoltage ()
    {
    }
    _eHndl < _eInstblTypeInfo > BatteryVoltage :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 20));
        return ti;
    }
    _mBeginClassInvariant (LeadImpedance)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem (_onRange (_mInteger (100), _mInteger (2500))
        ._ovIn (lead_impedance), "670,24");
    #endif
    _mEndClassInvariant
    LeadImpedance :: LeadImpedance (const _eInt _vlead_impedance) : _eAny (), lead_impedance (
        _vlead_impedance)
    {
        _mBuild;
        _mCheckClassInvariant ("674,5");
    }
    void LeadImpedance :: chg_lead_impedance (const _eInt x)
    {
        _mSchema (chg_lead_impedance);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (100), _mInteger (2500))._ovIn (x), "677,15");
        #endif
        lead_impedance = x;
        _mCheckClassInvariant ("678,14");
    }
    _eBool LeadImpedance :: operator == (const LeadImpedance & _vArg_668_13) const
    {
        _mOperator (=);
        return (_vArg_668_13.lead_impedance == lead_impedance);
    }
    LeadImpedance :: LeadImpedance ()
    {
    }
    _eHndl < _eInstblTypeInfo > LeadImpedance :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 21));
        return ti;
    }
    MeasuredParameters :: MeasuredParameters (const LeadImpedance _vc_LeadImpedance, const
        BatteryVoltage _vc_BatteryVoltage, const RWave _vc_RWave, const PWave _vc_PWave) : _eAny (),
        c_LeadImpedance (_vc_LeadImpedance), c_BatteryVoltage (_vc_BatteryVoltage), c_RWave (
        _vc_RWave), c_PWave (_vc_PWave)
    {
        _mBuild;
    }
    void MeasuredParameters :: set_p_wave (const _eInt x)
    {
        _mSchema (set_p_wave);
        c_PWave.chg_p_wave (x);
    }
    void MeasuredParameters :: set_r_wave (const _eInt x)
    {
        _mSchema (set_r_wave);
        c_RWave.chg_r_wave (x);
    }
    void MeasuredParameters :: set_lead_impedance (const _eInt x)
    {
        _mSchema (set_lead_impedance);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (100), _mInteger (2500))._ovIn (x), "706,15");
        #endif
        c_LeadImpedance.chg_lead_impedance (x);
    }
    void MeasuredParameters :: set_battery_voltage (const _eInt x)
    {
        _mSchema (set_battery_voltage);
        c_BatteryVoltage.chg_battery_voltage (x);
    }
    _eBool MeasuredParameters :: operator == (const MeasuredParameters & _vArg_684_13) const
    {
        _mOperator (=);
        return ((((_vArg_684_13.c_LeadImpedance == c_LeadImpedance) && (_vArg_684_13.
            c_BatteryVoltage == c_BatteryVoltage)) && (_vArg_684_13.c_RWave == c_RWave)) && (
            _vArg_684_13.c_PWave == c_PWave));
    }
    MeasuredParameters :: MeasuredParameters ()
    {
    }
    _eHndl < _eInstblTypeInfo > MeasuredParameters :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 22));
        return ti;
    }
    _eBool BO_MODE :: isVOO () const
    {
        _mFunction (isVOO);
        return (true == bo_mode.inVOO ());
    }
    _eBool BO_MODE :: isAOO () const
    {
        _mFunction (isAOO);
        return (true == bo_mode.inAOO ());
    }
    _eBool BO_MODE :: isDOO () const
    {
        _mFunction (isDOO);
        return (true == bo_mode.inAOO ());
    }
    _eBool BO_MODE :: isDDI () const
    {
        _mFunction (isDDI);
        return (true == bo_mode.inDDI ());
    }
    _eBool BO_MODE :: isVVI () const
    {
        _mFunction (isVVI);
        return (true == bo_mode.inVVI ());
    }
    _eBool BO_MODE :: isAAI () const
    {
        _mFunction (isAAI);
        return (true == bo_mode.inAAI ());
    }
    _eBool BO_MODE :: isVVT () const
    {
        _mFunction (isVVT);
        return (true == bo_mode.inVVT ());
    }
    _eBool BO_MODE :: isAAT () const
    {
        _mFunction (isAAT);
        return (true == bo_mode.inAAT ());
    }
    _eBool BO_MODE :: isVDD () const
    {
        _mFunction (isVDD);
        return (true == bo_mode.inVDD ());
    }
    BO_MODE :: BO_MODE (const BOM _vbo_mode) : _eAny (), bo_mode (_vbo_mode)
    {
        _mBuild;
    }
    void BO_MODE :: chg_bo_mode (const BOM x)
    {
        _mSchema (chg_bo_mode);
        bo_mode = x;
    }
    _eBool BO_MODE :: operator == (const BO_MODE & _vArg_720_13) const
    {
        _mOperator (=);
        return (_vArg_720_13.bo_mode == bo_mode);
    }
    BO_MODE :: BO_MODE ()
    {
    }
    _eHndl < _eInstblTypeInfo > BO_MODE :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 23));
        return ti;
    }
    _mBeginClassInvariant (LRL)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem (_onRange (_mInteger (30), _mInteger (175)).
        _ovIn (lower_rate_limit), "763,26");
    #endif
    _mEndClassInvariant
    LRL :: LRL (const _eInt _vlower_rate_limit) : _eAny (), lower_rate_limit (_vlower_rate_limit)
    {
        _mBuild;
        _mCheckClassInvariant ("768,5");
    }
    void LRL :: chg_lower_rate_limit (const _eInt x)
    {
        _mSchema (chg_lower_rate_limit);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (30), _mInteger (175))._ovIn (x), "771,15");
        #endif
        lower_rate_limit = x;
        _mCheckClassInvariant ("772,14");
    }
    _eBool LRL :: operator == (const LRL & _vArg_760_13) const
    {
        _mOperator (=);
        return (_vArg_760_13.lower_rate_limit == lower_rate_limit);
    }
    LRL :: LRL ()
    {
    }
    _eHndl < _eInstblTypeInfo > LRL :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 24));
        return ti;
    }
    _mBeginClassInvariant (URL)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem (_onRange (_mInteger (30), _mInteger (175)).
        _ovIn (upper_rate_limit), "781,26");
    #endif
    _mEndClassInvariant
    URL :: URL (const _eInt _vupper_rate_limit) : _eAny (), upper_rate_limit (_vupper_rate_limit)
    {
        _mBuild;
        _mCheckClassInvariant ("786,5");
    }
    void URL :: chg_upper_rate_limit (const _eInt x)
    {
        _mSchema (chg_upper_rate_limit);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (30), _mInteger (175))._ovIn (x), "789,15");
        #endif
        upper_rate_limit = x;
        _mCheckClassInvariant ("790,14");
    }
    _eBool URL :: operator == (const URL & _vArg_778_13) const
    {
        _mOperator (=);
        return (_vArg_778_13.upper_rate_limit == upper_rate_limit);
    }
    URL :: URL ()
    {
    }
    _eHndl < _eInstblTypeInfo > URL :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 25));
        return ti;
    }
    _mBeginClassInvariant (MaxSensorRate)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem (_onRange (_mInteger (50), _mInteger (175)).
        _ovIn (max_sensor_rate), "799,25");
    #endif
    _mEndClassInvariant
    MaxSensorRate :: MaxSensorRate (const _eInt _vmax_sensor_rate) : _eAny (), max_sensor_rate (
        _vmax_sensor_rate)
    {
        _mBuild;
        _mCheckClassInvariant ("804,5");
    }
    void MaxSensorRate :: chg_max_sensor_rate (const _eInt x)
    {
        _mSchema (chg_max_sensor_rate);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (50), _mInteger (175))._ovIn (x), "807,15");
        #endif
        max_sensor_rate = x;
        _mCheckClassInvariant ("808,14");
    }
    _eBool MaxSensorRate :: operator == (const MaxSensorRate & _vArg_796_13) const
    {
        _mOperator (=);
        return (_vArg_796_13.max_sensor_rate == max_sensor_rate);
    }
    MaxSensorRate :: MaxSensorRate ()
    {
    }
    _eHndl < _eInstblTypeInfo > MaxSensorRate :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 26));
        return ti;
    }
    _mBeginClassInvariant (FixedAVDelay)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem (_onRange (_mInteger (70000), _mInteger (
        300000))._ovIn (fixed_av_delay), "817,24");
    #endif
    _mEndClassInvariant
    FixedAVDelay :: FixedAVDelay (const _eInt _vfixed_av_delay) : _eAny (), fixed_av_delay (
        _vfixed_av_delay)
    {
        _mBuild;
        _mCheckClassInvariant ("822,5");
    }
    void FixedAVDelay :: chg_fixed_av_delay (const _eInt x)
    {
        _mSchema (chg_fixed_av_delay);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (70000), _mInteger (300000))._ovIn (x), "825,15");
        #endif
        fixed_av_delay = x;
        _mCheckClassInvariant ("826,14");
    }
    _eBool FixedAVDelay :: operator == (const FixedAVDelay & _vArg_814_13) const
    {
        _mOperator (=);
        return (_vArg_814_13.fixed_av_delay == fixed_av_delay);
    }
    FixedAVDelay :: FixedAVDelay ()
    {
    }
    _eHndl < _eInstblTypeInfo > FixedAVDelay :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 27));
        return ti;
    }
    DynamicAVDelay :: DynamicAVDelay (const SWITCH :: _eEnum _vdynamic_av_delay) : _eAny (),
        dynamic_av_delay (_vdynamic_av_delay)
    {
        _mBuild;
    }
    void DynamicAVDelay :: chg_dynamic_av_delay (const SWITCH :: _eEnum x)
    {
        _mSchema (chg_dynamic_av_delay);
        dynamic_av_delay = x;
    }
    _eBool DynamicAVDelay :: operator == (const DynamicAVDelay & _vArg_832_13) const
    {
        _mOperator (=);
        return (_vArg_832_13.dynamic_av_delay == dynamic_av_delay);
    }
    DynamicAVDelay :: DynamicAVDelay ()
    {
    }
    _eHndl < _eInstblTypeInfo > DynamicAVDelay :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 28));
        return ti;
    }
    _mBeginClassInvariant (MinDynamicAVDelay)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem (_onRange (_mInteger (30000), _mInteger (
        100000))._ovIn (min_dyn_av_delay), "848,26");
    #endif
    _mEndClassInvariant
    MinDynamicAVDelay :: MinDynamicAVDelay (const _eInt _vmin_dyn_av_delay) : _eAny (),
        min_dyn_av_delay (_vmin_dyn_av_delay)
    {
        _mBuild;
        _mCheckClassInvariant ("853,5");
    }
    void MinDynamicAVDelay :: chg_min_dyn_av_delay (const _eInt x)
    {
        _mSchema (chg_min_dyn_av_delay);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (30000), _mInteger (100000))._ovIn (x), "856,15");
        #endif
        min_dyn_av_delay = x;
        _mCheckClassInvariant ("857,14");
    }
    _eBool MinDynamicAVDelay :: operator == (const MinDynamicAVDelay & _vArg_846_13) const
    {
        _mOperator (=);
        return (_vArg_846_13.min_dyn_av_delay == min_dyn_av_delay);
    }
    MinDynamicAVDelay :: MinDynamicAVDelay ()
    {
    }
    _eHndl < _eInstblTypeInfo > MinDynamicAVDelay :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 29));
        return ti;
    }
    _mBeginClassInvariant (SensedAVDelayOffset)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem ((((sensed_av_delay_offset == _ePair <
        SWITCH :: _eEnum _mComma _eInt > (SWITCH :: OFF, _mInteger (0))) || ((SWITCH :: OFF ==
        sensed_av_delay_offset.x) && (((- _mInteger (100000)) <= sensed_av_delay_offset.y) && (
        sensed_av_delay_offset.y <= (- _mInteger (10000)))))) || ((SWITCH :: ON ==
        sensed_av_delay_offset.x) && (((- _mInteger (100000)) <= sensed_av_delay_offset.y) && (
        sensed_av_delay_offset.y <= (- _mInteger (10000)))))), "866,33");
    #endif
    _mEndClassInvariant
    SensedAVDelayOffset :: SensedAVDelayOffset (const _ePair < SWITCH :: _eEnum _mComma _eInt >
        _vsensed_av_delay_offset) : _eAny (), sensed_av_delay_offset (_vsensed_av_delay_offset)
    {
        _mBuild;
        _mCheckClassInvariant ("874,5");
    }
    void SensedAVDelayOffset :: chg_sensed_av_delay_offset (const _ePair < SWITCH :: _eEnum _mComma
        _eInt > a)
    {
        _mSchema (chg_sensed_av_delay_offset);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (((a == _ePair < SWITCH :: _eEnum _mComma _eInt > (SWITCH :: OFF,
            _mInteger (0))) || ((SWITCH :: ON == a.x) && (((- _mInteger (100000)) <= a.y) && (a.y <=
            (- _mInteger (100000)))))), "877,16");
        #endif
        sensed_av_delay_offset = a;
        _mCheckClassInvariant ("880,14");
    }
    _eBool SensedAVDelayOffset :: operator == (const SensedAVDelayOffset & _vArg_863_13) const
    {
        _mOperator (=);
        return (_vArg_863_13.sensed_av_delay_offset == sensed_av_delay_offset);
    }
    SensedAVDelayOffset :: SensedAVDelayOffset ()
    {
    }
    _eHndl < _eInstblTypeInfo > SensedAVDelayOffset :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 30));
        return ti;
    }
    _mBeginClassInvariant (APulseAmpRegulated)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem ((((a_pulse_amp_regulated == _ePair < SWITCH
        :: _eEnum _mComma _eInt > (SWITCH :: OFF, _mInteger (0))) || ((SWITCH :: ON ==
        a_pulse_amp_regulated.x) && ((_mInteger (500000) <= a_pulse_amp_regulated.y) && (
        a_pulse_amp_regulated.y <= _mInteger (3200000))))) || ((SWITCH :: ON ==
        a_pulse_amp_regulated.x) && ((_mInteger (3500000) <= a_pulse_amp_regulated.y) && (
        a_pulse_amp_regulated.y <= _mInteger (7000000))))), "889,32");
    #endif
    _mEndClassInvariant
    APulseAmpRegulated :: APulseAmpRegulated (const _ePair < SWITCH :: _eEnum _mComma _eInt >
        _va_pulse_amp_regulated) : _eAny (), a_pulse_amp_regulated (_va_pulse_amp_regulated)
    {
        _mBuild;
        _mCheckClassInvariant ("898,5");
    }
    void APulseAmpRegulated :: chg_a_pulse_amp_regulated (const _ePair < SWITCH :: _eEnum _mComma
        _eInt > a)
    {
        _mSchema (chg_a_pulse_amp_regulated);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre ((((a == _ePair < SWITCH :: _eEnum _mComma _eInt > (SWITCH :: OFF,
            _mInteger (0))) || ((SWITCH :: ON == a.x) && ((_mInteger (500000) <= a.y) && (a.y <=
            _mInteger (3200000))))) || ((SWITCH :: ON == a.x) && ((_mInteger (3500000) <= a.y) && (a
            .y <= _mInteger (7000000))))), "901,16");
        #endif
        a_pulse_amp_regulated = a;
        _mCheckClassInvariant ("906,14");
    }
    _eBool APulseAmpRegulated :: operator == (const APulseAmpRegulated & _vArg_886_13) const
    {
        _mOperator (=);
        return (_vArg_886_13.a_pulse_amp_regulated == a_pulse_amp_regulated);
    }
    APulseAmpRegulated :: APulseAmpRegulated ()
    {
    }
    _eHndl < _eInstblTypeInfo > APulseAmpRegulated :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 31));
        return ti;
    }
    _mBeginClassInvariant (VPulseAmpRegulated)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem ((((v_pulse_amp_regulated == _ePair < SWITCH
        :: _eEnum _mComma _eInt > (SWITCH :: OFF, _mInteger (0))) || ((SWITCH :: ON ==
        v_pulse_amp_regulated.x) && ((_mInteger (500000) <= v_pulse_amp_regulated.y) && (
        v_pulse_amp_regulated.y <= _mInteger (3200000))))) || ((SWITCH :: ON ==
        v_pulse_amp_regulated.x) && ((_mInteger (3500000) <= v_pulse_amp_regulated.y) && (
        v_pulse_amp_regulated.y <= _mInteger (7000000))))), "915,32");
    #endif
    _mEndClassInvariant
    VPulseAmpRegulated :: VPulseAmpRegulated (const _ePair < SWITCH :: _eEnum _mComma _eInt >
        _vv_pulse_amp_regulated) : _eAny (), v_pulse_amp_regulated (_vv_pulse_amp_regulated)
    {
        _mBuild;
        _mCheckClassInvariant ("922,5");
    }
    void VPulseAmpRegulated :: chg_v_pulse_amp_regulated (const _ePair < SWITCH :: _eEnum _mComma
        _eInt > a)
    {
        _mSchema (chg_v_pulse_amp_regulated);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre ((((a == _ePair < SWITCH :: _eEnum _mComma _eInt > (SWITCH :: OFF,
            _mInteger (0))) || ((SWITCH :: ON == a.x) && ((_mInteger (500000) <= a.y) && (a.y <=
            _mInteger (3200000))))) || ((SWITCH :: ON == a.x) && ((_mInteger (3500000) <= a.y) && (a
            .y <= _mInteger (7000000))))), "925,16");
        #endif
        v_pulse_amp_regulated = a;
        _mCheckClassInvariant ("928,14");
    }
    _eBool VPulseAmpRegulated :: operator == (const VPulseAmpRegulated & _vArg_912_13) const
    {
        _mOperator (=);
        return (_vArg_912_13.v_pulse_amp_regulated == v_pulse_amp_regulated);
    }
    VPulseAmpRegulated :: VPulseAmpRegulated ()
    {
    }
    _eHndl < _eInstblTypeInfo > VPulseAmpRegulated :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 32));
        return ti;
    }
    _mBeginClassInvariant (APulseAmpUnregulated)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem (((a_pulse_amp_unregulated == _ePair <
        SWITCH :: _eEnum _mComma _eInt > (SWITCH :: OFF, _mInteger (0))) || ((SWITCH :: ON ==
        a_pulse_amp_unregulated.x) && ((((_mInteger (1250000) == a_pulse_amp_unregulated.y) || (
        _mInteger (2500000) == a_pulse_amp_unregulated.y)) || (_mInteger (3750000) ==
        a_pulse_amp_unregulated.y)) || (_mInteger (5000000) == a_pulse_amp_unregulated.y)))),
        "937,33");
    #endif
    _mEndClassInvariant
    APulseAmpUnregulated :: APulseAmpUnregulated (const _ePair < SWITCH :: _eEnum _mComma _eInt >
        _va_pulse_amp_unregulated) : _eAny (), a_pulse_amp_unregulated (_va_pulse_amp_unregulated)
    {
        _mBuild;
        _mCheckClassInvariant ("946,5");
    }
    void APulseAmpUnregulated :: chg_a_pulse_amp_unregulated (const _ePair < SWITCH :: _eEnum
        _mComma _eInt > a)
    {
        _mSchema (chg_a_pulse_amp_unregulated);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (((a == _ePair < SWITCH :: _eEnum _mComma _eInt > (SWITCH :: OFF,
            _mInteger (0))) || ((SWITCH :: ON == a.x) && ((((_mInteger (1250000) == a.y) || (
            _mInteger (2500000) == a.y)) || (_mInteger (3750000) == a.y)) || (_mInteger (5000000) ==
            a.y)))), "949,15");
        #endif
        a_pulse_amp_unregulated = a;
        _mCheckClassInvariant ("955,14");
    }
    _eBool APulseAmpUnregulated :: operator == (const APulseAmpUnregulated & _vArg_934_13) const
    {
        _mOperator (=);
        return (_vArg_934_13.a_pulse_amp_unregulated == a_pulse_amp_unregulated);
    }
    APulseAmpUnregulated :: APulseAmpUnregulated ()
    {
    }
    _eHndl < _eInstblTypeInfo > APulseAmpUnregulated :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 33));
        return ti;
    }
    _mBeginClassInvariant (VPulseAmpUnregulated)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem (((v_pulse_amp_unregulated == _ePair <
        SWITCH :: _eEnum _mComma _eInt > (SWITCH :: OFF, _mInteger (0))) || ((SWITCH :: ON ==
        v_pulse_amp_unregulated.x) && ((((_mInteger (1250000) == v_pulse_amp_unregulated.y) || (
        _mInteger (2500000) == v_pulse_amp_unregulated.y)) || (_mInteger (3750000) ==
        v_pulse_amp_unregulated.y)) || (_mInteger (5000000) == v_pulse_amp_unregulated.y)))),
        "964,33");
    #endif
    _mEndClassInvariant
    VPulseAmpUnregulated :: VPulseAmpUnregulated (const _ePair < SWITCH :: _eEnum _mComma _eInt >
        _vv_pulse_amp_unregulated) : _eAny (), v_pulse_amp_unregulated (_vv_pulse_amp_unregulated)
    {
        _mBuild;
        _mCheckClassInvariant ("973,5");
    }
    void VPulseAmpUnregulated :: chg_v_pulse_amp_unregulated (const _ePair < SWITCH :: _eEnum
        _mComma _eInt > a)
    {
        _mSchema (chg_v_pulse_amp_unregulated);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (((a == _ePair < SWITCH :: _eEnum _mComma _eInt > (SWITCH :: OFF,
            _mInteger (0))) || ((SWITCH :: ON == a.x) && ((((_mInteger (1250000) == a.y) || (
            _mInteger (2500000) == a.y)) || (_mInteger (3750000) == a.y)) || (_mInteger (5000000) ==
            a.y)))), "976,15");
        #endif
        v_pulse_amp_unregulated = a;
        _mCheckClassInvariant ("982,14");
    }
    _eBool VPulseAmpUnregulated :: operator == (const VPulseAmpUnregulated & _vArg_961_13) const
    {
        _mOperator (=);
        return (_vArg_961_13.v_pulse_amp_unregulated == v_pulse_amp_unregulated);
    }
    VPulseAmpUnregulated :: VPulseAmpUnregulated ()
    {
    }
    _eHndl < _eInstblTypeInfo > VPulseAmpUnregulated :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 34));
        return ti;
    }
    _mBeginClassInvariant (APulseWidth)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem (((_mInteger (50) == a_pulse_width) ||
        _onRange (_mInteger (100), _mInteger (1900))._ovIn (a_pulse_width)), "992,24");
    #endif
    _mEndClassInvariant
    APulseWidth :: APulseWidth (const _eInt _va_pulse_width) : _eAny (), a_pulse_width (
        _va_pulse_width)
    {
        _mBuild;
        _mCheckClassInvariant ("997,5");
    }
    void APulseWidth :: chg_a_pulse_width (const _eInt x)
    {
        _mSchema (chg_a_pulse_width);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (((_mInteger (50) == x) || _onRange (_mInteger (100), _mInteger (1900))
            ._ovIn (x)), "1000,16");
        #endif
        a_pulse_width = x;
        _mCheckClassInvariant ("1001,14");
    }
    _eBool APulseWidth :: operator == (const APulseWidth & _vArg_989_13) const
    {
        _mOperator (=);
        return (_vArg_989_13.a_pulse_width == a_pulse_width);
    }
    APulseWidth :: APulseWidth ()
    {
    }
    _eHndl < _eInstblTypeInfo > APulseWidth :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 35));
        return ti;
    }
    _mBeginClassInvariant (VPulseWidth)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem (((_mInteger (50) == v_pulse_width) ||
        _onRange (_mInteger (100), _mInteger (1900))._ovIn (v_pulse_width)), "1010,24");
    #endif
    _mEndClassInvariant
    VPulseWidth :: VPulseWidth (const _eInt _vv_pulse_width) : _eAny (), v_pulse_width (
        _vv_pulse_width)
    {
        _mBuild;
        _mCheckClassInvariant ("1015,5");
    }
    void VPulseWidth :: chg_v_pulse_width (const _eInt x)
    {
        _mSchema (chg_v_pulse_width);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (((_mInteger (50) == x) || _onRange (_mInteger (100), _mInteger (1900))
            ._ovIn (x)), "1018,16");
        #endif
        v_pulse_width = x;
        _mCheckClassInvariant ("1019,14");
    }
    _eBool VPulseWidth :: operator == (const VPulseWidth & _vArg_1007_13) const
    {
        _mOperator (=);
        return (_vArg_1007_13.v_pulse_width == v_pulse_width);
    }
    VPulseWidth :: VPulseWidth ()
    {
    }
    _eHndl < _eInstblTypeInfo > VPulseWidth :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 36));
        return ti;
    }
    _mBeginClassInvariant (ASensitivity)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem (((((_mInteger (250) == a_sensitivity) || (
        _mInteger (500) == a_sensitivity)) || (_mInteger (750) == a_sensitivity)) || _onRange (
        _mInteger (1000), _mInteger (10000))._ovIn (a_sensitivity)), "1028,24");
    #endif
    _mEndClassInvariant
    ASensitivity :: ASensitivity (const _eInt _va_sensitivity) : _eAny (), a_sensitivity (
        _va_sensitivity)
    {
        _mBuild;
        _mCheckClassInvariant ("1033,5");
    }
    void ASensitivity :: chg_a_sensitivity (const _eInt x)
    {
        _mSchema (chg_a_sensitivity);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (((((_mInteger (250) == x) || (_mInteger (500) == x)) || (_mInteger (
            750) == x)) || _onRange (_mInteger (1000), _mInteger (10000))._ovIn (x)), "1036,16");
        #endif
        a_sensitivity = x;
        _mCheckClassInvariant ("1038,14");
    }
    _eBool ASensitivity :: operator == (const ASensitivity & _vArg_1025_13) const
    {
        _mOperator (=);
        return (_vArg_1025_13.a_sensitivity == a_sensitivity);
    }
    ASensitivity :: ASensitivity ()
    {
    }
    _eHndl < _eInstblTypeInfo > ASensitivity :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 37));
        return ti;
    }
    _mBeginClassInvariant (VSensitivity)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem (((((_mInteger (250) == v_sensitivity) || (
        _mInteger (500) == v_sensitivity)) || (_mInteger (750) == v_sensitivity)) || _onRange (
        _mInteger (1000), _mInteger (10000))._ovIn (v_sensitivity)), "1047,24");
    #endif
    _mEndClassInvariant
    VSensitivity :: VSensitivity (const _eInt _vv_sensitivity) : _eAny (), v_sensitivity (
        _vv_sensitivity)
    {
        _mBuild;
        _mCheckClassInvariant ("1053,5");
    }
    void VSensitivity :: chg_v_sensitivity (const _eInt x)
    {
        _mSchema (chg_v_sensitivity);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (((((_mInteger (250) == x) || (_mInteger (500) == x)) || (_mInteger (
            750) == x)) || _onRange (_mInteger (1000), _mInteger (10000))._ovIn (x)), "1056,16");
        #endif
        v_sensitivity = x;
        _mCheckClassInvariant ("1058,14");
    }
    _eBool VSensitivity :: operator == (const VSensitivity & _vArg_1044_13) const
    {
        _mOperator (=);
        return (_vArg_1044_13.v_sensitivity == v_sensitivity);
    }
    VSensitivity :: VSensitivity ()
    {
    }
    _eHndl < _eInstblTypeInfo > VSensitivity :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 38));
        return ti;
    }
    _mBeginClassInvariant (VRefractoryPeriod)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem (_onRange (_mInteger (150000), _mInteger (
        500000))._ovIn (v_refract_period), "1067,26");
    #endif
    _mEndClassInvariant
    VRefractoryPeriod :: VRefractoryPeriod (const _eInt _vv_refract_period) : _eAny (),
        v_refract_period (_vv_refract_period)
    {
        _mBuild;
        _mCheckClassInvariant ("1071,5");
    }
    void VRefractoryPeriod :: chg_v_refract_period (const _eInt x)
    {
        _mSchema (chg_v_refract_period);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (150000), _mInteger (500000))._ovIn (x),
            "1074,15");
        #endif
        v_refract_period = x;
        _mCheckClassInvariant ("1075,14");
    }
    _eBool VRefractoryPeriod :: operator == (const VRefractoryPeriod & _vArg_1064_13) const
    {
        _mOperator (=);
        return (_vArg_1064_13.v_refract_period == v_refract_period);
    }
    VRefractoryPeriod :: VRefractoryPeriod ()
    {
    }
    _eHndl < _eInstblTypeInfo > VRefractoryPeriod :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 39));
        return ti;
    }
    _mBeginClassInvariant (ARefractoryPeriod)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem (_onRange (_mInteger (150000), _mInteger (
        500000))._ovIn (a_refract_period), "1084,26");
    #endif
    _mEndClassInvariant
    ARefractoryPeriod :: ARefractoryPeriod (const _eInt _va_refract_period) : _eAny (),
        a_refract_period (_va_refract_period)
    {
        _mBuild;
        _mCheckClassInvariant ("1089,5");
    }
    void ARefractoryPeriod :: chg_a_refract_period (const _eInt x)
    {
        _mSchema (chg_a_refract_period);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (150000), _mInteger (500000))._ovIn (x),
            "1092,15");
        #endif
        a_refract_period = x;
        _mCheckClassInvariant ("1093,14");
    }
    _eBool ARefractoryPeriod :: operator == (const ARefractoryPeriod & _vArg_1081_13) const
    {
        _mOperator (=);
        return (_vArg_1081_13.a_refract_period == a_refract_period);
    }
    ARefractoryPeriod :: ARefractoryPeriod ()
    {
    }
    _eHndl < _eInstblTypeInfo > ARefractoryPeriod :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 40));
        return ti;
    }
    _mBeginClassInvariant (PVARP)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem (_onRange (_mInteger (150000), _mInteger (
        500000))._ovIn (pvarp), "1102,15");
    #endif
    _mEndClassInvariant
    PVARP :: PVARP (const _eInt _vpvarp) : _eAny (), pvarp (_vpvarp)
    {
        _mBuild;
        _mCheckClassInvariant ("1107,5");
    }
    void PVARP :: chg_pvarp (const _eInt x)
    {
        _mSchema (chg_pvarp);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (150000), _mInteger (500000))._ovIn (x),
            "1110,15");
        #endif
        pvarp = x;
        _mCheckClassInvariant ("1111,14");
    }
    _eBool PVARP :: operator == (const PVARP & _vArg_1099_13) const
    {
        _mOperator (=);
        return (_vArg_1099_13.pvarp == pvarp);
    }
    PVARP :: PVARP ()
    {
    }
    _eHndl < _eInstblTypeInfo > PVARP :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 41));
        return ti;
    }
    _mBeginClassInvariant (PVARP_Ext)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem (((pvarp_ext == _ePair < SWITCH :: _eEnum
        _mComma _eInt > (SWITCH :: OFF, _mInteger (0))) || ((SWITCH :: ON == pvarp_ext.x) && ((
        _mInteger (50000) <= pvarp_ext.y) && (pvarp_ext.y <= _mInteger (400000))))), "1120,19");
    #endif
    _mEndClassInvariant
    PVARP_Ext :: PVARP_Ext (const _ePair < SWITCH :: _eEnum _mComma _eInt > _vpvarp_ext) : _eAny (),
        pvarp_ext (_vpvarp_ext)
    {
        _mBuild;
        _mCheckClassInvariant ("1126,5");
    }
    void PVARP_Ext :: chg_pvarp_ext (const _ePair < SWITCH :: _eEnum _mComma _eInt > a)
    {
        _mSchema (chg_pvarp_ext);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (((a == _ePair < SWITCH :: _eEnum _mComma _eInt > (SWITCH :: OFF,
            _mInteger (0))) || ((SWITCH :: ON == a.x) && ((_mInteger (50000) <= a.y) && (a.y <=
            _mInteger (400000))))), "1129,15");
        #endif
        pvarp_ext = a;
        _mCheckClassInvariant ("1131,14");
    }
    _eBool PVARP_Ext :: operator == (const PVARP_Ext & _vArg_1117_13) const
    {
        _mOperator (=);
        return (_vArg_1117_13.pvarp_ext == pvarp_ext);
    }
    PVARP_Ext :: PVARP_Ext ()
    {
    }
    _eHndl < _eInstblTypeInfo > PVARP_Ext :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 42));
        return ti;
    }
    _mBeginClassInvariant (HysteresisRateLimit)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem (((hysteresis_rate_limit == _ePair < SWITCH
        :: _eEnum _mComma _eInt > (SWITCH :: OFF, _mInteger (0))) || ((SWITCH :: ON ==
        hysteresis_rate_limit.x) && ((_mInteger (30) <= hysteresis_rate_limit.y) && (
        hysteresis_rate_limit.y <= _mInteger (175))))), "1140,31");
    #endif
    _mEndClassInvariant
    HysteresisRateLimit :: HysteresisRateLimit (const _ePair < SWITCH :: _eEnum _mComma _eInt >
        _vhysteresis_rate_limit) : _eAny (), hysteresis_rate_limit (_vhysteresis_rate_limit)
    {
        _mBuild;
        _mCheckClassInvariant ("1147,5");
    }
    void HysteresisRateLimit :: chg_hysteresis_rate_limit (const _ePair < SWITCH :: _eEnum _mComma
        _eInt > a)
    {
        _mSchema (chg_hysteresis_rate_limit);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (((a == _ePair < SWITCH :: _eEnum _mComma _eInt > (SWITCH :: OFF,
            _mInteger (0))) || ((SWITCH :: ON == a.x) && ((_mInteger (30) <= a.y) && (a.y <=
            _mInteger (175))))), "1150,15");
        #endif
        hysteresis_rate_limit = a;
        _mCheckClassInvariant ("1153,14");
    }
    _eBool HysteresisRateLimit :: operator == (const HysteresisRateLimit & _vArg_1137_13) const
    {
        _mOperator (=);
        return (_vArg_1137_13.hysteresis_rate_limit == hysteresis_rate_limit);
    }
    HysteresisRateLimit :: HysteresisRateLimit ()
    {
    }
    _eHndl < _eInstblTypeInfo > HysteresisRateLimit :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 43));
        return ti;
    }
    _mBeginClassInvariant (RateSmoothing)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem (((rate_smoothing == _ePair < SWITCH ::
        _eEnum _mComma _eInt > (SWITCH :: OFF, _mInteger (0))) || ((SWITCH :: ON == rate_smoothing.x)
        && ((((((((_mInteger (3) == rate_smoothing.y) || (_mInteger (6) == rate_smoothing.y)) || (
        _mInteger (9) == rate_smoothing.y)) || (_mInteger (12) == rate_smoothing.y)) || (_mInteger (
        15) == rate_smoothing.y)) || (_mInteger (18) == rate_smoothing.y)) || (_mInteger (21) ==
        rate_smoothing.y)) || (_mInteger (25) == rate_smoothing.y)))), "1163,24");
    #endif
    _mEndClassInvariant
    RateSmoothing :: RateSmoothing (const _ePair < SWITCH :: _eEnum _mComma _eInt > _vrate_smoothing)
        : _eAny (), rate_smoothing (_vrate_smoothing)
    {
        _mBuild;
        _mCheckClassInvariant ("1177,5");
    }
    void RateSmoothing :: chg_rate_smoothing (const _ePair < SWITCH :: _eEnum _mComma _eInt > a)
    {
        _mSchema (chg_rate_smoothing);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (((a == _ePair < SWITCH :: _eEnum _mComma _eInt > (SWITCH :: OFF,
            _mInteger (0))) || ((SWITCH :: ON == a.x) && ((((((((_mInteger (3) == a.y) || (_mInteger
            (6) == a.y)) || (_mInteger (9) == a.y)) || (_mInteger (12) == a.y)) || (_mInteger (15)
            == a.y)) || (_mInteger (18) == a.y)) || (_mInteger (21) == a.y)) || (_mInteger (25) == a
            .y)))), "1180,15");
        #endif
        rate_smoothing = a;
        _mCheckClassInvariant ("1190,14");
    }
    _eBool RateSmoothing :: operator == (const RateSmoothing & _vArg_1160_13) const
    {
        _mOperator (=);
        return (_vArg_1160_13.rate_smoothing == rate_smoothing);
    }
    RateSmoothing :: RateSmoothing ()
    {
    }
    _eHndl < _eInstblTypeInfo > RateSmoothing :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 44));
        return ti;
    }
    ATRMode :: ATRMode (const SWITCH :: _eEnum _vatr_mode) : _eAny (), atr_mode (_vatr_mode)
    {
        _mBuild;
    }
    void ATRMode :: chg_atr_mode (const SWITCH :: _eEnum x)
    {
        _mSchema (chg_atr_mode);
        atr_mode = x;
    }
    _eBool ATRMode :: operator == (const ATRMode & _vArg_1196_13) const
    {
        _mOperator (=);
        return (_vArg_1196_13.atr_mode == atr_mode);
    }
    ATRMode :: ATRMode ()
    {
    }
    _eHndl < _eInstblTypeInfo > ATRMode :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 45));
        return ti;
    }
    _mBeginClassInvariant (ATRDuration)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem ((((_mInteger (10) == atr_duration) ||
        _onRange (_mInteger (20), _mInteger (80))._ovIn (atr_duration)) || _onRange (_mInteger (100),
        _mInteger (2000))._ovIn (atr_duration)), "1213,23");
    #endif
    _mEndClassInvariant
    ATRDuration :: ATRDuration (const _eInt _vatr_duration) : _eAny (), atr_duration (_vatr_duration)
    {
        _mBuild;
        _mCheckClassInvariant ("1218,5");
    }
    void ATRDuration :: chg_atr_duration (const _eInt x)
    {
        _mSchema (chg_atr_duration);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre ((((_mInteger (10) == x) || _onRange (_mInteger (20), _mInteger (80)).
            _ovIn (x)) || _onRange (_mInteger (100), _mInteger (2000))._ovIn (x)), "1221,16");
        #endif
        atr_duration = x;
        _mCheckClassInvariant ("1222,14");
    }
    _eBool ATRDuration :: operator == (const ATRDuration & _vArg_1210_13) const
    {
        _mOperator (=);
        return (_vArg_1210_13.atr_duration == atr_duration);
    }
    ATRDuration :: ATRDuration ()
    {
    }
    _eHndl < _eInstblTypeInfo > ATRDuration :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 46));
        return ti;
    }
    _mBeginClassInvariant (ATRFallbackTime)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem ((((((_mInteger (1) == atr_fallback_time) ||
        (_mInteger (2) == atr_fallback_time)) || (_mInteger (3) == atr_fallback_time)) || (_mInteger
        (4) == atr_fallback_time)) || (_mInteger (5) == atr_fallback_time)), "1231,28");
    #endif
    _mEndClassInvariant
    ATRFallbackTime :: ATRFallbackTime (const _eInt _vatr_fallback_time) : _eAny (),
        atr_fallback_time (_vatr_fallback_time)
    {
        _mBuild;
        _mCheckClassInvariant ("1240,5");
    }
    void ATRFallbackTime :: chg_atr_fallback_time (const _eInt x)
    {
        _mSchema (chg_atr_fallback_time);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre ((((((_mInteger (1) == x) || (_mInteger (2) == x)) || (_mInteger (3)
            == x)) || (_mInteger (4) == x)) || (_mInteger (5) == x)), "1243,16");
        #endif
        atr_fallback_time = x;
        _mCheckClassInvariant ("1248,14");
    }
    _eBool ATRFallbackTime :: operator == (const ATRFallbackTime & _vArg_1228_13) const
    {
        _mOperator (=);
        return (_vArg_1228_13.atr_fallback_time == atr_fallback_time);
    }
    ATRFallbackTime :: ATRFallbackTime ()
    {
    }
    _eHndl < _eInstblTypeInfo > ATRFallbackTime :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 47));
        return ti;
    }
    _mBeginClassInvariant (VBlanking)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem (_onRange (_mInteger (30000), _mInteger (
        60000))._ovIn (v_blanking), "1257,21");
    #endif
    _mEndClassInvariant
    VBlanking :: VBlanking (const _eInt _vv_blanking) : _eAny (), v_blanking (_vv_blanking)
    {
        _mBuild;
        _mCheckClassInvariant ("1262,5");
    }
    void VBlanking :: chg_v_blanking (const _eInt x)
    {
        _mSchema (chg_v_blanking);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (30000), _mInteger (60000))._ovIn (x), "1265,15");
        #endif
        v_blanking = x;
        _mCheckClassInvariant ("1266,14");
    }
    _eBool VBlanking :: operator == (const VBlanking & _vArg_1254_13) const
    {
        _mOperator (=);
        return (_vArg_1254_13.v_blanking == v_blanking);
    }
    VBlanking :: VBlanking ()
    {
    }
    _eHndl < _eInstblTypeInfo > VBlanking :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 48));
        return ti;
    }
    ActivityThreshold :: ActivityThreshold (const ACTIVITY_THRESHOLD :: _eEnum _vact_threshold) :
        _eAny (), act_threshold (_vact_threshold)
    {
        _mBuild;
    }
    void ActivityThreshold :: chg_act_threshold (const ACTIVITY_THRESHOLD :: _eEnum x)
    {
        _mSchema (chg_act_threshold);
        act_threshold = x;
    }
    _eBool ActivityThreshold :: operator == (const ActivityThreshold & _vArg_1272_13) const
    {
        _mOperator (=);
        return (_vArg_1272_13.act_threshold == act_threshold);
    }
    ActivityThreshold :: ActivityThreshold ()
    {
    }
    _eHndl < _eInstblTypeInfo > ActivityThreshold :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 49));
        return ti;
    }
    _mBeginClassInvariant (ReactTime)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem (_onRange (_mInteger (10), _mInteger (50)).
        _ovIn (reaction_time), "1289,23");
    #endif
    _mEndClassInvariant
    ReactTime :: ReactTime (const _eInt _vreaction_time) : _eAny (), reaction_time (_vreaction_time)
    {
        _mBuild;
        _mCheckClassInvariant ("1294,5");
    }
    void ReactTime :: chg_reaction_time (const _eInt x)
    {
        _mSchema (chg_reaction_time);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (10), _mInteger (50))._ovIn (x), "1297,15");
        #endif
        reaction_time = x;
        _mCheckClassInvariant ("1298,14");
    }
    _eBool ReactTime :: operator == (const ReactTime & _vArg_1286_13) const
    {
        _mOperator (=);
        return (_vArg_1286_13.reaction_time == reaction_time);
    }
    ReactTime :: ReactTime ()
    {
    }
    _eHndl < _eInstblTypeInfo > ReactTime :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 50));
        return ti;
    }
    _mBeginClassInvariant (RespFactor)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem (_onRange (_mInteger (1), _mInteger (16)).
        _ovIn (response_factor), "1307,25");
    #endif
    _mEndClassInvariant
    RespFactor :: RespFactor (const _eInt _vresponse_factor) : _eAny (), response_factor (
        _vresponse_factor)
    {
        _mBuild;
        _mCheckClassInvariant ("1312,5");
    }
    void RespFactor :: chg_response_factor (const _eInt x)
    {
        _mSchema (chg_response_factor);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (1), _mInteger (16))._ovIn (x), "1315,15");
        #endif
        response_factor = x;
        _mCheckClassInvariant ("1316,14");
    }
    _eBool RespFactor :: operator == (const RespFactor & _vArg_1304_13) const
    {
        _mOperator (=);
        return (_vArg_1304_13.response_factor == response_factor);
    }
    RespFactor :: RespFactor ()
    {
    }
    _eHndl < _eInstblTypeInfo > RespFactor :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 51));
        return ti;
    }
    _mBeginClassInvariant (RecoveryTime)
    #if !defined(NDEBUG)
    _mBeginClassInvariantItem _mCheckClassInvariantItem (_onRange (_mInteger (120), _mInteger (960))
        ._ovIn (recovery_time), "1325,23");
    #endif
    _mEndClassInvariant
    RecoveryTime :: RecoveryTime (const _eInt _vrecovery_time) : _eAny (), recovery_time (
        _vrecovery_time)
    {
        _mBuild;
        _mCheckClassInvariant ("1330,5");
    }
    void RecoveryTime :: chg_recovery_time (const _eInt x)
    {
        _mSchema (chg_recovery_time);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (120), _mInteger (960))._ovIn (x), "1333,15");
        #endif
        recovery_time = x;
        _mCheckClassInvariant ("1334,14");
    }
    _eBool RecoveryTime :: operator == (const RecoveryTime & _vArg_1322_13) const
    {
        _mOperator (=);
        return (_vArg_1322_13.recovery_time == recovery_time);
    }
    RecoveryTime :: RecoveryTime ()
    {
    }
    _eHndl < _eInstblTypeInfo > RecoveryTime :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 52));
        return ti;
    }
    ProgrammableParameters :: ProgrammableParameters (const LRL _vc_LRL, const URL _vc_URL, const
        MaxSensorRate _vc_MaxSensorRate, const FixedAVDelay _vc_FixedAVDelay, const DynamicAVDelay
        _vc_DynamicAVDelay, const MinDynamicAVDelay _vc_MinDynamicAVDelay, const SensedAVDelayOffset
        _vc_SensedAVDelayOffset, const APulseAmpRegulated _vc_APulseAmpRegulated, const
        VPulseAmpRegulated _vc_VPulseAmpRegulated, const APulseAmpUnregulated
        _vc_APulseAmpUnregulated, const VPulseAmpUnregulated _vc_VPulseAmpUnregulated, const
        APulseWidth _vc_APulseWidth, const VPulseWidth _vc_VPulseWidth, const ASensitivity
        _vc_ASensitivity, const VSensitivity _vc_VSensitivity, const VRefractoryPeriod
        _vc_VRefractoryPeriod, const ARefractoryPeriod _vc_ARefractoryPeriod, const PVARP _vc_PVARP,
        const PVARP_Ext _vc_PVARP_Ext, const HysteresisRateLimit _vc_HysteresisRateLimit, const
        RateSmoothing _vc_RateSmoothing, const ATRMode _vc_ATRMode, const ATRDuration
        _vc_ATRDuration, const ATRFallbackTime _vc_ATRFallbackTime, const VBlanking _vc_VBlanking,
        const ActivityThreshold _vc_ActivityThreshold, const ReactTime _vc_ReactTime, const
        RespFactor _vc_RespFactor, const RecoveryTime _vc_RecoveryTime) : _eAny (), c_LRL (_vc_LRL),
        c_URL (_vc_URL), c_MaxSensorRate (_vc_MaxSensorRate), c_FixedAVDelay (_vc_FixedAVDelay),
        c_DynamicAVDelay (_vc_DynamicAVDelay), c_MinDynamicAVDelay (_vc_MinDynamicAVDelay),
        c_SensedAVDelayOffset (_vc_SensedAVDelayOffset), c_APulseAmpRegulated (
        _vc_APulseAmpRegulated), c_VPulseAmpRegulated (_vc_VPulseAmpRegulated),
        c_APulseAmpUnregulated (_vc_APulseAmpUnregulated), c_VPulseAmpUnregulated (
        _vc_VPulseAmpUnregulated), c_APulseWidth (_vc_APulseWidth), c_VPulseWidth (_vc_VPulseWidth),
        c_ASensitivity (_vc_ASensitivity), c_VSensitivity (_vc_VSensitivity), c_VRefractoryPeriod (
        _vc_VRefractoryPeriod), c_ARefractoryPeriod (_vc_ARefractoryPeriod), c_PVARP (_vc_PVARP),
        c_PVARP_Ext (_vc_PVARP_Ext), c_HysteresisRateLimit (_vc_HysteresisRateLimit),
        c_RateSmoothing (_vc_RateSmoothing), c_ATRMode (_vc_ATRMode), c_ATRDuration (_vc_ATRDuration)
        , c_ATRFallbackTime (_vc_ATRFallbackTime), c_VBlanking (_vc_VBlanking), c_ActivityThreshold
        (_vc_ActivityThreshold), c_ReactTime (_vc_ReactTime), c_RespFactor (_vc_RespFactor),
        c_RecoveryTime (_vc_RecoveryTime)
    {
        _mBuild;
    }
    void ProgrammableParameters :: set_lower_rate_limit (const _eInt x)
    {
        _mSchema (set_lower_rate_limit);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (30), _mInteger (175))._ovIn (x), "1432,13");
        #endif
        c_LRL.chg_lower_rate_limit (x);
    }
    void ProgrammableParameters :: set_upper_rate_limit (const _eInt x)
    {
        _mSchema (set_upper_rate_limit);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (30), _mInteger (175))._ovIn (x), "1436,15");
        #endif
        c_URL.chg_upper_rate_limit (x);
    }
    void ProgrammableParameters :: set_max_sensor_rate (const _eInt x)
    {
        _mSchema (set_max_sensor_rate);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (50), _mInteger (175))._ovIn (x), "1440,15");
        #endif
        c_MaxSensorRate.chg_max_sensor_rate (x);
    }
    void ProgrammableParameters :: set_fixed_av_delay (const _eInt x)
    {
        _mSchema (set_fixed_av_delay);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (70000), _mInteger (300000))._ovIn (x), "1444,15")
            ;
        #endif
        c_FixedAVDelay.chg_fixed_av_delay (x);
    }
    void ProgrammableParameters :: set_dynamic_av_delay (const SWITCH :: _eEnum x)
    {
        _mSchema (set_dynamic_av_delay);
        c_DynamicAVDelay.chg_dynamic_av_delay (x);
    }
    void ProgrammableParameters :: set_min_dyn_av_delay (const _eInt x)
    {
        _mSchema (set_min_dyn_av_delay);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (30000), _mInteger (100000))._ovIn (x), "1451,15")
            ;
        #endif
        c_MinDynamicAVDelay.chg_min_dyn_av_delay (x);
    }
    void ProgrammableParameters :: set_sensed_av_delay_offset (const _ePair < SWITCH :: _eEnum
        _mComma _eInt > a)
    {
        _mSchema (set_sensed_av_delay_offset);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (((a == _ePair < SWITCH :: _eEnum _mComma _eInt > (SWITCH :: OFF,
            _mInteger (0))) || ((SWITCH :: ON == a.x) && (((- _mInteger (100000)) <= a.y) && (a.y <=
            (- _mInteger (100000)))))), "1455,16");
        #endif
        c_SensedAVDelayOffset.chg_sensed_av_delay_offset (a);
    }
    void ProgrammableParameters :: set_a_pulse_amp_regulated (const _ePair < SWITCH :: _eEnum
        _mComma _eInt > a)
    {
        _mSchema (set_a_pulse_amp_regulated);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre ((((a == _ePair < SWITCH :: _eEnum _mComma _eInt > (SWITCH :: OFF,
            _mInteger (0))) || ((SWITCH :: ON == a.x) && ((_mInteger (500000) <= a.y) && (a.y <=
            _mInteger (3200000))))) || ((SWITCH :: ON == a.x) && ((_mInteger (3500000) <= a.y) && (a
            .y <= _mInteger (7000000))))), "1461,16");
        #endif
        c_APulseAmpRegulated.chg_a_pulse_amp_regulated (a);
    }
    void ProgrammableParameters :: set_v_pulse_amp_regulated (const _ePair < SWITCH :: _eEnum
        _mComma _eInt > a)
    {
        _mSchema (set_v_pulse_amp_regulated);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre ((((a == _ePair < SWITCH :: _eEnum _mComma _eInt > (SWITCH :: OFF,
            _mInteger (0))) || ((SWITCH :: ON == a.x) && ((_mInteger (500000) <= a.y) && (a.y <=
            _mInteger (3200000))))) || ((SWITCH :: ON == a.x) && ((_mInteger (3500000) <= a.y) && (a
            .y <= _mInteger (7000000))))), "1469,16");
        #endif
        c_VPulseAmpRegulated.chg_v_pulse_amp_regulated (a);
    }
    void ProgrammableParameters :: set_a_pulse_amp_unregulated (const _ePair < SWITCH :: _eEnum
        _mComma _eInt > a)
    {
        _mSchema (set_a_pulse_amp_unregulated);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (((a == _ePair < SWITCH :: _eEnum _mComma _eInt > (SWITCH :: OFF,
            _mInteger (0))) || ((SWITCH :: ON == a.x) && ((((_mInteger (1250000) == a.y) || (
            _mInteger (2500000) == a.y)) || (_mInteger (3750000) == a.y)) || (_mInteger (5000000) ==
            a.y)))), "1475,15");
        #endif
        c_APulseAmpUnregulated.chg_a_pulse_amp_unregulated (a);
    }
    void ProgrammableParameters :: set_v_pulse_amp_unregulated (const _ePair < SWITCH :: _eEnum
        _mComma _eInt > a)
    {
        _mSchema (set_v_pulse_amp_unregulated);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (((a == _ePair < SWITCH :: _eEnum _mComma _eInt > (SWITCH :: OFF,
            _mInteger (0))) || ((SWITCH :: ON == a.x) && ((((_mInteger (1250000) == a.y) || (
            _mInteger (2500000) == a.y)) || (_mInteger (3750000) == a.y)) || (_mInteger (5000000) ==
            a.y)))), "1484,15");
        #endif
        c_VPulseAmpUnregulated.chg_v_pulse_amp_unregulated (a);
    }
    void ProgrammableParameters :: set_a_pulse_width (const _eInt x)
    {
        _mSchema (set_a_pulse_width);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (((_mInteger (50) == x) || _onRange (_mInteger (100), _mInteger (1900))
            ._ovIn (x)), "1493,16");
        #endif
        c_APulseWidth.chg_a_pulse_width (x);
    }
    void ProgrammableParameters :: set_v_pulse_width (const _eInt x)
    {
        _mSchema (set_v_pulse_width);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (((_mInteger (50) == x) || _onRange (_mInteger (100), _mInteger (1900))
            ._ovIn (x)), "1497,16");
        #endif
        c_VPulseWidth.chg_v_pulse_width (x);
    }
    void ProgrammableParameters :: set_a_sensitivity (const _eInt x)
    {
        _mSchema (set_a_sensitivity);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (((((_mInteger (250) == x) || (_mInteger (500) == x)) || (_mInteger (
            750) == x)) || _onRange (_mInteger (1000), _mInteger (10000))._ovIn (x)), "1501,16");
        #endif
        c_ASensitivity.chg_a_sensitivity (x);
    }
    void ProgrammableParameters :: set_v_sensitivity (const _eInt x)
    {
        _mSchema (set_v_sensitivity);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (((((_mInteger (250) == x) || (_mInteger (500) == x)) || (_mInteger (
            750) == x)) || _onRange (_mInteger (1000), _mInteger (10000))._ovIn (x)), "1506,16");
        #endif
        c_VSensitivity.chg_v_sensitivity (x);
    }
    void ProgrammableParameters :: set_v_refract_period (const _eInt x)
    {
        _mSchema (set_v_refract_period);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (150000), _mInteger (500000))._ovIn (x),
            "1511,15");
        #endif
        c_VRefractoryPeriod.chg_v_refract_period (x);
    }
    void ProgrammableParameters :: set_a_refract_period (const _eInt x)
    {
        _mSchema (set_a_refract_period);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (150000), _mInteger (500000))._ovIn (x),
            "1515,15");
        #endif
        c_ARefractoryPeriod.chg_a_refract_period (x);
    }
    void ProgrammableParameters :: set_pvarp (const _eInt x)
    {
        _mSchema (set_pvarp);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (150000), _mInteger (500000))._ovIn (x),
            "1519,15");
        #endif
        c_PVARP.chg_pvarp (x);
    }
    void ProgrammableParameters :: set_pvarp_ext (const _ePair < SWITCH :: _eEnum _mComma _eInt > a)
    {
        _mSchema (set_pvarp_ext);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (((a == _ePair < SWITCH :: _eEnum _mComma _eInt > (SWITCH :: OFF,
            _mInteger (0))) || ((SWITCH :: ON == a.x) && ((_mInteger (50000) <= a.y) && (a.y <=
            _mInteger (400000))))), "1523,15");
        #endif
        c_PVARP_Ext.chg_pvarp_ext (a);
    }
    void ProgrammableParameters :: set_hysteresis_rate_limit (const _ePair < SWITCH :: _eEnum
        _mComma _eInt > a)
    {
        _mSchema (set_hysteresis_rate_limit);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (((a == _ePair < SWITCH :: _eEnum _mComma _eInt > (SWITCH :: OFF,
            _mInteger (0))) || ((SWITCH :: ON == a.x) && ((_mInteger (30) <= a.y) && (a.y <=
            _mInteger (175))))), "1528,15");
        #endif
        c_HysteresisRateLimit.chg_hysteresis_rate_limit (a);
    }
    void ProgrammableParameters :: set_rate_smoothing (const _ePair < SWITCH :: _eEnum _mComma _eInt
        > a)
    {
        _mSchema (set_rate_smoothing);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (((a == _ePair < SWITCH :: _eEnum _mComma _eInt > (SWITCH :: OFF,
            _mInteger (0))) || ((SWITCH :: ON == a.x) && ((((((((_mInteger (3) == a.y) || (_mInteger
            (6) == a.y)) || (_mInteger (9) == a.y)) || (_mInteger (12) == a.y)) || (_mInteger (15)
            == a.y)) || (_mInteger (18) == a.y)) || (_mInteger (21) == a.y)) || (_mInteger (25) == a
            .y)))), "1534,15");
        #endif
        c_RateSmoothing.chg_rate_smoothing (a);
    }
    void ProgrammableParameters :: set_atr_mode (const SWITCH :: _eEnum x)
    {
        _mSchema (set_atr_mode);
        c_ATRMode.chg_atr_mode (x);
    }
    void ProgrammableParameters :: set_atr_duration (const _eInt x)
    {
        _mSchema (set_atr_duration);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre ((((_mInteger (10) == x) || _onRange (_mInteger (20), _mInteger (80)).
            _ovIn (x)) || _onRange (_mInteger (100), _mInteger (2000))._ovIn (x)), "1550,16");
        #endif
        c_ATRDuration.chg_atr_duration (x);
    }
    void ProgrammableParameters :: set_atr_fallback_time (const _eInt x)
    {
        _mSchema (set_atr_fallback_time);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre ((((((_mInteger (1) == x) || (_mInteger (2) == x)) || (_mInteger (3)
            == x)) || (_mInteger (4) == x)) || (_mInteger (5) == x)), "1554,16");
        #endif
        c_ATRFallbackTime.chg_atr_fallback_time (x);
    }
    void ProgrammableParameters :: set_v_blanking (const _eInt x)
    {
        _mSchema (set_v_blanking);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (30000), _mInteger (60000))._ovIn (x), "1562,15");
        #endif
        c_VBlanking.chg_v_blanking (x);
    }
    void ProgrammableParameters :: set_act_threshold (const ACTIVITY_THRESHOLD :: _eEnum x)
    {
        _mSchema (set_act_threshold);
        c_ActivityThreshold.chg_act_threshold (x);
    }
    void ProgrammableParameters :: set_reaction_time (const _eInt x)
    {
        _mSchema (set_reaction_time);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (10), _mInteger (50))._ovIn (x), "1569,15");
        #endif
        c_ReactTime.chg_reaction_time (x);
    }
    void ProgrammableParameters :: set_response_factor (const _eInt x)
    {
        _mSchema (set_response_factor);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (1), _mInteger (16))._ovIn (x), "1573,15");
        #endif
        c_RespFactor.chg_response_factor (x);
    }
    void ProgrammableParameters :: set_recovery_time (const _eInt x)
    {
        _mSchema (set_recovery_time);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (_onRange (_mInteger (120), _mInteger (960))._ovIn (x), "1577,15");
        #endif
        c_RecoveryTime.chg_recovery_time (x);
    }
    _eBool ProgrammableParameters :: operator == (const ProgrammableParameters & _vArg_1341_13)
        const
    {
        _mOperator (=);
        return (((((((((((((((((((((((((((((_vArg_1341_13.c_LRL == c_LRL) && (_vArg_1341_13.c_URL ==
            c_URL)) && (_vArg_1341_13.c_MaxSensorRate == c_MaxSensorRate)) && (_vArg_1341_13.
            c_FixedAVDelay == c_FixedAVDelay)) && (_vArg_1341_13.c_DynamicAVDelay ==
            c_DynamicAVDelay)) && (_vArg_1341_13.c_MinDynamicAVDelay == c_MinDynamicAVDelay)) && (
            _vArg_1341_13.c_SensedAVDelayOffset == c_SensedAVDelayOffset)) && (_vArg_1341_13.
            c_APulseAmpRegulated == c_APulseAmpRegulated)) && (_vArg_1341_13.c_VPulseAmpRegulated ==
            c_VPulseAmpRegulated)) && (_vArg_1341_13.c_APulseAmpUnregulated ==
            c_APulseAmpUnregulated)) && (_vArg_1341_13.c_VPulseAmpUnregulated ==
            c_VPulseAmpUnregulated)) && (_vArg_1341_13.c_APulseWidth == c_APulseWidth)) && (
            _vArg_1341_13.c_VPulseWidth == c_VPulseWidth)) && (_vArg_1341_13.c_ASensitivity ==
            c_ASensitivity)) && (_vArg_1341_13.c_VSensitivity == c_VSensitivity)) && (_vArg_1341_13.
            c_VRefractoryPeriod == c_VRefractoryPeriod)) && (_vArg_1341_13.c_ARefractoryPeriod ==
            c_ARefractoryPeriod)) && (_vArg_1341_13.c_PVARP == c_PVARP)) && (_vArg_1341_13.
            c_PVARP_Ext == c_PVARP_Ext)) && (_vArg_1341_13.c_HysteresisRateLimit ==
            c_HysteresisRateLimit)) && (_vArg_1341_13.c_RateSmoothing == c_RateSmoothing)) && (
            _vArg_1341_13.c_ATRMode == c_ATRMode)) && (_vArg_1341_13.c_ATRDuration == c_ATRDuration))
            && (_vArg_1341_13.c_ATRFallbackTime == c_ATRFallbackTime)) && (_vArg_1341_13.c_VBlanking
            == c_VBlanking)) && (_vArg_1341_13.c_ActivityThreshold == c_ActivityThreshold)) && (
            _vArg_1341_13.c_ReactTime == c_ReactTime)) && (_vArg_1341_13.c_RespFactor ==
            c_RespFactor)) && (_vArg_1341_13.c_RecoveryTime == c_RecoveryTime));
    }
    ProgrammableParameters :: ProgrammableParameters ()
    {
    }
    _eHndl < _eInstblTypeInfo > ProgrammableParameters :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 53));
        return ti;
    }
    HistogramsSt :: HistogramsSt (const _eSeq < _eTriple < _eInt _mComma _eInt _mComma _eInt > >
        _vatrial_paced, const _eSeq < _eTriple < _eInt _mComma _eInt _mComma _eInt > >
        _vatrial_sensed, const _eSeq < _eTriple < _eInt _mComma _eInt _mComma _eInt > >
        _vventricular_paced, const _eSeq < _eTriple < _eInt _mComma _eInt _mComma _eInt > >
        _vventricular_sensed, const _eInt _vpvc_event, const _eInt _vatrial_tachy) : _eAny (),
        atrial_paced (_vatrial_paced), atrial_sensed (_vatrial_sensed), ventricular_paced (
        _vventricular_paced), ventricular_sensed (_vventricular_sensed), pvc_event (_vpvc_event),
        atrial_tachy (_vatrial_tachy)
    {
        _mBuild;
    }
    void HistogramsSt :: chg_atrial_paced (const _eInt _rmin, const _eInt _rmax, const _eInt qty)
    {
        _mSchema (chg_atrial_paced);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (((_mInteger (0) < _rmin) && (_mInteger (0) < _rmax)), "1603,17");
        #endif
        atrial_paced = atrial_paced._oPlusPlus (_eSeq < _eTriple < _eInt _mComma _eInt _mComma _eInt
            > > (_eTriple < _eInt _mComma _eInt _mComma _eInt > (_rmin, _rmax, qty)));
    }
    void HistogramsSt :: chg_atrial_sensed (const _eInt _rmin, const _eInt _rmax, const _eInt qty)
    {
        _mSchema (chg_atrial_sensed);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (((_mInteger (0) < _rmin) && (_mInteger (0) < _rmax)), "1607,17");
        #endif
        atrial_sensed = atrial_sensed._oPlusPlus (_eSeq < _eTriple < _eInt _mComma _eInt _mComma
            _eInt > > (_eTriple < _eInt _mComma _eInt _mComma _eInt > (_rmin, _rmax, qty)));
    }
    void HistogramsSt :: chg_ventricular_paced (const _eInt _rmin, const _eInt _rmax, const _eInt
        qty)
    {
        _mSchema (chg_ventricular_paced);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (((_mInteger (0) < _rmin) && (_mInteger (0) < _rmax)), "1611,17");
        #endif
        ventricular_paced = ventricular_paced._oPlusPlus (_eSeq < _eTriple < _eInt _mComma _eInt
            _mComma _eInt > > (_eTriple < _eInt _mComma _eInt _mComma _eInt > (_rmin, _rmax, qty)));
    }
    void HistogramsSt :: chg_ventricular_sensed (const _eInt _rmin, const _eInt _rmax, const _eInt
        qty)
    {
        _mSchema (chg_ventricular_sensed);
        #if !defined(NDEBUG)
        _mBeginPre _mCheckPre (((_mInteger (0) < _rmin) && (_mInteger (0) < _rmax)), "1615,17");
        #endif
        ventricular_sensed = ventricular_sensed._oPlusPlus (_eSeq < _eTriple < _eInt _mComma _eInt
            _mComma _eInt > > (_eTriple < _eInt _mComma _eInt _mComma _eInt > (_rmin, _rmax, qty)));
    }
    void HistogramsSt :: chg_pvc_event (const _eInt qty)
    {
        _mSchema (chg_pvc_event);
        pvc_event = qty;
    }
    void HistogramsSt :: chg_atrial_tachy (const _eInt qty)
    {
        _mSchema (chg_atrial_tachy);
        atrial_tachy = qty;
    }
    _eBool HistogramsSt :: operator == (const HistogramsSt & _vArg_1585_13) const
    {
        _mOperator (=);
        return ((((((_vArg_1585_13.atrial_paced == atrial_paced) && (_vArg_1585_13.atrial_sensed ==
            atrial_sensed)) && (_vArg_1585_13.ventricular_paced == ventricular_paced)) && (
            _vArg_1585_13.ventricular_sensed == ventricular_sensed)) && (_vArg_1585_13.pvc_event ==
            pvc_event)) && (_vArg_1585_13.atrial_tachy == atrial_tachy));
    }
    HistogramsSt :: HistogramsSt ()
    {
    }
    _eHndl < _eInstblTypeInfo > HistogramsSt :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 54));
        return ti;
    }
    BradycardiaSt :: BradycardiaSt (const BRADYCARDIA_STATE :: _eEnum _vbradycardia_state) : _eAny ()
        , bradycardia_state (_vbradycardia_state)
    {
        _mBuild;
    }
    void BradycardiaSt :: chg_bradycardia_state (const BRADYCARDIA_STATE :: _eEnum x)
    {
        _mSchema (chg_bradycardia_state);
        bradycardia_state = x;
    }
    _eBool BradycardiaSt :: operator == (const BradycardiaSt & _vArg_1628_13) const
    {
        _mOperator (=);
        return (_vArg_1628_13.bradycardia_state == bradycardia_state);
    }
    BradycardiaSt :: BradycardiaSt ()
    {
    }
    _eHndl < _eInstblTypeInfo > BradycardiaSt :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 55));
        return ti;
    }
    AtrialBipolarLead :: AtrialBipolarLead (const LEAD_CHAMBER_TYPE :: _eEnum _va_lead_chamber,
        const POLARITY :: _eEnum _va_lead_polarity) : _eAny (), a_lead_chamber (_va_lead_chamber),
        a_lead_polarity (_va_lead_polarity)
    {
        _mBuild;
    }
    void AtrialBipolarLead :: chg_a_lead_chamber (const LEAD_CHAMBER_TYPE :: _eEnum x)
    {
        _mSchema (chg_a_lead_chamber);
        a_lead_chamber = x;
    }
    void AtrialBipolarLead :: chg_a_lead_polarity (const POLARITY :: _eEnum x)
    {
        _mSchema (chg_a_lead_polarity);
        a_lead_polarity = x;
    }
    _eBool AtrialBipolarLead :: operator == (const AtrialBipolarLead & _vArg_1643_13) const
    {
        _mOperator (=);
        return ((_vArg_1643_13.a_lead_chamber == a_lead_chamber) && (_vArg_1643_13.a_lead_polarity
            == a_lead_polarity));
    }
    AtrialBipolarLead :: AtrialBipolarLead ()
    {
    }
    _eHndl < _eInstblTypeInfo > AtrialBipolarLead :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 56));
        return ti;
    }
    VentricularBipolarLead :: VentricularBipolarLead (const LEAD_CHAMBER_TYPE :: _eEnum
        _vv_lead_chamber, const POLARITY :: _eEnum _vv_lead_polarity) : _eAny (), v_lead_chamber (
        _vv_lead_chamber), v_lead_polarity (_vv_lead_polarity)
    {
        _mBuild;
    }
    void VentricularBipolarLead :: chg_v_lead_chamber (const LEAD_CHAMBER_TYPE :: _eEnum x)
    {
        _mSchema (chg_v_lead_chamber);
        v_lead_chamber = x;
    }
    void VentricularBipolarLead :: chg_v_lead_polarity (const POLARITY :: _eEnum x)
    {
        _mSchema (chg_v_lead_polarity);
        v_lead_polarity = x;
    }
    _eBool VentricularBipolarLead :: operator == (const VentricularBipolarLead & _vArg_1660_13)
        const
    {
        _mOperator (=);
        return ((_vArg_1660_13.v_lead_chamber == v_lead_chamber) && (_vArg_1660_13.v_lead_polarity
            == v_lead_polarity));
    }
    VentricularBipolarLead :: VentricularBipolarLead ()
    {
    }
    _eHndl < _eInstblTypeInfo > VentricularBipolarLead :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 57));
        return ti;
    }
    LeadsSt :: LeadsSt (const AtrialBipolarLead _vc_AtrialBipolarLead, const VentricularBipolarLead
        _vc_VentricularBipolarLead) : _eAny (), c_AtrialBipolarLead (_vc_AtrialBipolarLead),
        c_VentricularBipolarLead (_vc_VentricularBipolarLead)
    {
        _mBuild;
    }
    void LeadsSt :: chg_a_lead_chamber (const LEAD_CHAMBER_TYPE :: _eEnum x)
    {
        _mSchema (chg_a_lead_chamber);
        c_AtrialBipolarLead.chg_a_lead_chamber (x);
    }
    void LeadsSt :: chg_a_lead_polarity (const POLARITY :: _eEnum x)
    {
        _mSchema (chg_a_lead_polarity);
        c_AtrialBipolarLead.chg_a_lead_polarity (x);
    }
    void LeadsSt :: chg_v_lead_chamber (const LEAD_CHAMBER_TYPE :: _eEnum x)
    {
        _mSchema (chg_v_lead_chamber);
        c_VentricularBipolarLead.chg_v_lead_chamber (x);
    }
    void LeadsSt :: chg_v_lead_polarity (const POLARITY :: _eEnum x)
    {
        _mSchema (chg_v_lead_polarity);
        c_VentricularBipolarLead.chg_v_lead_polarity (x);
    }
    _eBool LeadsSt :: operator == (const LeadsSt & _vArg_1677_13) const
    {
        _mOperator (=);
        return ((_vArg_1677_13.c_AtrialBipolarLead == c_AtrialBipolarLead) && (_vArg_1677_13.
            c_VentricularBipolarLead == c_VentricularBipolarLead));
    }
    LeadsSt :: LeadsSt ()
    {
    }
    _eHndl < _eInstblTypeInfo > LeadsSt :: _aMyTypeInfo ()
    {
        static _eHndl < _eInstblTypeInfo > ti;
        ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (&
            _aobjLoaderNode), 58));
        return ti;
    }
}


// End of file.
