//***********************************************************************************************
//* File: 'Classes_2.hpp'
//* THIS IS A GENERATED FILE: DO NOT EDIT. Please edit the Perfect Developer source file instead!
//*
//* Generated from: 'C:\Users\user\Dropbox\Academico\Pesquisa\pacemaker-project\pacemaker-pd\Classes.pd'
//* by Perfect Developer version 6.00.00.03 at 13:45:28 UTC on Thursday April 10th 2014
//* Using command line options:
//* -z1 -el=3 -em=100 -gl=EmbeddedC++ -gs=1 -gv=ISO -gw=100 -gdp=1 -gdo=0 -gdc=3 -gda=1 -gdA=0 -gdl=0 -gdr=0 -gdt=0 -gdi=1 -st=4 -sb=C:\Program Files\Escher Technologies\Verification Studio 6\Bin\builtin.pd -sr=C:\Program Files\Escher Technologies\Verification Studio 6\Bin\rubric.pd -q=0 -gk=Pacemaker -eM=0 -@=C:\Users\user\AppData\Local\Temp\etf402C.tmp
//***********************************************************************************************


#if !defined(_hInline_Classes)
#define _hInline_Classes

// Inline code (may be empty)
// File inclusions for forward declarations

// File inclusions for full declarations

// File inclusions for inline code


template < class X, class Y, class Z, class K, class M > _eRank :: _eEnum _n5_quin < X _mComma Y
    _mComma Z _mComma K _mComma M > :: _oRank (const _n5_quin < X _mComma Y _mComma Z _mComma K
    _mComma M > & a) const
{
    _mOperator (~~);
    const _eRank :: _eEnum _vLet_t1_25_16 = _onRank (x, a.x);
    if ((_vLet_t1_25_16 == _eRank :: same))
    {
        const _eRank :: _eEnum _vLet_t2_27_23 = _onRank (y, a.y);
        if ((_vLet_t2_27_23 == _eRank :: same))
        {
            const _eRank :: _eEnum _vLet_t3_29_30 = _onRank (z, a.z);
            if ((_vLet_t3_29_30 == _eRank :: same))
            {
                const _eRank :: _eEnum _vLet_t4_31_38 = _onRank (k, a.k);
                return ((_vLet_t4_31_38 == _eRank :: same) ?
                _onRank (m, a.m) : _vLet_t4_31_38);
            }
            else
            {
                return _vLet_t3_29_30;
            }
        }
        else
        {
            return _vLet_t2_27_23;
        }
    }
    else
    {
        return _vLet_t1_25_16;
    }
}

template < class X, class Y, class Z, class K, class M > _eSeq < _eChar > _n5_quin < X _mComma Y
    _mComma Z _mComma K _mComma M > :: toString () const
{
    _mFunction (toString);
    return :: _ltoString (x).prepend (_mChar ('(')).append (_mChar (','))._oPlusPlus (:: _ltoString
        (y).append (_mChar (',')))._oPlusPlus (:: _ltoString (z).append (_mChar (',')))._oPlusPlus (
        :: _ltoString (k).append (_mChar (',')))._oPlusPlus (:: _ltoString (m).append (_mChar (')')))
        ;
}

template < class X, class Y, class Z, class K, class M > _n5_quin < X _mComma Y _mComma Z _mComma K
    _mComma M > :: _n5_quin (const X _vx, const Y _vy, const Z _vz, const K _vk, const M _vm) :
    _eAny (), x (_vx), y (_vy), z (_vz), k (_vk), m (_vm)
{
    _mBuild;
}

template < class X, class Y, class Z, class K, class M > _eBool _n5_quin < X _mComma Y _mComma Z
    _mComma K _mComma M > :: operator == (const _n5_quin < X _mComma Y _mComma Z _mComma K _mComma M
    > & _vArg_21_9) const
{
    _mOperator (=);
    return (((((_vArg_21_9.x == x) && (_vArg_21_9.y == y)) && (_vArg_21_9.z == z)) && (_vArg_21_9.k
        == k)) && (_vArg_21_9.m == m));
}

template < class X, class Y, class Z, class K, class M > _eBool _n5_quin < X _mComma Y _mComma Z
    _mComma K _mComma M > :: operator < (const _n5_quin < X _mComma Y _mComma Z _mComma K _mComma M
    > & _vArg_21_9) const
{
    _mOperator (<);
    return (_onRank ((* this), _vArg_21_9) == _eRank :: below);
}

template < class X, class Y, class Z, class K, class M > _eBool _n5_quin < X _mComma Y _mComma Z
    _mComma K _mComma M > :: operator <= (const _n5_quin < X _mComma Y _mComma Z _mComma K _mComma M
    > & _vArg_21_9) const
{
    _mOperator (<=);
    return (!(_onRank ((* this), _vArg_21_9) == _eRank :: above));
}

template < class X, class Y, class Z, class K, class M > _n5_quin < X _mComma Y _mComma Z _mComma K
    _mComma M > :: _n5_quin ()
{
}

template < class X, class Y, class Z, class K, class M > _eHndl < _eInstblTypeInfo > _n5_quin < X
    _mComma Y _mComma Z _mComma K _mComma M > :: _aMyTypeInfo ()
{
    static _eHndl < _eInstblTypeInfo > ti;
    ti = _eHndl < _eInstblTypeInfo > (new _eInstblTypeInfo (_eModuleDescriptorAddress (
        _agetObjLoaderNode_Classes ()), 0)) -> defineArgs (_eSeq < _eHndl < _eInstblTypeInfo > > ().
        append (_atypeInform < X > :: get ()).append (_atypeInform < Y > :: get ()).append (
        _atypeInform < Z > :: get ()).append (_atypeInform < K > :: get ()).append (_atypeInform < M
        > :: get ()));
    return ti;
}

#endif

// End of file.
