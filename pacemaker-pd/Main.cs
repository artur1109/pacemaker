//***********************************************************************************************
//* File: 'Main.cs'
//* THIS IS A GENERATED FILE: DO NOT EDIT. Please edit the Perfect Developer source file instead!
//*
//* Generated from: 'C:\Users\user\Dropbox\Academico\Pesquisa\pacemaker-project\pacemaker-pd\Main.pd'
//* by Perfect Developer version 6.00.00.03 at 13:44:03 UTC on Thursday April 10th 2014
//* Using command line options:
//* -z1 -el=3 -em=100 -gl=C# -gs=1 -gv=ISO -gw=100 -gdp=1 -gdo=0 -gdc=3 -gda=1 -gdA=0 -gdl=0 -gdr=0 -gdt=0 -gdi=1 -st=4 -sb=C:\Program Files\Escher Technologies\Verification Studio 6\Bin\builtin.pd -sr=C:\Program Files\Escher Technologies\Verification Studio 6\Bin\rubric.pd -q=0 -gk=Pacemaker -eM=0 -@=C:\Users\user\AppData\Local\Temp\etfF112.tmp
//***********************************************************************************************

// Namespaces imported
using System;

using Ertsys;
using Pacemaker;
#pragma warning disable 659 // disable warning "Foo overrides Object.Equals() but does not override Object.GetHashCode()"

namespace Pacemaker
{
    public sealed class PulseGenerator : _eAny
    {
        public ProgrammableParameters c_ProgrammableParameters;
        public MeasuredParameters c_MeasuredParameters;
        public TimeSt c_TimeSt;
        public SensingPulse c_SensingPulse;
        public PacingPulse c_PacingPulse;
        public BatteryStatus c_BatteryStatus;
        public EventMarkers c_EventMarkers;
        public NoiseDetection c_NoiseDetection;
        public BO_MODE c_BO_MODE;
        public int _nz_PulseWidth (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((0 < x))) throw new _xPre ("Main.pd:32,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            return _eSystem._oDiv (60000, x);
        }
        public int _nz_PulsePerMinute (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((0 < x))) throw new _xPre ("Main.pd:36,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            return _eSystem._oDiv (60000, x);
        }
        public int countAP (int min, int max)
        {
            _eSeq < _n5_quin < MarkerABB, int, int, int, int > > _vThose_40_13 = new _eSeq <
                _n5_quin < MarkerABB, int, int, int, int > > ();
            {
                int _vCaptureCount_i_40_38 = c_EventMarkers.atrial_marker._oHash ();
                int _vLoopCounter_40_20 = 0;
                for (;;)
                {
                    if ((_vLoopCounter_40_20 == _vCaptureCount_i_40_38)) break;
                    if (((((c_EventMarkers.atrial_marker._oIndex (_vLoopCounter_40_20).x ==
                        MarkerABB.AP) && (0 < c_EventMarkers.atrial_marker._oIndex (
                        _vLoopCounter_40_20).y)) && (min <= _nz_PulsePerMinute (c_EventMarkers.
                        atrial_marker._oIndex (_vLoopCounter_40_20).y))) && (_nz_PulsePerMinute (
                        c_EventMarkers.atrial_marker._oIndex (_vLoopCounter_40_20).y) <= max)))
                    {
                        _vThose_40_13 = _vThose_40_13.append (c_EventMarkers.atrial_marker._oIndex (
                            _vLoopCounter_40_20));
                    }
                    else
                    {
                    }
                    _vLoopCounter_40_20 = _eSystem._oSucc (_vLoopCounter_40_20);
                }
            }
            return _vThose_40_13._oHash ();
        }
        public int countAS (int min, int max)
        {
            _eSeq < _n5_quin < MarkerABB, int, int, int, int > > _vThose_43_13 = new _eSeq <
                _n5_quin < MarkerABB, int, int, int, int > > ();
            {
                int _vCaptureCount_i_43_38 = c_EventMarkers.atrial_marker._oHash ();
                int _vLoopCounter_43_20 = 0;
                for (;;)
                {
                    if ((_vLoopCounter_43_20 == _vCaptureCount_i_43_38)) break;
                    if (((((c_EventMarkers.atrial_marker._oIndex (_vLoopCounter_43_20).x ==
                        MarkerABB.AS) && (0 < c_EventMarkers.atrial_marker._oIndex (
                        _vLoopCounter_43_20).y)) && (min <= _nz_PulsePerMinute (c_EventMarkers.
                        atrial_marker._oIndex (_vLoopCounter_43_20).y))) && (_nz_PulsePerMinute (
                        c_EventMarkers.atrial_marker._oIndex (_vLoopCounter_43_20).y) <= max)))
                    {
                        _vThose_43_13 = _vThose_43_13.append (c_EventMarkers.atrial_marker._oIndex (
                            _vLoopCounter_43_20));
                    }
                    else
                    {
                    }
                    _vLoopCounter_43_20 = _eSystem._oSucc (_vLoopCounter_43_20);
                }
            }
            return _vThose_43_13._oHash ();
        }
        public int countVP (int min, int max)
        {
            _eSeq < _n5_quin < MarkerABB, int, int, int, int > > _vThose_46_13 = new _eSeq <
                _n5_quin < MarkerABB, int, int, int, int > > ();
            {
                int _vCaptureCount_i_46_38 = c_EventMarkers.ventricular_marker._oHash ();
                int _vLoopCounter_46_20 = 0;
                for (;;)
                {
                    if ((_vLoopCounter_46_20 == _vCaptureCount_i_46_38)) break;
                    if (((((c_EventMarkers.ventricular_marker._oIndex (_vLoopCounter_46_20).x ==
                        MarkerABB.VP) && (0 < c_EventMarkers.ventricular_marker._oIndex (
                        _vLoopCounter_46_20).y)) && (min <= _nz_PulsePerMinute (c_EventMarkers.
                        ventricular_marker._oIndex (_vLoopCounter_46_20).y))) && (_nz_PulsePerMinute
                        (c_EventMarkers.ventricular_marker._oIndex (_vLoopCounter_46_20).y) <= max)))
                    {
                        _vThose_46_13 = _vThose_46_13.append (c_EventMarkers.ventricular_marker.
                            _oIndex (_vLoopCounter_46_20));
                    }
                    else
                    {
                    }
                    _vLoopCounter_46_20 = _eSystem._oSucc (_vLoopCounter_46_20);
                }
            }
            return _vThose_46_13._oHash ();
        }
        public int countVS (int min, int max)
        {
            _eSeq < _n5_quin < MarkerABB, int, int, int, int > > _vThose_49_13 = new _eSeq <
                _n5_quin < MarkerABB, int, int, int, int > > ();
            {
                int _vCaptureCount_i_49_38 = c_EventMarkers.ventricular_marker._oHash ();
                int _vLoopCounter_49_20 = 0;
                for (;;)
                {
                    if ((_vLoopCounter_49_20 == _vCaptureCount_i_49_38)) break;
                    if (((((c_EventMarkers.ventricular_marker._oIndex (_vLoopCounter_49_20).x ==
                        MarkerABB.VS) && (0 < c_EventMarkers.ventricular_marker._oIndex (
                        _vLoopCounter_49_20).y)) && (min <= _nz_PulsePerMinute (c_EventMarkers.
                        ventricular_marker._oIndex (_vLoopCounter_49_20).y))) && (_nz_PulsePerMinute
                        (c_EventMarkers.ventricular_marker._oIndex (_vLoopCounter_49_20).y) <= max)))
                    {
                        _vThose_49_13 = _vThose_49_13.append (c_EventMarkers.ventricular_marker.
                            _oIndex (_vLoopCounter_49_20));
                    }
                    else
                    {
                    }
                    _vLoopCounter_49_20 = _eSystem._oSucc (_vLoopCounter_49_20);
                }
            }
            return _vThose_49_13._oHash ();
        }
        public int countPVC ()
        {
            _eSeq < _n5_quin < MarkerABB, int, int, int, int > > _vThose_52_13 = new _eSeq <
                _n5_quin < MarkerABB, int, int, int, int > > ();
            {
                int _vCaptureCount_i_52_38 = c_EventMarkers.ventricular_marker._oHash ();
                int _vLoopCounter_52_20 = 0;
                for (;;)
                {
                    if ((_vLoopCounter_52_20 == _vCaptureCount_i_52_38)) break;
                    if ((c_EventMarkers.ventricular_marker._oIndex (_vLoopCounter_52_20).x ==
                        MarkerABB.PVC))
                    {
                        _vThose_52_13 = _vThose_52_13.append (c_EventMarkers.ventricular_marker.
                            _oIndex (_vLoopCounter_52_20));
                    }
                    else
                    {
                    }
                    _vLoopCounter_52_20 = _eSystem._oSucc (_vLoopCounter_52_20);
                }
            }
            return _vThose_52_13._oHash ();
        }
        public int countAT ()
        {
            _eSeq < _n5_quin < MarkerABB, int, int, int, int > > _vThose_54_13 = new _eSeq <
                _n5_quin < MarkerABB, int, int, int, int > > ();
            {
                int _vCaptureCount_i_54_38 = c_EventMarkers.atrial_marker._oHash ();
                int _vLoopCounter_54_20 = 0;
                for (;;)
                {
                    if ((_vLoopCounter_54_20 == _vCaptureCount_i_54_38)) break;
                    if ((c_EventMarkers.atrial_marker._oIndex (_vLoopCounter_54_20).x == MarkerABB.
                        AT))
                    {
                        _vThose_54_13 = _vThose_54_13.append (c_EventMarkers.atrial_marker._oIndex (
                            _vLoopCounter_54_20));
                    }
                    else
                    {
                    }
                    _vLoopCounter_54_20 = _eSystem._oSucc (_vLoopCounter_54_20);
                }
            }
            return _vThose_54_13._oHash ();
        }
        public PulseGenerator (ProgrammableParameters _vc_ProgrammableParameters, MeasuredParameters
            _vc_MeasuredParameters, TimeSt _vc_TimeSt, BO_MODE _vc_BO_MODE, SensingPulse
            _vc_SensingPulse, PacingPulse _vc_PacingPulse, BatteryStatus _vc_BatteryStatus,
            EventMarkers _vc_EventMarkers, NoiseDetection _vc_NoiseDetection) : base ()
        {
            c_ProgrammableParameters = _vc_ProgrammableParameters;
            c_MeasuredParameters = _vc_MeasuredParameters;
            c_TimeSt = _vc_TimeSt;
            c_BO_MODE = _vc_BO_MODE;
            c_SensingPulse = _vc_SensingPulse;
            c_PacingPulse = _vc_PacingPulse;
            c_BatteryStatus = _vc_BatteryStatus;
            c_EventMarkers = _vc_EventMarkers;
            c_NoiseDetection = _vc_NoiseDetection;
        }
        public bool preSetVOO ()
        {
            return (((((((true == c_BO_MODE.isVOO ()) && (0 < c_ProgrammableParameters.c_URL.
                upper_rate_limit)) && (0 < c_ProgrammableParameters.c_LRL.lower_rate_limit)) && ((((
                BATT_STATUS_LEVEL.BOL == c_BatteryStatus.batt_status_level) || (BATT_STATUS_LEVEL.
                ERN == c_BatteryStatus.batt_status_level)) || (BATT_STATUS_LEVEL.ERT ==
                c_BatteryStatus.batt_status_level)) || (BATT_STATUS_LEVEL.ERP == c_BatteryStatus.
                batt_status_level))) && ((c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (
                c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time)) && (
                _nz_PulseWidth (c_ProgrammableParameters.c_LRL.lower_rate_limit) <= (c_TimeSt.time -
                c_PacingPulse.last_p_v_pulse))) && (c_EventMarkers.ventricular_marker.empty () || (
                c_EventMarkers.ventricular_marker._oIndex ((c_EventMarkers.ventricular_marker._oHash
                () - 1)).m < c_TimeSt.time)));
        }
        public void _nz_SetVOO ()
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(preSetVOO ())) throw new _xPre ("Main.pd:77,9");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            PacingPulse _vUnshare_78_11 = ((PacingPulse) c_PacingPulse.Clone ());
            c_PacingPulse = _vUnshare_78_11;
            _vUnshare_78_11.chg_paced_ventricular (c_ProgrammableParameters.c_VPulseAmpRegulated.
                v_pulse_amp_regulated.y, c_ProgrammableParameters.c_VPulseWidth.v_pulse_width,
                c_TimeSt.time);
            EventMarkers _vUnshare_81_10 = ((EventMarkers) c_EventMarkers.Clone ());
            c_EventMarkers = _vUnshare_81_10;
            _vUnshare_81_10.chg_ventricular_marker (MarkerABB.VP, c_ProgrammableParameters.
                c_VPulseAmpRegulated.v_pulse_amp_regulated.y, c_ProgrammableParameters.c_VPulseWidth
                .v_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
        }
        public bool preSetAOO ()
        {
            return (((((((true == c_BO_MODE.isAOO ()) && (0 < c_ProgrammableParameters.c_URL.
                upper_rate_limit)) && (0 < c_ProgrammableParameters.c_LRL.lower_rate_limit)) && ((((
                BATT_STATUS_LEVEL.BOL == c_BatteryStatus.batt_status_level) || (BATT_STATUS_LEVEL.
                ERN == c_BatteryStatus.batt_status_level)) || (BATT_STATUS_LEVEL.ERT ==
                c_BatteryStatus.batt_status_level)) || (BATT_STATUS_LEVEL.ERP == c_BatteryStatus.
                batt_status_level))) && ((c_PacingPulse.last_p_a_pulse + _nz_PulseWidth (
                c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time)) && (
                _nz_PulseWidth (c_ProgrammableParameters.c_LRL.lower_rate_limit) <= (c_TimeSt.time -
                c_PacingPulse.last_p_a_pulse))) && (c_EventMarkers.atrial_marker.empty () || (
                c_EventMarkers.atrial_marker._oIndex ((c_EventMarkers.atrial_marker._oHash () - 1)).
                m < c_TimeSt.time)));
        }
        public void _nz_SetAOO ()
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(preSetAOO ())) throw new _xPre ("Main.pd:97,9");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            PacingPulse _vUnshare_98_11 = ((PacingPulse) c_PacingPulse.Clone ());
            c_PacingPulse = _vUnshare_98_11;
            _vUnshare_98_11.chg_paced_atrial (c_ProgrammableParameters.c_APulseAmpRegulated.
                a_pulse_amp_regulated.y, c_ProgrammableParameters.c_APulseWidth.a_pulse_width,
                c_TimeSt.time);
            EventMarkers _vUnshare_100_14 = ((EventMarkers) c_EventMarkers.Clone ());
            c_EventMarkers = _vUnshare_100_14;
            _vUnshare_100_14.chg_atrial_marker (MarkerABB.AP, c_ProgrammableParameters.
                c_APulseAmpRegulated.a_pulse_amp_regulated.y, c_ProgrammableParameters.c_APulseWidth
                .a_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
        }
        public bool preSetDOO ()
        {
            return (((((((true == c_BO_MODE.isDOO ()) && (0 < c_ProgrammableParameters.c_URL.
                upper_rate_limit)) && (0 < c_ProgrammableParameters.c_LRL.lower_rate_limit)) && ((
                BATT_STATUS_LEVEL.BOL == c_BatteryStatus.batt_status_level) || (BATT_STATUS_LEVEL.
                ERN == c_BatteryStatus.batt_status_level))) && (((c_PacingPulse.last_p_a_pulse +
                _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time)
                && (_nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit) <= (c_TimeSt.
                time - c_PacingPulse.last_p_a_pulse)))) && (c_EventMarkers.ventricular_marker.empty
                () || (c_EventMarkers.ventricular_marker._oIndex ((c_EventMarkers.ventricular_marker
                ._oHash () - 1)).m < c_TimeSt.time))) && (c_EventMarkers.atrial_marker.empty () || (
                c_EventMarkers.atrial_marker._oIndex ((c_EventMarkers.atrial_marker._oHash () - 1)).
                m < c_TimeSt.time)));
        }
        public void _nz_SetDOO ()
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(preSetDOO ())) throw new _xPre ("Main.pd:117,9");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            PacingPulse _vUnshare_118_11 = ((PacingPulse) c_PacingPulse.Clone ());
            c_PacingPulse = _vUnshare_118_11;
            _vUnshare_118_11.chg_paced_dual (c_ProgrammableParameters.c_APulseWidth.a_pulse_width,
                c_ProgrammableParameters.c_APulseAmpRegulated.a_pulse_amp_regulated.y,
                c_ProgrammableParameters.c_VPulseWidth.v_pulse_width, c_ProgrammableParameters.
                c_VPulseAmpRegulated.v_pulse_amp_regulated.y, c_TimeSt.time);
            EventMarkers _vUnshare_123_10 = ((EventMarkers) c_EventMarkers.Clone ());
            c_EventMarkers = _vUnshare_123_10;
            _vUnshare_123_10.chg_dual_marker (MarkerABB.AP, c_ProgrammableParameters.
                c_APulseAmpRegulated.a_pulse_amp_regulated.y, c_ProgrammableParameters.c_APulseWidth
                .a_pulse_width, MarkerABB.VP, c_ProgrammableParameters.c_VPulseAmpRegulated.
                v_pulse_amp_regulated.y, c_ProgrammableParameters.c_VPulseWidth.v_pulse_width,
                c_NoiseDetection.noise, c_TimeSt.time);
        }
        public bool preSetDDI ()
        {
            return (((((true == c_BO_MODE.isDDI ()) && ((BATT_STATUS_LEVEL.BOL == c_BatteryStatus.
                batt_status_level) || (BATT_STATUS_LEVEL.ERN == c_BatteryStatus.batt_status_level)))
                && (((((((_nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit) -
                c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) + c_PacingPulse.
                last_p_v_pulse) == (c_TimeSt.time - c_PacingPulse.last_p_v_pulse)) || (((
                c_PacingPulse.last_p_v_pulse + c_ProgrammableParameters.c_PVARP.pvarp) <
                c_SensingPulse.last_s_a_pulse) && (c_SensingPulse.last_s_a_pulse < ((_nz_PulseWidth
                (c_ProgrammableParameters.c_URL.upper_rate_limit) - c_ProgrammableParameters.
                c_FixedAVDelay.fixed_av_delay) + c_PacingPulse.last_p_v_pulse)))) || ((((
                c_PacingPulse.last_p_v_pulse + c_ProgrammableParameters.c_FixedAVDelay.
                fixed_av_delay) == c_PacingPulse.last_p_a_pulse) && (c_PacingPulse.last_p_v_pulse <
                c_SensingPulse.last_s_v_pulse)) && (c_SensingPulse.last_s_v_pulse < (c_PacingPulse.
                last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)))))
                || (((((_nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit) -
                c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) + c_PacingPulse.
                last_p_v_pulse) == c_PacingPulse.last_p_a_pulse) && (c_SensingPulse.last_s_v_pulse <
                c_PacingPulse.last_p_v_pulse)) && ((c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (
                c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time))) || (((((((
                c_PacingPulse.last_p_v_pulse + c_ProgrammableParameters.c_PVARP.pvarp) <
                c_SensingPulse.last_s_a_pulse) && (c_SensingPulse.last_s_a_pulse < ((_nz_PulseWidth
                (c_ProgrammableParameters.c_URL.upper_rate_limit) - c_ProgrammableParameters.
                c_FixedAVDelay.fixed_av_delay) + c_PacingPulse.last_p_v_pulse))) && (c_PacingPulse.
                last_p_v_pulse < c_SensingPulse.last_s_v_pulse)) && (c_SensingPulse.last_s_v_pulse <
                (c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.
                upper_rate_limit)))) && (0 < c_ProgrammableParameters.c_URL.upper_rate_limit)) && (0
                < c_ProgrammableParameters.c_LRL.lower_rate_limit)))) && (c_EventMarkers.
                ventricular_marker.empty () || (c_EventMarkers.ventricular_marker._oIndex ((
                c_EventMarkers.ventricular_marker._oHash () - 1)).m < c_TimeSt.time))) && (
                c_EventMarkers.atrial_marker.empty () || (c_EventMarkers.atrial_marker._oIndex ((
                c_EventMarkers.atrial_marker._oHash () - 1)).m < c_TimeSt.time)));
        }
        public void _nz_SetDDI ()
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(preSetDDI ())) throw new _xPre ("Main.pd:171,9");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((0 < c_ProgrammableParameters.c_URL.upper_rate_limit))) throw new _xPre (
                        "Main.pd:171,69");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((0 < c_ProgrammableParameters.c_LRL.lower_rate_limit))) throw new _xPre (
                        "Main.pd:171,124");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            if ((((_nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit) -
                c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) + c_PacingPulse.
                last_p_v_pulse) == (c_TimeSt.time - c_PacingPulse.last_p_v_pulse)))
            {
                PacingPulse _vUnshare_176_14 = ((PacingPulse) c_PacingPulse.Clone ());
                c_PacingPulse = _vUnshare_176_14;
                _vUnshare_176_14.chg_paced_atrial (c_ProgrammableParameters.c_APulseWidth.
                    a_pulse_width, c_ProgrammableParameters.c_APulseAmpRegulated.
                    a_pulse_amp_regulated.y, c_TimeSt.time);
                EventMarkers _vUnshare_179_17 = ((EventMarkers) c_EventMarkers.Clone ());
                c_EventMarkers = _vUnshare_179_17;
                _vUnshare_179_17.chg_atrial_marker (MarkerABB.AP, c_ProgrammableParameters.
                    c_APulseAmpRegulated.a_pulse_amp_regulated.y, c_ProgrammableParameters.
                    c_APulseWidth.a_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
            }
            else if (((((c_PacingPulse.last_p_v_pulse + c_ProgrammableParameters.c_PVARP.pvarp) <
                c_SensingPulse.last_s_a_pulse) && (c_SensingPulse.last_s_a_pulse < ((_nz_PulseWidth
                (c_ProgrammableParameters.c_URL.upper_rate_limit) - c_ProgrammableParameters.
                c_FixedAVDelay.fixed_av_delay) + c_PacingPulse.last_p_v_pulse))) && (0 <
                c_ProgrammableParameters.c_URL.upper_rate_limit)))
            {
                EventMarkers _vUnshare_191_17 = ((EventMarkers) c_EventMarkers.Clone ());
                c_EventMarkers = _vUnshare_191_17;
                _vUnshare_191_17.unchg_atrial_marker ();
            }
            else if ((((((c_PacingPulse.last_p_v_pulse + c_ProgrammableParameters.c_FixedAVDelay.
                fixed_av_delay) == c_PacingPulse.last_p_a_pulse) && (c_PacingPulse.last_p_v_pulse <
                c_SensingPulse.last_s_v_pulse)) && (c_SensingPulse.last_s_v_pulse < (c_PacingPulse.
                last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit))))
                && (0 < c_ProgrammableParameters.c_URL.upper_rate_limit)))
            {
                EventMarkers _vUnshare_199_17 = ((EventMarkers) c_EventMarkers.Clone ());
                c_EventMarkers = _vUnshare_199_17;
                _vUnshare_199_17.unchg_ventricular_marker ();
            }
            else if ((((((((c_PacingPulse.last_p_v_pulse + c_ProgrammableParameters.c_PVARP.pvarp) <
                c_SensingPulse.last_s_a_pulse) && (c_SensingPulse.last_s_a_pulse < ((_nz_PulseWidth
                (c_ProgrammableParameters.c_URL.upper_rate_limit) - c_ProgrammableParameters.
                c_FixedAVDelay.fixed_av_delay) + c_PacingPulse.last_p_v_pulse))) && (c_PacingPulse.
                last_p_v_pulse < c_SensingPulse.last_s_v_pulse)) && (0 < c_ProgrammableParameters.
                c_URL.upper_rate_limit)) && (0 < c_ProgrammableParameters.c_LRL.lower_rate_limit))
                && (c_SensingPulse.last_s_v_pulse < (c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (
                c_ProgrammableParameters.c_URL.upper_rate_limit)))))
            {
                EventMarkers _vUnshare_211_17 = ((EventMarkers) c_EventMarkers.Clone ());
                c_EventMarkers = _vUnshare_211_17;
                _vUnshare_211_17.unchg_ventricular_marker ();
            }
            else if (((((((_nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit) -
                c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) + c_PacingPulse.
                last_p_v_pulse) == c_PacingPulse.last_p_a_pulse) && (c_SensingPulse.last_s_v_pulse <
                c_PacingPulse.last_p_v_pulse)) && ((c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (
                c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time)) && (0 <
                c_ProgrammableParameters.c_URL.upper_rate_limit)))
            {
                PacingPulse _vUnshare_218_18 = ((PacingPulse) c_PacingPulse.Clone ());
                c_PacingPulse = _vUnshare_218_18;
                _vUnshare_218_18.chg_paced_ventricular (c_ProgrammableParameters.c_VPulseWidth.
                    v_pulse_width, c_ProgrammableParameters.c_VPulseAmpRegulated.
                    v_pulse_amp_regulated.y, c_TimeSt.time);
                EventMarkers _vUnshare_221_18 = ((EventMarkers) c_EventMarkers.Clone ());
                c_EventMarkers = _vUnshare_221_18;
                _vUnshare_221_18.chg_ventricular_marker (MarkerABB.VP, c_ProgrammableParameters.
                    c_VPulseAmpRegulated.v_pulse_amp_regulated.y, c_ProgrammableParameters.
                    c_VPulseWidth.v_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
            }
            else
            {
            }
        }
        public bool preSetVDD ()
        {
            return ((((((true == c_BO_MODE.isVDD ()) && (0 < c_ProgrammableParameters.c_URL.
                upper_rate_limit)) && ((BATT_STATUS_LEVEL.BOL == c_BatteryStatus.batt_status_level)
                || (BATT_STATUS_LEVEL.ERN == c_BatteryStatus.batt_status_level))) && ((((((((
                c_SensingPulse.last_s_a_pulse < c_PacingPulse.last_p_v_pulse) && ((c_SensingPulse.
                last_s_a_pulse + c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) == c_TimeSt
                .time)) && (c_SensingPulse.last_s_v_pulse < c_TimeSt.time)) || (((c_SensingPulse.
                last_s_a_pulse < c_SensingPulse.last_s_v_pulse) && ((c_SensingPulse.last_s_a_pulse +
                c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) == c_TimeSt.time)) && (
                c_SensingPulse.last_s_v_pulse < c_TimeSt.time))) || ((c_SensingPulse.last_s_v_pulse
                < c_SensingPulse.last_s_a_pulse) && ((c_PacingPulse.last_p_v_pulse + _nz_PulseWidth
                (c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time))) || ((
                c_PacingPulse.last_p_v_pulse < c_SensingPulse.last_s_a_pulse) && ((c_PacingPulse.
                last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit))
                == c_TimeSt.time))) || (((c_SensingPulse.last_s_a_pulse < c_PacingPulse.
                last_p_v_pulse) && ((c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (
                c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time)) && (c_TimeSt.
                time < (c_PacingPulse.last_p_v_pulse + c_ProgrammableParameters.c_FixedAVDelay.
                fixed_av_delay)))) || (((c_SensingPulse.last_s_a_pulse < c_SensingPulse.
                last_s_v_pulse) && ((c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (
                c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time)) && (c_TimeSt.
                time < (c_PacingPulse.last_p_v_pulse + c_ProgrammableParameters.c_FixedAVDelay.
                fixed_av_delay))))) && (c_EventMarkers.ventricular_marker.empty () || (
                c_EventMarkers.ventricular_marker._oIndex ((c_EventMarkers.ventricular_marker._oHash
                () - 1)).m < c_TimeSt.time))) && (c_EventMarkers.atrial_marker.empty () || (
                c_EventMarkers.atrial_marker._oIndex ((c_EventMarkers.atrial_marker._oHash () - 1)).
                m < c_TimeSt.time)));
        }
        public void _nz_SetVDD ()
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(preSetVDD ())) throw new _xPre ("Main.pd:270,9");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            PacingPulse _vUnshare_271_11 = ((PacingPulse) c_PacingPulse.Clone ());
            c_PacingPulse = _vUnshare_271_11;
            _vUnshare_271_11.chg_paced_ventricular (c_ProgrammableParameters.c_VPulseWidth.
                v_pulse_width, c_ProgrammableParameters.c_VPulseAmpRegulated.v_pulse_amp_regulated.y,
                c_TimeSt.time);
            EventMarkers _vUnshare_274_9 = ((EventMarkers) c_EventMarkers.Clone ());
            c_EventMarkers = _vUnshare_274_9;
            _vUnshare_274_9.chg_ventricular_marker (MarkerABB.VP, c_ProgrammableParameters.
                c_VPulseAmpRegulated.v_pulse_amp_regulated.y, c_ProgrammableParameters.c_VPulseWidth
                .v_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
        }
        public bool preSetVVI ()
        {
            return ((((((true == c_BO_MODE.isVVI ()) && (0 < c_ProgrammableParameters.c_URL.
                upper_rate_limit)) && (0 < c_ProgrammableParameters.c_HysteresisRateLimit.
                hysteresis_rate_limit.y)) && ((BATT_STATUS_LEVEL.BOL == c_BatteryStatus.
                batt_status_level) || (BATT_STATUS_LEVEL.ERN == c_BatteryStatus.batt_status_level)))
                && ((((((c_TimeSt.time - c_PacingPulse.last_p_v_pulse) < (c_TimeSt.time -
                c_SensingPulse.last_s_v_pulse)) && ((c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (
                c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time)) && (
                c_ProgrammableParameters.c_VRefractoryPeriod.v_refract_period <= (c_PacingPulse.
                last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit))))
                && (_nz_PulseWidth (c_ProgrammableParameters.c_LRL.lower_rate_limit) <= (c_TimeSt.
                time - c_PacingPulse.last_p_v_pulse))) || (((((c_TimeSt.time - c_SensingPulse.
                last_s_v_pulse) < (c_TimeSt.time - c_PacingPulse.last_p_v_pulse)) && ((
                c_SensingPulse.last_s_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.
                c_HysteresisRateLimit.hysteresis_rate_limit.y)) == c_TimeSt.time)) && (
                c_ProgrammableParameters.c_VRefractoryPeriod.v_refract_period <= (c_SensingPulse.
                last_s_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_HysteresisRateLimit.
                hysteresis_rate_limit.y)))) && (c_ProgrammableParameters.c_URL.upper_rate_limit <= (
                c_TimeSt.time - c_SensingPulse.last_s_v_pulse))))) && (c_EventMarkers.
                ventricular_marker.empty () || (c_EventMarkers.ventricular_marker._oIndex ((
                c_EventMarkers.ventricular_marker._oHash () - 1)).m < c_TimeSt.time)));
        }
        public void _nz_SetVVI ()
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(preSetVVI ())) throw new _xPre ("Main.pd:307,9");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            PacingPulse _vUnshare_308_11 = ((PacingPulse) c_PacingPulse.Clone ());
            c_PacingPulse = _vUnshare_308_11;
            _vUnshare_308_11.chg_paced_ventricular (c_ProgrammableParameters.c_VPulseWidth.
                v_pulse_width, c_ProgrammableParameters.c_VPulseAmpRegulated.v_pulse_amp_regulated.y,
                c_TimeSt.time);
            EventMarkers _vUnshare_311_9 = ((EventMarkers) c_EventMarkers.Clone ());
            c_EventMarkers = _vUnshare_311_9;
            _vUnshare_311_9.chg_ventricular_marker (MarkerABB.VP, c_ProgrammableParameters.
                c_VPulseAmpRegulated.v_pulse_amp_regulated.y, c_ProgrammableParameters.c_VPulseWidth
                .v_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
        }
        public bool preSetAAI ()
        {
            return (((((((true == c_BO_MODE.isAAI ()) && (0 < c_ProgrammableParameters.c_URL.
                upper_rate_limit)) && (0 < c_ProgrammableParameters.c_HysteresisRateLimit.
                hysteresis_rate_limit.y)) && ((((BATT_STATUS_LEVEL.BOL == c_BatteryStatus.
                batt_status_level) || (BATT_STATUS_LEVEL.ERN == c_BatteryStatus.batt_status_level))
                || (BATT_STATUS_LEVEL.ERT == c_BatteryStatus.batt_status_level)) || (
                BATT_STATUS_LEVEL.ERP == c_BatteryStatus.batt_status_level))) && ((((((c_TimeSt.time
                - c_PacingPulse.last_p_a_pulse) < (c_TimeSt.time - c_SensingPulse.last_s_a_pulse))
                && ((c_PacingPulse.last_p_a_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.
                upper_rate_limit)) == c_TimeSt.time)) && (c_ProgrammableParameters.
                c_ARefractoryPeriod.a_refract_period <= (c_PacingPulse.last_p_a_pulse +
                _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)))) && (
                _nz_PulseWidth (c_ProgrammableParameters.c_LRL.lower_rate_limit) <= (c_TimeSt.time -
                c_PacingPulse.last_p_a_pulse))) || ((((c_TimeSt.time - c_SensingPulse.last_s_a_pulse)
                < (c_TimeSt.time - c_PacingPulse.last_p_a_pulse)) && ((c_SensingPulse.last_s_a_pulse
                + _nz_PulseWidth (c_ProgrammableParameters.c_HysteresisRateLimit.
                hysteresis_rate_limit.y)) == c_TimeSt.time)) && (c_ProgrammableParameters.
                c_ARefractoryPeriod.a_refract_period <= (c_PacingPulse.last_p_a_pulse +
                _nz_PulseWidth (c_ProgrammableParameters.c_HysteresisRateLimit.hysteresis_rate_limit
                .y)))))) && (SWITCH.ON == c_ProgrammableParameters.c_HysteresisRateLimit.
                hysteresis_rate_limit.x)) && (c_EventMarkers.atrial_marker.empty () || (
                c_EventMarkers.atrial_marker._oIndex ((c_EventMarkers.atrial_marker._oHash () - 1)).
                m < c_TimeSt.time)));
        }
        public void _nz_SetAAI ()
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(preSetAAI ())) throw new _xPre ("Main.pd:338,9");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            PacingPulse _vUnshare_339_11 = ((PacingPulse) c_PacingPulse.Clone ());
            c_PacingPulse = _vUnshare_339_11;
            _vUnshare_339_11.chg_paced_atrial (c_ProgrammableParameters.c_APulseWidth.a_pulse_width,
                c_ProgrammableParameters.c_APulseAmpRegulated.a_pulse_amp_regulated.y, c_TimeSt.time)
                ;
            EventMarkers _vUnshare_342_13 = ((EventMarkers) c_EventMarkers.Clone ());
            c_EventMarkers = _vUnshare_342_13;
            _vUnshare_342_13.chg_atrial_marker (MarkerABB.AP, c_ProgrammableParameters.
                c_APulseAmpRegulated.a_pulse_amp_regulated.y, c_ProgrammableParameters.c_APulseWidth
                .a_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
        }
        public bool preSetVVT ()
        {
            return (((((true == c_BO_MODE.isVVT ()) && (0 < c_ProgrammableParameters.c_URL.
                upper_rate_limit)) && ((((BATT_STATUS_LEVEL.BOL == c_BatteryStatus.batt_status_level)
                || (BATT_STATUS_LEVEL.ERN == c_BatteryStatus.batt_status_level)) || (
                BATT_STATUS_LEVEL.ERT == c_BatteryStatus.batt_status_level)) || (BATT_STATUS_LEVEL.
                ERP == c_BatteryStatus.batt_status_level))) && ((((((c_PacingPulse.last_p_v_pulse +
                _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time)
                && (c_ProgrammableParameters.c_VRefractoryPeriod.v_refract_period <= (c_PacingPulse.
                last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit))))
                && (_nz_PulseWidth (c_ProgrammableParameters.c_LRL.lower_rate_limit) <= (c_TimeSt.
                time - c_PacingPulse.last_p_v_pulse))) || (((c_TimeSt.time < (c_SensingPulse.
                last_s_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)))
                && (c_ProgrammableParameters.c_VRefractoryPeriod.v_refract_period <= (c_SensingPulse
                .last_s_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit))))
                && (_nz_PulseWidth (c_ProgrammableParameters.c_LRL.lower_rate_limit) <= (
                c_SensingPulse.last_s_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.
                upper_rate_limit))))) || ((c_TimeSt.time == c_SensingPulse.last_s_v_pulse) && (
                c_ProgrammableParameters.c_VRefractoryPeriod.v_refract_period <= (c_SensingPulse.
                last_s_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)))))
                ) && (c_EventMarkers.ventricular_marker.empty () || (c_EventMarkers.
                ventricular_marker._oIndex ((c_EventMarkers.ventricular_marker._oHash () - 1)).m <
                c_TimeSt.time)));
        }
        public void _nz_SetVVT ()
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(preSetVVT ())) throw new _xPre ("Main.pd:374,9");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            PacingPulse _vUnshare_375_11 = ((PacingPulse) c_PacingPulse.Clone ());
            c_PacingPulse = _vUnshare_375_11;
            _vUnshare_375_11.chg_paced_ventricular (c_ProgrammableParameters.c_VPulseAmpRegulated.
                v_pulse_amp_regulated.y, c_ProgrammableParameters.c_VPulseWidth.v_pulse_width,
                c_TimeSt.time);
            EventMarkers _vUnshare_378_14 = ((EventMarkers) c_EventMarkers.Clone ());
            c_EventMarkers = _vUnshare_378_14;
            _vUnshare_378_14.chg_ventricular_marker (MarkerABB.VP, c_ProgrammableParameters.
                c_VPulseAmpRegulated.v_pulse_amp_regulated.y, c_ProgrammableParameters.c_VPulseWidth
                .v_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
        }
        public bool preSetAAT ()
        {
            return ((((((true == c_BO_MODE.isAAT ()) && (0 < c_ProgrammableParameters.c_URL.
                upper_rate_limit)) && (0 < c_ProgrammableParameters.c_LRL.lower_rate_limit)) && ((((
                BATT_STATUS_LEVEL.BOL == c_BatteryStatus.batt_status_level) || (BATT_STATUS_LEVEL.
                ERN == c_BatteryStatus.batt_status_level)) || (BATT_STATUS_LEVEL.ERT ==
                c_BatteryStatus.batt_status_level)) || (BATT_STATUS_LEVEL.ERP == c_BatteryStatus.
                batt_status_level))) && ((((((c_PacingPulse.last_p_a_pulse + _nz_PulseWidth (
                c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time) && (
                c_ProgrammableParameters.c_ARefractoryPeriod.a_refract_period <= (c_PacingPulse.
                last_p_a_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit))))
                && (_nz_PulseWidth (c_ProgrammableParameters.c_LRL.lower_rate_limit) <= (
                c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.
                upper_rate_limit)))) || ((c_TimeSt.time < (c_SensingPulse.last_s_a_pulse +
                _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit))) && (
                c_ProgrammableParameters.c_ARefractoryPeriod.a_refract_period <= (c_SensingPulse.
                last_s_a_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)))))
                || ((c_TimeSt.time == c_SensingPulse.last_s_a_pulse) && (c_ProgrammableParameters.
                c_ARefractoryPeriod.a_refract_period <= (c_SensingPulse.last_s_a_pulse +
                _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)))))) && (
                c_EventMarkers.atrial_marker.empty () || (c_EventMarkers.atrial_marker._oIndex ((
                c_EventMarkers.atrial_marker._oHash () - 1)).m < c_TimeSt.time)));
        }
        public void _nz_SetAAT ()
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(preSetAAT ())) throw new _xPre ("Main.pd:408,9");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            PacingPulse _vUnshare_409_11 = ((PacingPulse) c_PacingPulse.Clone ());
            c_PacingPulse = _vUnshare_409_11;
            _vUnshare_409_11.chg_paced_atrial (c_ProgrammableParameters.c_APulseWidth.a_pulse_width,
                c_ProgrammableParameters.c_APulseAmpRegulated.a_pulse_amp_regulated.y, c_TimeSt.time)
                ;
            EventMarkers _vUnshare_412_9 = ((EventMarkers) c_EventMarkers.Clone ());
            c_EventMarkers = _vUnshare_412_9;
            _vUnshare_412_9.chg_atrial_marker (MarkerABB.AP, c_ProgrammableParameters.
                c_APulseAmpRegulated.a_pulse_amp_regulated.y, c_ProgrammableParameters.c_APulseWidth
                .a_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
        }
        public bool preAtriumStartTime ()
        {
            return (((CHAMBERS.C_DUAL == c_BO_MODE.bo_mode.chambers_sensed) || (CHAMBERS.C_ATRIUM ==
                c_BO_MODE.bo_mode.chambers_sensed)) && ((c_MeasuredParameters.c_PWave.p_wave <
                c_TimeSt.a_curr_measurement) && (0 == c_MeasuredParameters.c_PWave.p_wave)));
        }
        public void _nz_AtriumStartTime ()
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(preAtriumStartTime ())) throw new _xPre ("Main.pd:424,10");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            MeasuredParameters _vUnshare_425_11 = ((MeasuredParameters) c_MeasuredParameters.Clone ()
                );
            c_MeasuredParameters = _vUnshare_425_11;
            _vUnshare_425_11.set_p_wave (c_TimeSt.a_curr_measurement);
            TimeSt _vUnshare_426_11 = ((TimeSt) c_TimeSt.Clone ());
            c_TimeSt = _vUnshare_426_11;
            _vUnshare_426_11.chg_a_start_time (c_TimeSt.time);
        }
        public bool preAtriumMax ()
        {
            return (((CHAMBERS.C_DUAL == c_BO_MODE.bo_mode.chambers_sensed) || (CHAMBERS.C_ATRIUM ==
                c_BO_MODE.bo_mode.chambers_sensed)) && ((c_MeasuredParameters.c_PWave.p_wave <
                c_TimeSt.a_curr_measurement) && (0 < c_MeasuredParameters.c_PWave.p_wave)));
        }
        public void _nz_AtriumMax ()
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(preAtriumMax ())) throw new _xPre ("Main.pd:436,9");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            MeasuredParameters _vUnshare_437_11 = ((MeasuredParameters) c_MeasuredParameters.Clone ()
                );
            c_MeasuredParameters = _vUnshare_437_11;
            _vUnshare_437_11.set_p_wave (c_TimeSt.a_curr_measurement);
            TimeSt _vUnshare_438_11 = ((TimeSt) c_TimeSt.Clone ());
            c_TimeSt = _vUnshare_438_11;
            _vUnshare_438_11.chg_a_max (c_TimeSt.a_curr_measurement);
        }
        public bool preAtriumEndTime ()
        {
            return (((CHAMBERS.C_DUAL == c_BO_MODE.bo_mode.chambers_sensed) || (CHAMBERS.C_ATRIUM ==
                c_BO_MODE.bo_mode.chambers_sensed)) && ((0 == c_TimeSt.a_curr_measurement) && (0 <
                c_MeasuredParameters.c_PWave.p_wave)));
        }
        public void _nz_AtriumEndTime ()
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(preAtriumEndTime ())) throw new _xPre ("Main.pd:447,9");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            MeasuredParameters _vUnshare_448_11 = ((MeasuredParameters) c_MeasuredParameters.Clone ()
                );
            c_MeasuredParameters = _vUnshare_448_11;
            _vUnshare_448_11.set_p_wave (0);
            TimeSt _vUnshare_449_11 = ((TimeSt) c_TimeSt.Clone ());
            c_TimeSt = _vUnshare_449_11;
            _vUnshare_449_11.chg_a_delay (c_TimeSt.time);
        }
        public bool preAtrialMeasurement ()
        {
            return ((preAtriumStartTime () || preAtriumMax ()) || preAtriumEndTime ());
        }
        public void _nz_AtrialMeasurement ()
        {
            if (preAtriumStartTime ())
            {
                _nz_AtriumStartTime ();
            }
            else if (preAtriumMax ())
            {
                _nz_AtriumMax ();
            }
            else if (preAtriumEndTime ())
            {
                _nz_AtriumEndTime ();
            }
            else
            {
            }
        }
        public bool preVentricularStartTime ()
        {
            return (((CHAMBERS.C_DUAL == c_BO_MODE.bo_mode.chambers_sensed) || (CHAMBERS.C_VENTRICLE
                == c_BO_MODE.bo_mode.chambers_sensed)) && ((c_MeasuredParameters.c_RWave.r_wave <
                c_TimeSt.v_curr_measurement) && (0 == c_MeasuredParameters.c_RWave.r_wave)));
        }
        public void _nz_VentricularStartTime ()
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(preVentricularStartTime ())) throw new _xPre ("Main.pd:470,9");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            MeasuredParameters _vUnshare_471_11 = ((MeasuredParameters) c_MeasuredParameters.Clone ()
                );
            c_MeasuredParameters = _vUnshare_471_11;
            _vUnshare_471_11.set_r_wave (c_TimeSt.v_curr_measurement);
            TimeSt _vUnshare_472_11 = ((TimeSt) c_TimeSt.Clone ());
            c_TimeSt = _vUnshare_472_11;
            _vUnshare_472_11.chg_v_start_time (c_TimeSt.time);
        }
        public bool preVentricularMax ()
        {
            return (((CHAMBERS.C_DUAL == c_BO_MODE.bo_mode.chambers_sensed) || (CHAMBERS.C_VENTRICLE
                == c_BO_MODE.bo_mode.chambers_sensed)) && ((c_MeasuredParameters.c_RWave.r_wave <
                c_TimeSt.v_curr_measurement) && (0 < c_MeasuredParameters.c_RWave.r_wave)));
        }
        public void _nz_VentricularMax ()
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(preVentricularMax ())) throw new _xPre ("Main.pd:482,9");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            MeasuredParameters _vUnshare_483_11 = ((MeasuredParameters) c_MeasuredParameters.Clone ()
                );
            c_MeasuredParameters = _vUnshare_483_11;
            _vUnshare_483_11.set_r_wave (c_TimeSt.v_curr_measurement);
            TimeSt _vUnshare_484_11 = ((TimeSt) c_TimeSt.Clone ());
            c_TimeSt = _vUnshare_484_11;
            _vUnshare_484_11.chg_v_max (c_TimeSt.v_curr_measurement);
        }
        public bool preVentricularEndTime ()
        {
            return (((CHAMBERS.C_DUAL == c_BO_MODE.bo_mode.chambers_sensed) || (CHAMBERS.C_VENTRICLE
                == c_BO_MODE.bo_mode.chambers_sensed)) && ((0 == c_TimeSt.v_curr_measurement) && (0
                < c_MeasuredParameters.c_RWave.r_wave)));
        }
        public void _nz_VentricularEndTime ()
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(preVentricularEndTime ())) throw new _xPre ("Main.pd:493,9");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            MeasuredParameters _vUnshare_494_11 = ((MeasuredParameters) c_MeasuredParameters.Clone ()
                );
            c_MeasuredParameters = _vUnshare_494_11;
            _vUnshare_494_11.set_r_wave (0);
            TimeSt _vUnshare_495_11 = ((TimeSt) c_TimeSt.Clone ());
            c_TimeSt = _vUnshare_495_11;
            _vUnshare_495_11.chg_v_delay (c_TimeSt.time);
        }
        public bool preVentricularMeasurement ()
        {
            return ((preVentricularStartTime () || preVentricularMax ()) || preVentricularEndTime ())
                ;
        }
        public void _nz_VentricularMeasurement ()
        {
            if (preVentricularStartTime ())
            {
                _nz_VentricularStartTime ();
            }
            else if (preVentricularMax ())
            {
                _nz_VentricularMax ();
            }
            else if (preVentricularEndTime ())
            {
                _nz_VentricularEndTime ();
            }
            else
            {
            }
        }
        public bool preSensingModule ()
        {
            return (preAtrialMeasurement () || preVentricularMeasurement ());
        }
        public bool preAtrialSensedMarkerA ()
        {
            return (((1 < c_EventMarkers.atrial_marker._oHash ()) && (c_EventMarkers.atrial_marker.
                _oIndex ((c_EventMarkers.atrial_marker._oHash () - 1)).m < c_TimeSt.time)) && ((
                c_TimeSt.a_start_time - c_EventMarkers.atrial_marker._oIndex ((c_EventMarkers.
                atrial_marker._oHash () - 1)).m) < _nz_PulseWidth (c_ProgrammableParameters.c_URL.
                upper_rate_limit)));
        }
        public bool preAtrialSensedMarkerB ()
        {
            return (((1 < c_EventMarkers.atrial_marker._oHash ()) && (c_EventMarkers.atrial_marker.
                _oIndex ((c_EventMarkers.atrial_marker._oHash () - 1)).m < c_TimeSt.time)) && (
                _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit) < (c_TimeSt.
                a_start_time - c_EventMarkers.atrial_marker._oIndex ((c_EventMarkers.atrial_marker.
                _oHash () - 1)).m)));
        }
        public bool preAtrialSensedMarker ()
        {
            return (preAtrialSensedMarkerA () || preAtrialSensedMarkerB ());
        }
        public void _nz_AtrialSensedMarker ()
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(((CHAMBERS.C_ATRIUM == c_BO_MODE.bo_mode.chambers_sensed) || (CHAMBERS.
                        C_DUAL == c_BO_MODE.bo_mode.chambers_sensed)))) throw new _xPre (
                        "Main.pd:525,44");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            if (c_EventMarkers.atrial_marker.empty ())
            {
                EventMarkers _vUnshare_527_13 = ((EventMarkers) c_EventMarkers.Clone ());
                c_EventMarkers = _vUnshare_527_13;
                _vUnshare_527_13.chg_atrial_marker (MarkerABB.AS, (c_TimeSt.a_delay - c_TimeSt.
                    a_start_time), c_TimeSt.a_max, c_NoiseDetection.noise, c_TimeSt.time);
                SensingPulse _vUnshare_530_13 = ((SensingPulse) c_SensingPulse.Clone ());
                c_SensingPulse = _vUnshare_530_13;
                _vUnshare_530_13.chg_sensed_atrial ((c_TimeSt.a_delay - c_TimeSt.a_start_time),
                    c_TimeSt.a_max, c_TimeSt.time);
            }
            else if (preAtrialSensedMarkerA ())
            {
                EventMarkers _vUnshare_533_13 = ((EventMarkers) c_EventMarkers.Clone ());
                c_EventMarkers = _vUnshare_533_13;
                _vUnshare_533_13.chg_atrial_marker (MarkerABB.AS, (c_TimeSt.a_delay - c_TimeSt.
                    a_start_time), c_TimeSt.a_max, c_NoiseDetection.noise, c_TimeSt.time);
                SensingPulse _vUnshare_536_13 = ((SensingPulse) c_SensingPulse.Clone ());
                c_SensingPulse = _vUnshare_536_13;
                _vUnshare_536_13.chg_sensed_atrial ((c_TimeSt.a_delay - c_TimeSt.a_start_time),
                    c_TimeSt.a_max, c_TimeSt.time);
            }
            else if (preAtrialSensedMarkerB ())
            {
                EventMarkers _vUnshare_539_13 = ((EventMarkers) c_EventMarkers.Clone ());
                c_EventMarkers = _vUnshare_539_13;
                _vUnshare_539_13.chg_atrial_marker (MarkerABB.AT, (c_TimeSt.a_delay - c_TimeSt.
                    a_start_time), c_TimeSt.a_max, c_NoiseDetection.noise, c_TimeSt.time);
                SensingPulse _vUnshare_542_13 = ((SensingPulse) c_SensingPulse.Clone ());
                c_SensingPulse = _vUnshare_542_13;
                _vUnshare_542_13.chg_sensed_atrial ((c_TimeSt.a_delay - c_TimeSt.a_start_time),
                    c_TimeSt.a_max, c_TimeSt.time);
            }
            else
            {
            }
        }
        public bool preVentricularSensedMarker ()
        {
            return (((c_TimeSt.v_start_time <= c_TimeSt.v_delay) && (c_EventMarkers.
                ventricular_marker.empty () || (c_EventMarkers.ventricular_marker._oIndex ((
                c_EventMarkers.ventricular_marker._oHash () - 1)).m < c_TimeSt.v_start_time))) && ((
                CHAMBERS.C_VENTRICLE == c_BO_MODE.bo_mode.chambers_sensed) || (CHAMBERS.C_DUAL ==
                c_BO_MODE.bo_mode.chambers_sensed)));
        }
        public void _nz_VentricularSensedMarker ()
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(preVentricularSensedMarker ())) throw new _xPre ("Main.pd:552,13");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            if ((c_EventMarkers.ventricular_marker.empty () && c_EventMarkers.atrial_marker.empty ())
                )
            {
                SensingPulse _vUnshare_554_19 = ((SensingPulse) c_SensingPulse.Clone ());
                c_SensingPulse = _vUnshare_554_19;
                _vUnshare_554_19.chg_sensed_ventricular ((c_TimeSt.v_delay - c_TimeSt.v_start_time),
                    c_TimeSt.v_max, c_TimeSt.v_start_time, 0);
                EventMarkers _vUnshare_558_17 = ((EventMarkers) c_EventMarkers.Clone ());
                c_EventMarkers = _vUnshare_558_17;
                _vUnshare_558_17.chg_ventricular_marker (MarkerABB.VS, c_TimeSt.v_max, (c_TimeSt.
                    v_delay - c_TimeSt.v_start_time), c_NoiseDetection.noise, c_TimeSt.v_start_time);
            }
            else
            {
                bool _vCondResult_564_49;
                if (((1 < c_EventMarkers.ventricular_marker._oHash ()) && (!c_EventMarkers.
                    atrial_marker.empty ())))
                {
                    int _vCaptureCommonOperand_566_53 = c_EventMarkers.atrial_marker.last ().m;
                    _vCondResult_564_49 = ((c_EventMarkers.ventricular_marker.last ().m <
                        _vCaptureCommonOperand_566_53) && (_vCaptureCommonOperand_566_53 < c_TimeSt.
                        v_start_time));
                }
                else
                {
                    _vCondResult_564_49 = false;
                }
                if (_vCondResult_564_49)
                {
                    SensingPulse _vUnshare_568_19 = ((SensingPulse) c_SensingPulse.Clone ());
                    c_SensingPulse = _vUnshare_568_19;
                    _vUnshare_568_19.chg_sensed_ventricular ((c_TimeSt.v_delay - c_TimeSt.
                        v_start_time), c_TimeSt.v_max, c_TimeSt.v_start_time, (c_EventMarkers.
                        ventricular_marker.last ().m - c_EventMarkers.ventricular_marker.front ().
                        last ().m));
                    EventMarkers _vUnshare_573_17 = ((EventMarkers) c_EventMarkers.Clone ());
                    c_EventMarkers = _vUnshare_573_17;
                    _vUnshare_573_17.chg_ventricular_marker (MarkerABB.VS, c_TimeSt.v_max, (c_TimeSt
                        .v_delay - c_TimeSt.v_start_time), c_NoiseDetection.noise, c_TimeSt.
                        v_start_time);
                }
                else
                {
                    bool _vCondResult_580_49;
                    if ((((1 < c_EventMarkers.ventricular_marker._oHash ()) && (!c_EventMarkers.
                        ventricular_marker.empty ())) && (!c_EventMarkers.atrial_marker.empty ())))
                    {
                        int _vCaptureCommonOperand_582_53 = c_EventMarkers.atrial_marker.last ().m;
                        _vCondResult_580_49 = (!((c_EventMarkers.ventricular_marker.last ().m <
                            _vCaptureCommonOperand_582_53) && (_vCaptureCommonOperand_582_53 <
                            c_TimeSt.v_start_time)));
                    }
                    else
                    {
                        _vCondResult_580_49 = false;
                    }
                    if (_vCondResult_580_49)
                    {
                        SensingPulse _vUnshare_584_18 = ((SensingPulse) c_SensingPulse.Clone ());
                        c_SensingPulse = _vUnshare_584_18;
                        _vUnshare_584_18.chg_sensed_ventricular ((c_TimeSt.v_delay - c_TimeSt.
                            v_start_time), c_TimeSt.v_max, c_TimeSt.v_start_time, (c_EventMarkers.
                            ventricular_marker.last ().m - c_EventMarkers.ventricular_marker.front ()
                            .last ().m));
                        EventMarkers _vUnshare_589_17 = ((EventMarkers) c_EventMarkers.Clone ());
                        c_EventMarkers = _vUnshare_589_17;
                        _vUnshare_589_17.chg_ventricular_marker (MarkerABB.PVC, c_TimeSt.v_max, (
                            c_TimeSt.v_delay - c_TimeSt.v_start_time), c_NoiseDetection.noise,
                            c_TimeSt.v_start_time);
                    }
                    else
                    {
                    }
                }
            }
        }
        public void _nz_AugmentationMarker ()
        {
            if ((((!c_EventMarkers.atrial_marker.empty ()) && (!c_EventMarkers.atrial_marker.front ()
                .empty ())) && ((c_EventMarkers.atrial_marker.front ().last ().x == MarkerABB.AS) &&
                (c_EventMarkers.atrial_marker.last ().x == MarkerABB.AT))))
            {
                EventMarkers _vUnshare_602_19 = ((EventMarkers) c_EventMarkers.Clone ());
                c_EventMarkers = _vUnshare_602_19;
                _vUnshare_602_19.chg_augmentation_marker (MarkerABB.ATR_Dur, c_EventMarkers.
                    atrial_marker.last ().m);
            }
            else if ((((!c_EventMarkers.atrial_marker.empty ()) && (!c_EventMarkers.atrial_marker.
                front ().empty ())) && ((c_EventMarkers.atrial_marker.front ().last ().x ==
                MarkerABB.AT) && (c_EventMarkers.atrial_marker.last ().x == MarkerABB.AT))))
            {
                EventMarkers _vUnshare_608_19 = ((EventMarkers) c_EventMarkers.Clone ());
                c_EventMarkers = _vUnshare_608_19;
                _vUnshare_608_19.chg_augmentation_marker (MarkerABB.ATR_FB, c_EventMarkers.
                    atrial_marker.last ().m);
            }
            else if ((((!c_EventMarkers.atrial_marker.empty ()) && (!c_EventMarkers.atrial_marker.
                front ().empty ())) && ((c_EventMarkers.atrial_marker.front ().last ().x ==
                MarkerABB.AT) && (c_EventMarkers.atrial_marker.last ().x == MarkerABB.AS))))
            {
                EventMarkers _vUnshare_614_19 = ((EventMarkers) c_EventMarkers.Clone ());
                c_EventMarkers = _vUnshare_614_19;
                _vUnshare_614_19.chg_augmentation_marker (MarkerABB.ATR_End, c_EventMarkers.
                    atrial_marker.last ().m);
            }
            else if ((((!c_EventMarkers.ventricular_marker.empty ()) && (!c_EventMarkers.
                ventricular_marker.front ().empty ())) && ((c_EventMarkers.ventricular_marker.front
                ().last ().x == MarkerABB.VS) && (c_EventMarkers.ventricular_marker.last ().x ==
                MarkerABB.PVC))))
            {
                EventMarkers _vUnshare_620_19 = ((EventMarkers) c_EventMarkers.Clone ());
                c_EventMarkers = _vUnshare_620_19;
                _vUnshare_620_19.chg_augmentation_marker (MarkerABB.PVPext, c_EventMarkers.
                    ventricular_marker.last ().m);
            }
            else
            {
            }
        }
        public void _nz_SetMode ()
        {
            if (preSetVOO ())
            {
                _nz_SetVOO ();
            }
            else if (preSetAOO ())
            {
                _nz_SetAOO ();
            }
            else if (preSetDOO ())
            {
                _nz_SetDOO ();
            }
            else if (preSetDDI ())
            {
                _nz_SetDDI ();
            }
            else if (preSetVDD ())
            {
                _nz_SetVDD ();
            }
            else if (preSetVVI ())
            {
                _nz_SetVVI ();
            }
            else if (preSetAAI ())
            {
                _nz_SetAAI ();
            }
            else if (preSetVVT ())
            {
                _nz_SetVVT ();
            }
            else if (preSetAAT ())
            {
                _nz_SetAAT ();
            }
            else
            {
            }
        }
        public bool preSensingMarkers ()
        {
            return (preAtrialSensedMarker () || preVentricularSensedMarker ());
        }
        public void _nz_SensingMarkers ()
        {
            if (preAtrialSensedMarker ())
            {
                _nz_AtrialSensedMarker ();
            }
            else if (preVentricularSensedMarker ())
            {
                _nz_VentricularSensedMarker ();
            }
            else if (true)
            {
                _nz_AugmentationMarker ();
            }
            else
            {
            }
        }
        public void _nz_SensingModule ()
        {
            if (preVentricularMeasurement ())
            {
                _nz_VentricularMeasurement ();
            }
            else if (preAtrialMeasurement ())
            {
                _nz_AtrialMeasurement ();
            }
            else
            {
            }
        }
        public void _nz_SetTimer ()
        {
            TimeSt _vUnshare_652_11 = ((TimeSt) c_TimeSt.Clone ());
            c_TimeSt = _vUnshare_652_11;
            _vUnshare_652_11.chg_time ((1 + c_TimeSt.time));
        }
        public void _nz_BradyTherapy ()
        {
            _nz_SetTimer ();
            _nz_SensingModule ();
            _nz_SensingMarkers ();
            _nz_SetMode ();
        }
        public void _nz_GetPWave (int device_p_wave)
        {
            MeasuredParameters _vUnshare_663_10 = ((MeasuredParameters) c_MeasuredParameters.Clone ()
                );
            c_MeasuredParameters = _vUnshare_663_10;
            _vUnshare_663_10.set_p_wave (device_p_wave);
        }
        public void _nz_GetRWave (int device_r_wave)
        {
            MeasuredParameters _vUnshare_666_10 = ((MeasuredParameters) c_MeasuredParameters.Clone ()
                );
            c_MeasuredParameters = _vUnshare_666_10;
            _vUnshare_666_10.set_r_wave (device_r_wave);
        }
        public void _nz_GetBatteryVoltage (int device_battery_voltage)
        {
            MeasuredParameters _vUnshare_669_10 = ((MeasuredParameters) c_MeasuredParameters.Clone ()
                );
            c_MeasuredParameters = _vUnshare_669_10;
            _vUnshare_669_10.set_battery_voltage (device_battery_voltage);
        }
        public void _nz_GetLeadImpedance (int device_lead_impedance)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (100, 2500)._ovIn (device_lead_impedance))) throw new
                        _xPre ("Main.pd:672,31");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            MeasuredParameters _vUnshare_673_10 = ((MeasuredParameters) c_MeasuredParameters.Clone ()
                );
            c_MeasuredParameters = _vUnshare_673_10;
            _vUnshare_673_10.set_lead_impedance (device_lead_impedance);
        }
        public void _nz_SetBradycardiaOperationMode (BOM bradycardia_op_mode)
        {
            BO_MODE _vUnshare_676_11 = ((BO_MODE) c_BO_MODE.Clone ());
            c_BO_MODE = _vUnshare_676_11;
            _vUnshare_676_11.chg_bo_mode (bradycardia_op_mode);
        }
        public bool _lEqual (PulseGenerator _vArg_9_9)
        {
            if (this == _vArg_9_9)
            {
                return true;
            }
            return ((((((((_vArg_9_9.c_ProgrammableParameters._lEqual (c_ProgrammableParameters) &&
                _vArg_9_9.c_MeasuredParameters._lEqual (c_MeasuredParameters)) && _vArg_9_9.c_TimeSt
                ._lEqual (c_TimeSt)) && _vArg_9_9.c_SensingPulse._lEqual (c_SensingPulse)) &&
                _vArg_9_9.c_PacingPulse._lEqual (c_PacingPulse)) && _vArg_9_9.c_BatteryStatus.
                _lEqual (c_BatteryStatus)) && _vArg_9_9.c_EventMarkers._lEqual (c_EventMarkers)) &&
                _vArg_9_9.c_NoiseDetection._lEqual (c_NoiseDetection)) && _vArg_9_9.c_BO_MODE.
                _lEqual (c_BO_MODE));
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (PulseGenerator) &&
                _lEqual ((PulseGenerator) _lArg));
        }
    }

}


// End of file.
