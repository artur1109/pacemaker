//***********************************************************************************************
//* File: 'Classes_0.hpp'
//* THIS IS A GENERATED FILE: DO NOT EDIT. Please edit the Perfect Developer source file instead!
//*
//* Generated from: 'C:\Users\user\Dropbox\Academico\Pesquisa\pacemaker-project\pacemaker-pd\Classes.pd'
//* by Perfect Developer version 6.00.00.03 at 13:45:28 UTC on Thursday April 10th 2014
//* Using command line options:
//* -z1 -el=3 -em=100 -gl=EmbeddedC++ -gs=1 -gv=ISO -gw=100 -gdp=1 -gdo=0 -gdc=3 -gda=1 -gdA=0 -gdl=0 -gdr=0 -gdt=0 -gdi=1 -st=4 -sb=C:\Program Files\Escher Technologies\Verification Studio 6\Bin\builtin.pd -sr=C:\Program Files\Escher Technologies\Verification Studio 6\Bin\rubric.pd -q=0 -gk=Pacemaker -eM=0 -@=C:\Users\user\AppData\Local\Temp\etf402C.tmp
//***********************************************************************************************


#if !defined(_hForward_Classes)
#define _hForward_Classes


// Forward declarations


template < class X, class Y, class Z, class K, class M > class _n5_quin;
namespace ACTIVITY_THRESHOLD
{
    enum _eEnum
    {
        VERY_LOW, LOW, MED_LOW, MED, MED_HIGH, HIGH, VERY_HIGH
    };
    extern const _eNativeChar * _lnaming [7];
}

_mEnumStorableSupportHdr (ACTIVITY_THRESHOLD)
namespace BATT_STATUS_LEVEL
{
    enum _eEnum
    {
        BOL, ERN, ERT, ERP
    };
    extern const _eNativeChar * _lnaming [4];
}

_mEnumStorableSupportHdr (BATT_STATUS_LEVEL)
namespace BRADYCARDIA_STATE
{
    enum _eEnum
    {
        PERMANENT, TEMPORARY, PACE_NOW
    };
    extern const _eNativeChar * _lnaming [3];
}

_mEnumStorableSupportHdr (BRADYCARDIA_STATE)
namespace LEAD_CHAMBER_TYPE
{
    enum _eEnum
    {
        L_ATRIAL, L_VENTRICULAR
    };
    extern const _eNativeChar * _lnaming [2];
}

_mEnumStorableSupportHdr (LEAD_CHAMBER_TYPE)
namespace POLARITY
{
    enum _eEnum
    {
        UNIPOLAR, BIPOLAR
    };
    extern const _eNativeChar * _lnaming [2];
}

_mEnumStorableSupportHdr (POLARITY)
namespace MarkerABB
{
    enum _eEnum
    {
        AS, AP, AT, VS, VP, PVC, TN, SRef, HyPc, SRPc, Sm_Up, Sm_Down, ATR_Dur, ATR_FB, ATR_End,
            PVPext
    };
    extern const _eNativeChar * _lnaming [16];
}

_mEnumStorableSupportHdr (MarkerABB)
namespace SWITCH
{
    enum _eEnum
    {
        ON, OFF
    };
    extern const _eNativeChar * _lnaming [2];
}

_mEnumStorableSupportHdr (SWITCH)
namespace CHAMBERS
{
    enum _eEnum
    {
        C_NONE, C_DUAL, C_ATRIUM, C_VENTRICLE
    };
    extern const _eNativeChar * _lnaming [4];
}

_mEnumStorableSupportHdr (CHAMBERS)
namespace RESPONSE
{
    enum _eEnum
    {
        R_NONE, TRIGGERED, INHIBITED, TRACKED
    };
    extern const _eNativeChar * _lnaming [4];
}

_mEnumStorableSupportHdr (RESPONSE)
class BOM;
class TimeSt;
class SensingPulse;
class PacingPulse;
class BatteryStatus;
class NoiseDetection;
class EventMarkers;
class Accelerometer;
class PWave;
class RWave;
class BatteryVoltage;
class LeadImpedance;
class MeasuredParameters;
class BO_MODE;
class LRL;
class URL;
class MaxSensorRate;
class FixedAVDelay;
class DynamicAVDelay;
class MinDynamicAVDelay;
class SensedAVDelayOffset;
class APulseAmpRegulated;
class VPulseAmpRegulated;
class APulseAmpUnregulated;
class VPulseAmpUnregulated;
class APulseWidth;
class VPulseWidth;
class ASensitivity;
class VSensitivity;
class VRefractoryPeriod;
class ARefractoryPeriod;
class PVARP;
class PVARP_Ext;
class HysteresisRateLimit;
class RateSmoothing;
class ATRMode;
class ATRDuration;
class ATRFallbackTime;
class VBlanking;
class ActivityThreshold;
class ReactTime;
class RespFactor;
class RecoveryTime;
class ProgrammableParameters;
class HistogramsSt;
class BradycardiaSt;
class AtrialBipolarLead;
class VentricularBipolarLead;
class LeadsSt;

#endif

// End of file.
