//***********************************************************************************************
//* File: 'Classes_1.hpp'
//* THIS IS A GENERATED FILE: DO NOT EDIT. Please edit the Perfect Developer source file instead!
//*
//* Generated from: 'C:\Users\user\Dropbox\Academico\Pesquisa\pacemaker-project\pacemaker-pd\Classes.pd'
//* by Perfect Developer version 6.00.00.03 at 13:45:28 UTC on Thursday April 10th 2014
//* Using command line options:
//* -z1 -el=3 -em=100 -gl=EmbeddedC++ -gs=1 -gv=ISO -gw=100 -gdp=1 -gdo=0 -gdc=3 -gda=1 -gdA=0 -gdl=0 -gdr=0 -gdt=0 -gdi=1 -st=4 -sb=C:\Program Files\Escher Technologies\Verification Studio 6\Bin\builtin.pd -sr=C:\Program Files\Escher Technologies\Verification Studio 6\Bin\rubric.pd -q=0 -gk=Pacemaker -eM=0 -@=C:\Users\user\AppData\Local\Temp\etf402C.tmp
//***********************************************************************************************


#if !defined(_hMain_Classes)
#define _hMain_Classes

// Full declarations


namespace Pacemaker
{
    extern const _eMDH * _agetObjLoaderNode_Classes ();
    template < class X, class Y, class Z, class K, class M > class _n5_quin : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (_n5_quin < X _mComma Y _mComma Z _mComma K _mComma M >)
        _mPerfectTypeInfoFwd
    public:
        X x;
    public:
        Y y;
    public:
        Z z;
    public:
        K k;
    public:
        M m;
    public:
        _eRank :: _eEnum _oRank (const _n5_quin < X _mComma Y _mComma Z _mComma K _mComma M > &)
            const;
    public:
        _eSeq < _eChar > toString () const;
    public:
        _n5_quin (const X, const Y, const Z, const K, const M);
    public:
        _eBool operator == (const _n5_quin < X _mComma Y _mComma Z _mComma K _mComma M > &) const;
    public:
        _eBool operator < (const _n5_quin < X _mComma Y _mComma Z _mComma K _mComma M > &) const;
    public:
        _eBool operator <= (const _n5_quin < X _mComma Y _mComma Z _mComma K _mComma M > &) const;
        _n5_quin ();
    };
    template < class X, class Y, class Z, class K, class M > _mRank (_n5_quin < X _mComma Y _mComma
        Z _mComma K _mComma M >)
    namespace ACTIVITY_THRESHOLD
    {
        _mEnumMembers _mEnumWrapper
    }
    _mEnumGlobalSupport (ACTIVITY_THRESHOLD)
    _mUserTypeInform (ACTIVITY_THRESHOLD :: _eEnum, _agetObjLoaderNode_Classes (), 1)
    namespace BATT_STATUS_LEVEL
    {
        _mEnumMembers _mEnumWrapper
    }
    _mEnumGlobalSupport (BATT_STATUS_LEVEL)
    _mUserTypeInform (BATT_STATUS_LEVEL :: _eEnum, _agetObjLoaderNode_Classes (), 2)
    namespace BRADYCARDIA_STATE
    {
        _mEnumMembers _mEnumWrapper
    }
    _mEnumGlobalSupport (BRADYCARDIA_STATE)
    _mUserTypeInform (BRADYCARDIA_STATE :: _eEnum, _agetObjLoaderNode_Classes (), 3)
    namespace LEAD_CHAMBER_TYPE
    {
        _mEnumMembers _mEnumWrapper
    }
    _mEnumGlobalSupport (LEAD_CHAMBER_TYPE)
    _mUserTypeInform (LEAD_CHAMBER_TYPE :: _eEnum, _agetObjLoaderNode_Classes (), 4)
    namespace POLARITY
    {
        _mEnumMembers _mEnumWrapper
    }
    _mEnumGlobalSupport (POLARITY)
    _mUserTypeInform (POLARITY :: _eEnum, _agetObjLoaderNode_Classes (), 5)
    namespace MarkerABB
    {
        _mEnumMembers _mEnumWrapper
    }
    _mEnumGlobalSupport (MarkerABB)
    _mUserTypeInform (MarkerABB :: _eEnum, _agetObjLoaderNode_Classes (), 6)
    namespace SWITCH
    {
        _mEnumMembers _mEnumWrapper
    }
    _mEnumGlobalSupport (SWITCH)
    _mUserTypeInform (SWITCH :: _eEnum, _agetObjLoaderNode_Classes (), 7)
    namespace CHAMBERS
    {
        _mEnumMembers _mEnumWrapper
    }
    _mEnumGlobalSupport (CHAMBERS)
    _mUserTypeInform (CHAMBERS :: _eEnum, _agetObjLoaderNode_Classes (), 8)
    namespace RESPONSE
    {
        _mEnumMembers _mEnumWrapper
    }
    _mEnumGlobalSupport (RESPONSE)
    _mUserTypeInform (RESPONSE :: _eEnum, _agetObjLoaderNode_Classes (), 9)
    class BOM : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (BOM)
        _mPerfectTypeInfoFwd
    public:
        SWITCH :: _eEnum _rswitch;
    public:
        CHAMBERS :: _eEnum chambers_paced;
    public:
        CHAMBERS :: _eEnum chambers_sensed;
    public:
        RESPONSE :: _eEnum response_to_sensing;
    public:
        _eBool rate_modulation;
    public:
        _eBool inVOO () const;
    public:
        _eBool inAOO () const;
    public:
        _eBool inDOO () const;
    public:
        _eBool inDDI () const;
    public:
        _eBool inVVI () const;
    public:
        _eBool inVDD () const;
    public:
        _eBool inAAI () const;
    public:
        _eBool inVVT () const;
    public:
        _eBool inAAT () const;
    public:
        _eBool inAXXX () const;
    public:
        _eBool inVXXX () const;
    public:
        _eBool inDXXX () const;
    public:
        _eBool inOXO () const;
    public:
        BOM (const SWITCH :: _eEnum, const CHAMBERS :: _eEnum, const CHAMBERS :: _eEnum, const
            RESPONSE :: _eEnum, const _eBool);
    public:
        _eBool operator == (const BOM &) const;
        BOM ();
    };
    class TimeSt : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (TimeSt)
        _mPerfectTypeInfoFwd
    public:
        _eInt time;
    public:
        _eInt a_start_time;
    public:
        _eInt a_max;
    public:
        _eInt a_delay;
    public:
        _eInt v_start_time;
    public:
        _eInt v_max;
    public:
        _eInt v_delay;
    public:
        _eInt a_curr_measurement;
    public:
        _eInt v_curr_measurement;
    public:
        TimeSt (const _eInt, const _eInt, const _eInt, const _eInt, const _eInt, const _eInt, const
            _eInt, const _eInt, const _eInt);
    public:
        void chg_time (const _eInt);
    public:
        void chg_a_start_time (const _eInt);
    public:
        void chg_a_max (const _eInt);
    public:
        void chg_a_delay (const _eInt);
    public:
        void chg_v_start_time (const _eInt);
    public:
        void chg_v_max (const _eInt);
    public:
        void chg_v_delay (const _eInt);
    public:
        void chg_v_curr_measurement (const _eInt);
    public:
        void chg_a_curr_measurement (const _eInt);
    public:
        _eBool operator == (const TimeSt &) const;
        TimeSt ();
        _mDeclareClassInvariant
    };
    class SensingPulse : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (SensingPulse)
        _mPerfectTypeInfoFwd
    public:
        _eInt cardiac_cycles_length;
    public:
        _eInt a_sensing_threshold;
    public:
        _eInt v_sensing_threshold;
    public:
        _eInt sensed_a_pulse_width;
    public:
        _eInt sensed_v_pulse_width;
    public:
        _eInt sensed_a_pulse_amp;
    public:
        _eInt sensed_v_pulse_amp;
    public:
        _eInt last_s_a_pulse;
    public:
        _eInt last_s_v_pulse;
    public:
        _eInt current_rate;
    public:
        SensingPulse (const _eInt, const _eInt, const _eInt, const _eInt, const _eInt, const _eInt,
            const _eInt, const _eInt, const _eInt, const _eInt);
    public:
        void chg_sensed_atrial (const _eInt, const _eInt, const _eInt);
    public:
        void chg_sensed_ventricular (const _eInt, const _eInt, const _eInt, const _eInt);
    public:
        void chg_cardiac_cycles_length (const _eInt);
    public:
        void chg_a_sensing_threshold (const _eInt);
    public:
        void chg_v_sensing_threshold (const _eInt);
    public:
        void chg_sensed_a_pulse_width (const _eInt);
    public:
        void chg_sensed_v_pulse_width (const _eInt);
    public:
        void chg_sensed_a_pulse_amp (const _eInt);
    public:
        void chg_sensed_v_pulse_amp (const _eInt);
    public:
        void chg_last_s_a_pulse (const _eInt);
    public:
        void chg_last_s_v_pulse (const _eInt);
    public:
        void chg_current_rate (const _eInt);
    public:
        _eBool operator == (const SensingPulse &) const;
        SensingPulse ();
    };
    class PacingPulse : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (PacingPulse)
        _mPerfectTypeInfoFwd
    public:
        _eInt paced_a_pulse_width;
    public:
        _eInt paced_v_pulse_width;
    public:
        _eInt paced_a_pulse_amp;
    public:
        _eInt paced_v_pulse_amp;
    public:
        _eInt last_p_a_pulse;
    public:
        _eInt last_p_v_pulse;
    public:
        PacingPulse (const _eInt, const _eInt, const _eInt, const _eInt, const _eInt, const _eInt);
    public:
        void chg_paced_atrial (const _eInt, const _eInt, const _eInt);
    public:
        void chg_paced_ventricular (const _eInt, const _eInt, const _eInt);
    public:
        void chg_paced_dual (const _eInt, const _eInt, const _eInt, const _eInt, const _eInt);
    public:
        void chg_paced_a_pulse_width (const _eInt);
    public:
        void chg_paced_v_pulse_width (const _eInt);
    public:
        void chg_paced_a_pulse_amp (const _eInt);
    public:
        void chg_paced_v_pulse_amp (const _eInt);
    public:
        void chg_last_p_a_pulse (const _eInt);
    public:
        void chg_last_p_v_pulse (const _eInt);
    public:
        _eBool operator == (const PacingPulse &) const;
        PacingPulse ();
    };
    class BatteryStatus : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (BatteryStatus)
        _mPerfectTypeInfoFwd
    public:
        BATT_STATUS_LEVEL :: _eEnum batt_status_level;
    public:
        explicit BatteryStatus (const BATT_STATUS_LEVEL :: _eEnum);
    public:
        void chg_batt_status_level (const BATT_STATUS_LEVEL :: _eEnum);
    public:
        _eBool operator == (const BatteryStatus &) const;
        BatteryStatus ();
        _mDeclareClassInvariant
    };
    class NoiseDetection : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (NoiseDetection)
        _mPerfectTypeInfoFwd
    public:
        _eInt noise;
    public:
        explicit NoiseDetection (const _eInt);
    public:
        void chg_noise (const _eInt);
    public:
        _eBool operator == (const NoiseDetection &) const;
        NoiseDetection ();
        _mDeclareClassInvariant
    };
    class EventMarkers : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (EventMarkers)
        _mPerfectTypeInfoFwd
    public:
        _eSeq < _n5_quin < MarkerABB :: _eEnum _mComma _eInt _mComma _eInt _mComma _eInt _mComma
            _eInt > > atrial_marker;
    public:
        _eSeq < _n5_quin < MarkerABB :: _eEnum _mComma _eInt _mComma _eInt _mComma _eInt _mComma
            _eInt > > ventricular_marker;
    public:
        _eSeq < _ePair < MarkerABB :: _eEnum _mComma _eInt > > augmentation_marker;
    public:
        EventMarkers (const _eSeq < _n5_quin < MarkerABB :: _eEnum _mComma _eInt _mComma _eInt
            _mComma _eInt _mComma _eInt > >, const _eSeq < _n5_quin < MarkerABB :: _eEnum _mComma
            _eInt _mComma _eInt _mComma _eInt _mComma _eInt > >, const _eSeq < _ePair < MarkerABB ::
            _eEnum _mComma _eInt > >);
    public:
        void chg_atrial_marker (const MarkerABB :: _eEnum, const _eInt, const _eInt, const _eInt,
            const _eInt);
    public:
        void chg_ventricular_marker (const MarkerABB :: _eEnum, const _eInt, const _eInt, const
            _eInt, const _eInt);
    public:
        void chg_dual_marker (const MarkerABB :: _eEnum, const _eInt, const _eInt, const MarkerABB
            :: _eEnum, const _eInt, const _eInt, const _eInt, const _eInt);
    public:
        void chg_augmentation_marker (const MarkerABB :: _eEnum, const _eInt);
    public:
        void unchg_dual_marker ();
    public:
        void unchg_atrial_marker ();
    public:
        void unchg_ventricular_marker ();
    public:
        void unchg_augmentation_marker ();
    public:
        _eBool operator == (const EventMarkers &) const;
        EventMarkers ();
        _mDeclareClassInvariant
    };
    class Accelerometer : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (Accelerometer)
        _mPerfectTypeInfoFwd
    public:
        _eInt accel;
    public:
        explicit Accelerometer (const _eInt);
    public:
        void chg_accel (const _eInt);
    public:
        _eBool operator == (const Accelerometer &) const;
        Accelerometer ();
        _mDeclareClassInvariant
    };
    class PWave : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (PWave)
        _mPerfectTypeInfoFwd
    public:
        _eInt p_wave;
    public:
        explicit PWave (const _eInt);
    public:
        void chg_p_wave (const _eInt);
    public:
        _eBool operator == (const PWave &) const;
        PWave ();
    };
    class RWave : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (RWave)
        _mPerfectTypeInfoFwd
    public:
        _eInt r_wave;
    public:
        explicit RWave (const _eInt);
    public:
        void chg_r_wave (const _eInt);
    public:
        _eBool operator == (const RWave &) const;
        RWave ();
    };
    class BatteryVoltage : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (BatteryVoltage)
        _mPerfectTypeInfoFwd
    public:
        _eInt battery_voltage;
    public:
        explicit BatteryVoltage (const _eInt);
    public:
        void chg_battery_voltage (const _eInt);
    public:
        _eBool operator == (const BatteryVoltage &) const;
        BatteryVoltage ();
    };
    class LeadImpedance : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (LeadImpedance)
        _mPerfectTypeInfoFwd
    public:
        _eInt lead_impedance;
    public:
        explicit LeadImpedance (const _eInt);
    public:
        void chg_lead_impedance (const _eInt);
    public:
        _eBool operator == (const LeadImpedance &) const;
        LeadImpedance ();
        _mDeclareClassInvariant
    };
    class MeasuredParameters : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (MeasuredParameters)
        _mPerfectTypeInfoFwd
    public:
        LeadImpedance c_LeadImpedance;
    public:
        BatteryVoltage c_BatteryVoltage;
    public:
        RWave c_RWave;
    public:
        PWave c_PWave;
    public:
        MeasuredParameters (const LeadImpedance, const BatteryVoltage, const RWave, const PWave);
    public:
        void set_p_wave (const _eInt);
    public:
        void set_r_wave (const _eInt);
    public:
        void set_lead_impedance (const _eInt);
    public:
        void set_battery_voltage (const _eInt);
    public:
        _eBool operator == (const MeasuredParameters &) const;
        MeasuredParameters ();
    };
    class BO_MODE : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (BO_MODE)
        _mPerfectTypeInfoFwd
    public:
        BOM bo_mode;
    public:
        _eBool isVOO () const;
    public:
        _eBool isAOO () const;
    public:
        _eBool isDOO () const;
    public:
        _eBool isDDI () const;
    public:
        _eBool isVVI () const;
    public:
        _eBool isAAI () const;
    public:
        _eBool isVVT () const;
    public:
        _eBool isAAT () const;
    public:
        _eBool isVDD () const;
    public:
        explicit BO_MODE (const BOM);
    public:
        void chg_bo_mode (const BOM);
    public:
        _eBool operator == (const BO_MODE &) const;
        BO_MODE ();
    };
    class LRL : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (LRL)
        _mPerfectTypeInfoFwd
    public:
        _eInt lower_rate_limit;
    public:
        explicit LRL (const _eInt);
    public:
        void chg_lower_rate_limit (const _eInt);
    public:
        _eBool operator == (const LRL &) const;
        LRL ();
        _mDeclareClassInvariant
    };
    class URL : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (URL)
        _mPerfectTypeInfoFwd
    public:
        _eInt upper_rate_limit;
    public:
        explicit URL (const _eInt);
    public:
        void chg_upper_rate_limit (const _eInt);
    public:
        _eBool operator == (const URL &) const;
        URL ();
        _mDeclareClassInvariant
    };
    class MaxSensorRate : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (MaxSensorRate)
        _mPerfectTypeInfoFwd
    public:
        _eInt max_sensor_rate;
    public:
        explicit MaxSensorRate (const _eInt);
    public:
        void chg_max_sensor_rate (const _eInt);
    public:
        _eBool operator == (const MaxSensorRate &) const;
        MaxSensorRate ();
        _mDeclareClassInvariant
    };
    class FixedAVDelay : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (FixedAVDelay)
        _mPerfectTypeInfoFwd
    public:
        _eInt fixed_av_delay;
    public:
        explicit FixedAVDelay (const _eInt);
    public:
        void chg_fixed_av_delay (const _eInt);
    public:
        _eBool operator == (const FixedAVDelay &) const;
        FixedAVDelay ();
        _mDeclareClassInvariant
    };
    class DynamicAVDelay : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (DynamicAVDelay)
        _mPerfectTypeInfoFwd
    public:
        SWITCH :: _eEnum dynamic_av_delay;
    public:
        explicit DynamicAVDelay (const SWITCH :: _eEnum);
    public:
        void chg_dynamic_av_delay (const SWITCH :: _eEnum);
    public:
        _eBool operator == (const DynamicAVDelay &) const;
        DynamicAVDelay ();
    };
    class MinDynamicAVDelay : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (MinDynamicAVDelay)
        _mPerfectTypeInfoFwd
    public:
        _eInt min_dyn_av_delay;
    public:
        explicit MinDynamicAVDelay (const _eInt);
    public:
        void chg_min_dyn_av_delay (const _eInt);
    public:
        _eBool operator == (const MinDynamicAVDelay &) const;
        MinDynamicAVDelay ();
        _mDeclareClassInvariant
    };
    class SensedAVDelayOffset : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (SensedAVDelayOffset)
        _mPerfectTypeInfoFwd
    public:
        _ePair < SWITCH :: _eEnum _mComma _eInt > sensed_av_delay_offset;
    public:
        explicit SensedAVDelayOffset (const _ePair < SWITCH :: _eEnum _mComma _eInt >);
    public:
        void chg_sensed_av_delay_offset (const _ePair < SWITCH :: _eEnum _mComma _eInt >);
    public:
        _eBool operator == (const SensedAVDelayOffset &) const;
        SensedAVDelayOffset ();
        _mDeclareClassInvariant
    };
    class APulseAmpRegulated : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (APulseAmpRegulated)
        _mPerfectTypeInfoFwd
    public:
        _ePair < SWITCH :: _eEnum _mComma _eInt > a_pulse_amp_regulated;
    public:
        explicit APulseAmpRegulated (const _ePair < SWITCH :: _eEnum _mComma _eInt >);
    public:
        void chg_a_pulse_amp_regulated (const _ePair < SWITCH :: _eEnum _mComma _eInt >);
    public:
        _eBool operator == (const APulseAmpRegulated &) const;
        APulseAmpRegulated ();
        _mDeclareClassInvariant
    };
    class VPulseAmpRegulated : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (VPulseAmpRegulated)
        _mPerfectTypeInfoFwd
    public:
        _ePair < SWITCH :: _eEnum _mComma _eInt > v_pulse_amp_regulated;
    public:
        explicit VPulseAmpRegulated (const _ePair < SWITCH :: _eEnum _mComma _eInt >);
    public:
        void chg_v_pulse_amp_regulated (const _ePair < SWITCH :: _eEnum _mComma _eInt >);
    public:
        _eBool operator == (const VPulseAmpRegulated &) const;
        VPulseAmpRegulated ();
        _mDeclareClassInvariant
    };
    class APulseAmpUnregulated : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (APulseAmpUnregulated)
        _mPerfectTypeInfoFwd
    public:
        _ePair < SWITCH :: _eEnum _mComma _eInt > a_pulse_amp_unregulated;
    public:
        explicit APulseAmpUnregulated (const _ePair < SWITCH :: _eEnum _mComma _eInt >);
    public:
        void chg_a_pulse_amp_unregulated (const _ePair < SWITCH :: _eEnum _mComma _eInt >);
    public:
        _eBool operator == (const APulseAmpUnregulated &) const;
        APulseAmpUnregulated ();
        _mDeclareClassInvariant
    };
    class VPulseAmpUnregulated : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (VPulseAmpUnregulated)
        _mPerfectTypeInfoFwd
    public:
        _ePair < SWITCH :: _eEnum _mComma _eInt > v_pulse_amp_unregulated;
    public:
        explicit VPulseAmpUnregulated (const _ePair < SWITCH :: _eEnum _mComma _eInt >);
    public:
        void chg_v_pulse_amp_unregulated (const _ePair < SWITCH :: _eEnum _mComma _eInt >);
    public:
        _eBool operator == (const VPulseAmpUnregulated &) const;
        VPulseAmpUnregulated ();
        _mDeclareClassInvariant
    };
    class APulseWidth : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (APulseWidth)
        _mPerfectTypeInfoFwd
    public:
        _eInt a_pulse_width;
    public:
        explicit APulseWidth (const _eInt);
    public:
        void chg_a_pulse_width (const _eInt);
    public:
        _eBool operator == (const APulseWidth &) const;
        APulseWidth ();
        _mDeclareClassInvariant
    };
    class VPulseWidth : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (VPulseWidth)
        _mPerfectTypeInfoFwd
    public:
        _eInt v_pulse_width;
    public:
        explicit VPulseWidth (const _eInt);
    public:
        void chg_v_pulse_width (const _eInt);
    public:
        _eBool operator == (const VPulseWidth &) const;
        VPulseWidth ();
        _mDeclareClassInvariant
    };
    class ASensitivity : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (ASensitivity)
        _mPerfectTypeInfoFwd
    public:
        _eInt a_sensitivity;
    public:
        explicit ASensitivity (const _eInt);
    public:
        void chg_a_sensitivity (const _eInt);
    public:
        _eBool operator == (const ASensitivity &) const;
        ASensitivity ();
        _mDeclareClassInvariant
    };
    class VSensitivity : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (VSensitivity)
        _mPerfectTypeInfoFwd
    public:
        _eInt v_sensitivity;
    public:
        explicit VSensitivity (const _eInt);
    public:
        void chg_v_sensitivity (const _eInt);
    public:
        _eBool operator == (const VSensitivity &) const;
        VSensitivity ();
        _mDeclareClassInvariant
    };
    class VRefractoryPeriod : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (VRefractoryPeriod)
        _mPerfectTypeInfoFwd
    public:
        _eInt v_refract_period;
    public:
        explicit VRefractoryPeriod (const _eInt);
    public:
        void chg_v_refract_period (const _eInt);
    public:
        _eBool operator == (const VRefractoryPeriod &) const;
        VRefractoryPeriod ();
        _mDeclareClassInvariant
    };
    class ARefractoryPeriod : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (ARefractoryPeriod)
        _mPerfectTypeInfoFwd
    public:
        _eInt a_refract_period;
    public:
        explicit ARefractoryPeriod (const _eInt);
    public:
        void chg_a_refract_period (const _eInt);
    public:
        _eBool operator == (const ARefractoryPeriod &) const;
        ARefractoryPeriod ();
        _mDeclareClassInvariant
    };
    class PVARP : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (PVARP)
        _mPerfectTypeInfoFwd
    public:
        _eInt pvarp;
    public:
        explicit PVARP (const _eInt);
    public:
        void chg_pvarp (const _eInt);
    public:
        _eBool operator == (const PVARP &) const;
        PVARP ();
        _mDeclareClassInvariant
    };
    class PVARP_Ext : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (PVARP_Ext)
        _mPerfectTypeInfoFwd
    public:
        _ePair < SWITCH :: _eEnum _mComma _eInt > pvarp_ext;
    public:
        explicit PVARP_Ext (const _ePair < SWITCH :: _eEnum _mComma _eInt >);
    public:
        void chg_pvarp_ext (const _ePair < SWITCH :: _eEnum _mComma _eInt >);
    public:
        _eBool operator == (const PVARP_Ext &) const;
        PVARP_Ext ();
        _mDeclareClassInvariant
    };
    class HysteresisRateLimit : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (HysteresisRateLimit)
        _mPerfectTypeInfoFwd
    public:
        _ePair < SWITCH :: _eEnum _mComma _eInt > hysteresis_rate_limit;
    public:
        explicit HysteresisRateLimit (const _ePair < SWITCH :: _eEnum _mComma _eInt >);
    public:
        void chg_hysteresis_rate_limit (const _ePair < SWITCH :: _eEnum _mComma _eInt >);
    public:
        _eBool operator == (const HysteresisRateLimit &) const;
        HysteresisRateLimit ();
        _mDeclareClassInvariant
    };
    class RateSmoothing : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (RateSmoothing)
        _mPerfectTypeInfoFwd
    public:
        _ePair < SWITCH :: _eEnum _mComma _eInt > rate_smoothing;
    public:
        explicit RateSmoothing (const _ePair < SWITCH :: _eEnum _mComma _eInt >);
    public:
        void chg_rate_smoothing (const _ePair < SWITCH :: _eEnum _mComma _eInt >);
    public:
        _eBool operator == (const RateSmoothing &) const;
        RateSmoothing ();
        _mDeclareClassInvariant
    };
    class ATRMode : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (ATRMode)
        _mPerfectTypeInfoFwd
    public:
        SWITCH :: _eEnum atr_mode;
    public:
        explicit ATRMode (const SWITCH :: _eEnum);
    public:
        void chg_atr_mode (const SWITCH :: _eEnum);
    public:
        _eBool operator == (const ATRMode &) const;
        ATRMode ();
    };
    class ATRDuration : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (ATRDuration)
        _mPerfectTypeInfoFwd
    public:
        _eInt atr_duration;
    public:
        explicit ATRDuration (const _eInt);
    public:
        void chg_atr_duration (const _eInt);
    public:
        _eBool operator == (const ATRDuration &) const;
        ATRDuration ();
        _mDeclareClassInvariant
    };
    class ATRFallbackTime : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (ATRFallbackTime)
        _mPerfectTypeInfoFwd
    public:
        _eInt atr_fallback_time;
    public:
        explicit ATRFallbackTime (const _eInt);
    public:
        void chg_atr_fallback_time (const _eInt);
    public:
        _eBool operator == (const ATRFallbackTime &) const;
        ATRFallbackTime ();
        _mDeclareClassInvariant
    };
    class VBlanking : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (VBlanking)
        _mPerfectTypeInfoFwd
    public:
        _eInt v_blanking;
    public:
        explicit VBlanking (const _eInt);
    public:
        void chg_v_blanking (const _eInt);
    public:
        _eBool operator == (const VBlanking &) const;
        VBlanking ();
        _mDeclareClassInvariant
    };
    class ActivityThreshold : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (ActivityThreshold)
        _mPerfectTypeInfoFwd
    public:
        ACTIVITY_THRESHOLD :: _eEnum act_threshold;
    public:
        explicit ActivityThreshold (const ACTIVITY_THRESHOLD :: _eEnum);
    public:
        void chg_act_threshold (const ACTIVITY_THRESHOLD :: _eEnum);
    public:
        _eBool operator == (const ActivityThreshold &) const;
        ActivityThreshold ();
    };
    class ReactTime : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (ReactTime)
        _mPerfectTypeInfoFwd
    public:
        _eInt reaction_time;
    public:
        explicit ReactTime (const _eInt);
    public:
        void chg_reaction_time (const _eInt);
    public:
        _eBool operator == (const ReactTime &) const;
        ReactTime ();
        _mDeclareClassInvariant
    };
    class RespFactor : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (RespFactor)
        _mPerfectTypeInfoFwd
    public:
        _eInt response_factor;
    public:
        explicit RespFactor (const _eInt);
    public:
        void chg_response_factor (const _eInt);
    public:
        _eBool operator == (const RespFactor &) const;
        RespFactor ();
        _mDeclareClassInvariant
    };
    class RecoveryTime : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (RecoveryTime)
        _mPerfectTypeInfoFwd
    public:
        _eInt recovery_time;
    public:
        explicit RecoveryTime (const _eInt);
    public:
        void chg_recovery_time (const _eInt);
    public:
        _eBool operator == (const RecoveryTime &) const;
        RecoveryTime ();
        _mDeclareClassInvariant
    };
    class ProgrammableParameters : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (ProgrammableParameters)
        _mPerfectTypeInfoFwd
    public:
        LRL c_LRL;
    public:
        URL c_URL;
    public:
        MaxSensorRate c_MaxSensorRate;
    public:
        FixedAVDelay c_FixedAVDelay;
    public:
        DynamicAVDelay c_DynamicAVDelay;
    public:
        MinDynamicAVDelay c_MinDynamicAVDelay;
    public:
        SensedAVDelayOffset c_SensedAVDelayOffset;
    public:
        APulseAmpRegulated c_APulseAmpRegulated;
    public:
        VPulseAmpRegulated c_VPulseAmpRegulated;
    public:
        APulseAmpUnregulated c_APulseAmpUnregulated;
    public:
        VPulseAmpUnregulated c_VPulseAmpUnregulated;
    public:
        APulseWidth c_APulseWidth;
    public:
        VPulseWidth c_VPulseWidth;
    public:
        ASensitivity c_ASensitivity;
    public:
        VSensitivity c_VSensitivity;
    public:
        VRefractoryPeriod c_VRefractoryPeriod;
    public:
        ARefractoryPeriod c_ARefractoryPeriod;
    public:
        PVARP c_PVARP;
    public:
        PVARP_Ext c_PVARP_Ext;
    public:
        HysteresisRateLimit c_HysteresisRateLimit;
    public:
        RateSmoothing c_RateSmoothing;
    public:
        ATRMode c_ATRMode;
    public:
        ATRDuration c_ATRDuration;
    public:
        ATRFallbackTime c_ATRFallbackTime;
    public:
        VBlanking c_VBlanking;
    public:
        ActivityThreshold c_ActivityThreshold;
    public:
        ReactTime c_ReactTime;
    public:
        RespFactor c_RespFactor;
    public:
        RecoveryTime c_RecoveryTime;
    public:
        ProgrammableParameters (const LRL, const URL, const MaxSensorRate, const FixedAVDelay, const
            DynamicAVDelay, const MinDynamicAVDelay, const SensedAVDelayOffset, const
            APulseAmpRegulated, const VPulseAmpRegulated, const APulseAmpUnregulated, const
            VPulseAmpUnregulated, const APulseWidth, const VPulseWidth, const ASensitivity, const
            VSensitivity, const VRefractoryPeriod, const ARefractoryPeriod, const PVARP, const
            PVARP_Ext, const HysteresisRateLimit, const RateSmoothing, const ATRMode, const
            ATRDuration, const ATRFallbackTime, const VBlanking, const ActivityThreshold, const
            ReactTime, const RespFactor, const RecoveryTime);
    public:
        void set_lower_rate_limit (const _eInt);
    public:
        void set_upper_rate_limit (const _eInt);
    public:
        void set_max_sensor_rate (const _eInt);
    public:
        void set_fixed_av_delay (const _eInt);
    public:
        void set_dynamic_av_delay (const SWITCH :: _eEnum);
    public:
        void set_min_dyn_av_delay (const _eInt);
    public:
        void set_sensed_av_delay_offset (const _ePair < SWITCH :: _eEnum _mComma _eInt >);
    public:
        void set_a_pulse_amp_regulated (const _ePair < SWITCH :: _eEnum _mComma _eInt >);
    public:
        void set_v_pulse_amp_regulated (const _ePair < SWITCH :: _eEnum _mComma _eInt >);
    public:
        void set_a_pulse_amp_unregulated (const _ePair < SWITCH :: _eEnum _mComma _eInt >);
    public:
        void set_v_pulse_amp_unregulated (const _ePair < SWITCH :: _eEnum _mComma _eInt >);
    public:
        void set_a_pulse_width (const _eInt);
    public:
        void set_v_pulse_width (const _eInt);
    public:
        void set_a_sensitivity (const _eInt);
    public:
        void set_v_sensitivity (const _eInt);
    public:
        void set_v_refract_period (const _eInt);
    public:
        void set_a_refract_period (const _eInt);
    public:
        void set_pvarp (const _eInt);
    public:
        void set_pvarp_ext (const _ePair < SWITCH :: _eEnum _mComma _eInt >);
    public:
        void set_hysteresis_rate_limit (const _ePair < SWITCH :: _eEnum _mComma _eInt >);
    public:
        void set_rate_smoothing (const _ePair < SWITCH :: _eEnum _mComma _eInt >);
    public:
        void set_atr_mode (const SWITCH :: _eEnum);
    public:
        void set_atr_duration (const _eInt);
    public:
        void set_atr_fallback_time (const _eInt);
    public:
        void set_v_blanking (const _eInt);
    public:
        void set_act_threshold (const ACTIVITY_THRESHOLD :: _eEnum);
    public:
        void set_reaction_time (const _eInt);
    public:
        void set_response_factor (const _eInt);
    public:
        void set_recovery_time (const _eInt);
    public:
        _eBool operator == (const ProgrammableParameters &) const;
        ProgrammableParameters ();
    };
    class HistogramsSt : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (HistogramsSt)
        _mPerfectTypeInfoFwd
    public:
        _eSeq < _eTriple < _eInt _mComma _eInt _mComma _eInt > > atrial_paced;
    public:
        _eSeq < _eTriple < _eInt _mComma _eInt _mComma _eInt > > atrial_sensed;
    public:
        _eSeq < _eTriple < _eInt _mComma _eInt _mComma _eInt > > ventricular_paced;
    public:
        _eSeq < _eTriple < _eInt _mComma _eInt _mComma _eInt > > ventricular_sensed;
    public:
        _eInt pvc_event;
    public:
        _eInt atrial_tachy;
    public:
        HistogramsSt (const _eSeq < _eTriple < _eInt _mComma _eInt _mComma _eInt > >, const _eSeq <
            _eTriple < _eInt _mComma _eInt _mComma _eInt > >, const _eSeq < _eTriple < _eInt _mComma
            _eInt _mComma _eInt > >, const _eSeq < _eTriple < _eInt _mComma _eInt _mComma _eInt > >,
            const _eInt, const _eInt);
    public:
        void chg_atrial_paced (const _eInt, const _eInt, const _eInt);
    public:
        void chg_atrial_sensed (const _eInt, const _eInt, const _eInt);
    public:
        void chg_ventricular_paced (const _eInt, const _eInt, const _eInt);
    public:
        void chg_ventricular_sensed (const _eInt, const _eInt, const _eInt);
    public:
        void chg_pvc_event (const _eInt);
    public:
        void chg_atrial_tachy (const _eInt);
    public:
        _eBool operator == (const HistogramsSt &) const;
        HistogramsSt ();
    };
    class BradycardiaSt : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (BradycardiaSt)
        _mPerfectTypeInfoFwd
    public:
        BRADYCARDIA_STATE :: _eEnum bradycardia_state;
    public:
        explicit BradycardiaSt (const BRADYCARDIA_STATE :: _eEnum);
    public:
        void chg_bradycardia_state (const BRADYCARDIA_STATE :: _eEnum);
    public:
        _eBool operator == (const BradycardiaSt &) const;
        BradycardiaSt ();
    };
    class AtrialBipolarLead : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (AtrialBipolarLead)
        _mPerfectTypeInfoFwd
    public:
        LEAD_CHAMBER_TYPE :: _eEnum a_lead_chamber;
    public:
        POLARITY :: _eEnum a_lead_polarity;
    public:
        AtrialBipolarLead (const LEAD_CHAMBER_TYPE :: _eEnum, const POLARITY :: _eEnum);
    public:
        void chg_a_lead_chamber (const LEAD_CHAMBER_TYPE :: _eEnum);
    public:
        void chg_a_lead_polarity (const POLARITY :: _eEnum);
    public:
        _eBool operator == (const AtrialBipolarLead &) const;
        AtrialBipolarLead ();
    };
    class VentricularBipolarLead : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (VentricularBipolarLead)
        _mPerfectTypeInfoFwd
    public:
        LEAD_CHAMBER_TYPE :: _eEnum v_lead_chamber;
    public:
        POLARITY :: _eEnum v_lead_polarity;
    public:
        VentricularBipolarLead (const LEAD_CHAMBER_TYPE :: _eEnum, const POLARITY :: _eEnum);
    public:
        void chg_v_lead_chamber (const LEAD_CHAMBER_TYPE :: _eEnum);
    public:
        void chg_v_lead_polarity (const POLARITY :: _eEnum);
    public:
        _eBool operator == (const VentricularBipolarLead &) const;
        VentricularBipolarLead ();
    };
    class LeadsSt : public _eAny
    {
    public:
        // FINAL DIRECT class ...
        _mPerfectTypeInfoHdrNA (LeadsSt)
        _mPerfectTypeInfoFwd
    public:
        AtrialBipolarLead c_AtrialBipolarLead;
    public:
        VentricularBipolarLead c_VentricularBipolarLead;
    public:
        LeadsSt (const AtrialBipolarLead, const VentricularBipolarLead);
    public:
        void chg_a_lead_chamber (const LEAD_CHAMBER_TYPE :: _eEnum);
    public:
        void chg_a_lead_polarity (const POLARITY :: _eEnum);
    public:
        void chg_v_lead_chamber (const LEAD_CHAMBER_TYPE :: _eEnum);
    public:
        void chg_v_lead_polarity (const POLARITY :: _eEnum);
    public:
        _eBool operator == (const LeadsSt &) const;
        LeadsSt ();
    };
}

#endif

// End of file.
