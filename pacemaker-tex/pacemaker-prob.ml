\documentclass{article}
\usepackage{czt}

\begin{document}
\begin{zed}
 [DATE,TIME,STRING,GRAPHIC]\\
\also CHAMBERRATEADAPTATIVEPACE ::= CRSINGLE | CRDUAL\\
\also ACTIVITYTHRESHOLD ::= VERYLOW | LOW  | MEDLOW | MED |\\
		 MEDHIGH | HIGH | VERYHIGH\\
\also SWITCH ::= ON | OFF\\
\also BOOLEAN ::= TRUE | FALSE\\
\also LEADCHAMBERTYPE ::= LATRIAL | LVENTRICULAR\\
\also POLARITY ::= UNIPOLAR | BIPOLAR\\
\also CHAMBERS ::= CNONE | CATRIUM | CVENTRICLE | CDUAL\\
\also CHAMBER ::= ATRIUM | VENTRICLE\\
\also RESPONSE ::= RNONE | TRIGGERED | INHIBITED | TRACKED\\
\also BRADYCARDIASTATE ::= PERMANENT | TEMPORARY | PACENOW \\
			| MAGNET | POR\\
\also BATTSTATUSLEVEL ::= BOL | ERN | ERT | ERP\\
\end{zed}
\begin{zed}
SWITCHPARAMZ == \{ val  : \num\\; switch:SWITCH  \\
	 | switch = ON \lor switch = OFF \\
	 @ (switch, val) \}\\
\end{zed}
\begin{zed}
SWITCHPARAMZ == \{ val : \num\\; switch:SWITCH \\
	 | switch = ON \lor switch = OFF \\
	 @ (switch, val) \}\\
\end{zed}




%(*
%\section{PG State}
*)
%(* \section{Lead} *)

%(* B - Measured Parameters *)

\begin{schema}{PWave}
 pWave : \num\\ %(* mV *)\\
 %incPWave   : \num\\ %(* mV *)\\
 %tolPWave   : \nat  %(* percent *)\\
%\where
 %incPWave = (1 \div 10)
 %tolPWave  = 5
\end{schema}

\begin{schema}{PWave'}
 pWave' : \num\\ %(* mV *)
 %incPWave'   : \num\\ %(* mV *)\\
 %tolPWave'   : \nat  %(* percent *)
\end{schema}

\begin{schema}{RWave}
 rWave : \num\\ %(* mV *)\\
 %incRWave   : \num\\ %(* mV *)\\
 %tolRWave   : \nat  %(* percent *)\\
%\where
 %incRWave = (1 \div 10)\\
 %tolRWave  = 5\\
\end{schema}
\begin{schema}{RWave'}
 rWave' : \num\\ %(* mV *)\\
 %incRWave'   : \num\\ %(* mV *)\\
 %tolRWave'   : \nat  %(* percent *)\\
\end{schema}

\begin{schema}{BatteryVoltage}
 batteryVoltage : \num\\ %(* mV *)\\
 %incBatteryVoltage   : \num\\ %(* mV *)\\
 %tolBatteryVoltage   : \nat  %(* percent *)\\
\where
 %incBatteryVoltage = (1 \div 100)\\
 %tolBatteryVoltage = 2\\
\end{schema}
\begin{schema}{BatteryVoltage'}
 batteryVoltage' : \num\\ %(* mV *)\\
 %incBatteryVoltage'   : \num\\ %(* mV *)\\
 %tolBatteryVoltage'   : \nat  %(* percent *)\\
\end{schema}

\begin{schema}{LeadImpedance}
 leadImpedance : \num\\ %(* Ohms *)\\
 %incLeadImpedance   : \num\\ %(* Ohms *)\\
 %tolLeadImpedance   : \nat  %(* percent *)\\
\where
 %incLeadImpedance = 50\\
 ((100) \leq leadImpedance \leq (500))
   \implies (%tolLeadImpedance = 30)\\
 \lor ((500) \leq leadImpedance \leq (2000))
   \implies (%tolLeadImpedance = 25)\\
 \lor ((2000) \leq leadImpedance \leq (2500))
   \implies (%tolLeadImpedance = 30)\\
\end{schema}

\begin{schema}{LeadImpedance'}
 leadImpedance' : \num\\ %(* Ohms *)\\
 %incLeadImpedance'   : \num\\ %(* Ohms *)\\
 %tolLeadImpedance'   : \nat  %(* percent *)\\
\end{schema}


\begin{zed}
 MeasuredParameters == PWave \land\\
  		RWave \land\\
  		BatteryVoltage \land\\
  		LeadImpedance\\
\end{zed}
\begin{zed}
 MeasuredParameters' == PWave' \land\\
  		RWave' \land\\
  		BatteryVoltage' \land\\
  		LeadImpedance'\\
\end{zed}
%(*
%3.3 - Lead Support
%*)

\begin{schema}{AtrialBipolarLead}
 aLeadChamber : LEADCHAMBERTYPE\\
 aLeadPolarity : POLARITY\\
\where
 aLeadChamber = LATRIAL\\
 aLeadPolarity = BIPOLAR\\
\end{schema}

\begin{schema}{AtrialBipolarLead'}
 aLeadChamber' : LEADCHAMBERTYPE\\
 aLeadPolarity' : POLARITY\\
\end{schema}

\begin{schema}{VentricularBipolarLead}
 vLeadChamber : LEADCHAMBERTYPE\\
 vLeadPolarity : POLARITY\\
\where
 vLeadChamber = LVENTRICULAR\\
 vLeadPolarity = BIPOLAR\\
\end{schema}

\begin{schema}{VentricularBipolarLead'}
 vLeadChamber' : LEADCHAMBERTYPE\\
 vLeadPolarity : POLARITY\\
\end{schema}

\begin{zed}
 Leads ==  AtrialBipolarLead \land VentricularBipolarLead\\
\end{zed}
\begin{zed}
 Leads' ==  AtrialBipolarLead' \land VentricularBipolarLead'\\
\end{zed}

%(*Event Markers*)

\begin{schema}{EventMarkers}
 atrialMarker : RTATRIALMARKERS\\
 ventricularMarker : RTVENTRICULARMARKERS\\
 augmentationMarker : RTAUGMENTATIONMARKERS\\
\end{schema}

\begin{schema}{EventMarkers'}
 atrialMarker' : RTATRIALMARKERS\\
 ventricularMarker' : RTVENTRICULARMARKERS\\
 augmentationMarker' : RTAUGMENTATIONMARKERS\\
\end{schema}

%(* 3.4 - Pacing Pulse*)

\begin{schema}{PacingPulse}
 pacedAPulseWidth : \num\\ %(* ms *)\\
 pacedVPulseWidth : \num\\ %(* ms *)\\
 pacedAPulseAmp : \num\\ %(* mV *)\\
 pacedVPulseAmp : \num\\ %(* mV *)\\
 lastPAPulse : \num\\ %(* TIME *)\\
 lastPVPulse : \num\\ %(* TIME *)\\
\end{schema}

\begin{schema}{PacingPulse'}
 pacedAPulseWidth' : \num\\ %(* ms *)\\
 pacedVPulseWidth' : \num\\ %(* ms *)\\
 pacedAPulseAmp' : \num\\ %(* mV *)\\
 pacedVPulseAmp' : \num\\ %(* mV *)\\
 lastPAPulse' : \num\\ %(* TIME *)\\
 lastPVPulse' : \num\\ %(* TIME *)\\
\end{schema}

%(*
3.4 - SensingPulse
*)

\begin{schema}{SensingPulse}
 cardiacCyclesLength : \num\\ %(* ms *)\\
 aSensingThreshold : \num\\ %(* mV *)\\
 vSensingThreshold : \num\\ %(* mV *)\\
 sensedAPulseWidth : \num\\ %(* ms *)\\
 sensedVPulseWidth : \num\\ %(* ms *)\\
 sensedAPulseAmp    : \num\\ %(* mV *)\\
 sensedVPulseAmp    : \num\\ %(* mV *)\\
 lastSAPulse : \num\\ %(* TIME *)\\
 lastSVPulse : \num\\ %(* TIME *)\\
 currentRate : \nat %(* BPM *)\\
\end{schema}
\begin{schema}{SensingPulse'}
 cardiacCyclesLength' : \num\\ %(* ms *)\\
 aSensingThreshold' : \num\\ %(* mV *)\\
 vSensingThreshold' : \num\\ %(* mV *)\\
 sensedAPulseWidth' : \num\\ %(* ms *)\\
 sensedVPulseWidth' : \num\\ %(* ms *)\\
 sensedAPulseAmp' : \num\\ %(* mV *)\\
 sensedVPulseAmp' : \num\\ %(* mV *)\\
 lastSAPulse' : \num\\ %(* TIME *)\\
 lastSVPulse' : \num\\ %(* TIME *)\\
 currentRate' : \nat %(* BPM *)\\
\end{schema}

%(*
Here we start Sensed Event Markers, state component
of PulseGenerator.
*)
\begin{schema}{TimeSt}
 time : \num\\
 aStartTime : \num\\
 aMax : \num\\
 aDelay : \num\\
 vStartTime : \num\\
 vMax : \num\\
 vDelay : \num\\
 aCurrMeasurement : \num\\
 vCurrMeasurement : \num\\
\end{schema}

\begin{schema}{TimeSt'}
 time' : \num\\
 aStartTime' : \num\\
 aMax' : \num\\
 aDelay' : \num\\
 vStartTime' : \num\\
 vMax' : \num\\
 vDelay' : \num\\
 aCurrMeasurement' : \num\\
 vCurrMeasurement' : \num\\
\end{schema}

%(*
Accelerometer
*)

\begin{schema}{Accelerometer}
 accelerometer : \num\\ %(* mV *)\\
\end{schema}

\begin{schema}{Accelerometer'}
 accelerometer' : \num\\ %(* mV *)\\
\end{schema}

%(*
4.4 - Battery
*)

\begin{schema}{BatteryStatus}
 battStatusLevel : BATTSTATUSLEVEL\\
\end{schema}
\begin{schema}{BatteryStatus'}
 battStatusLevel' : BATTSTATUSLEVEL\\
\end{schema}

%(*
3.8 - Implant Data
*)

\begin{schema}{ImplantData}
 deviceSerial : STRING\\
 deviceModel : STRING\\
 paceThreshold : \num\\ %(* Volts*1000 = mV *)\\
 indicationForPacing : STRING\\
 implantDeviceDate: DATE\\
 implantLeadDate : DATE\\
\end{schema}

\begin{schema}{ImplantData'}
 deviceSerial' : STRING\\
 deviceModel' : STRING\\
 paceThreshold' : \num\\ %(* Volts*1000 = mV *)\\
 indicationForPacing' : STRING\\
 implantDeviceDate: DATE\\
 implantLeadDate' : DATE\\
\end{schema}
%(*
 - Telemetry
*)

\begin{schema}{TelemetrySession}
 telemetryMode  : SWITCH\\
\end{schema}
\begin{schema}{TelemetrySession'}
 telemetryMode' : SWITCH\\
\end{schema}

%(* A - Programmable Parameters *)
\begin{schema}{LRL}
 lowerRateLimit : \nat\\ %(* ppm *)
 %incLowerRateLimit : \num\\ %(* ms *)\\
 %tolLowerRateLimit : \num\\  %(* ms *)\\
\where
 lowerRateLimit \in 30 \upto 175
% (30 \leq lowerRateLimit < 50) \implies (%incLowerRateLimit = 5)\\
% (50 \leq lowerRateLimit < 90) \implies (%incLowerRateLimit = 1)\\
%  (90 \leq lowerRateLimit \leq 175) \implies (%incLowerRateLimit = 5)\\
 %tolLowerRateLimit = 8\\
\end{schema}

\begin{schema}{LRL'}
 lowerRateLimit' : \nat %(* ppm *)
\where
 lowerRateLimit'=60 %(* ppm *)
 %incLowerRateLimit' : \num\\ %(* ms *)\\
 %tolLowerRateLimit' : \num\\ %(* ms *)\\
\end{schema}


\begin{schema}{URL}
 upperRateLimit : \nat\\ %(* ppm *)\\
 %incUpperRateLimit : \num\\ %(* ms *)\\
 %tolUpperRateLimit : \num\\  %(* ms *)\\
\where
 upperRateLimit \in 50 \upto 175\\
 %incUpperRateLimit = 5\\
 %tolUpperRateLimit = 8\\
\end{schema}

\begin{schema}{URL'}
 upperRateLimit' : \nat\\ %(* ppm *)\\
 %incUpperRateLimit' : \num\\ %(* ms *)\\
 %tolUpperRateLimit' : \num\\ %(* ms *)\\
\end{schema}

\begin{schema}{MaxSensorRate}
 maxSensorRate : \nat\\ %(* ppm *)\\
 %incMaxSensorRate : \num\\ %(* ms *)\\
 %tolMaxSensorRate : \num\\  %(* ms *)\\
\where
 maxSensorRate \in 50 \upto 175\\
 %incMaxSensorRate = 5\\
 %tolMaxSensorRate = 4\\
\end{schema}

\begin{schema}{MaxSensorRate'}
 maxSensorRate' : \nat\\ %(* ppm *)\\
 %incMaxSensorRate' : \num\\ %(* ms *)\\
 %tolMaxSensorRate' : \num\\ %(* ms *)\\
\end{schema}

\begin{schema}{FixedAVDelay}
 fixedAvDelay : \num\\ %(* ms *)\\
 %incFixedAvDelay : \num\\ %(* ms *)\\
 %tolFixedAvDelay : \num\\  %(* ms *)\\
\where
 fixedAvDelay \in 70 \upto  300\\
 %incFixedAvDelay = 10\\
 %tolFixedAvDelay = 8\\
\end{schema}

\begin{schema}{FixedAVDelay'}
 fixedAvDelay' : \num\\ %(* ms *)\\
 %incFixedAvDelay' : \num\\ %(* ms *)\\
 %tolFixedAvDelay' : \num\\ %(* ms *)\\
\end{schema}

\begin{schema}{DynamicAVDelay}
 dynamicAvDelay  : SWITCH %(*ON or OFF*)\\
\end{schema}
\begin{schema}{DynamicAVDelay'}
 dynamicAvDelay' : SWITCH %(*ON or OFF*)\\
\end{schema}


\begin{schema}{MinDynamicAVDelay}
 minDynAvDelay : \num\\ %(* ms *)\\
 %incMinDynAvDelay : \num\\ %(* ms *)\\
\where
 minDynAvDelay \in 30 \upto 100\\
 %incMinDynAvDelay = 10\\
\end{schema}

\begin{schema}{MinDynamicAVDelay'}
 minDynAvDelay' : \num\\ %(* ms *)\\
 %incMinDynAvDelay' : \num\\ %(* ms *)\\
\end{schema}



\begin{schema}{SensedAVDelayOffset}
sensedAvDelayOffset : SWITCHPARAMZ\\ %(* ms *)\\
%incSensedAvDelayOffset : \num\\ %(* ms *)\\
%tolSensedAvDelayOffset : \num\\ %(* ms *)\\
\where
sensedAvDelayOffset \in \{(OFF,0)\} \cup \{ n : \num | (\negate 10) \leq n \leq (\negate 100) @ (ON,n) \}\\
 %incSensedAvDelayOffset =\negate 10\\
 %tolSensedAvDelayOffset =1\\
\end{schema}

\begin{schema}{SensedAVDelayOffset'}
sensedAvDelayOffset' : SWITCHPARAMZ\\ %(* ms *)\\
%incSensedAvDelayOffset' : \num\\ %(* ms *)\\
%tolSensedAvDelayOffset' : \num\\ %(* ms *)\\
\end{schema}

\begin{schema}{APulseAmpRegulated}
 aPulseAmpRegulated : SWITCHPARAMZ\\ %(* Volts*1000 = mV *)\\
 %incAPulseAmpRegulated    : \num\\ %(* ms *)\\
 %tolAPulseAmpRegulated    : \num\\ %(* ms *)\\
\where
 aPulseAmpRegulated \in \{ (OFF,0) \} \cup \\
		\{ n:\num\\ | (500) \leq n \leq 3200 @ (ON, n) \} \cup \\
		\{ n:\num\\ | 3500 \leq n \leq 7000 @ (ON, n) \}\\
% \forall  n:SWITCHPARAMZ @ \\
%	(((ON,500).2 \leq n.2 \leq (ON,3200).2)\\
% \implies (%incAPulseAmpRegulated = 100))\\
% \lor (((ON,3500).2 \leq n.2 \leq (ON,7000).2)\\
% \implies (%incAPulseAmpRegulated = 500))\\
 %tolAPulseAmpRegulated = 12 \\
\end{schema}

\begin{schema}{APulseAmpRegulated'}
 aPulseAmpRegulated' : SWITCHPARAMZ\\ %(* Volts*1000 = mV *)\\
 %incAPulseAmpRegulated' : \num\\ %(* ms *)\\
 %tolAPulseAmpRegulated' : \num\\ %(* ms *)\\
\end{schema}

\begin{schema}{VPulseAmpRegulated}
 vPulseAmpRegulated : SWITCHPARAMZ; %(* Volts*1000 = mV *)\\
 %incVPulseAmpRegulated    : \num\\ %(* ms *)\\
 %tolVPulseAmpRegulated    : \num\\ %(* ms *)\\
\where
 vPulseAmpRegulated \in \{ (OFF,0) \} \cup \\
		\{ n:\num\\ | (500) \leq n \leq 3200 @ (ON, n) \} \cup \\
		\{ n:\num\\ | 3500 \leq n \leq 7000 @ (ON, n) \}\\
% \forall   vPulseAmpRegulated:SWITCHPARAMZ @ \\
%	(((ON,500).2 \leq vPulseAmpRegulated.2 \leq (ON,3200).2)\\
%   \implies (%incVPulseAmpRegulated = 100))\\
%  \lor (((ON,3500).2 \leq vPulseAmpRegulated.2 \leq (ON,7000).2)\\
% \implies (%incVPulseAmpRegulated = 500))\\
 %tolVPulseAmpRegulated = 12  \\
\end{schema}

\begin{schema}{VPulseAmpRegulated'}
 vPulseAmpRegulated' : SWITCHPARAMZ; %(* Volts*1000 = mV *)\\
 %incVPulseAmpRegulated' : \num\\ %(* ms *)\\
 %tolVPulseAmpRegulated' : \num\\ %(* ms *)\\
\end{schema}

\begin{schema}{APulseAmpUnregulated}
 aPulseAmpUnregulated  : SWITCHPARAMZ \\
 %(* Volts*1000 = mV *)\\
\where
 aPulseAmpUnregulated \in \{ (OFF,0) \} \cup \{ (ON,1250) \} \cup \{ (ON,2500) \}\\
      \cup \{ (ON,3750) \} \cup  \{ (ON,5000) \}\\
\end{schema}

\begin{schema}{APulseAmpUnregulated'}
 aPulseAmpUnregulated' : SWITCHPARAMZ \\
 %(* Volts*1000 = mV *)\\
\end{schema}

\begin{schema}{VPulseAmpUnregulated}
 vPulseAmpUnregulated  : SWITCHPARAMZ %(* Volts*1000 = mV *)\\
\where
 vPulseAmpUnregulated \in \{ (OFF,0) \} \cup \{ (ON,1250) \} \cup \{ (ON,2500) \}\\
      \cup \{ (ON,3750) \} \cup \{ (ON,5000) \}\\
\end{schema}

\begin{schema}{VPulseAmpUnregulated'}
 vPulseAmpUnregulated' : SWITCHPARAMZ %(* Volts*1000 = mV *)\\
\end{schema}

\begin{schema}{APulseWidth}
 aPulseWidth  : \num\\ %(* ms *)\\
 %incAPulseWidth : \num\\ %(* ms *)\\
 %tolAPulseWidth : \num\\ %(* ms *)\\
\where
 aPulseWidth \in \{ 5 \div 100 \} \cup \{ x:\num\\ | (1 \div 10) \leq x \leq (19 \div 10) @ x \}\\
% ((1 \div 10) \leq aPulseWidth \leq (19 \div 10)) \implies (%incAPulseWidth = (1 \div 10))\\
 %tolAPulseWidth = (2 \div 10)\\
\end{schema}

\begin{schema}{APulseWidth'}
 aPulseWidth' : \num\\ %(* ms *)\\
 %incAPulseWidth' : \num\\ %(* ms *)\\
 %tolAPulseWidth' : \num\\ %(* ms *)\\
\end{schema}

\begin{schema}{VPulseWidth}
 vPulseWidth  : \num\\ %(* ms *)\\
 %incVPulseWidth : \num\\ %(* ms *)\\
 %tolVPulseWidth : \num\\ %(* ms *)\\
\where
 vPulseWidth \in \{ 5 \div 100 \} \cup \{ x:\num\\ | (1 \div 10) \leq x \leq (19 \div 10) @ x \}\\
% ((1 \div 10) \leq vPulseWidth \leq (19 \div 10)) \implies (%incVPulseWidth = (1 \div 10))\\
 %tolVPulseWidth = (2 \div 10)\\
\end{schema}

\begin{schema}{VPulseWidth'}
 vPulseWidth' : \num\\ %(* ms *)\\
 %incVPulseWidth' : \num\\ %(* ms *)\\
 %tolVPulseWidth' : \num\\ %(* ms *)\\
\end{schema}

\begin{schema}{ASensitivity}
 aSensitivity  : \num\\ %(* mV *)\\
 %incASensitivity : \num\\ %(* ms *)\\
 %tolASensitivity : \num\\ %(* ms *)\\
\where
 aSensitivity \in \{ 25 \div 100 \} \cup \{ 5 \div 10 \} \cup \{ 75 \div 100 \} \cup \{ x:\num\\ | 1 \leq x \leq 10  @ x \}\\
% (1 \leq aSensitivity \leq 10) \implies (%incASensitivity = (5 \div 10))\\
 %tolASensitivity = 20\\
\end{schema}

\begin{schema}{ASensitivity'}
 aSensitivity' : \num\\ %(* mV *)\\
 %incASensitivity' : \num\\ %(* ms *)\\
 %tolASensitivity' : \num\\ %(* ms *)\\
\end{schema}

\begin{schema}{VSensitivity}
 vSensitivity  : \num\\ %(* mV *)\\
 %incVSensitivity : \num\\ %(* ms *)\\
 %tolVSensitivity : \num\\ %(* ms *)\\
\where
 vSensitivity \in \{ 25 \div 100 \} \cup \{ 5 \div 10 \} \cup \{ 75 \div 100 \} \cup \{ x:\num\\ | 1 \leq x \leq 10 @ x \}\\
% (1 \leq vSensitivity \leq 10) \implies (%incVSensitivity = (5 \div 10))\\
 %tolVSensitivity = 20\\
\end{schema}

\begin{schema}{VSensitivity'}
 vSensitivity' : \num\\ %(* mV *)\\
 %incVSensitivity' : \num\\ %(* ms *)\\
 %tolVSensitivity' : \num\\ %(* ms *)\\
\end{schema}

\begin{schema}{VRefractoryPeriod}
 vRefractPeriod  : \num\\ %(* ms *)\\
 %incVRefractPeriod : \num\\ %(* ms *)\\
 %tolVRefractPeriod : \num\\ %(* ms *)\\
\where
 vRefractPeriod \in 150 \upto  500\\
% (150 \leq vRefractPeriod \leq 500) \implies (%incVRefractPeriod = 10)\\
 %tolVRefractPeriod = 8\\
\end{schema}

\begin{schema}{VRefractoryPeriod'}
 vRefractPeriod' : \num\\ %(* ms *)\\
 %incVRefractPeriod' : \num\\ %(* ms *)\\
 %tolVRefractPeriod' : \num\\ %(* ms *)\\
\end{schema}

\begin{schema}{ARefractoryPeriod}
 aRefractPeriod  : \num\\ %(* ms *)\\
 %incARefractPeriod : \num\\ %(* ms *)\\
 %tolARefractPeriod : \num\\ %(* ms *)\\
\where
 aRefractPeriod \in 150 \upto  500\\
% (150 \leq aRefractPeriod \leq 500) \implies (%incARefractPeriod = 10)\\
 %tolARefractPeriod = 8\\
\end{schema}

\begin{schema}{ARefractoryPeriod'}
 aRefractPeriod' : \num\\ %(* ms *)\\
 %incARefractPeriod' : \num\\ %(* ms *)\\
 %tolARefractPeriod' : \num\\ %(* ms *)\\
\end{schema}

\begin{schema}{PVARP}
 pvarp : \num\\ %(* ms *)\\
 %incPvarp : \num\\ %(* ms *)\\
 %tolPvarp : \num\\ %(* ms *)\\
\where
 pvarp \in 150 \upto  500\\
% (150 \leq pvarp \leq 500) \implies (%incPvarp = 10)\\
 %tolPvarp = 8\\
\end{schema}

\begin{schema}{PVARP'}
 pvarp' : \num\\ %(* ms *)\\
 %incPvarp' : \num\\ %(* ms *)\\
 %tolPvarp' : \num\\ %(* ms *)\\
\end{schema}

\begin{schema}{PVARPExt}
 pvarpExt	: SWITCHPARAMZ; %(* ms *)\\
 %incPvarpExt 	 : \num\\ %(* ms *)\\
 %tolPvarpExt 	 : \num\\ %(* ms *)\\
\where
 pvarpExt \in \{ (OFF,0) \} \cup \{ n:\num\\ | 50 \leq n \leq 400 @ (ON, n) \}\\
% \forall   pvarpExt :SWITCHPARAMZ @ \\
%	(((ON,50).2 \leq pvarpExt.2 \leq (ON,400).2) \implies (%incPvarpExt = 50))\\
 %tolPvarpExt = 8\\
\end{schema}

\begin{schema}{PVARPExt'}
 pvarpExt' : SWITCHPARAMZ; %(* ms *)\\
 %incPvarpExt' : \num\\ %(* ms *)\\
 %tolPvarpExt' : \num\\ %(* ms *)\\
\end{schema}

\begin{schema}{HysterisisRateLimit}
 hysteresisRateLimit : SWITCHPARAMZ; %(* ppm *)\\
 %tolHysteresisRateLimit : \num\\ %(* ms *)\\
\where
 hysteresisRateLimit \in \{ (OFF, 0) \} \cup \{ n:\nat |  6 \leq n \leq 35 @ (ON, n) \}\\
 %tolHysteresisRateLimit = 8\\
\end{schema}

\begin{schema}{HysterisisRateLimit'}
 hysteresisRateLimit' : SWITCHPARAMZ; %(* ppm *)\\
 %tolHysteresisRateLimit' : \num\\ %(* ms *)\\
\end{schema}

\begin{schema}{RateSmoothing}
 rateSmoothing  : SWITCHPARAMZ; %(* percent *)\\
 %tolRateSmoothing   : \nat %(* percent *)\\
\where rateSmoothing \in \{ (OFF,0) \} \cup \{ (ON,3) \} \cup \{ (ON,6) \} \cup \{ (ON,9) \} \cup\\
 \{ (ON,12) \} \cup \{ (ON,15) \} \cup \{ (ON,18) \} \cup \{ (ON,21) \} \cup \{ (ON,25) \}\\
 %tolRateSmoothing = 8\\
\end{schema}

\begin{schema}{RateSmoothing'}
 rateSmoothing' : SWITCHPARAMZ; %(* percent *)\\
 %tolRateSmoothing' : \nat %(* percent *)\\
\end{schema}

\begin{schema}{ATRMode}
 atrMode	 : SWITCH %(*ON or OFF*)\\
\end{schema}

\begin{schema}{ATRMode'}
 atrMode	' : SWITCH %(*ON or OFF*)\\
\end{schema}

\begin{schema}{ATRDuration}
 atrDuration	 : \nat; %(* cc *)\\
 %incAtrDuration : \nat; %(* cc *)\\
 %tolAtrDuration : \nat %(* cc *)\\
\where
 atrDuration \in \{ 10 \} \cup \{ n:\nat | 20 \leq n \leq 80 @ n \} \cup \{ n:\nat | 100 \leq n \leq 2000 @ n \}\\
% (20 \leq atrDuration \leq 80) \implies (%incAtrDuration = 20)\\
% \land (100 \leq atrDuration \leq 2000) \implies (%incAtrDuration = 100)\\
 %tolAtrDuration = 1\\
\end{schema}

\begin{schema}{ATRDuration'}
 atrDuration	' : \nat; %(* cc *)\\
 %incAtrDuration' : \nat; %(* cc *)\\
 %tolAtrDuration' : \nat %(* cc *)\\
\end{schema}

\begin{schema}{ATRFallbackTime}
 atrFallbackTime    : \num\\ %(* min*60000 = ms *)\\
 %incAtrFallbackTime : \num\\ %(* min*60000 = ms *)\\
 %tolAtrFallbackTime : \nat %(* cc *)\\
\where
 atrFallbackTime \in \{ 1 * 60000 \} \cup \{ 2 * 60000 \} \cup \{ 3 * 60000 \} \cup \{ 4 * 60000 \} \cup \{ 5 * 60000 \}\\
 %incAtrFallbackTime = 60000\\
 %tolAtrFallbackTime = 1 \\
\end{schema}

\begin{schema}{ATRFallbackTime'}
 atrFallbackTime' : \num\\ %(* min*60000 = ms *)\\
 %incAtrFallbackTime' : \num\\ %(* min*60000 = ms *)\\
 %tolAtrFallbackTime' : \nat %(* cc *)\\
\end{schema}

\begin{schema}{VBlanking}
 vBlanking : \num\\ %(* ms *)\\
 %incBlanking : \num\\ %(* ms *)\\
\where
 vBlanking \in \{ n:\num\\ | 30 \leq n \leq 60 @ n \}\\
 %incBlanking = 10\\
\end{schema}

\begin{schema}{VBlanking'}
 vBlanking' : \num\\ %(* ms *)\\
 %incBlanking' : \num\\ %(* ms *)\\
\end{schema}

\begin{schema}{ActivityThreshold}
 actThreshold   : ACTIVITYTHRESHOLD\\
\end{schema}
\begin{schema}{ActivityThreshold'}
 actThreshold' : ACTIVITYTHRESHOLD\\
\end{schema}

\begin{schema}{ReactTime}
 reactionTime : \num\\ %(* sec*1000 = ms *)\\
 %incReactionTime : \num\\ %(* sec*1000 = ms *)\\
 %tolReactionTime : \num\\ %(* sec*1000 = ms *)\\
\where
 reactionTime \in 10000  \upto  50000\\
 %incReactionTime = 10000\\
 %tolReactionTime = 3\\
\end{schema}

\begin{schema}{ReactTime'}
 reactionTime' : \num\\ %(* sec*1000 = ms *)\\
 %incReactionTime' : \num\\ %(* sec*1000 = ms *)\\
 %tolReactionTime' : \num\\ %(* sec*1000 = ms *)\\
\end{schema}

\begin{schema}{RespFactor}
 responseFactor : \nat; %(* unit *)\\
 %incResponseFactor  : \nat %(* unit *)\\
\where
 responseFactor \in 1 \upto 16\\
 %incResponseFactor = 1\\
\end{schema}

\begin{schema}{RespFactor'}
 responseFactor' : \nat; %(* unit *)\\
 %incResponseFactor' : \nat %(* unit *)\\
\end{schema}

\begin{schema}{RecoveryTime}
 recoveryTime : \num\\ %(* min*60000 = ms *)\\
 %incRecoveryTime : \num\\ %(* sec*1000 = ms *)\\
 %tolRecoveryTime : \num\\ %(* sec*1000 = ms *)\\
\where
 recoveryTime \in \{ n:\num\\ | 2 \leq n \leq 16 @ n * 60000 \}\\
 %incRecoveryTime = 60000\\
 %tolRecoveryTime = 30000\\
\end{schema}

\begin{schema}{RecoveryTime'}
 recoveryTime' : \num\\ %(* min*60000 = ms *)\\
 %incRecoveryTime' : \num\\ %(* sec*1000 = ms *)\\
 %tolRecoveryTime' : \num\\ %(* sec*1000 = ms *)\\
\end{schema}

\begin{schema}{BradyState}
 bradycardiaState: BRADYCARDIASTATE\\
\end{schema}

\begin{schema}{BradyState'}
 bradycardiaState: BRADYCARDIASTATE\\
\end{schema}

%\begin{schema}{RateAdaptivePacing}
% Accelerometer\\
% ActivityThreshold\\
% LRL\\
% MaxSensorRate\\
% SensingPulse\\
% ReactTime\\
% RespFactor\\
% RecoveryTime\\
%\where
% \exists x: (\nat \cross ACTIVITYTHRESHOLD \cross \nat \cross \num\\ \cross \num\\)\\
%	@ x = RateAdaptAlgorithm(accelerometer, lowerRateLimit, maxSensorRate)\\		% \land x.2 = actThreshold \land reactionTime = x.4 \land responseFactor = x.3\\
%		 \land recoveryTime = x.5 \land currentRate = x.1\\
%\end{schema}

%(* Bradycardia Operation Mode *)

\begin{schema}{BOM}
 switch: SWITCH\\
 chambersPaced: CHAMBERS\\
 chambersSensed: CHAMBERS\\
 responseToSensing: RESPONSE\\
 rateModulation : BOOLEAN\\
\end{schema}

%(*Bradycardia Operation modes for Programmable Parameters*)


\begin{schema}{DDD}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 switch = ON\\
 chambersPaced = CDUAL\\
 chambersSensed = CDUAL\\
 responseToSensing = TRACKED\\
 rateModulation = FALSE
\end{schema}

\begin{schema}{VDD}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \}\\
 switch = ON\\
 chambersPaced = CVENTRICLE\\
 chambersSensed = CDUAL\\
 responseToSensing = TRACKED\\
 rateModulation = 
FALSE
\end{schema}

\begin{schema}{DDI}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\\
 switch = ON\\
 chambersPaced = CDUAL\\
 chambersSensed = CDUAL\\
 responseToSensing = INHIBITED\\
 rateModulation = 
FALSE\\
\end{schema}

\begin{schema}{VOO}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \}\\
 switch = ON\\
 chambersPaced = CVENTRICLE\\
 chambersSensed = CNONE\\
 responseToSensing = RNONE\\
 rateModulation =FALSE
\end{schema}

\begin{schema}{AOO}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \}\\
 switch = ON\\
 chambersPaced = CATRIUM\\
 chambersSensed = CNONE\\
 responseToSensing = RNONE\\
 rateModulation =FALSE
\end{schema}

\begin{schema}{DOO}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\\
 switch = ON\\
 chambersPaced = CDUAL\\
 chambersSensed = CNONE\\
 responseToSensing = RNONE\\
 rateModulation = FALSE
\end{schema}

\begin{schema}{VVI}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \}\\
 switch = ON\\
 chambersPaced = CVENTRICLE\\
 chambersSensed = CVENTRICLE\\
 responseToSensing = INHIBITED\\
 rateModulation = FALSE\\
 hysteresisRateLimit.1 = ON\\
\end{schema}

\begin{schema}{AAI}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \}\\
 switch = ON\\
 chambersPaced = CATRIUM\\
 chambersSensed = CATRIUM\\
 responseToSensing = INHIBITED\\
 rateModulation = 
FALSE\\
 hysteresisRateLimit.1 = ON\\
\end{schema}

\begin{schema}{VVT}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \}\\
 switch = ON\\
 chambersPaced = CVENTRICLE\\
 chambersSensed = CVENTRICLE\\
 responseToSensing = TRIGGERED\\
 rateModulation = 
FALSE\\
\end{schema}

\begin{schema}{AAT}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \}\\
 switch = ON\\
 chambersPaced = CATRIUM\\
 chambersSensed = CATRIUM\\
 responseToSensing = TRIGGERED\\
 rateModulation = 
FALSE\\
\end{schema}

\begin{schema}{DDDR}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\\
 switch = ON\\
 chambersPaced = CDUAL\\
 chambersSensed = CDUAL\\
 responseToSensing = TRACKED\\
 rateModulation = TRUE\\
\end{schema}

\begin{schema}{VDDR}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\\
 switch = ON\\
 chambersPaced = CVENTRICLE\\
 chambersSensed = CDUAL\\
 responseToSensing = TRACKED\\
 rateModulation = TRUE\\
\end{schema}

\begin{schema}{DDIR}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\\
 switch = ON\\
 chambersPaced = CDUAL\\
 chambersSensed = CDUAL\\
 responseToSensing = INHIBITED\\
 rateModulation = TRUE\\
\end{schema}

\begin{schema}{VOOR}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\\
 switch = ON\\
 chambersPaced = CVENTRICLE\\
 chambersSensed = CNONE\\
 responseToSensing = RNONE\\
 rateModulation = TRUE\\
\end{schema}

\begin{schema}{AOOR}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\\
 switch = ON\\
 chambersPaced = CATRIUM\\
 chambersSensed = CNONE\\
 responseToSensing = RNONE\\
 rateModulation = TRUE\\
\end{schema}
\begin{schema}{DOOR}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\\
 switch = ON\\
 chambersPaced = CDUAL\\
 chambersSensed = CNONE\\
 responseToSensing = RNONE\\
 rateModulation = TRUE\\
\end{schema}

\begin{schema}{VVIR}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\\
 switch = ON\\
 chambersPaced = CVENTRICLE\\
 chambersSensed = CVENTRICLE\\
 responseToSensing = INHIBITED\\
 rateModulation = TRUE\\
 hysteresisRateLimit.1 = ON\\
\end{schema}

\begin{schema}{AAIR}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\\
 switch = ON\\
 chambersPaced = CATRIUM\\
 chambersSensed = CATRIUM\\
 responseToSensing = INHIBITED\\
 rateModulation = TRUE\\
 hysteresisRateLimit.1 = ON\\
\end{schema}


	%(* Bradycardia Operation Modes only available in temporary operation*)

\begin{schema}{OVO}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 telemetryMode = ON\\
 switch = ON\\
 chambersPaced = CNONE\\
 chambersSensed = CVENTRICLE\\
 responseToSensing = RNONE\\
 rateModulation = 
FALSE\\
\end{schema}

\begin{schema}{OAO}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 telemetryMode = ON\\
 switch = ON\\
 chambersPaced = CNONE\\
 chambersSensed = CATRIUM\\
 responseToSensing = RNONE\\
 rateModulation = FALSE\\
\end{schema}

\begin{schema}{ODO}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 telemetryMode = ON\\
 switch = ON\\
 chambersPaced = CNONE\\
 chambersSensed = CDUAL\\
 responseToSensing = RNONE\\
 rateModulation = 
FALSE\\
\end{schema}

\begin{schema}{OOO}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 telemetryMode = ON\\
 switch = ON\\
 chambersPaced = CNONE\\
 chambersSensed = CNONE\\
 responseToSensing = RNONE\\
 rateModulation = FALSE
\end{schema}

\begin{schema}{BOMOFF}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 switch = OFF\\
\end{schema}

%(*Non rate-adaptive single chamber modes*)
\begin{zed}
 SCNRAMODE == VDD \lor VOO \lor AOO \lor VVI \lor AAI \lor VVT \lor AAT\\
\end{zed}

%(*Non rate-adaptive dual chamber modes*)

\begin{zed}
 DCNRAMODE == DDD  \lor DDI  \lor DOO\\
\end{zed}

%(*Non rate-adaptive modes is composed by non rate-adaptive single chamber modes and non rate-adaptive dual chamber modes*)

\begin{zed}
 NRAMODE == DCNRAMODE \lor SCNRAMODE\\
\end{zed}

%(*Rate-adaptive single chamber modes*)
\begin{zed}
 SCRAMODE == VDDR \lor VOOR \lor AOOR \lor VVIR \lor AAIR\\
\end{zed}

%(*Rate-adaptive dual chamber modes*)
\begin{zed}
 DCRAMODE == DDDR \lor DDIR \lor DOOR\\
\end{zed}

%(*Non rate-adaptive modes is composed by rate-adaptive single chamber modes and rate-adaptive dual chamber modes*)

\begin{zed}
 RAMODE == SCRAMODE \lor DCRAMODE\\
\end{zed}

%(*Bradycardia Operation Modes is composed by non rate-adaptive modes and rate-adaptive modes*)

\begin{zed}
 MODE ==  NRAMODE \lor RAMODE \lor BOMOFF\\
\end{zed}

%(*Bradycardia Operation Modes only for Temporary Bradycardia Pacing, it means that the pacemaker only senses the heart*)

\begin{zed}
 SENSEDONLY == OVO \lor OAO \lor ODO \lor OOO\\
\end{zed}

%(*Pace asynchronously with a fixed pacing rate*)
\begin{zed}
 PACEDONLY == VOO \lor AOO \lor DOO \lor OOO\\
\end{zed}

%(*BO MODE for Programmable Parameters*)

\begin{schema}{BOMODE}
 boMode : MODE\\
\end{schema}
\begin{schema}{BOMODE'}
 boMode' : MODE\\
\end{schema}

\begin{zed}
 ProgrammableParameters  == BOMODE\\
   \land LRL \\
   \land URL \\
   \land MaxSensorRate \\
   \land FixedAVDelay \\
   \land DynamicAVDelay \\
   \land MinDynamicAVDelay \\
   \land SensedAVDelayOffset \\
   \land APulseAmpRegulated \\
   \land VPulseAmpRegulated \\
   \land APulseAmpUnregulated \\
   \land VPulseAmpUnregulated \\
   \land APulseWidth \\
   \land VPulseWidth \\
   \land ASensitivity \\
   \land VSensitivity \\
   \land VRefractoryPeriod \\
   \land ARefractoryPeriod \\
   \land PVARP \\
   \land PVARPExt \\
   \land HysterisisRateLimit \\
   \land RateSmoothing \\
   \land ATRMode \\
   \land ATRDuration \\
   \land ATRFallbackTime \\
   \land VBlanking \\
   \land ActivityThreshold \\
   \land ReactTime \\
   \land RespFactor \\
   \land RecoveryTime \\
   \land BradyState\\
\end{zed}
\begin{zed}
 ProgrammableParameters' == BOMODE'\\
	\land LRL' \\
	\land URL' \\
	\land MaxSensorRate' \\
	\land FixedAVDelay' \\
	\land DynamicAVDelay' \\
	\land MinDynamicAVDelay' \\
	\land SensedAVDelayOffset' \\
	\land APulseAmpRegulated' \\
	\land VPulseAmpRegulated' \\
	\land APulseAmpUnregulated' \\
	\land VPulseAmpUnregulated' \\
	\land APulseWidth' \\
	\land VPulseWidth' \\
	\land ASensitivity' \\
	\land VSensitivity' \\
	\land VRefractoryPeriod' \\
	\land ARefractoryPeriod' \\
	\land PVARP' \\
	\land PVARPExt' \\
	\land HysterisisRateLimit' \\
	\land RateSmoothing' \\
	\land ATRMode' \\
	\land ATRDuration' \\
	\land ATRFallbackTime' \\
	\land VBlanking' \\
	\land ActivityThreshold' \\
	\land ReactTime' \\
	\land RespFactor' \\
	\land RecoveryTime' \\
	\land BradyState'\\
\end{zed}
%(*
3.7 - Magnet Test
*)

\begin{schema}{MagnetTest}
 magnetRate : \nat; %(* ppm *)\\
 magnetTest  : SWITCH %(*ON or OFF*)\\
\end{schema}

\begin{schema}{MagnetTest'}
 magnetRate' : \nat; %(* ppm *)\\
 magnetTest'  : SWITCH %(*ON or OFF*)\\
\end{schema}



%(*State of Device (or Pulse Generator)*)

\begin{zed}
 PulseGenerator ==\\ 
 	 MeasuredParameters\\
%	 \land Leads\\
%	 \land Accelerometer\\
%	 \land EventMarkers\\
%	 \land PacingPulse\\
	 \land SensingPulse\\
	 \land TimeSt\\
%	 \land BatteryStatus\\
%	 \land ImplantData \\
%	 \land TelemetrySession\\
%	 \land ProgrammableParameters\\
%		 \land MagnetTest\\
\end{zed}

\begin{zed}
 PulseGenerator' == MeasuredParameters'\\
%	 \land Leads'\\
%	 \land Accelerometer'\\
%	 \land EventMarkers'\\
%	 \land PacingPulse'\\
	 \land SensingPulse'\\
	 \land TimeSt'\\
%	 \land BatteryStatus'\\
%	 \land ImplantData'\\
%	 \land TelemetrySession'\\
	 \land ProgrammableParameters'\\
%		 \land MagnetTest'\\
\end{zed}

%\begin{zed}
% PG == [PGcomp : PulseGenerator]\\
%\end{zed}

%(* -Initialization *)
%(* B - Measured Parameters *)

\begin{schema}{PWaveInit}
 PWave'\\
\where
 pWave' = 0\\
\end{schema}

\begin{schema}{RWaveInit}
 RWave'\\
\where
 rWave' = 0\\
\end{schema}

\begin{schema}{BatteryVoltageInit}
 BatteryVoltage'\\
\end{schema}

\begin{schema}{LeadImpedanceInit}
 LeadImpedance'\\
\end{schema}

\begin{zed}
 MeasuredParametersInit == PWaveInit \land\\
  	RWaveInit \land\\
  	BatteryVoltageInit \land\\
  	LeadImpedanceInit\\
\end{zed}

%(*
3.3 - Lead Support
*)

\begin{schema}{LeadsInit}
 Leads'\\
\end{schema}

%(*
Event Markers
*)

\begin{schema}{EventMarkersInit}
 EventMarkers'\\
\end{schema}

%(*
3.4 - Pacing Pulse
*)

\begin{schema}{PacingPulseInit}
 PacingPulse'\\
\end{schema}

%(*
Sensing Pulse
*)

\begin{schema}{SensingPulseInit}
SensingPulse'\\
\end{schema}

\begin{schema}{TimeStInit}
 TimeSt'\\
\where
 time' = 0\\
\end{schema}

%(*
Accelerometer
*)

\begin{schema}{AccelerometerInit}
 Accelerometer'\\
\end{schema}
%(*
4.4 - Battery
*)

\begin{schema}{BatteryStatusInit}
 BatteryStatus'\\
\where
 battStatusLevel' = BOL\\
\end{schema}
%(*
3.8 - Implant Data
*)
\begin{schema}{ImplantDataInit}
 ImplantData'\\
\end{schema}

%(*
 - Telemetry
*)

\begin{schema}{TelemetrySessionInit}
 TelemetrySession'\\
\where
 telemetryMode' = OFF\\
\end{schema}

%(* A - Programable Parameters *)

\begin{schema}{BOMODEInit}
 BOMODE'\\
\end{schema}

\begin{schema}{LRLInit}
 LRL'\\
\end{schema}

\begin{schema}{URLInit}
 URL'\\
\end{schema}

\begin{schema}{MaxSensorRateInit}
 MaxSensorRate'\\
\end{schema}

\begin{schema}{FixedAVDelayInit}
 FixedAVDelay'\\
\end{schema}

\begin{schema}{DynamicAVDelayInit}
 DynamicAVDelay'\\
\end{schema}

\begin{schema}{MinDynamicAVDelayInit}
 MinDynamicAVDelay'\\
\end{schema}

\begin{schema}{SensedAVDelayOffsetInit}
 SensedAVDelayOffset'\\
\end{schema}

\begin{schema}{APulseAmpRegulatedInit}
 APulseAmpRegulated'\\
\end{schema}

\begin{schema}{VPulseAmpRegulatedInit}
 VPulseAmpRegulated'\\
\end{schema}

\begin{schema}{APulseAmpUnregulatedInit}
 APulseAmpUnregulated'\\
\end{schema}

\begin{schema}{VPulseAmpUnregulatedInit}
 VPulseAmpUnregulated'\\
\end{schema}

\begin{schema}{APulseWidthInit}
 APulseWidth'\\
\end{schema}

\begin{schema}{VPulseWidthInit}
 VPulseWidth'\\
\end{schema}

\begin{schema}{ASensitivityInit}
 ASensitivity'\\
\end{schema}

\begin{schema}{VSensitivityInit}
 VSensitivity'\\
\end{schema}

\begin{schema}{VRefractoryPeriodInit}
 VRefractoryPeriod'\\
\end{schema}

\begin{schema}{ARefractoryPeriodInit}
 ARefractoryPeriod'\\
\end{schema}

\begin{schema}{PVARPInit}
 PVARP'\\
\end{schema}

\begin{schema}{PVARPExtInit}
 PVARPExt'\\
\end{schema}

\begin{schema}{HysterisisRateLimitInit}
 HysterisisRateLimit'\\
\end{schema}

\begin{schema}{RateSmoothingInit}
 RateSmoothing'\\
\end{schema}

\begin{schema}{ATRModeInit}
 ATRMode'\\
\end{schema}

\begin{schema}{ATRDurationInit}
 ATRDuration'\\
\end{schema}

\begin{schema}{ATRFallbackTimeInit}
 ATRFallbackTime'\\
\end{schema}

\begin{schema}{VBlankingInit}
 VBlanking'\\
\end{schema}

\begin{schema}{ActivityThresholdInit}
 ActivityThreshold'\\
\end{schema}

\begin{schema}{ReactTimeInit}
 ReactTime'\\
\end{schema}

\begin{schema}{RespFactorInit}
 RespFactor'\\
\end{schema}

\begin{schema}{RecoveryTimeInit}
 RecoveryTime'\\
\end{schema}

\begin{schema}{BradyStateInit}
 BradyState'\\
\end{schema}

\begin{zed}
 ProgrammableParametersInit  == BOMODEInit \land\\
  LRLInit \land\\
  URLInit \land \\
  MaxSensorRateInit \land\\
  FixedAVDelayInit \land \\
  DynamicAVDelayInit \land \\
  MinDynamicAVDelayInit \land\\
  SensedAVDelayOffsetInit \land\\
  APulseAmpRegulatedInit \land\\
  VPulseAmpRegulatedInit \land\\
  APulseAmpUnregulatedInit \land \\
  VPulseAmpUnregulatedInit \land\\
  APulseWidthInit \land\\
  VPulseWidthInit \land\\
  ASensitivityInit \land\\
  VSensitivityInit \land\\
  VRefractoryPeriodInit \land\\
  ARefractoryPeriodInit \land\\
  PVARPInit \land\\
  PVARPExtInit \land \\
  HysterisisRateLimitInit \land\\
  RateSmoothingInit \land\\
  ATRModeInit \land\\
  ATRDurationInit \land\\
  ATRFallbackTimeInit \land\\
  VBlankingInit \land \\
  ActivityThresholdInit \land \\
  ReactTimeInit \land\\
  RespFactorInit \land\\
  RecoveryTimeInit \land\\
  BradyStateInit\\
\end{zed}


%(*
3.7 - Magnet Test
*)

\begin{schema}{MagnetTestInit}
 MagnetTest'\\
\where
 magnetRate' = 0\\
 magnetTest' = ON\\
\end{schema}

%(*PulseGeneratorInit*)

\begin{zed}
 PulseGeneratorInit == MeasuredParametersInit\\
			\land LeadsInit\\
			\land AccelerometerInit\\
			\land EventMarkersInit\\
			\land PacingPulseInit\\
			\land SensingPulseInit\\
			\land TimeStInit\\
			\land BatteryStatusInit\\
			\land ImplantDataInit \\
			\land TelemetrySessionInit\\
			\land ProgrammableParametersInit\\
			\land MagnetTestInit\\
\end{zed}

%(*\section{DCM State}*)

\begin{schema}{DCM}
 dcmModelNumber   : STRING\\
 dcmSoftwareRevNumber : STRING\\
 dcmSerialNumber  : STRING\\
 dcmInstName : STRING\\
 dcmDate : DATE\\
 dcmTime : TIME\\
\end{schema}

\begin{schema}{DCM'}
 dcmModelNumber' : STRING\\
 dcmSoftwareRevNumber' : STRING\\
 dcmSerialNumber' : STRING\\
 dcmInstName' : STRING\\
 dcmDate' : DATE\\
 dcmTime' : TIME\\
\end{schema}

\begin{schema}{DCMInit}
 DCM'\\
\end{schema}

%(*\section{Operations over Pulse Generator}*)


\begin{schema}{SetVDDok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in VDD) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \} \land\\
 (((lastSAPulse < (lastPVPulse)) \land
(lastSAPulse + fixedAvDelay = time) \land\\
(lastSVPulse < time)) \land
	((pacedVPulseAmp'= vPulseAmpRegulated.2) \land\\
(pacedVPulseWidth' = vPulseWidth) \land
(lastPVPulse' = time) \land \\
 ventricularMarker' =
ventricularMarker \cat \langle(VP,vPulseAmpRegulated.2,\\
		vPulseWidth, 0, time)\rangle) \lor\\
 ((lastSAPulse < (lastSVPulse)) \land
(lastSAPulse + fixedAvDelay = time) \land\\
(lastSVPulse < time)) \land
	((pacedVPulseAmp'= vPulseAmpRegulated.2) \land\\
(pacedVPulseWidth' = vPulseWidth) \land
(lastPVPulse' = time) \land \\
 ventricularMarker' = ventricularMarker \cat \langle(VP,vPulseAmpRegulated.2,\\
					vPulseWidth, 0, time)\rangle) \lor\\
 ((lastSAPulse > (lastSVPulse)) \land
(lastPVPulse + PulseWidth(lowerRateLimit) = time)) \land\\
	((pacedVPulseAmp'= vPulseAmpRegulated.2) \land
(pacedVPulseWidth' = vPulseWidth) \land\\
(lastPVPulse' = time) \land
 ventricularMarker' = ventricularMarker \cat \langle(VP,vPulseAmpRegulated.2,\\
					vPulseWidth, 0, time)\rangle) \lor\\
 ((lastSAPulse > (lastPVPulse)) \land
(lastPVPulse + PulseWidth(lowerRateLimit) = time)) \land\\
	((pacedVPulseAmp'= vPulseAmpRegulated.2) \land
(pacedVPulseWidth' = vPulseWidth) \land\\
(lastPVPulse' = time) \land \\
 ventricularMarker' = ventricularMarker \cat \langle(VP,vPulseAmpRegulated.2,\\
					vPulseWidth, 0, time)\rangle) \lor\\
 ((lastSAPulse < (lastPVPulse)) \land
(lastPVPulse + PulseWidth(lowerRateLimit) = time) \land\\
(lastPVPulse + fixedAvDelay > time)) \land
	((pacedVPulseAmp'= vPulseAmpRegulated.2) \land\\
(pacedVPulseWidth' = vPulseWidth) \land
(lastPVPulse' = time) \land \\
 ventricularMarker' = ventricularMarker \cat \langle(VP,vPulseAmpRegulated.2,\\
					vPulseWidth, 0, time)\rangle) \lor\\
 ((lastSAPulse < (lastSVPulse)) \land
(lastPVPulse + PulseWidth(lowerRateLimit) = time) \land\\
(lastPVPulse + fixedAvDelay > time)) \land
	((pacedVPulseAmp'= vPulseAmpRegulated.2) \land\\
(pacedVPulseWidth' = vPulseWidth) \land
(lastPVPulse' = time) \land \\
 ventricularMarker' = ventricularMarker \cat \langle(VP,vPulseAmpRegulated.2,\\
					vPulseWidth, 0, time)\rangle)) \land\\
 \theta(PulseGenerator \hide (ventricularMarker, pacedVPulseAmp,\\
 pacedVPulseWidth, lastPVPulse))'\\
 = \theta (PulseGenerator \hide (ventricularMarker, pacedVPulseAmp,\\
	 pacedVPulseWidth, lastPVPulse))\\
\end{schema}

\begin{schema}{SetVDDnok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in VDD) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \} \land\\
 \lnot(((lastSAPulse < (lastPVPulse)) \land\\
(lastSAPulse + fixedAvDelay = time) \land\\
(lastSVPulse < time)) \land\\
 ((lastSAPulse < (lastSVPulse)) \land\\
(lastSAPulse + fixedAvDelay = time) \land\\
(lastSVPulse < time)) \land\\
 ((lastSAPulse > (lastSVPulse)) \land\\
(lastPVPulse + PulseWidth(lowerRateLimit) = time)) \land\\
 ((lastSAPulse > (lastPVPulse)) \land\\
(lastPVPulse + PulseWidth(lowerRateLimit) = time)) \land\\
 ((lastSAPulse < (lastPVPulse)) \land\\
(lastPVPulse + PulseWidth(lowerRateLimit) = time) \land\\
(lastPVPulse + fixedAvDelay > time)) \land\\
 ((lastSAPulse < (lastSVPulse)) \land\\
(lastPVPulse + PulseWidth(lowerRateLimit) = time) \land\\
(lastPVPulse + fixedAvDelay > time))) \land\\
\theta (PulseGenerator)' = \theta (PulseGenerator)\\
\end{schema}

\begin{zed}
 SetVDD == SetVDDok \lor SetVDDnok\\
\end{zed}

\begin{schema}{SetDDIok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in DDI) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \} \land\\
 (((time - lastPVPulse) = (lastPVPulse + \\
				((PulseWidth(lowerRateLimit)) -\\
					fixedAvDelay)))\\
		\land ((pacedAPulseAmp' = aPulseAmpRegulated.2) \land\\
	  (pacedAPulseWidth' = aPulseWidth) \land\\
	  (lastPAPulse = time) \land\\
			atrialMarker' = \\
				atrialMarker \cat \langle(AP,aPulseAmpRegulated.2,\\
					aPulseWidth, 0,\\
					time)\rangle) \lor\\
 ((lastSAPulse > (lastPVPulse + pvarp)) \land\\
  (lastSAPulse < (lastPVPulse + ((PulseWidth(lowerRateLimit)) -\\
					fixedAvDelay))))\\
		\land atrialMarker' = atrialMarker \lor\\
 (((lastPVPulse + fixedAvDelay) = lastPAPulse) \land\\
	(lastSVPulse > lastPVPulse) \land\\
	(lastSVPulse < (lastPVPulse + PulseWidth(lowerRateLimit))))\\
		\land ventricularMarker' = ventricularMarker \land\\
 ((lastPAPulse = (lastPVPulse + ((PulseWidth(lowerRateLimit)) -\\
					fixedAvDelay))) \land\\
	(lastSVPulse < lastPVPulse) \land\\
	((lastPVPulse + (PulseWidth(lowerRateLimit))) = time))\\
		\land ((pacedVPulseAmp' = vPulseAmpRegulated.2) \land\\
	  (pacedVPulseWidth' = vPulseWidth) \land\\
	  (lastPVPulse = time) \land\\
		ventricularMarker' = \\
			ventricularMarker \cat \langle(VP,vPulseAmpRegulated.2,\\
						vPulseWidth, 0,\\
						time)\rangle) \lor\\
 ((lastSAPulse > (lastPVPulse + pvarp)) \land\\
  (lastSAPulse < (lastPVPulse + ((PulseWidth(lowerRateLimit)) -\\
					fixedAvDelay))) \land \\
  (lastSVPulse > lastPVPulse) \land\\
  (lastSVPulse < (lastPVPulse + ((PulseWidth(lowerRateLimit))))))\\
	\land ventricularMarker' = ventricularMarker) \land\\
\theta (PulseGenerator \hide (atrialMarker,ventricularMarker,\\
			pacedAPulseAmp, pacedAPulseWidth, lastPAPulse,\\
			pacedVPulseAmp, pacedVPulseWidth, lastPVPulse))' =\\
 \theta (PulseGenerator \hide (atrialMarker,ventricularMarker,\\
			pacedAPulseAmp, pacedAPulseWidth, lastPAPulse,\\
			pacedVPulseAmp, pacedVPulseWidth, lastPVPulse))\\
\end{schema}
\begin{schema}{SetDDInok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in DDI) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \} \land\\
\lnot((((time - lastPVPulse) = (lastPVPulse + \\
				((PulseWidth(lowerRateLimit)) -\\
					fixedAvDelay)))) \land\\
 ((lastSAPulse > (lastPVPulse + pvarp)) \land\\
  (lastSAPulse < (lastPVPulse + ((PulseWidth(lowerRateLimit)) -\\
					fixedAvDelay)))) \land\\
 (((lastPVPulse + fixedAvDelay) = lastPAPulse) \land\\
	(lastSVPulse > lastPVPulse) \land\\
	(lastSVPulse < (lastPVPulse + PulseWidth(lowerRateLimit))))\\
		\lor ventricularMarker' = ventricularMarker \land\\
 ((lastPAPulse = (lastPVPulse + ((PulseWidth(lowerRateLimit)) -\\
					fixedAvDelay))) \land\\
	(lastSVPulse < lastPVPulse) \land\\
	((lastPVPulse + (PulseWidth(lowerRateLimit))) = time))) \land\\
 ((lastSAPulse > (lastPVPulse + pvarp)) \land\\
  (lastSAPulse < (lastPVPulse + ((PulseWidth(lowerRateLimit)) -\\
					fixedAvDelay))) \land \\
  (lastSVPulse > lastPVPulse) \land\\
  (lastSVPulse < (lastPVPulse + ((PulseWidth(lowerRateLimit)))))) \land\\
\theta (PulseGenerator)' =\theta (PulseGenerator)\\
\end{schema}

\begin{zed}
 SetDDI == SetDDIok \lor SetDDInok\\
\end{zed}

\begin{schema}{SetVOOok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in VOO) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \} \land\\
 (((lastPVPulse + PulseWidth(lowerRateLimit)) = time) \land\\
(time - lastPVPulse) \geq  PulseWidth(upperRateLimit)) \land\\
	((pacedVPulseAmp' = vPulseAmpRegulated.2) \land\\
(pacedVPulseWidth' = vPulseWidth) \land\\
(lastPVPulse' = time) \land \\
 ventricularMarker' =\\
ventricularMarker \cat \langle(VP,vPulseAmpRegulated.2,\\
					vPulseWidth, 0,\\
					time)\rangle) \land\\
\theta (PulseGenerator \hide (ventricularMarker, pacedVPulseAmp, pacedVPulseWidth, lastPVPulse))'
   = \theta (PulseGenerator \hide (ventricularMarker, pacedVPulseAmp, pacedVPulseWidth, lastPVPulse))\\
\end{schema}

\begin{schema}{SetVOOnok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in VOO) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \} \land\\
\lnot(((lastPVPulse + PulseWidth(lowerRateLimit)) = time) \land\\
(time - lastPVPulse) \geq  PulseWidth(upperRateLimit)) \land\\
\theta (PulseGenerator)' =\theta (PulseGenerator)\\
\end{schema}
\begin{zed}
 SetVOO == SetVOOok \lor SetVOOnok\\
\end{zed}


\begin{schema}{SetAOOok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in AOO) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \} \land\\
 (((lastPAPulse + PulseWidth(lowerRateLimit)) = time) \land\\
(time - lastPAPulse) \\
				\geq  PulseWidth(upperRateLimit)) \land \\
	((pacedAPulseAmp' = aPulseAmpRegulated.2) \land\\
(pacedAPulseWidth' = aPulseWidth) \land\\
(lastPAPulse' = time) \land \\
 atrialMarker' =\\
atrialMarker \cat \langle(AP,aPulseAmpRegulated.2,\\
					aPulseWidth, 0,\\
					time)\rangle) \land\\
\theta (PulseGenerator \hide (atrialMarker,\\
			pacedAPulseAmp,\\
		pacedAPulseWidth,\\
		lastPAPulse))' =\\
 \theta (PulseGenerator \hide (atrialMarker,\\
			pacedAPulseAmp,\\
		pacedAPulseWidth,\\
		lastPAPulse))'\\
\end{schema}

\begin{schema}{SetAOOnok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in AOO) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \} \land\\
\lnot(((lastPAPulse + PulseWidth(lowerRateLimit)) = time) \land\\
(time - lastPAPulse) \\
				\geq  PulseWidth(upperRateLimit)) \land\\
\theta (PulseGenerator)' =\\
 \theta (PulseGenerator)\\
\end{schema}
\begin{zed}
 SetAOO == SetAOOok \lor SetAOOnok\\
\end{zed}

\begin{schema}{SetDOOok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in DOO) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \} \land\\
 (((lastPAPulse + PulseWidth(lowerRateLimit)) = time) \land\\
(time - lastPAPulse) \\
				\geq  PulseWidth(upperRateLimit))  \land \\
	((pacedAPulseAmp' = aPulseAmpRegulated.2) \land\\
(pacedAPulseWidth' = aPulseWidth) \land\\
(lastPAPulse' = time) \land\\
	(pacedVPulseAmp' = vPulseAmpRegulated.2) \land\\
(pacedVPulseWidth' = vPulseWidth) \land\\
(lastPVPulse' = time + fixedAvDelay) \land \\
 atrialMarker' =\\
atrialMarker \cat \langle(AP,aPulseAmpRegulated.2,\\
					aPulseWidth, 0,\\
					time)\rangle \land \\
 ventricularMarker' =\\
ventricularMarker \cat \langle(VP,vPulseAmpRegulated.2,\\
					vPulseWidth, 0,\\
					time + fixedAvDelay)\rangle) \land\\
\theta (PulseGenerator \hide (atrialMarker,ventricularMarker,\\
			pacedAPulseAmp,\\
		pacedAPulseWidth,\\
		lastPAPulse,\\
			pacedVPulseAmp,\\
		pacedVPulseWidth,\\
		lastPVPulse))' =\\
 \theta (PulseGenerator \hide (atrialMarker,ventricularMarker,\\
			pacedAPulseAmp,\\
		pacedAPulseWidth,\\
		lastPAPulse,\\
			pacedVPulseAmp,\\
		pacedVPulseWidth,\\
		lastPVPulse))\\
\end{schema}
\begin{schema}{SetDOOnok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in DOO) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \} \land\\
\lnot (((lastPAPulse + PulseWidth(lowerRateLimit)) = time) \land\\
(time - lastPAPulse) \\
				\geq  PulseWidth(upperRateLimit)) \land\\
\theta (PulseGenerator)' =\\
 \theta (PulseGenerator)\\
\end{schema}
\begin{zed}
 SetDOO == SetDOOok \lor SetDOOnok\\
\end{zed}

\begin{schema}{SetVVIok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in VVI) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \} \land\\
 ((((time - lastPVPulse) < (time - lastSVPulse)) \land\\
((lastPVPulse + PulseWidth(lowerRateLimit)) = time) \land\\
(lastPVPulse + PulseWidth(lowerRateLimit)) \\
				\geq  vRefractPeriod \land\\
((time - lastPVPulse)	\geq  PulseWidth(upperRateLimit))) \land \\
	((pacedVPulseAmp' = vPulseAmpRegulated.2) \land\\
(pacedVPulseWidth' = vPulseWidth) \land\\
(lastPVPulse' = time) \land \\
 ventricularMarker' =\\
ventricularMarker \cat \langle(VP,vPulseAmpRegulated.2,\\
					vPulseWidth, 0,\\
					time)\rangle) \lor\\
 (((time - lastPVPulse) > (time - lastSVPulse)) \land\\
((lastSVPulse + PulseWidth(hysteresisRateLimit.2)) = time) \land\\
(lastSVPulse + PulseWidth(hysteresisRateLimit.2)) \\
				\geq  vRefractPeriod \land\\
((time - lastSVPulse) \geq  PulseWidth(upperRateLimit))) \land \\
	((pacedVPulseAmp' = vPulseAmpRegulated.2) \land\\
(pacedVPulseWidth' = vPulseWidth) \land\\
(lastPVPulse' = time) \land \\
 ventricularMarker' =\\
ventricularMarker \cat \langle(VP,vPulseAmpRegulated.2,\\
					vPulseWidth, 0,\\
					time)\rangle)) \land\\
\theta (PulseGenerator \hide (ventricularMarker,\\
			pacedVPulseAmp,\\
		pacedVPulseWidth,\\
		lastPVPulse))' =\\
 \theta (PulseGenerator \hide (ventricularMarker,\\
			pacedVPulseAmp,\\
		pacedVPulseWidth,\\
		lastPVPulse))\\
\end{schema}

\begin{schema}{SetVVInok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in VVI) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \} \land\\
\lnot((((time - lastPVPulse) < (time - lastSVPulse)) \land\\
((lastPVPulse + PulseWidth(lowerRateLimit)) = time) \land\\
(lastPVPulse + PulseWidth(lowerRateLimit)) \\
				\geq  vRefractPeriod \land\\
((time - lastPVPulse)	\geq  PulseWidth(upperRateLimit))) \land\\
 (((time - lastPVPulse) > (time - lastSVPulse)) \land\\
((lastSVPulse + PulseWidth(hysteresisRateLimit.2)) = time) \land\\
(lastSVPulse + PulseWidth(hysteresisRateLimit.2)) \\
				\geq  vRefractPeriod\land\\
((time - lastSVPulse) \geq  PulseWidth(upperRateLimit)))) \land\\
\theta (PulseGenerator)' =\theta (PulseGenerator)\\
\end{schema}

\begin{zed}
 SetVVI == SetVVIok \lor SetVVInok\\
\end{zed}


\begin{schema}{SetAAIok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in AAI) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \} \land\\
 ((((time - lastPAPulse) < (time - lastSAPulse)) \land\\
((lastPAPulse + PulseWidth(lowerRateLimit)) = time) \land\\
(lastPAPulse + PulseWidth(lowerRateLimit)) \\
				\geq  aRefractPeriod \land\\
((time - lastPAPulse)	\geq  PulseWidth(upperRateLimit))) \land \\
	((pacedAPulseAmp' = aPulseAmpRegulated.2) \land\\
(pacedAPulseWidth' = aPulseWidth) \land\\
(lastPAPulse' = time) \land\\
 atrialMarker' =\\
atrialMarker \cat \langle(AP,aPulseAmpRegulated.2,\\
					aPulseWidth, 0,\\
					time)\rangle) \lor\\
 (((time - lastPAPulse) > (time - lastSAPulse)) \land\\
((lastSAPulse + PulseWidth(hysteresisRateLimit.2)) = time) \land\\
(lastPAPulse + PulseWidth(hysteresisRateLimit.2)) \\
				\geq  aRefractPeriod) \land \\
	((pacedAPulseAmp' = aPulseAmpRegulated.2) \land\\
(pacedAPulseWidth' = aPulseWidth) \land\\
(lastPAPulse' = time) \land\\
 atrialMarker' =\\
atrialMarker \cat \langle(AP,aPulseAmpRegulated.2,\\
					aPulseWidth, 0,\\
					time)\rangle)) \land\\
\theta (PulseGenerator \hide (atrialMarker,pacedAPulseAmp,\\
		pacedAPulseWidth,\\
		lastPAPulse))' =\\
 \theta (PulseGenerator \hide (atrialMarker,pacedAPulseAmp,\\
		pacedAPulseWidth,\\
		lastPAPulse)) \\
\end{schema}

\begin{schema}{SetAAInok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in AAI) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \} \land\\
\lnot((((time - lastPAPulse) < (time - lastSAPulse)) \land\\
((lastPAPulse + PulseWidth(lowerRateLimit)) = time) \land\\
(lastPAPulse + PulseWidth(lowerRateLimit)) \\
				\geq  aRefractPeriod \land\\
((time - lastPAPulse)	\geq  PulseWidth(upperRateLimit))) \land\\
 (((time - lastPAPulse) > (time - lastSAPulse)) \land\\
((lastSAPulse + PulseWidth(hysteresisRateLimit.2)) = time) \land\\
(lastPAPulse + PulseWidth(hysteresisRateLimit.2)) \\
				\geq  aRefractPeriod)) \land\\
\theta (PulseGenerator)' =\\
 \theta (PulseGenerator)\\
\end{schema}
\begin{zed}
 SetAAI == SetAAIok \lor SetAAInok\\
\end{zed}

\begin{schema}{SetVVTok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in VVT) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \} \land\\
 ((((lastPVPulse + PulseWidth(lowerRateLimit)) = time) \land\\
(lastPVPulse + PulseWidth(lowerRateLimit)) \\
				\geq  vRefractPeriod \land\\
(time - lastPVPulse)\\
				\geq  PulseWidth(upperRateLimit)) \land \\
	((pacedVPulseAmp' = vPulseAmpRegulated.2) \land\\
(pacedVPulseWidth' = vPulseWidth) \land\\
(lastPVPulse' = time) \land \\
 ventricularMarker' =\\
ventricularMarker \cat \langle(VP,vPulseAmpRegulated.2,\\
					vPulseWidth, 0,\\
					time)\rangle) \lor\\
 (((lastSVPulse + PulseWidth(lowerRateLimit)) > time) \land\\
(lastSVPulse + PulseWidth(lowerRateLimit)) \\
				\geq  vRefractPeriod \land\\
(lastSVPulse + PulseWidth(lowerRateLimit)) \\
				\geq  PulseWidth(upperRateLimit)) \land \\
	((pacedVPulseAmp' = vPulseAmpRegulated.2) \land\\
(pacedVPulseWidth' = vPulseWidth) \land\\
(lastPVPulse' = time) \land \\
 ventricularMarker' =\\
ventricularMarker \cat \langle(VP,vPulseAmpRegulated.2,\\
					vPulseWidth, 0,\\
					time)\rangle) \lor\\
 ((lastSVPulse = time) \land\\
(lastSVPulse + PulseWidth(lowerRateLimit)) \\
				\geq  vRefractPeriod) \land \\
	((pacedVPulseAmp' = vPulseAmpRegulated.2) \land\\
(pacedVPulseWidth' = vPulseWidth) \land\\
(lastPVPulse' = time) \land \\
 ventricularMarker' =\\
ventricularMarker \cat \langle(VP,vPulseAmpRegulated.2,\\
					vPulseWidth, 0,\\
					time)\rangle)) \land\\
\theta (PulseGenerator \hide (ventricularMarker,\\
			pacedVPulseAmp,\\
		pacedVPulseWidth,\\
		lastPVPulse))' =\\
 \theta (PulseGenerator \hide (ventricularMarker,\\
			pacedVPulseAmp,\\
		pacedVPulseWidth,\\
		lastPVPulse))\\
\end{schema}

\begin{schema}{SetVVTnok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in VVT) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \} \land\\
\lnot((((lastPVPulse + PulseWidth(lowerRateLimit)) = time) \land\\
(lastPVPulse + PulseWidth(lowerRateLimit)) \\
				\geq  vRefractPeriod \land\\
(time - lastPVPulse)\\
				\geq  PulseWidth(upperRateLimit)) \land\\
 (((lastSVPulse + PulseWidth(lowerRateLimit)) > time) \land\\
(lastSVPulse + PulseWidth(lowerRateLimit)) \\
				\geq  vRefractPeriod \land\\
(lastSVPulse + PulseWidth(lowerRateLimit)) \\
				\geq  PulseWidth(upperRateLimit)) \land\\
 ((lastSVPulse = time) \land\\
(lastSVPulse + PulseWidth(lowerRateLimit)) \\
				\geq  vRefractPeriod)) \land\\
\theta (PulseGenerator)' =\\
 \theta (PulseGenerator)\\
\end{schema}

\begin{zed}
 SetVVT == SetVVTok \lor SetVVTnok\\
\end{zed}

\begin{schema}{SetAATok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in AAT) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \} \land\\
 ((((lastPAPulse + PulseWidth(lowerRateLimit)) = time) \land\\
(lastPAPulse + PulseWidth(lowerRateLimit)) \\
				\geq  aRefractPeriod \land\\
(lastPVPulse + PulseWidth(lowerRateLimit)) \\
				\geq  PulseWidth(upperRateLimit)) \land \\
	((pacedAPulseAmp' = aPulseAmpRegulated.2) \land\\
(pacedAPulseWidth' = aPulseWidth) \land\\
(lastPAPulse' = time) \land \\
 atrialMarker' =\\
atrialMarker \cat \langle(AP,aPulseAmpRegulated.2,\\
					aPulseWidth, 0,\\
					time)\rangle) \lor\\
 (((lastSAPulse + PulseWidth(lowerRateLimit)) > time) \land\\
(lastSAPulse + PulseWidth(lowerRateLimit)) \\
				\geq  aRefractPeriod) \land \\
	((pacedAPulseAmp' = aPulseAmpRegulated.2) \land\\
(pacedAPulseWidth' = aPulseWidth) \land\\
(lastPAPulse' = time) \land \\
 atrialMarker' =\\
atrialMarker \cat \langle(AP,aPulseAmpRegulated.2,\\
					aPulseWidth, 0,\\
					time)\rangle) \lor\\
 ((lastSAPulse = time) \land\\
(lastSAPulse + PulseWidth(lowerRateLimit)) \\
				\geq  aRefractPeriod) \land \\
	((pacedAPulseAmp' = aPulseAmpRegulated.2) \land\\
(pacedAPulseWidth' = aPulseWidth) \land\\
(lastPAPulse' = time) \land \\
 atrialMarker' =\\
atrialMarker \cat \langle(AP,aPulseAmpRegulated.2,\\
					aPulseWidth, 0,\\
					time)\rangle)) \land\\
\theta (PulseGenerator \hide (atrialMarker,\\
			pacedAPulseAmp,\\
		pacedAPulseWidth,\\
		lastPAPulse))' =\\
 \theta (PulseGenerator \hide (atrialMarker,\\
			pacedAPulseAmp,\\
		pacedAPulseWidth,\\
		lastPAPulse))\\
\end{schema}

\begin{schema}{SetAATnok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in AAT) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \} \land\\
\lnot((((lastPAPulse + PulseWidth(lowerRateLimit)) = time) \land\\
(lastPAPulse + PulseWidth(lowerRateLimit)) \\
				\geq  aRefractPeriod \land\\
(lastPVPulse + PulseWidth(lowerRateLimit)) \\
				\geq  PulseWidth(upperRateLimit)) \land\\
 (((lastSAPulse + PulseWidth(lowerRateLimit)) > time) \land\\
(lastSAPulse + PulseWidth(lowerRateLimit)) \\
				\geq  aRefractPeriod) \lor\\
 ((lastSAPulse = time) \land\\
(lastSAPulse + PulseWidth(lowerRateLimit)) \\
				\geq  aRefractPeriod)) \land\\
\theta (PulseGenerator)' =\theta (PulseGenerator)\\
\end{schema}
\begin{zed}
 SetAAT == SetAATok \lor SetAATnok\\
\end{zed}

\begin{schema}{SetVDDRok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in VDDR) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \} \land\\
 (((lastSAPulse < (lastPVPulse)) \land\\
(lastSAPulse + fixedAvDelay = time) \land\\
(lastSVPulse < time)) \land\\
	((pacedVPulseAmp'= vPulseAmpRegulated.2) \land\\
(pacedVPulseWidth' = vPulseWidth) \land\\
(lastPVPulse' = time) \land \\
 ventricularMarker' =\\
ventricularMarker \cat \langle(VP,vPulseAmpRegulated.2,\\
					vPulseWidth, 0,\\
					time)\rangle) \lor\\
 ((lastSAPulse < (lastSVPulse)) \land\\
(lastSAPulse + fixedAvDelay = time) \land\\
(lastSVPulse < time)) \land\\
	((pacedVPulseAmp'= vPulseAmpRegulated.2) \land\\
(pacedVPulseWidth' = vPulseWidth) \land\\
(lastPVPulse' = time) \land \\
 ventricularMarker' =\\
ventricularMarker \cat \langle(VP,vPulseAmpRegulated.2,\\
					vPulseWidth, 0,\\
					time)\rangle) \lor\\
 ((lastSAPulse > (lastSVPulse)) \land\\
(lastPVPulse + PulseWidth(lowerRateLimit) = time)) \land\\
	((pacedVPulseAmp'= vPulseAmpRegulated.2) \land\\
(pacedVPulseWidth' = vPulseWidth) \land\\
(lastPVPulse' = time) \land \\
 ventricularMarker' =\\
ventricularMarker \cat \langle(VP,vPulseAmpRegulated.2,\\
					vPulseWidth, 0,\\
					time)\rangle) \lor\\
 ((lastSAPulse > (lastPVPulse)) \land\\
(lastPVPulse + PulseWidth(lowerRateLimit) = time)) \land\\
	((pacedVPulseAmp'= vPulseAmpRegulated.2) \land\\
(pacedVPulseWidth' = vPulseWidth) \land\\
(lastPVPulse' = time) \land \\
 ventricularMarker' =\\
ventricularMarker \cat \langle(VP,vPulseAmpRegulated.2,\\
					vPulseWidth, 0,\\
					time)\rangle) \lor\\
 ((lastSAPulse < (lastPVPulse)) \land\\
(lastPVPulse + PulseWidth(lowerRateLimit) = time) \land\\
(lastPVPulse + fixedAvDelay > time)) \land\\
	((pacedVPulseAmp'= vPulseAmpRegulated.2) \land\\
(pacedVPulseWidth' = vPulseWidth) \land\\
(lastPVPulse' = time) \land \\
 ventricularMarker' =\\
ventricularMarker \cat \langle(VP,vPulseAmpRegulated.2,\\
					vPulseWidth, 0,\\
					time)\rangle) \lor\\
 ((lastSAPulse < (lastSVPulse)) \land\\
(lastPVPulse + PulseWidth(lowerRateLimit) = time) \land\\
(lastPVPulse + fixedAvDelay > time)) \land\\
	((pacedVPulseAmp'= vPulseAmpRegulated.2) \land\\
(pacedVPulseWidth' = vPulseWidth) \land\\
(lastPVPulse' = time) \land \\
 ventricularMarker' =\\
ventricularMarker \cat \langle(VP,vPulseAmpRegulated.2,\\
					vPulseWidth, 0,\\
					time)\rangle)) \land\\
\theta (PulseGenerator \hide (ventricularMarker,\\
			pacedVPulseAmp,\\
		pacedVPulseWidth,\\
		lastPVPulse))' =\\
 \theta (PulseGenerator \hide (ventricularMarker,\\
			pacedVPulseAmp,\\
		pacedVPulseWidth,\\
		lastPVPulse))\\
\end{schema}

\begin{schema}{SetVDDRnok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in VDDR) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \} \land\\
\lnot(((lastSAPulse < (lastPVPulse)) \land\\
(lastSAPulse + fixedAvDelay = time) \land\\
(lastSVPulse < time)) \land\\
 ((lastSAPulse < (lastSVPulse)) \land\\
(lastSAPulse + fixedAvDelay = time) \land\\
(lastSVPulse < time)) \land\\
 ((lastSAPulse > (lastSVPulse)) \land\\
(lastPVPulse + PulseWidth(lowerRateLimit) = time)) \land\\
 ((lastSAPulse > (lastPVPulse)) \land\\
(lastPVPulse + PulseWidth(lowerRateLimit) = time)) \land\\
 ((lastSAPulse < (lastPVPulse)) \land\\
(lastPVPulse + PulseWidth(lowerRateLimit) = time) \land\\
(lastPVPulse + fixedAvDelay > time)) \land\\
 ((lastSAPulse < (lastSVPulse)) \land\\
(lastPVPulse + PulseWidth(lowerRateLimit) = time) \land\\
(lastPVPulse + fixedAvDelay > time))) \land\\
\theta (PulseGenerator)' =\theta (PulseGenerator)\\
\end{schema}
\begin{zed}
 SetVDDR == SetVDDRok \lor SetVDDRnok\\
\end{zed}

\begin{schema}{SetDDIRok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in DDIR) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \} \land\\
 (((time - lastPVPulse) = (lastPVPulse + \\
		((PulseWidth(lowerRateLimit)) -\\
		fixedAvDelay)))\\
		\land ((pacedAPulseAmp' = aPulseAmpRegulated.2) \land\\
	  (pacedAPulseWidth' = aPulseWidth) \land\\
	  (lastPAPulse = time) \land\\
			atrialMarker' = \\
				atrialMarker \cat \langle(AP,aPulseAmpRegulated.2,\\
					aPulseWidth, 0,\\
					time)\rangle) \land\\
 ((lastSAPulse > (lastPVPulse + pvarp)) \land\\
  (lastSAPulse < (lastPVPulse + ((PulseWidth(lowerRateLimit)) -\\
					fixedAvDelay))))\\
		\land atrialMarker' = atrialMarker \lor\\
 (((lastPVPulse + fixedAvDelay) = lastPAPulse) \land\\
	(lastSVPulse > lastPVPulse) \land\\
	(lastSVPulse < (lastPVPulse + PulseWidth(lowerRateLimit))))\\
		\land ventricularMarker' = ventricularMarker \lor\\
 ((lastPAPulse = (lastPVPulse + ((PulseWidth(lowerRateLimit)) -\\
					fixedAvDelay))) \land\\
	(lastSVPulse < lastPVPulse) \land\\
	((lastPVPulse + (PulseWidth(lowerRateLimit))) = time))\\
		\land ((pacedVPulseAmp' = vPulseAmpRegulated.2) \land\\
	  (pacedVPulseWidth' = vPulseWidth) \land\\
	  (lastPVPulse = time) \land\\
		ventricularMarker' = \\
			ventricularMarker \cat \langle(VP,vPulseAmpRegulated.2,\\
						vPulseWidth, 0,\\
						time)\rangle) \lor\\
 ((lastSAPulse > (lastPVPulse + pvarp)) \land\\
  (lastSAPulse < (lastPVPulse + ((PulseWidth(lowerRateLimit)) -\\
					fixedAvDelay))) \land \\
  (lastSVPulse > lastPVPulse) \land\\
  (lastSVPulse < (lastPVPulse + ((PulseWidth(lowerRateLimit))))))\\
	\land ventricularMarker' = ventricularMarker) \land\\
\theta (PulseGenerator \hide (atrialMarker,\\
			ventricularMarker,\\
			pacedVPulseAmp,\\
		pacedVPulseWidth,\\
		lastPVPulse,\\
			pacedAPulseAmp,\\
		pacedAPulseWidth,\\
		lastPAPulse))' =\\
 \theta (PulseGenerator \hide (atrialMarker,\\
			ventricularMarker,\\
			pacedVPulseAmp,\\
		pacedVPulseWidth,\\
		lastPVPulse,\\
			pacedAPulseAmp,\\
		pacedAPulseWidth,\\
		lastPAPulse))\\
\end{schema}

\begin{schema}{SetDDIRnok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in DDIR) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \} \land\\
\lnot((((time - lastPVPulse) = (lastPVPulse + \\
		((PulseWidth(lowerRateLimit)) -\\
		fixedAvDelay)))) \land\\
 ((lastSAPulse > (lastPVPulse + pvarp)) \land\\
  (lastSAPulse < (lastPVPulse + ((PulseWidth(lowerRateLimit)) -\\
					fixedAvDelay))))\land\\
 (((lastPVPulse + fixedAvDelay) = lastPAPulse) \land\\
	(lastSVPulse > lastPVPulse) \land\\
	(lastSVPulse < (lastPVPulse + PulseWidth(lowerRateLimit))))\land\\
 ((lastPAPulse = (lastPVPulse + ((PulseWidth(lowerRateLimit)) -\\
					fixedAvDelay))) \land\\
	(lastSVPulse < lastPVPulse) \land\\
	((lastPVPulse + (PulseWidth(lowerRateLimit))) = time))) \land\\
 ((lastSAPulse > (lastPVPulse + pvarp)) \land\\
  (lastSAPulse < (lastPVPulse + ((PulseWidth(lowerRateLimit)) -\\
					fixedAvDelay))) \land \\
  (lastSVPulse > lastPVPulse) \land\\
  (lastSVPulse < (lastPVPulse + ((PulseWidth(lowerRateLimit)))))) \land\\
\theta (PulseGenerator)' =\theta (PulseGenerator)\\
\end{schema}
\begin{zed}
 SetDDIR == SetDDIRok \lor SetDDIRnok\\
\end{zed}

\begin{schema}{SetVOORok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in VOOR) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \} \land\\
 (((lastPVPulse + PulseWidth(lowerRateLimit)) = time) \land\\
(time - lastPVPulse) \geq  PulseWidth(upperRateLimit)) \land \\
	((pacedVPulseAmp' = vPulseAmpRegulated.2) \land\\
(pacedVPulseWidth' = vPulseWidth) \land\\
(lastPVPulse' = time) \land \\
 ventricularMarker' =\\
ventricularMarker \cat \langle(VP,vPulseAmpRegulated.2,\\
					vPulseWidth, 0,\\
					time)\rangle) \land\\
\theta (PulseGenerator \hide (ventricularMarker,\\
			pacedVPulseAmp,\\
		pacedVPulseWidth,\\
		lastPVPulse))' =\\
 \theta (PulseGenerator \hide (ventricularMarker,\\
			pacedVPulseAmp,\\
		pacedVPulseWidth,\\
		lastPVPulse))\\
\end{schema}

\begin{schema}{SetVOORnok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in VOOR) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \} \land\\
\lnot(((lastPVPulse + PulseWidth(lowerRateLimit)) = time) \land\\
(time - lastPVPulse) \geq  PulseWidth(upperRateLimit)) \land\\
\theta (PulseGenerator)' =\\
 \theta (PulseGenerator)\\
\end{schema}
\begin{zed}
 SetVOOR == SetVOORok \lor SetVOORnok\\
\end{zed}

\begin{schema}{SetAOORok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in AOOR) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \} \land\\
 (((lastPAPulse + PulseWidth(lowerRateLimit)) = time) \land\\
(time - lastPAPulse) \\
				\geq  PulseWidth(upperRateLimit)) \land \\
	((pacedAPulseAmp' = aPulseAmpRegulated.2) \land\\
(pacedAPulseWidth' = aPulseWidth) \land\\
(lastPAPulse' = time) \land \\
 atrialMarker' =\\
atrialMarker \cat \langle(AP,aPulseAmpRegulated.2,\\
					aPulseWidth, 0,\\
					time)\rangle) \land\\
\theta(PulseGenerator \hide (atrialMarker,\\
			pacedAPulseAmp,\\
		pacedAPulseWidth,\\
		lastPAPulse))' =\\
\theta(PulseGenerator \hide (atrialMarker,\\
			pacedAPulseAmp,\\
		pacedAPulseWidth,\\
		lastPAPulse))\\
\end{schema}
\begin{schema}{SetAOORnok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in AOOR) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \} \land\\
\lnot(((lastPAPulse + PulseWidth(lowerRateLimit)) = time) \land\\
(time - lastPAPulse) \\
				\geq  PulseWidth(upperRateLimit)) \land\\
\theta (PulseGenerator)' =\theta (PulseGenerator)\\
\end{schema}
\begin{zed}
 SetAOOR == SetAOORok \lor SetAOORnok\\
\end{zed}

\begin{schema}{SetDOORok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in DOOR) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \} \land\\
 (((lastPAPulse + PulseWidth(lowerRateLimit)) = time) \land\\
(time - lastPAPulse) \\
				\geq  PulseWidth(upperRateLimit))  \land \\
	((pacedAPulseAmp' = aPulseAmpRegulated.2) \land\\
(pacedAPulseWidth' = aPulseWidth) \land\\
(lastPAPulse' = time) \land\\
	(pacedVPulseAmp' = vPulseAmpRegulated.2) \land\\
(pacedVPulseWidth' = vPulseWidth) \land\\
(lastPVPulse' = time + fixedAvDelay) \land \\
 atrialMarker' =\\
atrialMarker \cat \langle(AP,aPulseAmpRegulated.2,\\
					aPulseWidth, 0,\\
					time)\rangle \land \\
 ventricularMarker' =\\
ventricularMarker \cat \langle(VP,vPulseAmpRegulated.2,\\
					vPulseWidth, 0,\\
					time + fixedAvDelay)\rangle) \land\\
\theta (PulseGenerator \hide (atrialMarker,ventricularMarker,\\
			pacedAPulseAmp,\\
		pacedAPulseWidth,\\
		lastPAPulse,\\
			pacedVPulseAmp,\\
		pacedVPulseWidth,\\
		lastPVPulse))' =\\
 \theta (PulseGenerator \hide (atrialMarker,ventricularMarker,\\
			pacedAPulseAmp,\\
		pacedAPulseWidth,\\
		lastPAPulse,\\
			pacedVPulseAmp,\\
		pacedVPulseWidth,\\
		lastPVPulse))\\
\end{schema}
\begin{schema}{SetDOORnok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in DOOR) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \} \land\\
\lnot(((lastPAPulse + PulseWidth(lowerRateLimit)) = time) \land\\
(time - lastPAPulse) \\
				\geq  PulseWidth(upperRateLimit)) \land\\
\theta (PulseGenerator)' =\theta (PulseGenerator)\\
\end{schema}
\begin{zed}
 SetDOOR == SetDOORok \lor SetDOORnok\\
\end{zed}

\begin{schema}{SetVVIRok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in VVIR) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \} \land\\
( (((time - lastPVPulse) < (time - lastSVPulse)) \land\\
((lastPVPulse + PulseWidth(lowerRateLimit)) = time) \land\\
(lastPVPulse + PulseWidth(lowerRateLimit)) \\
				\geq  vRefractPeriod \land\\
((time - lastPVPulse)	\geq  PulseWidth(upperRateLimit))) \land\\
	((pacedVPulseAmp' = vPulseAmpRegulated.2) \land\\
(pacedVPulseWidth' = vPulseWidth) \land\\
(lastPVPulse' = time) \land \\
 ventricularMarker' =\\
ventricularMarker \cat \langle(VP,vPulseAmpRegulated.2,\\
					vPulseWidth, 0,\\
					time)\rangle) \lor\\
 (((time - lastPVPulse) > (time - lastSVPulse)) \land\\
((lastSVPulse + PulseWidth(hysteresisRateLimit.2)) = time) \land\\
(lastSVPulse + PulseWidth(hysteresisRateLimit.2)) \\
				\geq  vRefractPeriod \land\\
((time - lastSVPulse) \geq  PulseWidth(upperRateLimit))) \land \\
	((pacedVPulseAmp' = vPulseAmpRegulated.2) \land\\
(pacedVPulseWidth' = vPulseWidth) \land\\
(lastPVPulse' = time) \land \\
 ventricularMarker' =\\
ventricularMarker \cat \langle(VP,vPulseAmpRegulated.2,\\
					vPulseWidth, 0,\\
					time)\rangle)) \land\\
 \theta (PulseGenerator \hide (ventricularMarker,\\
			pacedVPulseAmp,\\
		pacedVPulseWidth,\\
		lastPVPulse))' =\\
 \theta (PulseGenerator \hide (ventricularMarker,\\
			pacedVPulseAmp,\\
		pacedVPulseWidth,\\
		lastPVPulse))\\
\end{schema}
\begin{schema}{SetVVIRnok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in VVIR) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \} \land\\
\lnot((((time - lastPVPulse) < (time - lastSVPulse)) \land\\
((lastPVPulse + PulseWidth(lowerRateLimit)) = time) \land\\
(lastPVPulse + PulseWidth(lowerRateLimit)) \\
				\geq  vRefractPeriod \land\\
((time - lastPVPulse)	\geq  PulseWidth(upperRateLimit)))) \land\\
 (((time - lastPVPulse) > (time - lastSVPulse)) \land\\
((lastSVPulse + PulseWidth(hysteresisRateLimit.2)) = time) \land\\
(lastSVPulse + PulseWidth(hysteresisRateLimit.2)) \\
				\geq  vRefractPeriod \land\\
((time - lastSVPulse) \geq  PulseWidth(upperRateLimit))) \land\\
 \theta (PulseGenerator)' =\theta (PulseGenerator)\\
\end{schema}
\begin{zed}
 SetVVIR == SetVVIRok \lor SetVVIRnok\\
\end{zed}

\begin{schema}{SetAAIRok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in AAIR) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \} \land\\
 (((time - lastPAPulse) < (time - lastSAPulse)) \land\\
((lastPAPulse + PulseWidth(lowerRateLimit)) = time) \land\\
(lastPAPulse + PulseWidth(lowerRateLimit)) \\
				\geq  aRefractPeriod \land\\
((time - lastPAPulse)	\geq  PulseWidth(upperRateLimit))) \land \\
	((pacedAPulseAmp' = aPulseAmpRegulated.2) \land\\
(pacedAPulseWidth' = aPulseWidth) \land\\
(lastPAPulse' = time) \land\\
 atrialMarker' =\\
atrialMarker \cat \langle(AP,aPulseAmpRegulated.2,\\
					aPulseWidth, 0,\\
					time)\rangle) \land\\
 (((time - lastPAPulse) > (time - lastSAPulse)) \land\\
((lastSAPulse + PulseWidth(hysteresisRateLimit.2)) = time) \land\\
(lastPAPulse + PulseWidth(hysteresisRateLimit.2)) \\
				\geq  aRefractPeriod) \land\\
	((pacedAPulseAmp' = aPulseAmpRegulated.2) \land\\
(pacedAPulseWidth' = aPulseWidth) \land\\
(lastPAPulse' = time) \land\\
 atrialMarker' =\\
atrialMarker \cat \langle(AP,aPulseAmpRegulated.2,\\
					aPulseWidth, 0,\\
					time)\rangle) \land\\
\theta (PulseGenerator \hide (atrialMarker,\\
			pacedAPulseAmp,\\
		pacedAPulseWidth,\\
		lastPAPulse))' =\\
 \theta (PulseGenerator \hide (atrialMarker,\\
			pacedAPulseAmp,\\
		pacedAPulseWidth,\\
		lastPAPulse))\\
\end{schema}
\begin{schema}{SetAAIRnok}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (boMode \in AAIR) \land\\
 battStatusLevel \in \{ BOL \}\cup\{ ERN \}\cup\{ ERT \}\cup\{ ERP \} \land\\
\lnot((((time - lastPAPulse) < (time - lastSAPulse)) \land\\
((lastPAPulse + PulseWidth(lowerRateLimit)) = time) \land\\
(lastPAPulse + PulseWidth(lowerRateLimit)) \\
				\geq  aRefractPeriod \land\\
((time - lastPAPulse)	\geq  PulseWidth(upperRateLimit))) \land \\
	((pacedAPulseAmp' = aPulseAmpRegulated.2) \land\\
(pacedAPulseWidth' = aPulseWidth) \land\\
(lastPAPulse' = time)) \land\\
 (((time - lastPAPulse) > (time - lastSAPulse)) \land\\
((lastSAPulse + PulseWidth(hysteresisRateLimit.2)) = time) \land\\
(lastPAPulse + PulseWidth(hysteresisRateLimit.2)) \\
				\geq  aRefractPeriod) \land\\
	((pacedAPulseAmp' = aPulseAmpRegulated.2) \land\\
(pacedAPulseWidth' = aPulseWidth) \land\\
(lastPAPulse' = time))) \land\\
\theta (PulseGenerator)' =\theta (PulseGenerator)\\
\end{schema}

\begin{zed}
 SetAAIR == SetAAIRok \lor SetAAIRnok\\
\end{zed}


\begin{zed}
 SetMode == (SetVDD \lor SetDDI \lor \\
		SetVOO \lor SetAOO \lor \\
		SetDOO \lor SetVVI \lor \\
		SetAAI \lor SetVVT \lor \\
		SetAAT \lor SetVDDR \lor \\
		SetDDIR \lor SetVOOR \lor \\
		SetAOOR \lor SetDOOR \lor \\
		SetVVIR \lor SetAAIR)\\
\end{zed}

\begin{schema}{SetTimer}
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
time' = time + 1 \land\\
	\theta(PulseGenerator \hide (time))' \\
		=\theta (PulseGenerator \hide  (time))\\
\end{schema}


%(*Get Pulse Amplitudes - Measured Parameters*)
\begin{schema}{GetPWave}
% \Delta PulseGenerator\\
% \Delta PG\\
devicePWave? : \num\\
 \Delta PulseGenerator\\
% \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
pWave' = devicePWave?\\
	\theta(PulseGenerator \hide (pWave))' \\
		=\theta (PulseGenerator \hide (pWave))\\
\end{schema}

\begin{schema}{GetRWave}
 \Delta PulseGenerator\\
% \Delta PG\\
 deviceRWave? : \num\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
rWave' = deviceRWave?\\
	\theta(PulseGenerator \hide (rWave))' \\
		=\theta (PulseGenerator \hide (rWave))\\
\end{schema}


\begin{schema}{GetBatteryVoltage}
 \Delta PulseGenerator\\
% \Delta PG\\
 deviceBatteryVoltage? : \num\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator'@\\
batteryVoltage' = deviceBatteryVoltage?\\
	\theta(PulseGenerator \hide (batteryVoltage))' \\
		=\theta (PulseGenerator \hide (batteryVoltage))\\
\end{schema}
\begin{schema}{GetLeadImpedance}
 \Delta PulseGenerator\\
% \Delta PG\\
 deviceLeadImpedance? : \num\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
leadImpedance' = deviceLeadImpedance?\\
	\theta(PulseGenerator \hide (batteryVoltage))' \\
		=\theta (PulseGenerator \hide (batteryVoltage))\\
\end{schema}

%(*Output Pulse*)
\begin{schema}{OutPacedPWave}
 \Xi PulseGenerator\\
% \Xi PG\\
 outPacedAPulseWidth! : \num\\ %(* ms *)\\
 outPacedAPulseAmp!    : \num\\ %(* mV *)\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
outPacedAPulseWidth! = pacedAPulseWidth\\
outPacedAPulseAmp! = pacedAPulseAmp \\
\end{schema}

\begin{schema}{OutPacedRWave}
 \Xi PulseGenerator\\
% \Xi PG\\
 outPacedVPulseWidth! : \num\\ %(* ms *)\\
 outPacedVPulseAmp!    : \num\\ %(* mV *)\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
outPacedVPulseWidth! = pacedVPulseWidth\\
outPacedVPulseAmp! = pacedVPulseAmp \\
\end{schema}

%(*Set Programmable Parameters*)
% there are operations to set the value of each parameter
\begin{schema}{SetProgParam}
 \Delta PulseGenerator\\
% \Delta PG\\
 progLowerRateLimit? : \nat; %(* ppm *)\\
 progUpperRateLimit? : \nat; %(* ppm *)\\
 progMaxSensorRate? : \nat; %(* ppm *)\\
 progFixedAvDelay? : \num\\ %(* ms *)\\
 progDynamicAvDelay?: SWITCH; %(*ON or OFF*)\\
 progMinDynAvDelay? : \num\\ %(* ms *)\\
 progSensedAvDelayOffset? : SWITCHPARAMZ; %(* ms *)\\
 progAPulseAmpRegulated?  : SWITCHPARAMZ\\
 progVPulseAmpRegulated?  : SWITCHPARAMZ\\
 progAPulseAmpUnregulated?: SWITCHPARAMZ \\
 progVPulseAmpUnregulated?: SWITCHPARAMZ\\
 progAPulseWidth? : \num\\ %(* ms *)\\
 progVPulseWidth? : \num\\ %(* ms *)\\
 progASensitivity? : \num\\ %(* mV *)\\
 progVSensitivity? : \num\\ %(* mV *)\\
 progVRefractPeriod? : \num\\ %(* ms *)\\
 progARefractPeriod? : \num\\ %(* ms *)\\
 progPvarp? : \num\\ %(* ms *)\\
 progPvarpExt?: SWITCHPARAMZ; %(* ms *)\\
 progHysteresisRateLimit?: SWITCHPARAMZ; %(* ppm *)\\
 progRateSmoothing?: SWITCHPARAMZ; %(* percent *)\\
 progAtrMode?	: SWITCH; %(*ON or OFF*)\\
 progAtrDuration? : \nat; %(* cc *)\\
 progAtrFallbackTime?  : \num\\ %(* min*60000 = ms *)\\
 progVBlanking	? : \num\\ %(* ms *)\\
 progActThreshold?: ACTIVITYTHRESHOLD\\
 progReactionTime ? : \num\\ %(* sec*1000 = ms *)\\
 progResponseFactor? : \nat; %(* unit *)\\
 progRecoveryTime? : \num\\ %(* min*60000 = ms *)\\
 progBradycardiaState?: BRADYCARDIASTATE\\
 progBoMode?  : MODE\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
lowerRateLimit' = progLowerRateLimit? \land\\
upperRateLimit' = progUpperRateLimit? \land\\
maxSensorRate' = progMaxSensorRate? \land\\
fixedAvDelay' = progFixedAvDelay? \land\\
dynamicAvDelay'	= progDynamicAvDelay? \land\\
minDynAvDelay'	= progMinDynAvDelay? \land\\
sensedAvDelayOffset' = progSensedAvDelayOffset? \land\\
aPulseAmpRegulated' = progAPulseAmpRegulated? \land\\
vPulseAmpRegulated' = progVPulseAmpRegulated? \land\\
aPulseAmpUnregulated' = progAPulseAmpUnregulated? \land\\
vPulseAmpUnregulated' = progVPulseAmpUnregulated? \land\\
aPulseWidth' = progAPulseWidth? \land\\
vPulseWidth' = progVPulseWidth? \land\\
aSensitivity' = progASensitivity? \land\\
vSensitivity' = progVSensitivity? \land\\
vRefractPeriod' = progVRefractPeriod? \land\\
aRefractPeriod' = progARefractPeriod? \land\\
pvarp' = progPvarp? \land\\
pvarpExt' = progPvarpExt? \land\\
hysteresisRateLimit' = progHysteresisRateLimit? \land\\
rateSmoothing' = progRateSmoothing? \land\\
atrMode' = progAtrMode? \land\\
atrDuration' = progAtrDuration? \land\\
atrFallbackTime' = progAtrFallbackTime? \land\\
vBlanking' = progVBlanking	? \land\\
actThreshold' = progActThreshold? \land\\
reactionTime' = progReactionTime? \land\\
responseFactor' = progResponseFactor? \land\\
recoveryTime' = progRecoveryTime? \land\\
bradycardiaState' = progBradycardiaState? \land\\
boMode' = progBoMode? \land\\
	\theta(PulseGenerator \hide (lowerRateLimit,\\
				upperRateLimit,\\
				maxSensorRate,\\
				fixedAvDelay,\\
				dynamicAvDelay,\\
				minDynAvDelay,\\
				sensedAvDelayOffset,\\
				aPulseAmpRegulated,\\
				vPulseAmpRegulated,\\
				aPulseAmpUnregulated,\\
				vPulseAmpUnregulated,\\
				aPulseWidth,\\
				vPulseWidth,\\
				aSensitivity,\\
				vSensitivity,\\
				vRefractPeriod,\\
				aRefractPeriod,\\
				pvarp,\\
				pvarpExt,\\
				hysteresisRateLimit,\\
				rateSmoothing,\\
				atrMode,\\
				atrDuration,\\
				atrFallbackTime,\\
				vBlanking,\\
				actThreshold,\\
				reactionTime,\\
				responseFactor,\\
				recoveryTime,\\
				bradycardiaState,\\
				boMode))' \\
		=\theta (PulseGenerator \hide (lowerRateLimit,\\
				upperRateLimit,\\
				maxSensorRate,\\
				fixedAvDelay,\\
				dynamicAvDelay,\\
				minDynAvDelay,\\
				sensedAvDelayOffset,\\
				aPulseAmpRegulated,\\
				vPulseAmpRegulated,\\
				aPulseAmpUnregulated,\\
				vPulseAmpUnregulated,\\
				aPulseWidth,\\
				vPulseWidth,\\
				aSensitivity,\\
				vSensitivity,\\
				vRefractPeriod,\\
				aRefractPeriod,\\
				pvarp,\\
				pvarpExt,\\
				hysteresisRateLimit,\\
				rateSmoothing,\\
				atrMode,\\
				atrDuration,\\
				atrFallbackTime,\\
				vBlanking,\\
				actThreshold,\\
				reactionTime,\\
				responseFactor,\\
				recoveryTime,\\
				bradycardiaState,\\
				boMode))\\
\end{schema}

%(*4.6 Histograms*)

\begin{schema}{AtrialPacedEventHistogram}
 \Xi PulseGenerator\\
% \Xi PG\\
atrialPaced! : HISTOGRAMS\\
\where
 \exists  PulseGenerator@\\
 atrialPaced! = \langle(30,40,CountEvents(AP,30,40))\rangle\\
		\cat \langle(40,50,CountEvents(AP,40,50))\rangle\\
		\cat \langle(50,60,CountEvents(AP,50,60))\rangle\\
		\cat \langle(60,70,CountEvents(AP,60,70))\rangle\\
		\cat \langle(70,80,CountEvents(AP,70,80))\rangle\\
		\cat \langle(80,90,CountEvents(AP,80,90))\rangle\\
		\cat \langle(90,100,CountEvents(AP,90,100))\rangle\\
		\cat \langle(100,110,CountEvents(AP,100,110))\rangle\\
		\cat \langle(110,120,CountEvents(AP,110,120))\rangle\\
		\cat \langle(120,130,CountEvents(AP,120,130))\rangle\\
		\cat \langle(130,140,CountEvents(AP,130,140))\rangle\\
		\cat \langle(140,150,CountEvents(AP,140,150))\rangle\\
		\cat \langle(150,160,CountEvents(AP,150,160))\rangle\\
		\cat \langle(160,170,CountEvents(AP,160,170))\rangle\\
		\cat \langle(170,175,CountEvents(AP,170,175))\rangle\\
\end{schema}

\begin{schema}{AtrialSensedEventHistogram}
 \Xi PulseGenerator\\
% \Xi PG\\
 atrialSensed! : HISTOGRAMS\\
\where
 \exists  PulseGenerator@\\
 atrialSensed! = \langle(30,40,CountEvents(AS,30,40))\rangle\\
		\cat \langle(40,50,CountEvents(AS,40,50))\rangle\\
		\cat \langle(50,60,CountEvents(AS,50,60))\rangle\\
		\cat \langle(60,70,CountEvents(AS,60,70))\rangle\\
		\cat \langle(70,80,CountEvents(AS,70,80))\rangle\\
		\cat \langle(80,90,CountEvents(AS,80,90))\rangle\\
		\cat \langle(90,100,CountEvents(AS,90,100))\rangle\\
		\cat \langle(100,110,CountEvents(AS,100,110))\rangle\\
		\cat \langle(110,120,CountEvents(AS,110,120))\rangle\\
		\cat \langle(120,130,CountEvents(AS,120,130))\rangle\\
		\cat \langle(130,140,CountEvents(AS,130,140))\rangle\\
		\cat \langle(140,150,CountEvents(AS,140,150))\rangle\\
		\cat \langle(150,160,CountEvents(AS,150,160))\rangle\\
		\cat \langle(160,170,CountEvents(AS,160,170))\rangle\\
		\cat \langle(170,175,CountEvents(AS,170,175))\rangle\\
\end{schema}

\begin{schema}{VentricularPacedEventHistogram}
 \Xi PulseGenerator\\
% \Xi PG\\
 ventricularPaced! : HISTOGRAMS\\
\where
 \exists  PulseGenerator@\\
 ventricularPaced! = \langle(30,40,CountEvents(VP,30,40))\rangle\\
		\cat \langle(40,50,CountEvents(VP,40,50))\rangle\\
		\cat \langle(50,60,CountEvents(VP,50,60))\rangle\\
		\cat \langle(60,70,CountEvents(VP,60,70))\rangle\\
		\cat \langle(70,80,CountEvents(VP,70,80))\rangle\\
		\cat \langle(80,90,CountEvents(VP,80,90))\rangle\\
		\cat \langle(90,100,CountEvents(VP,90,100))\rangle\\
		\cat \langle(100,110,CountEvents(VP,100,110))\rangle\\
		\cat \langle(110,120,CountEvents(VP,110,120))\rangle\\
		\cat \langle(120,130,CountEvents(VP,120,130))\rangle\\
		\cat \langle(130,140,CountEvents(VP,130,140))\rangle\\
		\cat \langle(140,150,CountEvents(VP,140,150))\rangle\\
		\cat \langle(150,160,CountEvents(VP,150,160))\rangle\\
		\cat \langle(160,170,CountEvents(VP,160,170))\rangle\\
		\cat \langle(170,175,CountEvents(VP,170,175))\rangle\\
\end{schema}

\begin{schema}{VentricularSensedEventHistogram}
 \Xi PulseGenerator\\
% \Xi PG\\
 ventricularSensed! : HISTOGRAMS\\
\where
 \exists  PulseGenerator@\\
 ventricularSensed! = \langle(30,40,CountEvents(VS,30,40))\rangle\\
		\cat \langle(40,50,CountEvents(VS,40,50))\rangle\\
		\cat \langle(50,60,CountEvents(VS,50,60))\rangle\\
		\cat \langle(60,70,CountEvents(VS,60,70))\rangle\\
		\cat \langle(70,80,CountEvents(VS,70,80))\rangle\\
		\cat \langle(80,90,CountEvents(VS,80,90))\rangle\\
		\cat \langle(90,100,CountEvents(VS,90,100))\rangle\\
		\cat \langle(100,110,CountEvents(VS,100,110))\rangle\\
		\cat \langle(110,120,CountEvents(VS,110,120))\rangle\\
		\cat \langle(120,130,CountEvents(VS,120,130))\rangle\\
		\cat \langle(130,140,CountEvents(VS,130,140))\rangle\\
		\cat \langle(140,150,CountEvents(VS,140,150))\rangle\\
		\cat \langle(150,160,CountEvents(VS,150,160))\rangle\\
		\cat \langle(160,170,CountEvents(VS,160,170))\rangle\\
		\cat \langle(170,175,CountEvents(VS,170,175))\rangle\\
\end{schema}

\begin{schema}{PVCEventHistogram}
 \Xi PulseGenerator\\
% \Xi PG\\
 pvc! : \nat\\
\where
 \exists  PulseGenerator @\\
 pvc! = \#(ventricularMarker \filter \{ width:\num\\ \\
	 amp : \num\\ noise:\num\\ t:\num\\
	 @ (PVC, width, amp, noise, t) \})\\
\end{schema}

\begin{schema}{AtrialTachycardiaEventHistogram}
 \Xi PulseGenerator\\
% \Xi PG\\
 atrialTachy! : \nat\\
\where
 \exists  PulseGenerator@\\
 atrialTachy! = \#(atrialMarker \filter \{ width:\num\\ \\
	 amp : \num\\ noise:\num\\ t:\num\\
	 @ (AT, width, amp, noise, t) \})\\
\end{schema}


%(*2.5.1 - Implant (page 12)*)
%(*1 - Interrogate the system*)

\begin{schema}{IntAtrialPaceLead}
 \Xi PulseGenerator\\
% \Xi PG\\
 atrialPaceLeadPolarity!  : POLARITY\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 bradycardiaState = TEMPORARY \land\\
 telemetryMode = ON \land\\
 atrialPaceLeadPolarity! = aLeadPolarity\\
\end{schema}

\begin{schema}{IntVentricularPaceLead}
 \Xi PulseGenerator\\
% \Xi PG\\
 ventricularPaceLeadPolarity!  : POLARITY\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 bradycardiaState = TEMPORARY \land\\
 telemetryMode = ON \land\\
 ventricularPaceLeadPolarity! = vLeadPolarity\\
\end{schema}

\begin{zed}
 InterrogateLeadPolarity == IntAtrialPaceLead \land\\
	 IntVentricularPaceLead		\\
\end{zed}

\begin{schema}{InterrogateImplantData}
 \Xi PulseGenerator\\
% \Xi PG\\
 intDeviceSerial! : STRING\\
 intDeviceModel! : STRING\\
 intPaceThreshold! : \num\\ %(* Volts*1000 = mV *)\\
 intIndicationForPacing! : STRING\\
 intImplantDeviceDate!: DATE\\
 intImplantLeadDate! : DATE\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 bradycardiaState = TEMPORARY \land\\
 telemetryMode = ON \land\\
 intDeviceSerial! = deviceSerial \land\\
 intDeviceModel! = deviceModel \land\\
 intPaceThreshold! = paceThreshold \land\\
 intIndicationForPacing! = indicationForPacing \land\\
 intImplantDeviceDate! = implantDeviceDate \land\\
 intImplantLeadDate! = implantLeadDate\\
\end{schema}

\begin{zed}
 ImplantInterrogatePG == InterrogateLeadPolarity \land		 \\
	 InterrogateImplantData		 \\
\end{zed}


%(*2 - Review battery status*)

\begin{schema}{ReviewBattStatus}
 \Xi PulseGenerator\\
% \Xi PG\\
 batteryStatus!: BATTSTATUSLEVEL\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 bradycardiaState = TEMPORARY \land\\
 telemetryMode = ON \land\\
 batteryStatus! = battStatusLevel\\
\end{schema}

%(*6 - Evaluate ventricular and atrial lead signal ampitudes, impedances, and pacing thresholds.*)

\begin{schema}{ImplantEvaluateSignal}
 \Xi PulseGenerator\\
% \Xi PG\\
 intALeadSignalAmplitude!  : \num\\ %(* Volts*1000 = mV *)\\
 intVLeadSignalAmplitude!  : \num\\ %(* Volts*1000 = mV *)\\
 intLeadImpedance!  : \num\\
 intPaceThreshold!  : \num\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 bradycardiaState = TEMPORARY \land\\
 telemetryMode = ON \land\\
 intLeadImpedance! = leadImpedance \land\\
 intALeadSignalAmplitude! = pWave \land\\
 intVLeadSignalAmplitude! = rWave \land\\
 intPaceThreshold! = paceThreshold\\
\end{schema}

%(*2.5.3 - Predischarge Follow-Up*)
%(*1 - Interrogating the device and obtaining bradycardia sensing and pacing data.*)
\begin{schema}{PredischargeFollowUp}
 \Xi PulseGenerator\\
% \Xi PG\\
 intSensedAPulseAmp!  : \num\\ %(* Volts*1000 = mV *)\\
 intSensedVPulseAmp!  : \num\\ %(* Volts*1000 = mV *)\\
 intSensedAPulseWidth!  : \num\\ \\
 intSensedAPulseWidth!  : \num\\ \\
 intPacedAPulseAmp!  : \num\\ %(* Volts*1000 = mV *)\\
 intPacedVPulseAmp!  : \num\\ %(* Volts*1000 = mV *)\\
 intPacedAPulseWidth!  : \num\\ \\
 intPacedAPulseWidth!  : \num\\ \\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 bradycardiaState = TEMPORARY \land\\
 telemetryMode = ON \land\\
 intSensedAPulseAmp! =  sensedAPulseAmp \land\\
 intSensedVPulseAmp! =  sensedVPulseAmp \land\\
 intSensedAPulseWidth! =  sensedAPulseWidth \land\\
 intSensedAPulseWidth! =  sensedVPulseWidth \land\\
 intPacedAPulseAmp! =  pacedAPulseAmp \land\\
 intPacedVPulseAmp! =  pacedVPulseAmp \land\\
 intPacedAPulseWidth! =  pacedAPulseWidth \land\\
 intPacedAPulseWidth! =  pacedVPulseWidth\\
\end{schema}

%(*2.5.4 - Routine Follow-Up*)
%(*1 - Verify the administrative data.*)

\begin{schema}{AdmDataFollowUp}
 \Xi PulseGenerator\\
% \Xi PG\\
 intDeviceSerial!: STRING\\
 intDeviceModel! : STRING\\
 intPaceThreshold! : \num\\ %(* Volts*1000 = mV *)\\
 intIndicationForPacing !: STRING\\
 intImplantDeviceDate!: DATE\\
 intImplantLeadDate! : DATE\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 bradycardiaState = TEMPORARY \land\\
 telemetryMode = ON \land\\
 intDeviceSerial! = deviceSerial \land\\
 intDeviceModel! = deviceModel \land\\
 intPaceThreshold! = paceThreshold \land\\
 intIndicationForPacing! = indicationForPacing \land\\
 intImplantDeviceDate! = implantDeviceDate \land\\
 intImplantLeadDate! = implantLeadDate\\
\end{schema}

%(*Check on the programmed data*)
\begin{schema}{PrintProgFollowUp}
 \Xi PulseGenerator\\
% \Xi PG\\
 intBoMode!: MODE\\
 intLrl! : \nat\\
 intARefractPeriod! : \num\\
 intVRefractPeriod! : \num\\
 intHysteresisRateLimit !: SWITCHPARAMZ\\
 intASensitivity! : \num\\
 intVSensitivity! : \num\\
 intAPulseAmp! : SWITCHPARAMZ; \\
 intVPulseAmp! : SWITCHPARAMZ\\
 intVPulseWidth!  : \num\\ %(* Volts*1000 = mV *)\\
 intAPulseWidth!  : \num\\ %(* Volts*1000 = mV *)\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 bradycardiaState = TEMPORARY \land\\
 telemetryMode = ON \land\\
 intBoMode! = boMode \land\\
 intLrl! = lowerRateLimit \land\\
 intARefractPeriod! = aRefractPeriod \land\\
 intVRefractPeriod! = vRefractPeriod \land\\
 intHysteresisRateLimit! = hysteresisRateLimit \land\\
 intASensitivity! = aSensitivity \land\\
 intVSensitivity! = vSensitivity \land\\
 intAPulseAmp! =  aPulseAmpRegulated \land\\
 intVPulseAmp! =  vPulseAmpRegulated \land\\
 intAPulseWidth! =  aPulseWidth \land\\
 intAPulseWidth! =  vPulseWidth\\
\end{schema}

%(*Examine the measured or real  -time data*)
\begin{schema}{PrintMeasFollowUp}
 \Xi PulseGenerator\\
% \Xi PG\\
 intSensedAPulseAmp!  : \num\\
 intSensedVPulseAmp!  : \num\\ \\
 intSensedAPulseWidth!  : \num\\ \\
 intSensedAPulseWidth!  : \num\\
 intSensedBeat!  : \num\\
 intLeadImpedance!  : \num\\
 intALeadPolarity! : POLARITY; \\
 intVLeadPolarity! : POLARITY\\
 intBatteryVoltage!  : \num\\
 intBatteryCurrentDrain!  : \num\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 bradycardiaState = TEMPORARY \land\\
 telemetryMode = ON \land\\
 intSensedBeat! = (60000 \div sensedAPulseAmp) \land\\
 intSensedAPulseAmp! =  sensedAPulseAmp \land\\
 intSensedVPulseAmp! =  sensedVPulseAmp \land\\
 intSensedAPulseWidth! =  sensedAPulseWidth \land\\
 intSensedAPulseWidth! =  sensedVPulseWidth \land\\
 intLeadImpedance! = leadImpedance \land\\
 intALeadPolarity! = aLeadPolarity \land\\
 intVLeadPolarity! = vLeadPolarity \land\\
 intBatteryVoltage! = batteryVoltage \land\\
 intBatteryCurrentDrain! = Current (leadImpedance, batteryVoltage)\\
\end{schema}


%(*3 - System Requirements*)

%(*3.2.4 - Printed Reports*)


\begin{schema}{ReportHeaderInfo}
 \Xi DCM\\
 \Xi PulseGenerator\\
% \Xi PG\\
 outDcmSoftwareRevNumber! : STRING\\
 outDcmSerialNumber!  : STRING\\
 outDcmInstName! : STRING\\
 outDcmDate! : DATE\\
 outDcmTime! : TIME\\
 outDeviceSerial! : STRING\\
 outDeviceModel!  : STRING\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 bradycardiaState = TEMPORARY \land\\
 telemetryMode = ON \land\\
 outDcmSoftwareRevNumber! = dcmSoftwareRevNumber \land\\
 outDcmSerialNumber! = dcmSerialNumber \land\\
 outDcmInstName! = dcmInstName \land\\
 outDcmDate! = dcmDate \land\\
 outDcmTime! = dcmTime \land\\
 outDeviceSerial! = deviceSerial \land\\
 outDeviceModel! = deviceModel\\
\end{schema}

%(*1. A Bradycardia Parameters Report shall be available.*)

\begin{schema}{ReportBradycardiaParam}
 \Xi PulseGenerator\\
% \Xi PG\\
 outBoMode!  : MODE\\
 outLowerRateLimit! : \nat\\
 outUpperRateLimit! : \nat\\
 outFixedAvDelay!   : \num\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 bradycardiaState = TEMPORARY \land\\
 telemetryMode = ON \land\\
 outBoMode! = boMode \land\\
 outLowerRateLimit! = lowerRateLimit \land\\
 outUpperRateLimit! = upperRateLimit \land\\
 outFixedAvDelay! = fixedAvDelay\\
\end{schema}

\begin{zed}
 PrintRepBradycardiaParam == ReportBradycardiaParam \\
				\land ReportHeaderInfo\\
\end{zed}


%(*2. A Temporary Parameters Report shall be available.*)
%(*Used in Magnet test to change bradycardia operation mode*)
\begin{schema}{AXXX}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 switch = ON\\
 chambersPaced = CATRIUM\\
\end{schema}

\begin{schema}{VXXX}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 switch = ON\\
 chambersPaced = CVENTRICLE\\
\end{schema}

\begin{schema}{DXXX}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 switch = ON\\
 chambersPaced = CDUAL\\
\end{schema}
\begin{schema}{OXO}
 BOM\\
 HysterisisRateLimit\\
 BatteryStatus\\
 TelemetrySession\\
\where
 switch = ON\\
 chambersPaced = CNONE\\
 responseToSensing = RNONE\\
\end{schema}

\begin{schema}{RepTemporaryParam}
 \Xi PulseGenerator\\
% \Xi PG\\
 \Xi BOM\\
 outBoMode!  : MODE\\
 outLowerRateLimit! : \nat\\
 outUpperRateLimit! : \nat\\
 outFixedAvDelay!   : \num\\
 outPulseAmpRegulated!  : SWITCHPARAMZ\\
 outPulseAmpUnregulated!  : SWITCHPARAMZ\\
 outPulseWidth!   : \num\\
 outVentricularSensitivity!   : \num\\
 outAtrialSensitivity!   : \num\\
 outVentricularRefPeriod!   : \num\\
 outAtrialRefPeriod!   : \num\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 bradycardiaState = TEMPORARY \land\\
 telemetryMode = ON \land\\
 outBoMode! = boMode \land\\
 outLowerRateLimit! = lowerRateLimit \land\\
 outUpperRateLimit! = upperRateLimit \land\\
 outFixedAvDelay! = fixedAvDelay \land\\
 (((AXXX) \\
	\implies ((outPulseAmpRegulated! =\\
		aPulseAmpRegulated) \land\\
  (outPulseAmpUnregulated! =\\
		aPulseAmpUnregulated) \land\\
  (outPulseWidth! =\\
		aPulseWidth))) \lor\\
 (VXXX \\
	\implies ((outPulseAmpRegulated! =\\
		vPulseAmpRegulated) \land\\
  (outPulseAmpUnregulated! =\\
		vPulseAmpUnregulated) \land\\
  (outPulseWidth! =\\
		vPulseWidth)))) \land\\
 outVentricularSensitivity! = vSensitivity \land\\
 outAtrialSensitivity! = aSensitivity \land\\
 outVentricularRefPeriod! = vRefractPeriod \land\\
 outAtrialRefPeriod! = aRefractPeriod\\
\end{schema}

\begin{zed}
 PrintRepTemporaryParam == RepTemporaryParam \land ReportHeaderInfo\\
\end{zed}

%(*3. An Implant Data Report shall be available.*)
\begin{zed}
 PrintRepImplantInterrogatePG == \\
		ImplantInterrogatePG \land ReportHeaderInfo\\
\end{zed}

%(*4. A Threshold Test Results Report shall be available.

??*)
%(*5. A Measured Data Report shall be available.*)

\begin{schema}{ReportMeasuredData}
 \Xi PulseGenerator\\
% \Xi PG\\
 printPWave! : \num\\ %(* mV *)\\
 printRWave! : \num\\ %(* mV *)\\
 printBatteryVoltage!    : \num\\ %(* mV *)\\
 printLeadImpedance! : \num\\ %(* Ohms *)\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 bradycardiaState = TEMPORARY \land\\
 telemetryMode = ON \land\\
 printPWave! = pWave' \land\\
 printRWave! = rWave' \land\\
 printBatteryVoltage! = batteryVoltage' \land\\
 printLeadImpedance! = leadImpedance'  \\
\end{schema}
\begin{zed}
 PrintRepMeasuredParam == ReportMeasuredData \land ReportHeaderInfo\\
\end{zed}

%(*6. A Marker Legend Report shall be available.*)

\begin{schema}{ReportMarkerLegend}
 \Xi PulseGenerator\\
% \Xi PG\\
 printAtrialMarker! : RTATRIALMARKERS\\
 printVentricularMarker! : RTVENTRICULARMARKERS\\
 printAugmentationMarker! : RTAUGMENTATIONMARKERS\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 bradycardiaState = TEMPORARY \land\\
 telemetryMode = ON \land\\
 printAtrialMarker! = atrialMarker \land\\
 printVentricularMarker! = ventricularMarker \land\\
 printAugmentationMarker! = augmentationMarker\\
\end{schema}

\begin{zed}
 PrintRepMarkerLegend == ReportMarkerLegend \land ReportHeaderInfo\\
\end{zed}


%(*3.4 - Pacing Pulse*)
%(*3.4.1*)

\begin{schema}{SetAPacingPulseAmplitudeReg}
 \Delta PulseGenerator\\
% \Delta PG\\
 aPacingPulseAmpReg?: SWITCHPARAMZ %(* Volts*1000 = mV *)\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 telemetryMode = ON \land\\
 aPulseAmpRegulated' = aPacingPulseAmpReg?  \land\\
	\theta(PulseGenerator \hide (aPulseAmpRegulated))' \\
		=\theta (PulseGenerator \hide (aPulseAmpRegulated))\\
\end{schema}

\begin{schema}{SetVPacingPulseAmplitudeReg}
 \Delta PulseGenerator\\
% \Delta PG\\
 vPacingPulseAmpReg?: SWITCHPARAMZ %(* Volts*1000 = mV *)\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 telemetryMode = ON\\
 vPulseAmpRegulated' = vPacingPulseAmpReg?\\
	\theta(PulseGenerator \hide (vPulseAmpRegulated))' \\
		=\theta (PulseGenerator \hide (vPulseAmpRegulated))\\
\end{schema}

\begin{schema}{SetAPacingPulseAmplitudeUnreg}
 \Delta PulseGenerator\\
% \Delta PG\\
 aPacingPulseAmpUnreg?: SWITCHPARAMZ %(* Volts*1000 = mV *)\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 telemetryMode = ON \land\\
 aPulseAmpUnregulated' = aPacingPulseAmpUnreg?  \land\\
	\theta(PulseGenerator \hide (aPulseAmpUnregulated))' \\
		=\theta (PulseGenerator \hide (aPulseAmpUnregulated))\\
\end{schema}

\begin{schema}{SetVPacingPulseAmplitudeUnreg}
 \Delta PulseGenerator\\
% \Delta PG\\
 vPacingPulseAmpUnreg?: SWITCHPARAMZ %(* Volts*1000 = mV *)\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 telemetryMode = ON \land\\
 vPulseAmpUnregulated' = vPacingPulseAmpUnreg?  \land\\
	\theta(PulseGenerator \hide (vPulseAmpUnregulated))' \\
		=\theta (PulseGenerator \hide (vPulseAmpUnregulated))\\
\end{schema}



%(*3.4.2*)

\begin{schema}{SetAPacingPulseWidth}
 \Delta PulseGenerator\\
% \Delta PG\\
 aPacingPulseWidth? : \num\\ %(* ms *)\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 telemetryMode = ON \land\\
 aPulseWidth' = aPacingPulseWidth?  \land\\
	\theta(PulseGenerator \hide (aPulseWidth))' \\
		=\theta (PulseGenerator \hide (aPulseWidth))\\
\end{schema}

\begin{schema}{SetVPacingPulseWidth}
 \Delta PulseGenerator\\
% \Delta PG\\
 vPacingPulseWidth? : \num\\ %(* ms *)\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 telemetryMode = ON \land\\
 vPulseWidth' = vPacingPulseWidth?  \land\\
	\theta(PulseGenerator \hide (vPulseWidth))' \\
		=\theta (PulseGenerator \hide (vPulseWidth))\\
\end{schema}

%(*3.4.4*)

\begin{schema}{SetAVSensingThreshold}
 \Delta PulseGenerator\\
% \Delta PG\\
 progASensingThreshold? : \num\\ %(* mV *)\\
 progVSensingThreshold? : \num\\  %(* mV *)\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 telemetryMode = ON \land\\
 aSensingThreshold' = progASensingThreshold? \land\\
 vSensingThreshold' = progVSensingThreshold?  \land\\
	\theta(PulseGenerator \hide (aSensingThreshold,vSensingThreshold))' \\
		=\theta (PulseGenerator \hide (aSensingThreshold,vSensingThreshold))\\
\end{schema}

%(*3.5 - Bradycardia Operation Modes*)

\begin{schema}{SetBradycardiaOperationMode}
 \Delta PulseGenerator\\
% \Delta PG\\
 bradycardiaOpMode? : MODE\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 telemetryMode = ON \land\\
 boMode' = bradycardiaOpMode?  \land\\
	\theta(PulseGenerator \hide (boMode))' \\
		=\theta (PulseGenerator \hide (boMode))\\
\end{schema}

%(* 3.6 Bradycarda States*)

\begin{schema}{SwitchBradycardiaState}
 \Delta PulseGenerator\\
% \Delta PG\\
bstate?: BRADYCARDIASTATE\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 telemetryMode = ON \land\\
 bradycardiaState' = bstate?  \land\\
	\theta(PulseGenerator \hide (bradycardiaState))' \\
		=\theta (PulseGenerator \hide (bradycardiaState))\\
\end{schema}

%(*3.6.2 - Temporary State*)

\begin{schema}{TempBradyPatientDiagnosticTesting}
% \Xi PG\\
 \Xi PulseGenerator\\
 outLeadImpedance! : \num\\ %(* Ohms *)\\
 outPaceThreshold! : \num\\ %(* Volts*1000 = mV *)\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 telemetryMode = ON \land\\
 bradycardiaState = TEMPORARY \land\\
 outLeadImpedance! = leadImpedance \land\\
 outPaceThreshold! = paceThreshold\\
\end{schema}

%(*3.6.3 - Pace-Now State*)

\begin{schema}{SetPaceNowState}
 \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 bradycardiaState = PACENOW \land\\
 boMode \in VVI \land\\
 lowerRateLimit' = 65 \land\\
 aPulseAmpRegulated' = (ON,35 \div 10) \land\\
 aPulseAmpUnregulated' = (ON,375 \div 100) \land\\
 vPulseAmpRegulated' = (ON,35 \div 10) \land\\
 vPulseAmpUnregulated' = (ON,375 \div 100) \land\\
 aPulseWidth' = (4 \div 10) \land\\
 vPulseWidth' = (4 \div 10) \land\\
 vRefractPeriod' = 320 \land\\
 vSensitivity' = (15 \div 10)  \land\\
	\theta(PulseGenerator \hide (lowerRateLimit,\\
			aPulseAmpRegulated,\\
			aPulseAmpUnregulated,\\
			vPulseAmpRegulated,\\
			vPulseAmpUnregulated,\\
			aPulseWidth,\\
			vPulseWidth,\\
			vRefractPeriod,\\
			vSensitivity))' \\
		=\theta (PulseGenerator \hide (lowerRateLimit,\\
			aPulseAmpRegulated,\\
			aPulseAmpUnregulated,\\
			vPulseAmpRegulated,\\
			vPulseAmpUnregulated,\\
			aPulseWidth,\\
			vPulseWidth,\\
			vRefractPeriod,\\
			vSensitivity))\\
\end{schema}

%(*3.6.5 - Power-On Reset State*)

\begin{schema}{SetPowerOnResetState}
 \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
bradycardiaState=POR
\land boMode \in VVI
\land aPulseAmpRegulated' = (ON,35 \div 10)
\land aPulseAmpUnregulated' = (ON,375 \div 100)
\land vPulseAmpRegulated' = (ON,35 \div 10)
\land vPulseAmpUnregulated' = (ON,375 \div 100)
\land aPulseWidth' = (4 \div 10)
\land vPulseWidth' = (4 \div 10)
\land vRefractPeriod'=320
\land vSensitivity'=(15 \div 10)
\land \theta(PulseGenerator \hide (lowerRateLimit,\\
			aPulseAmpRegulated,\\
			aPulseAmpUnregulated,\\
			vPulseAmpRegulated,\\
			vPulseAmpUnregulated,\\
			aPulseWidth,\\
			vPulseWidth,\\
			vRefractPeriod,\\
			vSensitivity))' \\
		=\theta (PulseGenerator \hide (lowerRateLimit,\\
			aPulseAmpRegulated,\\
			aPulseAmpUnregulated,\\
			vPulseAmpRegulated,\\
			vPulseAmpUnregulated,\\
			aPulseWidth,\\
			vPulseWidth,\\
			vRefractPeriod,\\
			vSensitivity))\\
\end{schema}

%(*3.7 - Magnet Test*)

\begin{schema}{SwitchMagnetTest}
 \Delta PulseGenerator\\
% \Delta PG\\
 smagnet?: SWITCH\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 magnetTest' = smagnet?\\
	\theta(PulseGenerator \hide (magnetTest))' \\
		=\theta (PulseGenerator \hide (magnetTest))\\
\end{schema}

\begin{schema}{ChangeOpMode}
 \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 telemetryMode = ON \\
 bradycardiaState = MAGNET\\
 magnetTest = ON\\
 (boMode \in AXXX \implies boMode' \in AOO \lor\\
 boMode \in VXXX \implies boMode' \in VOO \lor\\
 boMode \in DXXX \implies boMode' \in DOO \lor\\
 boMode \in OXO \implies boMode' \in OOO)\\
	\theta(PulseGenerator \hide (boMode))' \\
		=\theta (PulseGenerator \hide (boMode))\\
\end{schema}

\begin{schema}{SetMagnetTest}
 \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 telemetryMode = ON \land\\
 bradycardiaState = MAGNET \land\\
 magnetTest = ON \land\\
 ((magnetRate \geq  100) \implies \\
	(battStatusLevel' = BOL)) \lor\\
 ((90 \leq magnetRate < 100) \implies \\
(battStatusLevel' = ERN)) \lor\\
 ((85 \leq magnetRate' < 90) \implies \\
(battStatusLevel' = ERT)) \lor\\
 (magnetRate < 85 \implies \\
	(battStatusLevel' = ERP))  \land\\
	\theta(PulseGenerator \hide (battStatusLevel))' \\
		=\theta (PulseGenerator \hide (battStatusLevel))\\
\end{schema}

%(*3.6.4 - Magnet State*)

\begin{zed}
 MagnetState == ChangeOpMode \land SetMagnetTest\\
\end{zed}

%(* 3.8 Implant Data (page 19) *)

\begin{schema}{SetAtrialLeadData}
 \Delta PulseGenerator\\
% \Delta PG\\
 setLeadPolarity? : POLARITY\\
 setLeadImpedance? : \num\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 telemetryMode = ON \land\\
 aLeadPolarity'  = setLeadPolarity? \land\\
 leadImpedance' = setLeadImpedance?  \land\\
	\theta(PulseGenerator \hide (aLeadPolarity, leadImpedance))' \\
		=\theta (PulseGenerator \hide (aLeadPolarity, leadImpedance))\\
\end{schema}

\begin{schema}{SetVentricularLeadData}
 \Delta PulseGenerator\\
% \Delta PG\\
 setLeadPolarity? : POLARITY\\
 setLeadImpedance? : \num\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 telemetryMode = ON \land\\
 vLeadPolarity'  = setLeadPolarity? \land\\
 leadImpedance' = setLeadImpedance?  \land\\
	\theta(PulseGenerator \hide (vLeadPolarity, leadImpedance))' \\
		=\theta (PulseGenerator \hide (vLeadPolarity, leadImpedance))\\
\end{schema}

\begin{zed}
 SetLeadsData == SetVentricularLeadData \land		 \\
	SetAtrialLeadData		 \\
\end{zed}


\begin{schema}{SetPGData}
 \Delta PulseGenerator\\
% \Delta PG\\
 deviceImplantDate?   : DATE\\
 devicePaceThreshold? : \num\\
 leadImplantDate?  : DATE\\
 patientIndication? : STRING\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 telemetryMode = ON  \land\\
 implantDeviceDate' = deviceImplantDate?  \land\\
 implantLeadDate' = leadImplantDate?  \land\\
 paceThreshold' = devicePaceThreshold?  \land\\
 indicationForPacing' = patientIndication?  \land\\
	\theta(PulseGenerator \hide (implantDeviceDate,\\
			implantLeadDate,\\
			paceThreshold,\\
			indicationForPacing))' \\
		=\theta (PulseGenerator \hide (implantDeviceDate,\\
			implantLeadDate,\\
			paceThreshold,\\
			indicationForPacing))\\
\end{schema}

\begin{zed}
 SetImplantPG == SetLeadsData \land		 \\
	SetPGData		 \\
\end{zed}

%(*4. Diagnostics*)
%(*4.2 P and R Wave Measurements*)

\begin{schema}{IntPRWave}
% \Xi PG\\
 \Xi PulseGenerator\\
 intPWave! : \num\\
 intRWave! : \num\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 telemetryMode = ON \land\\
 intPWave! = pWave \land\\
 intRWave! = rWave\\
\end{schema}

%(*4.3 Lead Impedance Measurements*)

\begin{schema}{LeadImpMeasurement}
% \Xi PG\\
 \Xi PulseGenerator\\
 intLeadImpedance! : \num\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 telemetryMode = ON \land\\
 intLeadImpedance! = leadImpedance\\
\end{schema}


%(*4.8 Event Markers*)

\begin{schema}{AtriumStartTimeOk}
 \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (((aCurrMeasurement > pWave) \land\\
pWave = 0) \land\\
 ((pWave' = aCurrMeasurement) \\
	\land aStartTime' = time))  \land\\
	\theta(PulseGenerator \hide (pWave,aStartTime))' \\
		=\theta (PulseGenerator \hide (pWave,aStartTime))\\
\end{schema}
\begin{schema}{AtriumStartTimeNOk}
 \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
\lnot((aCurrMeasurement > pWave) \land\\
pWave = 0)  \land\\
	\theta(PulseGenerator)' \\
		=\theta (PulseGenerator)\\
\end{schema}
\begin{zed}
 AtriumStartTime == AtriumStartTimeOk \lor AtriumStartTimeNOk\\
\end{zed}


\begin{schema}{AtriumMaxOk}
 \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 ((aCurrMeasurement \leq pWave) \land\\
 ((pWave' = aCurrMeasurement) \land\\
	aMax' = aCurrMeasurement))  \land\\
	\theta(PulseGenerator \hide (pWave,aMax))' \\
		=\theta (PulseGenerator \hide (pWave,aMax))\\
\end{schema}
\begin{schema}{AtriumMaxNOk}
 \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
\lnot (aCurrMeasurement \leq pWave) \land\\
	\theta(PulseGenerator)' \\
		=\theta (PulseGenerator)\\
\end{schema}
\begin{zed}
 AtriumMax == AtriumMaxOk \lor AtriumMaxNOk\\
\end{zed}

\begin{schema}{AtriumEndTimeOk}
 \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 ((vCurrMeasurement \leq 0) \land\\
 ((pWave' = aCurrMeasurement) \\
	\land aDelay' = time))  \land\\
	\theta(PulseGenerator \hide (pWave,aDelay))' \\
		=\theta (PulseGenerator \hide (pWave,aDelay))\\
\end{schema}

\begin{schema}{AtriumEndTimeNOk}
 \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 ((vCurrMeasurement \leq 0) \land\\
	\theta(PulseGenerator)' =\theta (PulseGenerator))\\
\end{schema}

\begin{zed}
 AtriumEndTime == AtriumEndTimeOk \lor AtriumEndTimeNOk\\
\end{zed}

\begin{zed}
 AtrialMeasurement == \\
		AtriumStartTime \lor AtriumMax \lor AtriumEndTime\\
\end{zed}

\begin{schema}{VentricleStartTimeOk}
 \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 (((vCurrMeasurement > rWave) \land\\
rWave = 0) \land\\
 ((rWave' = vCurrMeasurement) \\
	\land vStartTime' = time))  \land\\
	\theta(PulseGenerator \hide (rWave,vStartTime))' \\
		=\theta (PulseGenerator \hide (rWave,vStartTime))\\
\end{schema}
\begin{schema}{VentricleStartTimeNOk}
 \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 ((vCurrMeasurement > rWave) \land\\
rWave = 0) \land\\
	\theta(PulseGenerator)' \\
		=\theta (PulseGenerator)\\
\end{schema}

\begin{zed}
 VentricleStartTime == VentricleStartTimeOk \lor VentricleStartTimeNOk\\
\end{zed}

\begin{schema}{VentricleMaxOk}
 \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 ((vCurrMeasurement \leq rWave) \land\\
 ((rWave' = vCurrMeasurement) \land\\
	vMax' = aCurrMeasurement))  \land\\
	\theta(PulseGenerator \hide (rWave,vMax))' \\
		=\theta (PulseGenerator \hide (rWave,vMax))\\
\end{schema}
\begin{schema}{VentricleMaxNOk}
 \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
\lnot (vCurrMeasurement \leq rWave) \land\\
	\theta(PulseGenerator)' \\
		=\theta (PulseGenerator)
\end{schema}

\begin{zed}
 VentricleMax == VentricleMaxOk \lor VentricleMaxNOk\\
\end{zed}

\begin{schema}{VentricleEndTimeOk}
 \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 ((vCurrMeasurement \leq 0) \lor \\
 ((rWave' = vCurrMeasurement) \\
	\land vDelay' = time)) \land\\
	\theta(PulseGenerator \hide (rWave,vDelay))' \\
		=\theta (PulseGenerator \hide (rWave,vDelay))
\end{schema}

\begin{schema}{VentricleEndTimeNOk}
% \Delta PG
 \Delta PulseGenerator
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 ((vCurrMeasurement \leq 0) \land
	\theta(PulseGenerator)' = \theta (PulseGenerator))
\end{schema}

\begin{zed}
 VentricleEndTime == VentricleEndTimeOk \lor VentricleEndTimeNOk\\
\end{zed}


\begin{zed}
 VentricularMeasurement == \\
		VentricleStartTime \\
		\lor VentricleMax \\
		\lor VentricleEndTime\\
\end{zed}
% problema
\begin{zed}
 SensingModule == \\
		AtrialMeasurement\\
		\lor VentricularMeasurement\\
\end{zed}

\begin{schema}{AtrialSensedMarkerOk}
 \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 sensedAPulseWidth' = (aDelay - aStartTime) \land\\
 sensedAPulseAmp' = aMax \land\\
 lastSAPulse' = aStartTime \land\\
 (sensedAPulseWidth \leq PulseWidth(upperRateLimit)) \land\\
(atrialMarker' \\
		= atrialMarker \cat \langle(AS, sensedAPulseWidth, \\
				  sensedAPulseAmp, \\
				  0, lastSAPulse)\rangle) \land\\
 (sensedAPulseWidth > PulseWidth(upperRateLimit)) \land\\
(atrialMarker' \\
		= atrialMarker \cat \langle(AT, sensedAPulseWidth, \\
				  sensedAPulseAmp, \\
				  0, lastSAPulse)\rangle) \land\\
	\theta(PulseGenerator \hide (atrialMarker,\\
				sensedAPulseWidth,\\
				sensedAPulseAmp,\\
				lastSAPulse))' \\
		=\theta (PulseGenerator \hide (atrialMarker,\\
				sensedAPulseWidth,\\
				sensedAPulseAmp,\\
				lastSAPulse))\\
\end{schema}

\begin{schema}{AtrialSensedMarkerNOk}
 \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 sensedAPulseWidth' = (aDelay - aStartTime) \land\\
 sensedAPulseAmp' = aMax \land\\
 lastSAPulse' = aStartTime \land\\
\lnot(sensedAPulseWidth \leq PulseWidth(upperRateLimit)) \land\\
\lnot(sensedAPulseWidth > PulseWidth(upperRateLimit)) \land\\
	\theta(PulseGenerator)' \\
		=\theta (PulseGenerator)\\
\end{schema}
\begin{zed}
 AtrialSensedMarker == AtrialSensedMarkerOk \lor AtrialSensedMarkerNOk\\
\end{zed}

\begin{schema}{VentricularSensedMarkerOk}
 \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 \exists vSense,vSensePrevious: VENTRICULARMARKERANN\\
aSense: ATRIALMARKERANN |\\
aSense = last (atrialMarker) \land \\
vSense = last (ventricularMarker)  \land\\
vSensePrevious = \\
	last (front (ventricularMarker)) @\\
 cardiacCyclesLength' = (vSense.5 - vSensePrevious.5) \land\\
 sensedVPulseWidth' = (vDelay - vStartTime) \land\\
 sensedVPulseAmp' = vMax \land\\
 lastSVPulse' = vStartTime \land\\
 ((vSense.5 \leq aSense.5 \leq vStartTime) \land\\
(ventricularMarker' \\
		= ventricularMarker \cat \langle(VS,\\
		sensedVPulseWidth, \\
		sensedVPulseAmp, \\
		0, lastSVPulse)\rangle)) \lor\\
 ((\lnot{}(vSense.5 \leq aSense.5 \leq vStartTime)) \land\\
(ventricularMarker' \\
		= ventricularMarker \cat \langle(PVC,\\
		sensedVPulseWidth, \\
		sensedVPulseAmp, \\
		0, lastSVPulse)\rangle)) \land\\
	\theta(PulseGenerator \hide (ventricularMarker,\\
				sensedVPulseWidth,\\
				sensedVPulseAmp,\\
				lastSVPulse))' \\
		=\theta (PulseGenerator \hide (ventricularMarker,\\
				sensedVPulseWidth,\\
				sensedVPulseAmp,\\
				lastSVPulse))\\
\end{schema}
\begin{schema}{VentricularSensedMarkerNOk}
 \Delta PulseGenerator\\
% \Delta PG\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 \exists vSense: VENTRICULARMARKERANN\\
aSense: ATRIALMARKERANN |\\
aSense = last (atrialMarker) \land \\
vSense = last (ventricularMarker) @\\
 sensedVPulseWidth = (vDelay - vStartTime) \land\\
 sensedVPulseAmp = vMax \land\\
 lastSVPulse = vStartTime \land\\
 aSense = last (atrialMarker) \land\\
 vSense = last (ventricularMarker) \land
\lnot (vSense.5 \leq aSense.5 \leq vStartTime) \land
	\theta(PulseGenerator)' \\
		=\theta (PulseGenerator)\\
\end{schema}

\begin{zed}
 VentricularSensedMarker == VentricularSensedMarkerOk \lor \\
		 VentricularSensedMarkerNOk\\
\end{zed}

\begin{schema}{AugmentationMarkerOk}
 \Delta PulseGenerator\\
% \Delta PG\\
 aSense,aSensePrevious: ATRIALMARKERANN\\
 vSense,vSensePrevious: VENTRICULARMARKERANN\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @\\
 aSense = last (atrialMarker)\\
 aSensePrevious = last (front (atrialMarker))\\
 vSense = last (ventricularMarker) \land\\
 vSensePrevious = last (front (ventricularMarker)) \land\\
 ((aSensePrevious.1 = AS \land aSense.1 = AT) \land\\
(augmentationMarker' \\
		= augmentationMarker \cat \langle(ATRDur, aSense.5)\rangle)) \land\\
 ((aSensePrevious.1 = aSense.1 = AT) \land\\
(augmentationMarker' \\
		= augmentationMarker \cat \langle(ATRFB, aSense.5)\rangle)) \land\\
 ((aSensePrevious.1 = AT \land aSense.1 = AS) \land\\
(augmentationMarker' \\
		= augmentationMarker \cat \langle(ATREnd, aSense.5)\rangle)) \land\\
 ((vSensePrevious.1 = VS \land vSense.1 = PVC) \land\\
	(augmentationMarker' \\
		= augmentationMarker \cat \langle(PVPext, vSense.5)\rangle))  \land\\
	\theta (PulseGenerator \hide (augmentationMarker))'\\
		= \theta (PulseGenerator \hide (augmentationMarker))\\
\end{schema}

\begin{schema}{AugmentationMarkerNOk}
 \Delta PulseGenerator\\
% \Delta PG\\
 aSense,aSensePrevious: ATRIALMARKERANN\\
 vSense,vSensePrevious: VENTRICULARMARKERANN\\
\where
% \exists  PulseGenerator; PulseGenerator' |\\
%PGcomp =\theta PulseGenerator \land PGcomp' =\theta PulseGenerator' @
 aSense = last (atrialMarker)  \land\\
 aSensePrevious = last (front (atrialMarker))  \land\\
 vSense = last (ventricularMarker)  \land\\
 vSensePrevious = last (front (ventricularMarker))  \land\\
 (aSensePrevious.1 = AS \land aSense.1 = AT)  \land\\
 (aSensePrevious.1 = aSense.1 = AT)  \land\\
 (aSensePrevious.1 = AT \land aSense.1 = AS)  \land\\
 (vSensePrevious.1 = VS \land vSense.1 = PVC)  \land\\
	\theta(PulseGenerator)' \\
		=\theta (PulseGenerator)\\
\end{schema}

\begin{zed}
 AugmentationMarker == AugmentationMarkerOk \\
			\lor AugmentationMarkerNOk\\
\end{zed}

\begin{zed}
 SensingMarkers == AtrialSensedMarker \\
			\lor VentricularSensedMarker\\
\end{zed}

\begin{zed}
 BradyTherapy == (((SetTimer \semi SensingModule) \semi SensingMarkers) \semi SetMode)
\end{zed}

%(*\section{Operations over DCM}*)
%(*3.2 - Device Controller-Monitor*)
%(*3.2.3 - DCM Utility Functions*)

%\begin{schema}{SetDCMInfo}
% \Delta DCM\\
% setDcmInstName? : STRING\\
% setDcmDate? : DATE\\
% setDcmTime? : TIME\\
%\where
% dcmInstName' = setDcmInstName?\\
% dcmDate' = setDcmDate?  \\
% dcmTime' = setDcmTime?   \\
%\theta (DCM \hide (dcmInstName,\\
%	dcmDate,   \\
% dcmTime))' = \\
%	\theta(DCM \hide (dcmInstName,\\
%dcmDate,   \\
% dcmTime)) 
%\end{schema}


\end{document}
