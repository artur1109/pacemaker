﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Media;
using System.Threading;


using Ertsys;
using Pacemaker;
#pragma warning disable 659 // disable warning "Foo overrides Object.Equals() but does not override Object.GetHashCode()"


namespace Pacemaker
{
    public partial class PacemakerGUI : Form
    {
        
        bool therapy;
        public int lastTime;
        public int a_beats, v_beats;
        PulseGenerator pg = new PulseGenerator(new ProgrammableParameters(new LRL(60),
                                                                        new URL(120),
                                                                        new MaxSensorRate(120),
                                                                        new FixedAVDelay(150000),
                                                                        new DynamicAVDelay(SWITCH.OFF),
                                                                        new MinDynamicAVDelay(50000),
                                                                        new SensedAVDelayOffset(new _ePair<SWITCH, int>(SWITCH.OFF, 0)),
                                                                        new APulseAmpRegulated(new _ePair<SWITCH, int>(SWITCH.ON, 3500000)),
                                                                        new VPulseAmpRegulated(new _ePair<SWITCH, int>(SWITCH.ON, 3500000)),
                                                                        new APulseAmpUnregulated(new _ePair<SWITCH, int>(SWITCH.ON, 3750000)),
                                                                        new VPulseAmpUnregulated(new _ePair<SWITCH, int>(SWITCH.ON, 3750000)),
                                                                        new APulseWidth(400),
                                                                        new VPulseWidth(400),
                                                                        new ASensitivity(750),
                                                                        new VSensitivity(2500),
                                                                        new VRefractoryPeriod(320000),
                                                                        new ARefractoryPeriod(250000),
                                                                        new PVARP(250000),
                                                                        new PVARP_Ext(new _ePair<SWITCH, int>(SWITCH.OFF, 0)),
                                                                        new HysteresisRateLimit(new _ePair<SWITCH, int>(SWITCH.OFF, 00)),
                                                                        new RateSmoothing(new _ePair<SWITCH, int>(SWITCH.OFF, 0)),
                                                                        new ATRMode(SWITCH.OFF),
                                                                        new ATRDuration(20),
                                                                        new ATRFallbackTime(1),
                                                                        new VBlanking(40000),
                                                                        new ActivityThreshold(ACTIVITY_THRESHOLD.MED),
                                                                        new ReactTime(30),
                                                                        new RespFactor(8),
                                                                        new RecoveryTime(300)),
                                                new MeasuredParameters(new LeadImpedance(100),
                                                                        new BatteryVoltage(3500000),
                                                                        new RWave(0),
                                                                        new PWave(0)),
                                                new BO_MODE(new BOM(SWITCH.ON, CHAMBERS.C_VENTRICLE, CHAMBERS.C_NONE, RESPONSE.R_NONE, false)),
                                                new TimeSt(0, 0, 0, 0, 0, 0, 0, 0, 0),
                                                new SensingPulse(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                                                new PacingPulse(0, 0, 0, 0, 0, 0),
                                                new BatteryStatus(BATT_STATUS_LEVEL.BOL),
                                                new NoiseDetection(0),
                                                new EventMarkers(new _eSeq<_n5_quin<MarkerABB, int, int, int, int>>(),
                                                                        new _eSeq<_n5_quin<MarkerABB, int, int, int, int>>()),
                                                new Accelerometer(0));
        public PacemakerGUI()
        {
            InitializeComponent();
        }

        

        private void tableLayoutPanel5_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            //Display the battery status value
            if (pg.c_BatteryStatus.batt_status_level == BATT_STATUS_LEVEL.BOL)
            {
                monBattLevelValue.Text = "BOL";
                mpBattStatusValue.Text = "BOL";
            }
            else if (pg.c_BatteryStatus.batt_status_level == BATT_STATUS_LEVEL.ERN)
            {
                monBattLevelValue.Text = "ERN";
                mpBattStatusValue.Text = "ERN";
            }
            else if (pg.c_BatteryStatus.batt_status_level == BATT_STATUS_LEVEL.ERT)
            {
                monBattLevelValue.Text = "ERT";
                mpBattStatusValue.Text = "ERT";
            }
            else if (pg.c_BatteryStatus.batt_status_level == BATT_STATUS_LEVEL.ERP)
            {
                monBattLevelValue.Text = "ERP";
                mpBattStatusValue.Text = "ERP";
            }
            //Display the bradycardia operation mode
            if (pg.c_BO_MODE.bo_mode.inAOO())
            {
                ppBOMValue.Text = "AOO";
                monBOMValue.Text = "AOO";
            }
            else if (pg.c_BO_MODE.bo_mode.inVOO())
            {
                ppBOMValue.Text = "VOO";
                monBOMValue.Text = "VOO";
            }
            else if (pg.c_BO_MODE.bo_mode.inDOO())
            {
                ppBOMValue.Text = "DOO";
                monBOMValue.Text = "DOO";
            }
            else if (pg.c_BO_MODE.bo_mode.inDDI())
            {
                ppBOMValue.Text = "DDI";
                monBOMValue.Text = "DDI";
            }
            else if (pg.c_BO_MODE.bo_mode.inVVI())
            {
                ppBOMValue.Text = "VVI";
                monBOMValue.Text = "VVI";
            }
            else if (pg.c_BO_MODE.bo_mode.inVDD())
            {
                ppBOMValue.Text = "VDD";
                monBOMValue.Text = "VDD";
            }
            else if (pg.c_BO_MODE.bo_mode.inAAI())
            {
                ppBOMValue.Text = "AAI";
                monBOMValue.Text = "AAI";
            }
            else if (pg.c_BO_MODE.bo_mode.inVVT())
            {
                ppBOMValue.Text = "VVT";
            }
            else if (pg.c_BO_MODE.bo_mode.inAAT())
            {
                ppBOMValue.Text = "AAT";
                monBOMValue.Text = "AAT";
            }
            //Display predefined programmable parameters
            ppLRLValue.Text = Convert.ToString(pg.c_ProgrammableParameters.c_LRL.lower_rate_limit);
            monLRLValue.Text = Convert.ToString(pg.c_ProgrammableParameters.c_LRL.lower_rate_limit);
            ppURLValue.Text = Convert.ToString(pg.c_ProgrammableParameters.c_URL.upper_rate_limit);
            monURLValue.Text = Convert.ToString(pg.c_ProgrammableParameters.c_URL.upper_rate_limit);
            ppVPulseAmplitudeValue.Text = Convert.ToString(pg.c_ProgrammableParameters.c_VPulseAmpRegulated.v_pulse_amp_regulated.y / 1000);
            monVPAmpValue.Text = Convert.ToString(pg.c_ProgrammableParameters.c_VPulseAmpRegulated.v_pulse_amp_regulated.y /1000);
            ppAPulseAmplitudeValue.Text = Convert.ToString(pg.c_ProgrammableParameters.c_APulseAmpRegulated.a_pulse_amp_regulated.y / 1000);
            monAPAmpValue.Text = Convert.ToString(pg.c_ProgrammableParameters.c_APulseAmpRegulated.a_pulse_amp_regulated.y / 1000);
            ppVPulseWidthValue.Text = Convert.ToString(pg.c_ProgrammableParameters.c_VPulseWidth.v_pulse_width);
            monVPWidthValue.Text = Convert.ToString(pg.c_ProgrammableParameters.c_VPulseWidth.v_pulse_width);
            ppAPulseWidthValue.Text = Convert.ToString(pg.c_ProgrammableParameters.c_APulseWidth.a_pulse_width);
            monAPWidthValue.Text = Convert.ToString(pg.c_ProgrammableParameters.c_APulseWidth.a_pulse_width);
            ppFixedAVDelayValue.Text = Convert.ToString(pg.c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay / 1000);
            monFixAVDelayValue.Text = Convert.ToString(pg.c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay / 1000);
            
            
            //pg.c_EventMarkers.chg_atrial_marker(MarkerABB.AS, pg.c_SensingPulse.sensed_a_pulse_width, pg.c_SensingPulse.sensed_a_pulse_amp, 0, pg.c_TimeSt.time);
            //valueMarker.Text = pg.c_EventMarkers.atrial_marker.tail().ToString();
            therapy = true;
            pulso.ForeColor = Color.Black;
            while(therapy == true)
            {
                v_beats = 110;
                Thread.Sleep(1);
                monStatus.Text = "ON";
                Application.DoEvents();

                System.Media.SoundPlayer myPlayer = new System.Media.SoundPlayer();

                myPlayer = new System.Media.SoundPlayer();
                myPlayer.SoundLocation = @"C:\Users\Artur\Documents\Visual Studio 2010\Projects\PacemakerGUI\PacemakerGUI\heart.wav";
                monPWaveValue.Text = "0";
                monRWaveValue.Text = "0";
                pg._n_SetTimer();
                if ((lastTime + (60000 / v_beats)) == pg.c_TimeSt.time)
                {
                    myPlayer.Play();
                    pg.c_TimeSt.chg_a_curr_measurement(3500000);
                    monPWaveValue.Text = mpPWaveBar.Text;
                    pulso.ForeColor = Color.Red;
                    DisplayLastEvents();
                }
                if ((lastTime + (60000 / v_beats) + (pg.c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay/1000)) == pg.c_TimeSt.time)
                {
                    pg.c_TimeSt.chg_v_curr_measurement(3500000);
                    monRWaveValue.Text = Convert.ToString(3500000);

                    pulso.ForeColor = Color.Black;
                    lastTime = pg.c_TimeSt.time;
                }

                pg._n_SensingModule();
                pg._n_SensingMarkers();
                
                monTimeValue.Text = Convert.ToString(pg.c_TimeSt.time);
            }
                
               /* while (therapy)
                {
                    pg._n_BradyTherapy();
                }*/
                
            }
        private void DisplayLastEvents()
        {
            if (pg.c_EventMarkers.atrial_marker.last().x == MarkerABB.AS)
                evATValue.Text = "AS";
            else if (pg.c_EventMarkers.atrial_marker.last().x == MarkerABB.AP)
                evATValue.Text = "AP";
            else if (pg.c_EventMarkers.atrial_marker.last().x == MarkerABB.TN)
                evATValue.Text = "TN";
            else if (pg.c_EventMarkers.atrial_marker.last().x == MarkerABB.AT)
                evATValue.Text = "AT";

            if (pg.c_EventMarkers.atrial_marker.last().x == MarkerABB.VS)
                evVTypeValue.Text = "VS";
            else if (pg.c_EventMarkers.atrial_marker.last().x == MarkerABB.VP)
                evVTypeValue.Text = "VP";
            else if (pg.c_EventMarkers.atrial_marker.last().x == MarkerABB.TN)
                evVTypeValue.Text = "TN";
            else if (pg.c_EventMarkers.atrial_marker.last().x == MarkerABB.PVC)
                evVTypeValue.Text = "PVC";

            evAWidthValue.Text = Convert.ToString(pg.c_EventMarkers.atrial_marker.last().y);
            //evVWidthValue.Text = Convert.ToString(pg.c_EventMarkers.ventricular_marker.last().y);
            evAAmpValue.Text = Convert.ToString(pg.c_EventMarkers.atrial_marker.last().z);
            //evVAmpValue.Text = Convert.ToString(pg.c_EventMarkers.ventricular_marker.last().z);
            evANoiseValue.Text = Convert.ToString(pg.c_EventMarkers.atrial_marker.last().k);
            //evVNoiseValue.Text = Convert.ToString(pg.c_EventMarkers.ventricular_marker.last().k);
            evATimeValue.Text = Convert.ToString(pg.c_EventMarkers.atrial_marker.last().m);
            //evVTimeValue.Text = Convert.ToString(pg.c_EventMarkers.ventricular_marker.last().m);
        
        }
        

        private void stopButton_Click(object sender, EventArgs e)
        {
            therapy = false;
            monStatus.Text = "OFF";
        }

        private void ppBOMButton_Click(object sender, EventArgs e)
        {
            if (ppBOMValue.Text == "AOO")
            {
                pg.c_BO_MODE.chg_bo_mode(new BOM(SWITCH.ON, CHAMBERS.C_ATRIUM, CHAMBERS.C_NONE, RESPONSE.R_NONE, false));
                monBOMValue.Text = "AOO";
            }
            else if (ppBOMValue.Text == "VOO")
            {
                pg.c_BO_MODE.chg_bo_mode(new BOM(SWITCH.ON, CHAMBERS.C_VENTRICLE, CHAMBERS.C_NONE, RESPONSE.R_NONE, false));
                monBOMValue.Text = "VOO";
            }
            else if (ppBOMValue.Text == "DOO")
            {
                pg.c_BO_MODE.chg_bo_mode(new BOM(SWITCH.ON, CHAMBERS.C_DUAL, CHAMBERS.C_NONE, RESPONSE.R_NONE, false));
                monBOMValue.Text = "DOO";
            }
            else if (ppBOMValue.Text == "DDI")
            {
                pg.c_BO_MODE.chg_bo_mode(new BOM(SWITCH.ON, CHAMBERS.C_DUAL, CHAMBERS.C_DUAL, RESPONSE.INHIBITED, false));
                monBOMValue.Text = "DDi";
            }
            else if (ppBOMValue.Text == "VVI")
            {
                pg.c_BO_MODE.chg_bo_mode(new BOM(SWITCH.ON, CHAMBERS.C_VENTRICLE, CHAMBERS.C_VENTRICLE, RESPONSE.INHIBITED, false));
                monBOMValue.Text = "VVI";
            }
            else if (ppBOMValue.Text == "VDD")
            {
                pg.c_BO_MODE.chg_bo_mode(new BOM(SWITCH.ON, CHAMBERS.C_VENTRICLE, CHAMBERS.C_DUAL, RESPONSE.TRACKED, false));
                monBOMValue.Text = "VDD";
            }
            else if (ppBOMValue.Text == "AAI")
            {
                pg.c_BO_MODE.chg_bo_mode(new BOM(SWITCH.ON, CHAMBERS.C_ATRIUM, CHAMBERS.C_ATRIUM, RESPONSE.INHIBITED, false));
                monBOMValue.Text = "AAI";
            }
            else if (ppBOMValue.Text == "VVT")
            {
                pg.c_BO_MODE.chg_bo_mode(new BOM(SWITCH.ON, CHAMBERS.C_VENTRICLE, CHAMBERS.C_VENTRICLE, RESPONSE.TRIGGERED, false));
                monBOMValue.Text = "VVT";
            }
            else if (ppBOMValue.Text == "AAT")
            {
                pg.c_BO_MODE.chg_bo_mode(new BOM(SWITCH.ON, CHAMBERS.C_ATRIUM, CHAMBERS.C_ATRIUM, RESPONSE.TRIGGERED, false));
                monBOMValue.Text = "AAT";
            }
            
            
        }

        private void ppLRLButton_Click(object sender, EventArgs e)
        {
            
        }

        private void ppURLButton_Click(object sender, EventArgs e)
        {
            pg.c_ProgrammableParameters.set_upper_rate_limit(Convert.ToInt32(ppURLValue.Text));
            monURLValue.Text = Convert.ToString(pg.c_ProgrammableParameters.c_URL.upper_rate_limit);
        }

        private void mpBattStatusButton_Click(object sender, EventArgs e)
        {
            if(mpBattStatusValue.Text == "BOL")
            {
                pg.c_BatteryStatus.chg_batt_status_level(BATT_STATUS_LEVEL.BOL);                    
            }
            else if (mpBattStatusValue.Text == "ERN")
            {
                pg.c_BatteryStatus.chg_batt_status_level(BATT_STATUS_LEVEL.ERN);
            }
            else if (mpBattStatusValue.Text == "ERT")
            {
                pg.c_BatteryStatus.chg_batt_status_level(BATT_STATUS_LEVEL.ERT);
            }
            else if (mpBattStatusValue.Text == "ERP")
            {
                pg.c_BatteryStatus.chg_batt_status_level(BATT_STATUS_LEVEL.ERP);
            }
        }

        private void mpVentricularBeatsButton_Click(object sender, EventArgs e)
        {
            if (pg.c_BatteryStatus.batt_status_level == BATT_STATUS_LEVEL.BOL)
            {
                monBattLevelValue.Text = "BOL";
            }
            else if (pg.c_BatteryStatus.batt_status_level == BATT_STATUS_LEVEL.ERN)
            {
                monBattLevelValue.Text = "ERN";
            }
            else if (pg.c_BatteryStatus.batt_status_level == BATT_STATUS_LEVEL.ERT)
            {
                monBattLevelValue.Text = "ERT";
            }
            else if (pg.c_BatteryStatus.batt_status_level == BATT_STATUS_LEVEL.ERP)
            {
                monBattLevelValue.Text = "ERP";
            }
        }

        private void tableLayoutPanel6_Paint(object sender, PaintEventArgs e)
        {
            //monBPMValue.Text = Convert.ToString(60000000 / pg.c_SensingPulse.cardiac_cycles_length);
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label30_Click(object sender, EventArgs e)
        {

        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            mpPPMValue.Text = trackBar1.Text;
            v_beats = Convert.ToInt32(trackBar1.Text);
            monBPMValue.Text = trackBar1.Text;
        }

        private void mpAtrialBeatsButton_Click(object sender, EventArgs e)
        {
            monPWaveValue.Text = Convert.ToString(Convert.ToInt16(mpPWaveBar.Text));
            a_beats = Convert.ToInt16(mpPWaveBar.Text);
        }

        private void mpVentricularBeatsButton_Click_1(object sender, EventArgs e)
        {
            monRWaveValue.Text = Convert.ToString(Convert.ToInt16(mpRWaveBar.Text));
            v_beats = Convert.ToInt16(mpRWaveBar.Text);
        }


    }
}
