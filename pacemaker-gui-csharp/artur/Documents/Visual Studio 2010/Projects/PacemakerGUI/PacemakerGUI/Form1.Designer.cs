﻿namespace Pacemaker
{
    partial class PacemakerGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ppBOMButton = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.monStatus = new System.Windows.Forms.Label();
            this.monURLValue = new System.Windows.Forms.Label();
            this.monLRLValue = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.monAPulseWidth = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.monAPAmpValue = new System.Windows.Forms.Label();
            this.monVPAmpValue = new System.Windows.Forms.Label();
            this.monFixAVDelayValue = new System.Windows.Forms.Label();
            this.monBOMValue = new System.Windows.Forms.Label();
            this.monVPWidthValue = new System.Windows.Forms.Label();
            this.monAPWidthValue = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.monTimeValue = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.monPWaveValue = new System.Windows.Forms.Label();
            this.monRWaveValue = new System.Windows.Forms.Label();
            this.monBPMValue = new System.Windows.Forms.Label();
            this.monBattLevelValue = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.ppVPulseAmplitudeButton = new System.Windows.Forms.Button();
            this.ppVPulseAmplitudeValue = new System.Windows.Forms.MaskedTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ppLRLButton = new System.Windows.Forms.Button();
            this.ppAPulseWidthValue = new System.Windows.Forms.MaskedTextBox();
            this.ppURLValue = new System.Windows.Forms.MaskedTextBox();
            this.ppURLButton = new System.Windows.Forms.Button();
            this.ppAPulseWidthButton = new System.Windows.Forms.Button();
            this.ppVPulseWidthValue = new System.Windows.Forms.MaskedTextBox();
            this.ppAPulseAmplitudeValue = new System.Windows.Forms.MaskedTextBox();
            this.ppVPulseWidthButton = new System.Windows.Forms.Button();
            this.ppAPulseAmplitudeButton = new System.Windows.Forms.Button();
            this.ppFixedAVDelayValue = new System.Windows.Forms.MaskedTextBox();
            this.ppFixedAVDelayButton = new System.Windows.Forms.Button();
            this.ppBOMValue = new System.Windows.Forms.ComboBox();
            this.ppLRLValue = new System.Windows.Forms.MaskedTextBox();
            this.contentProgrammableParameters = new System.Windows.Forms.GroupBox();
            this.startButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.stopButton = new System.Windows.Forms.Button();
            this.resetButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.mpBattStatusValue = new System.Windows.Forms.ComboBox();
            this.tableRightPacemakerGUI = new System.Windows.Forms.TableLayoutPanel();
            this.valueMarker = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label42 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label40 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.mpAtrialBeatsButton = new System.Windows.Forms.Button();
            this.mpVentricularBeatsButton = new System.Windows.Forms.Button();
            this.mpBattStatusButton = new System.Windows.Forms.Button();
            this.mpPWaveValue = new System.Windows.Forms.Label();
            this.mpRWaveValue = new System.Windows.Forms.Label();
            this.mpPPMValue = new System.Windows.Forms.Label();
            this.tableLeftPacemakerGUI = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.pulso = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.evATValue = new System.Windows.Forms.Label();
            this.evANoiseValue = new System.Windows.Forms.Label();
            this.evAWidthValue = new System.Windows.Forms.Label();
            this.evAAmpValue = new System.Windows.Forms.Label();
            this.evATimeValue = new System.Windows.Forms.Label();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.evVTypeValue = new System.Windows.Forms.Label();
            this.evVWidthValue = new System.Windows.Forms.Label();
            this.evVAmpValue = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.evVNoiseValue = new System.Windows.Forms.Label();
            this.evVTimeValue = new System.Windows.Forms.Label();
            this.mpPWaveBar = new System.Windows.Forms.MaskedTextBox();
            this.mpRWaveBar = new System.Windows.Forms.MaskedTextBox();
            this.trackBar1 = new System.Windows.Forms.MaskedTextBox();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.contentProgrammableParameters.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableRightPacemakerGUI.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLeftPacemakerGUI.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.SuspendLayout();
            // 
            // ppBOMButton
            // 
            this.ppBOMButton.Location = new System.Drawing.Point(300, 23);
            this.ppBOMButton.Name = "ppBOMButton";
            this.ppBOMButton.Size = new System.Drawing.Size(33, 23);
            this.ppBOMButton.TabIndex = 29;
            this.ppBOMButton.Text = "Set";
            this.ppBOMButton.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Left;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label11.Location = new System.Drawing.Point(3, 26);
            this.label11.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(142, 23);
            this.label11.TabIndex = 27;
            this.label11.Text = "Bradycardia Operation Mode";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tableLayoutPanel4);
            this.groupBox3.Location = new System.Drawing.Point(3, 58);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(460, 302);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Monitor";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.groupBox5, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.groupBox4, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(454, 283);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.tableLayoutPanel6);
            this.groupBox5.Location = new System.Drawing.Point(230, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(221, 277);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 3;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.84615F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.15385F));
            this.tableLayoutPanel6.Controls.Add(this.label21, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.label22, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.label23, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.monStatus, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.monURLValue, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.monLRLValue, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.label33, 2, 1);
            this.tableLayoutPanel6.Controls.Add(this.label34, 2, 2);
            this.tableLayoutPanel6.Controls.Add(this.monAPulseWidth, 0, 3);
            this.tableLayoutPanel6.Controls.Add(this.label28, 0, 4);
            this.tableLayoutPanel6.Controls.Add(this.label29, 0, 5);
            this.tableLayoutPanel6.Controls.Add(this.label32, 0, 6);
            this.tableLayoutPanel6.Controls.Add(this.label35, 0, 7);
            this.tableLayoutPanel6.Controls.Add(this.label27, 0, 8);
            this.tableLayoutPanel6.Controls.Add(this.monAPAmpValue, 1, 5);
            this.tableLayoutPanel6.Controls.Add(this.monVPAmpValue, 1, 6);
            this.tableLayoutPanel6.Controls.Add(this.monFixAVDelayValue, 1, 7);
            this.tableLayoutPanel6.Controls.Add(this.monBOMValue, 1, 8);
            this.tableLayoutPanel6.Controls.Add(this.monVPWidthValue, 1, 4);
            this.tableLayoutPanel6.Controls.Add(this.monAPWidthValue, 1, 3);
            this.tableLayoutPanel6.Controls.Add(this.label30, 0, 9);
            this.tableLayoutPanel6.Controls.Add(this.monTimeValue, 1, 9);
            this.tableLayoutPanel6.Controls.Add(this.label31, 2, 3);
            this.tableLayoutPanel6.Controls.Add(this.label36, 2, 4);
            this.tableLayoutPanel6.Controls.Add(this.label37, 2, 5);
            this.tableLayoutPanel6.Controls.Add(this.label38, 2, 6);
            this.tableLayoutPanel6.Controls.Add(this.label39, 2, 7);
            this.tableLayoutPanel6.Controls.Add(this.label41, 2, 9);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 13;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(215, 258);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(3, 4);
            this.label21.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(43, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "Status";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(3, 24);
            this.label22.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(102, 13);
            this.label22.TabIndex = 2;
            this.label22.Text = "Upper Rate Limit";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(3, 44);
            this.label23.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(102, 13);
            this.label23.TabIndex = 3;
            this.label23.Text = "Lower Rate Limit";
            // 
            // monStatus
            // 
            this.monStatus.AutoSize = true;
            this.monStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.monStatus.Location = new System.Drawing.Point(118, 4);
            this.monStatus.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.monStatus.Name = "monStatus";
            this.monStatus.Size = new System.Drawing.Size(0, 13);
            this.monStatus.TabIndex = 5;
            // 
            // monURLValue
            // 
            this.monURLValue.AutoSize = true;
            this.monURLValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.monURLValue.Location = new System.Drawing.Point(118, 24);
            this.monURLValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.monURLValue.Name = "monURLValue";
            this.monURLValue.Size = new System.Drawing.Size(0, 13);
            this.monURLValue.TabIndex = 6;
            // 
            // monLRLValue
            // 
            this.monLRLValue.AutoSize = true;
            this.monLRLValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.monLRLValue.Location = new System.Drawing.Point(118, 44);
            this.monLRLValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.monLRLValue.Name = "monLRLValue";
            this.monLRLValue.Size = new System.Drawing.Size(0, 13);
            this.monLRLValue.TabIndex = 7;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label33.Location = new System.Drawing.Point(171, 24);
            this.label33.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(30, 13);
            this.label33.TabIndex = 10;
            this.label33.Text = "ppm";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label34.Location = new System.Drawing.Point(171, 44);
            this.label34.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(30, 13);
            this.label34.TabIndex = 11;
            this.label34.Text = "ppm";
            // 
            // monAPulseWidth
            // 
            this.monAPulseWidth.AutoSize = true;
            this.monAPulseWidth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.monAPulseWidth.Location = new System.Drawing.Point(3, 64);
            this.monAPulseWidth.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.monAPulseWidth.Name = "monAPulseWidth";
            this.monAPulseWidth.Size = new System.Drawing.Size(87, 13);
            this.monAPulseWidth.TabIndex = 4;
            this.monAPulseWidth.Text = "A Pulse Width";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label28.Location = new System.Drawing.Point(3, 84);
            this.label28.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(87, 13);
            this.label28.TabIndex = 14;
            this.label28.Text = "V Pulse Width";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label29.Location = new System.Drawing.Point(3, 104);
            this.label29.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(109, 13);
            this.label29.TabIndex = 15;
            this.label29.Text = "A Pulse Amplitude";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label32.Location = new System.Drawing.Point(3, 124);
            this.label32.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(109, 13);
            this.label32.TabIndex = 16;
            this.label32.Text = "V Pulse Amplitude";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label35.Location = new System.Drawing.Point(3, 144);
            this.label35.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(93, 13);
            this.label35.TabIndex = 17;
            this.label35.Text = "Fixed AV Delay";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(3, 164);
            this.label27.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(94, 13);
            this.label27.TabIndex = 12;
            this.label27.Text = "Brady Op Mode";
            // 
            // monAPAmpValue
            // 
            this.monAPAmpValue.AutoSize = true;
            this.monAPAmpValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.monAPAmpValue.Location = new System.Drawing.Point(118, 104);
            this.monAPAmpValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.monAPAmpValue.Name = "monAPAmpValue";
            this.monAPAmpValue.Size = new System.Drawing.Size(0, 13);
            this.monAPAmpValue.TabIndex = 19;
            // 
            // monVPAmpValue
            // 
            this.monVPAmpValue.AutoSize = true;
            this.monVPAmpValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.monVPAmpValue.Location = new System.Drawing.Point(118, 124);
            this.monVPAmpValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.monVPAmpValue.Name = "monVPAmpValue";
            this.monVPAmpValue.Size = new System.Drawing.Size(0, 13);
            this.monVPAmpValue.TabIndex = 20;
            // 
            // monFixAVDelayValue
            // 
            this.monFixAVDelayValue.AutoSize = true;
            this.monFixAVDelayValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.monFixAVDelayValue.Location = new System.Drawing.Point(118, 144);
            this.monFixAVDelayValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.monFixAVDelayValue.Name = "monFixAVDelayValue";
            this.monFixAVDelayValue.Size = new System.Drawing.Size(0, 13);
            this.monFixAVDelayValue.TabIndex = 21;
            // 
            // monBOMValue
            // 
            this.monBOMValue.AutoSize = true;
            this.monBOMValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.monBOMValue.Location = new System.Drawing.Point(118, 164);
            this.monBOMValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.monBOMValue.Name = "monBOMValue";
            this.monBOMValue.Size = new System.Drawing.Size(0, 13);
            this.monBOMValue.TabIndex = 22;
            // 
            // monVPWidthValue
            // 
            this.monVPWidthValue.AutoSize = true;
            this.monVPWidthValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.monVPWidthValue.Location = new System.Drawing.Point(118, 84);
            this.monVPWidthValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.monVPWidthValue.Name = "monVPWidthValue";
            this.monVPWidthValue.Size = new System.Drawing.Size(0, 13);
            this.monVPWidthValue.TabIndex = 23;
            // 
            // monAPWidthValue
            // 
            this.monAPWidthValue.AutoSize = true;
            this.monAPWidthValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.monAPWidthValue.Location = new System.Drawing.Point(118, 64);
            this.monAPWidthValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.monAPWidthValue.Name = "monAPWidthValue";
            this.monAPWidthValue.Size = new System.Drawing.Size(0, 13);
            this.monAPWidthValue.TabIndex = 24;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label30.Location = new System.Drawing.Point(3, 184);
            this.label30.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(101, 13);
            this.label30.TabIndex = 25;
            this.label30.Text = "Pacemaker Time";
            // 
            // monTimeValue
            // 
            this.monTimeValue.AutoSize = true;
            this.monTimeValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.monTimeValue.Location = new System.Drawing.Point(118, 184);
            this.monTimeValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.monTimeValue.Name = "monTimeValue";
            this.monTimeValue.Size = new System.Drawing.Size(0, 13);
            this.monTimeValue.TabIndex = 26;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label31.Location = new System.Drawing.Point(171, 64);
            this.label31.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(22, 13);
            this.label31.TabIndex = 27;
            this.label31.Text = "ms";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label36.Location = new System.Drawing.Point(171, 84);
            this.label36.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(22, 13);
            this.label36.TabIndex = 28;
            this.label36.Text = "ms";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label37.Location = new System.Drawing.Point(171, 104);
            this.label37.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(24, 13);
            this.label37.TabIndex = 29;
            this.label37.Text = "mV";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label38.Location = new System.Drawing.Point(171, 124);
            this.label38.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(24, 13);
            this.label38.TabIndex = 30;
            this.label38.Text = "mV";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label39.Location = new System.Drawing.Point(171, 144);
            this.label39.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(22, 13);
            this.label39.TabIndex = 31;
            this.label39.Text = "ms";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label41.Location = new System.Drawing.Point(171, 184);
            this.label41.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(22, 13);
            this.label41.TabIndex = 33;
            this.label41.Text = "ms";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tableLayoutPanel5);
            this.groupBox4.Location = new System.Drawing.Point(3, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(221, 277);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 3;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 113F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.84615F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.15385F));
            this.tableLayoutPanel5.Controls.Add(this.label12, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.label13, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.label19, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.label20, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.monPWaveValue, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.monRWaveValue, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.monBPMValue, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.monBattLevelValue, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.label24, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.label25, 2, 1);
            this.tableLayoutPanel5.Controls.Add(this.label26, 2, 2);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 5;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(215, 258);
            this.tableLayoutPanel5.TabIndex = 0;
            this.tableLayoutPanel5.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel5_Paint);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(3, 4);
            this.label12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "P Wave";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(3, 24);
            this.label13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "R Wave";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(3, 44);
            this.label19.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(33, 13);
            this.label19.TabIndex = 3;
            this.label19.Text = "BPM";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(3, 64);
            this.label20.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(82, 13);
            this.label20.TabIndex = 4;
            this.label20.Text = "Battery Level";
            // 
            // monPWaveValue
            // 
            this.monPWaveValue.AutoSize = true;
            this.monPWaveValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.monPWaveValue.Location = new System.Drawing.Point(116, 4);
            this.monPWaveValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.monPWaveValue.Name = "monPWaveValue";
            this.monPWaveValue.Size = new System.Drawing.Size(0, 13);
            this.monPWaveValue.TabIndex = 5;
            // 
            // monRWaveValue
            // 
            this.monRWaveValue.AutoSize = true;
            this.monRWaveValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.monRWaveValue.Location = new System.Drawing.Point(116, 24);
            this.monRWaveValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.monRWaveValue.Name = "monRWaveValue";
            this.monRWaveValue.Size = new System.Drawing.Size(0, 13);
            this.monRWaveValue.TabIndex = 6;
            // 
            // monBPMValue
            // 
            this.monBPMValue.AutoSize = true;
            this.monBPMValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.monBPMValue.Location = new System.Drawing.Point(116, 44);
            this.monBPMValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.monBPMValue.Name = "monBPMValue";
            this.monBPMValue.Size = new System.Drawing.Size(0, 13);
            this.monBPMValue.TabIndex = 7;
            // 
            // monBattLevelValue
            // 
            this.monBattLevelValue.AutoSize = true;
            this.monBattLevelValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.monBattLevelValue.Location = new System.Drawing.Point(116, 64);
            this.monBattLevelValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.monBattLevelValue.Name = "monBattLevelValue";
            this.monBattLevelValue.Size = new System.Drawing.Size(0, 13);
            this.monBattLevelValue.TabIndex = 8;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(170, 4);
            this.label24.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(24, 13);
            this.label24.TabIndex = 9;
            this.label24.Text = "mV";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label25.Location = new System.Drawing.Point(170, 24);
            this.label25.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(24, 13);
            this.label25.TabIndex = 10;
            this.label25.Text = "mV";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label26.Location = new System.Drawing.Point(170, 44);
            this.label26.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(33, 13);
            this.label26.TabIndex = 11;
            this.label26.Text = "BPM";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Left;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label4.Location = new System.Drawing.Point(300, 6);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 14);
            this.label4.TabIndex = 26;
            this.label4.Text = "Lower Rate Limit";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Left;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label10.Location = new System.Drawing.Point(3, 226);
            this.label10.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 26);
            this.label10.TabIndex = 22;
            this.label10.Text = "Fixed AV Delay";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Left;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label9.Location = new System.Drawing.Point(3, 197);
            this.label9.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(187, 23);
            this.label9.TabIndex = 21;
            this.label9.Text = "Ventricular Pulse Amplitude Regulated";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80.13468F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.86532F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tableLayoutPanel2.Controls.Add(this.ppBOMButton, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.label11, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label4, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label10, 0, 8);
            this.tableLayoutPanel2.Controls.Add(this.label9, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.ppVPulseAmplitudeButton, 2, 7);
            this.tableLayoutPanel2.Controls.Add(this.ppVPulseAmplitudeValue, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.label8, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.label7, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.label6, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.ppLRLButton, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.ppAPulseWidthValue, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.ppURLValue, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.ppURLButton, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.ppAPulseWidthButton, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.ppVPulseWidthValue, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.ppAPulseAmplitudeValue, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.ppVPulseWidthButton, 2, 5);
            this.tableLayoutPanel2.Controls.Add(this.ppAPulseAmplitudeButton, 2, 6);
            this.tableLayoutPanel2.Controls.Add(this.ppFixedAVDelayValue, 1, 8);
            this.tableLayoutPanel2.Controls.Add(this.ppFixedAVDelayButton, 2, 8);
            this.tableLayoutPanel2.Controls.Add(this.ppBOMValue, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.ppLRLValue, 1, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 9;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(340, 252);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // ppVPulseAmplitudeButton
            // 
            this.ppVPulseAmplitudeButton.Location = new System.Drawing.Point(300, 194);
            this.ppVPulseAmplitudeButton.Name = "ppVPulseAmplitudeButton";
            this.ppVPulseAmplitudeButton.Size = new System.Drawing.Size(33, 23);
            this.ppVPulseAmplitudeButton.TabIndex = 20;
            this.ppVPulseAmplitudeButton.Text = "Set";
            this.ppVPulseAmplitudeButton.UseVisualStyleBackColor = true;
            // 
            // ppVPulseAmplitudeValue
            // 
            this.ppVPulseAmplitudeValue.Location = new System.Drawing.Point(241, 194);
            this.ppVPulseAmplitudeValue.Name = "ppVPulseAmplitudeValue";
            this.ppVPulseAmplitudeValue.Size = new System.Drawing.Size(53, 20);
            this.ppVPulseAmplitudeValue.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Left;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label8.Location = new System.Drawing.Point(3, 168);
            this.label8.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(160, 23);
            this.label8.TabIndex = 17;
            this.label8.Text = "Atrial Pulse Amplitude Regulated";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Left;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label7.Location = new System.Drawing.Point(3, 139);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(117, 23);
            this.label7.TabIndex = 16;
            this.label7.Text = "Ventricular Pulse Width";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Left;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label6.Location = new System.Drawing.Point(3, 110);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 23);
            this.label6.TabIndex = 15;
            this.label6.Text = "Atrial Pulse Width";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Left;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label5.Location = new System.Drawing.Point(3, 84);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 20);
            this.label5.TabIndex = 14;
            this.label5.Text = "Upper Rate Limit";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(241, 4);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Value";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 4);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Parameter";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Left;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label3.Location = new System.Drawing.Point(3, 55);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 23);
            this.label3.TabIndex = 2;
            this.label3.Text = "Lower Rate Limit";
            // 
            // ppLRLButton
            // 
            this.ppLRLButton.Location = new System.Drawing.Point(300, 52);
            this.ppLRLButton.Name = "ppLRLButton";
            this.ppLRLButton.Size = new System.Drawing.Size(33, 23);
            this.ppLRLButton.TabIndex = 4;
            this.ppLRLButton.Text = "Set";
            this.ppLRLButton.UseVisualStyleBackColor = true;
            // 
            // ppAPulseWidthValue
            // 
            this.ppAPulseWidthValue.Location = new System.Drawing.Point(241, 107);
            this.ppAPulseWidthValue.Name = "ppAPulseWidthValue";
            this.ppAPulseWidthValue.Size = new System.Drawing.Size(53, 20);
            this.ppAPulseWidthValue.TabIndex = 5;
            // 
            // ppURLValue
            // 
            this.ppURLValue.Location = new System.Drawing.Point(241, 81);
            this.ppURLValue.Name = "ppURLValue";
            this.ppURLValue.Size = new System.Drawing.Size(53, 20);
            this.ppURLValue.TabIndex = 6;
            // 
            // ppURLButton
            // 
            this.ppURLButton.Location = new System.Drawing.Point(300, 81);
            this.ppURLButton.Name = "ppURLButton";
            this.ppURLButton.Size = new System.Drawing.Size(33, 20);
            this.ppURLButton.TabIndex = 7;
            this.ppURLButton.Text = "Set";
            this.ppURLButton.UseVisualStyleBackColor = true;
            // 
            // ppAPulseWidthButton
            // 
            this.ppAPulseWidthButton.Location = new System.Drawing.Point(300, 107);
            this.ppAPulseWidthButton.Name = "ppAPulseWidthButton";
            this.ppAPulseWidthButton.Size = new System.Drawing.Size(33, 23);
            this.ppAPulseWidthButton.TabIndex = 8;
            this.ppAPulseWidthButton.Text = "Set";
            this.ppAPulseWidthButton.UseVisualStyleBackColor = true;
            // 
            // ppVPulseWidthValue
            // 
            this.ppVPulseWidthValue.Location = new System.Drawing.Point(241, 136);
            this.ppVPulseWidthValue.Name = "ppVPulseWidthValue";
            this.ppVPulseWidthValue.Size = new System.Drawing.Size(53, 20);
            this.ppVPulseWidthValue.TabIndex = 9;
            // 
            // ppAPulseAmplitudeValue
            // 
            this.ppAPulseAmplitudeValue.Location = new System.Drawing.Point(241, 165);
            this.ppAPulseAmplitudeValue.Name = "ppAPulseAmplitudeValue";
            this.ppAPulseAmplitudeValue.Size = new System.Drawing.Size(53, 20);
            this.ppAPulseAmplitudeValue.TabIndex = 10;
            // 
            // ppVPulseWidthButton
            // 
            this.ppVPulseWidthButton.Location = new System.Drawing.Point(300, 136);
            this.ppVPulseWidthButton.Name = "ppVPulseWidthButton";
            this.ppVPulseWidthButton.Size = new System.Drawing.Size(33, 23);
            this.ppVPulseWidthButton.TabIndex = 11;
            this.ppVPulseWidthButton.Text = "Set";
            this.ppVPulseWidthButton.UseVisualStyleBackColor = true;
            // 
            // ppAPulseAmplitudeButton
            // 
            this.ppAPulseAmplitudeButton.Location = new System.Drawing.Point(300, 165);
            this.ppAPulseAmplitudeButton.Name = "ppAPulseAmplitudeButton";
            this.ppAPulseAmplitudeButton.Size = new System.Drawing.Size(33, 23);
            this.ppAPulseAmplitudeButton.TabIndex = 12;
            this.ppAPulseAmplitudeButton.Text = "Set";
            this.ppAPulseAmplitudeButton.UseVisualStyleBackColor = true;
            // 
            // ppFixedAVDelayValue
            // 
            this.ppFixedAVDelayValue.Location = new System.Drawing.Point(241, 223);
            this.ppFixedAVDelayValue.Name = "ppFixedAVDelayValue";
            this.ppFixedAVDelayValue.Size = new System.Drawing.Size(53, 20);
            this.ppFixedAVDelayValue.TabIndex = 23;
            // 
            // ppFixedAVDelayButton
            // 
            this.ppFixedAVDelayButton.Location = new System.Drawing.Point(300, 223);
            this.ppFixedAVDelayButton.Name = "ppFixedAVDelayButton";
            this.ppFixedAVDelayButton.Size = new System.Drawing.Size(33, 23);
            this.ppFixedAVDelayButton.TabIndex = 24;
            this.ppFixedAVDelayButton.Text = "Set";
            this.ppFixedAVDelayButton.UseVisualStyleBackColor = true;
            // 
            // ppBOMValue
            // 
            this.ppBOMValue.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ppBOMValue.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ppBOMValue.FormattingEnabled = true;
            this.ppBOMValue.Items.AddRange(new object[] {
            "AOO",
            "VOO",
            "DOO",
            "DDI",
            "VVI",
            "VDD",
            "AAI",
            "VVT",
            "AAT"});
            this.ppBOMValue.Location = new System.Drawing.Point(241, 23);
            this.ppBOMValue.Name = "ppBOMValue";
            this.ppBOMValue.Size = new System.Drawing.Size(53, 21);
            this.ppBOMValue.TabIndex = 25;
            // 
            // ppLRLValue
            // 
            this.ppLRLValue.Location = new System.Drawing.Point(241, 52);
            this.ppLRLValue.Name = "ppLRLValue";
            this.ppLRLValue.Size = new System.Drawing.Size(53, 20);
            this.ppLRLValue.TabIndex = 28;
            // 
            // contentProgrammableParameters
            // 
            this.contentProgrammableParameters.Controls.Add(this.tableLayoutPanel2);
            this.contentProgrammableParameters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contentProgrammableParameters.Location = new System.Drawing.Point(3, 3);
            this.contentProgrammableParameters.Name = "contentProgrammableParameters";
            this.contentProgrammableParameters.Size = new System.Drawing.Size(346, 271);
            this.contentProgrammableParameters.TabIndex = 0;
            this.contentProgrammableParameters.TabStop = false;
            this.contentProgrammableParameters.Text = "Programmable Parameters";
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(3, 3);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(75, 23);
            this.startButton.TabIndex = 0;
            this.startButton.Text = "Start";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.stopButton, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.startButton, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.resetButton, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.closeButton, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(457, 30);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // stopButton
            // 
            this.stopButton.Location = new System.Drawing.Point(84, 3);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(64, 23);
            this.stopButton.TabIndex = 3;
            this.stopButton.Text = "Stop";
            this.stopButton.UseVisualStyleBackColor = true;
            // 
            // resetButton
            // 
            this.resetButton.Location = new System.Drawing.Point(154, 3);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(75, 23);
            this.resetButton.TabIndex = 1;
            this.resetButton.Text = "Reset";
            this.resetButton.UseVisualStyleBackColor = true;
            // 
            // closeButton
            // 
            this.closeButton.Location = new System.Drawing.Point(235, 3);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 2;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(463, 49);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Controls";
            // 
            // mpBattStatusValue
            // 
            this.mpBattStatusValue.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.mpBattStatusValue.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.mpBattStatusValue.FormattingEnabled = true;
            this.mpBattStatusValue.Items.AddRange(new object[] {
            "BOL",
            "ERN",
            "ERT",
            "ERP"});
            this.mpBattStatusValue.Location = new System.Drawing.Point(171, 107);
            this.mpBattStatusValue.Name = "mpBattStatusValue";
            this.mpBattStatusValue.Size = new System.Drawing.Size(53, 21);
            this.mpBattStatusValue.TabIndex = 17;
            // 
            // tableRightPacemakerGUI
            // 
            this.tableRightPacemakerGUI.ColumnCount = 1;
            this.tableRightPacemakerGUI.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableRightPacemakerGUI.Controls.Add(this.valueMarker, 0, 3);
            this.tableRightPacemakerGUI.Controls.Add(this.groupBox2, 0, 0);
            this.tableRightPacemakerGUI.Controls.Add(this.groupBox3, 0, 1);
            this.tableRightPacemakerGUI.Controls.Add(this.groupBox6, 0, 2);
            this.tableRightPacemakerGUI.Dock = System.Windows.Forms.DockStyle.Right;
            this.tableRightPacemakerGUI.Location = new System.Drawing.Point(358, 0);
            this.tableRightPacemakerGUI.Name = "tableRightPacemakerGUI";
            this.tableRightPacemakerGUI.RowCount = 5;
            this.tableRightPacemakerGUI.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableRightPacemakerGUI.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableRightPacemakerGUI.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 165F));
            this.tableRightPacemakerGUI.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableRightPacemakerGUI.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableRightPacemakerGUI.Size = new System.Drawing.Size(469, 544);
            this.tableRightPacemakerGUI.TabIndex = 3;
            // 
            // valueMarker
            // 
            this.valueMarker.AutoSize = true;
            this.valueMarker.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.valueMarker.Location = new System.Drawing.Point(3, 532);
            this.valueMarker.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.valueMarker.Name = "valueMarker";
            this.valueMarker.Size = new System.Drawing.Size(0, 4);
            this.valueMarker.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(3, 280);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(346, 240);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Measured Parameters";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.tableLayoutPanel3.Controls.Add(this.label42, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.label14, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.button1, 3, 4);
            this.tableLayoutPanel3.Controls.Add(this.label40, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.label15, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.label16, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label17, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label18, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.mpAtrialBeatsButton, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.mpVentricularBeatsButton, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.mpBattStatusValue, 2, 5);
            this.tableLayoutPanel3.Controls.Add(this.mpBattStatusButton, 3, 5);
            this.tableLayoutPanel3.Controls.Add(this.mpPWaveValue, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.mpRWaveValue, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.mpPPMValue, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.mpPWaveBar, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.mpRWaveBar, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.trackBar1, 2, 4);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 6;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(340, 218);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Dock = System.Windows.Forms.DockStyle.Left;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label42.Location = new System.Drawing.Point(3, 110);
            this.label42.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(102, 108);
            this.label42.TabIndex = 37;
            this.label42.Text = "Battery Status Level";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(131, 4);
            this.label14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(25, 13);
            this.label14.TabIndex = 23;
            this.label14.Text = "Val";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(299, 78);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(33, 23);
            this.button1.TabIndex = 22;
            this.button1.Text = "Set";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Dock = System.Windows.Forms.DockStyle.Left;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label40.Location = new System.Drawing.Point(3, 81);
            this.label40.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(30, 23);
            this.label40.TabIndex = 20;
            this.label40.Text = "PPM";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Left;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label15.Location = new System.Drawing.Point(3, 55);
            this.label15.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(47, 20);
            this.label15.TabIndex = 14;
            this.label15.Text = "R Wave";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(171, 4);
            this.label16.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(43, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "Select";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(3, 4);
            this.label17.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(64, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Parameter";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Dock = System.Windows.Forms.DockStyle.Left;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label18.Location = new System.Drawing.Point(3, 26);
            this.label18.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(46, 23);
            this.label18.TabIndex = 2;
            this.label18.Text = "P Wave";
            // 
            // mpAtrialBeatsButton
            // 
            this.mpAtrialBeatsButton.Location = new System.Drawing.Point(299, 23);
            this.mpAtrialBeatsButton.Name = "mpAtrialBeatsButton";
            this.mpAtrialBeatsButton.Size = new System.Drawing.Size(33, 23);
            this.mpAtrialBeatsButton.TabIndex = 4;
            this.mpAtrialBeatsButton.Text = "Set";
            this.mpAtrialBeatsButton.UseVisualStyleBackColor = true;
            // 
            // mpVentricularBeatsButton
            // 
            this.mpVentricularBeatsButton.Location = new System.Drawing.Point(299, 52);
            this.mpVentricularBeatsButton.Name = "mpVentricularBeatsButton";
            this.mpVentricularBeatsButton.Size = new System.Drawing.Size(33, 20);
            this.mpVentricularBeatsButton.TabIndex = 7;
            this.mpVentricularBeatsButton.Text = "Set";
            this.mpVentricularBeatsButton.UseVisualStyleBackColor = true;
            this.mpVentricularBeatsButton.Click += new System.EventHandler(this.mpVentricularBeatsButton_Click_1);
            // 
            // mpBattStatusButton
            // 
            this.mpBattStatusButton.Location = new System.Drawing.Point(299, 107);
            this.mpBattStatusButton.Name = "mpBattStatusButton";
            this.mpBattStatusButton.Size = new System.Drawing.Size(33, 23);
            this.mpBattStatusButton.TabIndex = 8;
            this.mpBattStatusButton.Text = "Set";
            this.mpBattStatusButton.UseVisualStyleBackColor = true;
            // 
            // mpPWaveValue
            // 
            this.mpPWaveValue.AutoSize = true;
            this.mpPWaveValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.mpPWaveValue.Location = new System.Drawing.Point(131, 24);
            this.mpPWaveValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.mpPWaveValue.Name = "mpPWaveValue";
            this.mpPWaveValue.Size = new System.Drawing.Size(0, 13);
            this.mpPWaveValue.TabIndex = 34;
            // 
            // mpRWaveValue
            // 
            this.mpRWaveValue.AutoSize = true;
            this.mpRWaveValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.mpRWaveValue.Location = new System.Drawing.Point(131, 53);
            this.mpRWaveValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.mpRWaveValue.Name = "mpRWaveValue";
            this.mpRWaveValue.Size = new System.Drawing.Size(0, 13);
            this.mpRWaveValue.TabIndex = 35;
            // 
            // mpPPMValue
            // 
            this.mpPPMValue.AutoSize = true;
            this.mpPPMValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.mpPPMValue.Location = new System.Drawing.Point(131, 79);
            this.mpPPMValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.mpPPMValue.Name = "mpPPMValue";
            this.mpPPMValue.Size = new System.Drawing.Size(0, 13);
            this.mpPPMValue.TabIndex = 36;
            // 
            // tableLeftPacemakerGUI
            // 
            this.tableLeftPacemakerGUI.ColumnCount = 1;
            this.tableLeftPacemakerGUI.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLeftPacemakerGUI.Controls.Add(this.groupBox1, 0, 1);
            this.tableLeftPacemakerGUI.Controls.Add(this.contentProgrammableParameters, 0, 0);
            this.tableLeftPacemakerGUI.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLeftPacemakerGUI.Location = new System.Drawing.Point(0, 0);
            this.tableLeftPacemakerGUI.Name = "tableLeftPacemakerGUI";
            this.tableLeftPacemakerGUI.RowCount = 3;
            this.tableLeftPacemakerGUI.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.01289F));
            this.tableLeftPacemakerGUI.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.98711F));
            this.tableLeftPacemakerGUI.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLeftPacemakerGUI.Size = new System.Drawing.Size(352, 544);
            this.tableLeftPacemakerGUI.TabIndex = 2;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.tableLayoutPanel7);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Location = new System.Drawing.Point(3, 366);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(463, 159);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Events";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel9, 1, 1);
            this.tableLayoutPanel7.Controls.Add(this.pulso, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.label43, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel8, 0, 1);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.3125F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 79.6875F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(457, 140);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // pulso
            // 
            this.pulso.AutoSize = true;
            this.pulso.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pulso.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.pulso.Location = new System.Drawing.Point(3, 4);
            this.pulso.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.pulso.Name = "pulso";
            this.pulso.Size = new System.Drawing.Size(222, 24);
            this.pulso.TabIndex = 7;
            this.pulso.Text = "Atrial Pulse";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label43.Location = new System.Drawing.Point(231, 4);
            this.label43.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(103, 13);
            this.label43.TabIndex = 8;
            this.label43.Text = "Ventricular Pulse";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 3;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.64865F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 81F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.35135F));
            this.tableLayoutPanel8.Controls.Add(this.evATValue, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.evAWidthValue, 1, 1);
            this.tableLayoutPanel8.Controls.Add(this.evAAmpValue, 1, 2);
            this.tableLayoutPanel8.Controls.Add(this.label48, 0, 4);
            this.tableLayoutPanel8.Controls.Add(this.label47, 0, 3);
            this.tableLayoutPanel8.Controls.Add(this.label46, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this.label45, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.label44, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.label49, 2, 1);
            this.tableLayoutPanel8.Controls.Add(this.label50, 2, 2);
            this.tableLayoutPanel8.Controls.Add(this.label51, 2, 3);
            this.tableLayoutPanel8.Controls.Add(this.label52, 2, 4);
            this.tableLayoutPanel8.Controls.Add(this.evANoiseValue, 1, 3);
            this.tableLayoutPanel8.Controls.Add(this.evATimeValue, 1, 4);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 31);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 7;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 53F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.Size = new System.Drawing.Size(222, 106);
            this.tableLayoutPanel8.TabIndex = 9;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label44.Location = new System.Drawing.Point(3, 4);
            this.label44.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(35, 13);
            this.label44.TabIndex = 2;
            this.label44.Text = "Type";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label45.Location = new System.Drawing.Point(3, 21);
            this.label45.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(40, 13);
            this.label45.TabIndex = 3;
            this.label45.Text = "Width";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label46.Location = new System.Drawing.Point(3, 41);
            this.label46.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(62, 13);
            this.label46.TabIndex = 4;
            this.label46.Text = "Amplitude";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label47.Location = new System.Drawing.Point(3, 61);
            this.label47.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(39, 13);
            this.label47.TabIndex = 5;
            this.label47.Text = "Noise";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label48.Location = new System.Drawing.Point(3, 81);
            this.label48.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(34, 13);
            this.label48.TabIndex = 6;
            this.label48.Text = "Time";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label49.Location = new System.Drawing.Point(152, 21);
            this.label49.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(67, 16);
            this.label49.TabIndex = 7;
            this.label49.Text = "ms";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(152, 41);
            this.label50.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(22, 13);
            this.label50.TabIndex = 8;
            this.label50.Text = "mV";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(152, 61);
            this.label51.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(0, 13);
            this.label51.TabIndex = 9;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(152, 81);
            this.label52.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(20, 13);
            this.label52.TabIndex = 10;
            this.label52.Text = "ms";
            // 
            // evATValue
            // 
            this.evATValue.AutoSize = true;
            this.evATValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.evATValue.Location = new System.Drawing.Point(71, 4);
            this.evATValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.evATValue.Name = "evATValue";
            this.evATValue.Size = new System.Drawing.Size(75, 13);
            this.evATValue.TabIndex = 11;
            // 
            // evANoiseValue
            // 
            this.evANoiseValue.AutoSize = true;
            this.evANoiseValue.Location = new System.Drawing.Point(71, 61);
            this.evANoiseValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.evANoiseValue.Name = "evANoiseValue";
            this.evANoiseValue.Size = new System.Drawing.Size(0, 13);
            this.evANoiseValue.TabIndex = 12;
            // 
            // evAWidthValue
            // 
            this.evAWidthValue.AutoSize = true;
            this.evAWidthValue.Location = new System.Drawing.Point(71, 21);
            this.evAWidthValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.evAWidthValue.Name = "evAWidthValue";
            this.evAWidthValue.Size = new System.Drawing.Size(0, 13);
            this.evAWidthValue.TabIndex = 13;
            // 
            // evAAmpValue
            // 
            this.evAAmpValue.AutoSize = true;
            this.evAAmpValue.Location = new System.Drawing.Point(71, 41);
            this.evAAmpValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.evAAmpValue.Name = "evAAmpValue";
            this.evAAmpValue.Size = new System.Drawing.Size(0, 13);
            this.evAAmpValue.TabIndex = 14;
            // 
            // evATimeValue
            // 
            this.evATimeValue.AutoSize = true;
            this.evATimeValue.Location = new System.Drawing.Point(71, 81);
            this.evATimeValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.evATimeValue.Name = "evATimeValue";
            this.evATimeValue.Size = new System.Drawing.Size(0, 13);
            this.evATimeValue.TabIndex = 15;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 3;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.64865F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 81F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.35135F));
            this.tableLayoutPanel9.Controls.Add(this.evVTypeValue, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.evVWidthValue, 1, 1);
            this.tableLayoutPanel9.Controls.Add(this.evVAmpValue, 1, 2);
            this.tableLayoutPanel9.Controls.Add(this.label61, 0, 4);
            this.tableLayoutPanel9.Controls.Add(this.label62, 0, 3);
            this.tableLayoutPanel9.Controls.Add(this.label63, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this.label64, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.label65, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.label66, 2, 1);
            this.tableLayoutPanel9.Controls.Add(this.label67, 2, 2);
            this.tableLayoutPanel9.Controls.Add(this.label69, 2, 4);
            this.tableLayoutPanel9.Controls.Add(this.evVNoiseValue, 1, 3);
            this.tableLayoutPanel9.Controls.Add(this.evVTimeValue, 1, 4);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(231, 31);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 7;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 53F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel9.Size = new System.Drawing.Size(223, 106);
            this.tableLayoutPanel9.TabIndex = 10;
            // 
            // evVTypeValue
            // 
            this.evVTypeValue.AutoSize = true;
            this.evVTypeValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.evVTypeValue.Location = new System.Drawing.Point(72, 4);
            this.evVTypeValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.evVTypeValue.Name = "evVTypeValue";
            this.evVTypeValue.Size = new System.Drawing.Size(75, 13);
            this.evVTypeValue.TabIndex = 11;
            // 
            // evVWidthValue
            // 
            this.evVWidthValue.AutoSize = true;
            this.evVWidthValue.Location = new System.Drawing.Point(72, 21);
            this.evVWidthValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.evVWidthValue.Name = "evVWidthValue";
            this.evVWidthValue.Size = new System.Drawing.Size(0, 13);
            this.evVWidthValue.TabIndex = 13;
            // 
            // evVAmpValue
            // 
            this.evVAmpValue.AutoSize = true;
            this.evVAmpValue.Location = new System.Drawing.Point(72, 41);
            this.evVAmpValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.evVAmpValue.Name = "evVAmpValue";
            this.evVAmpValue.Size = new System.Drawing.Size(0, 13);
            this.evVAmpValue.TabIndex = 14;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label61.Location = new System.Drawing.Point(3, 81);
            this.label61.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(34, 13);
            this.label61.TabIndex = 6;
            this.label61.Text = "Time";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label62.Location = new System.Drawing.Point(3, 61);
            this.label62.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(39, 13);
            this.label62.TabIndex = 5;
            this.label62.Text = "Noise";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label63.Location = new System.Drawing.Point(3, 41);
            this.label63.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(62, 13);
            this.label63.TabIndex = 4;
            this.label63.Text = "Amplitude";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label64.Location = new System.Drawing.Point(3, 21);
            this.label64.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(40, 13);
            this.label64.TabIndex = 3;
            this.label64.Text = "Width";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label65.Location = new System.Drawing.Point(3, 4);
            this.label65.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(35, 13);
            this.label65.TabIndex = 2;
            this.label65.Text = "Type";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label66.Location = new System.Drawing.Point(153, 21);
            this.label66.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(67, 16);
            this.label66.TabIndex = 7;
            this.label66.Text = "ms";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(153, 41);
            this.label67.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(22, 13);
            this.label67.TabIndex = 8;
            this.label67.Text = "mV";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(153, 81);
            this.label69.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(20, 13);
            this.label69.TabIndex = 10;
            this.label69.Text = "ms";
            // 
            // evVNoiseValue
            // 
            this.evVNoiseValue.AutoSize = true;
            this.evVNoiseValue.Location = new System.Drawing.Point(72, 61);
            this.evVNoiseValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.evVNoiseValue.Name = "evVNoiseValue";
            this.evVNoiseValue.Size = new System.Drawing.Size(0, 13);
            this.evVNoiseValue.TabIndex = 12;
            // 
            // evVTimeValue
            // 
            this.evVTimeValue.AutoSize = true;
            this.evVTimeValue.Location = new System.Drawing.Point(72, 81);
            this.evVTimeValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.evVTimeValue.Name = "evVTimeValue";
            this.evVTimeValue.Size = new System.Drawing.Size(0, 13);
            this.evVTimeValue.TabIndex = 15;
            // 
            // mpPWaveBar
            // 
            this.mpPWaveBar.Location = new System.Drawing.Point(171, 23);
            this.mpPWaveBar.Name = "mpPWaveBar";
            this.mpPWaveBar.Size = new System.Drawing.Size(100, 20);
            this.mpPWaveBar.TabIndex = 38;
            // 
            // mpRWaveBar
            // 
            this.mpRWaveBar.Location = new System.Drawing.Point(171, 52);
            this.mpRWaveBar.Name = "mpRWaveBar";
            this.mpRWaveBar.Size = new System.Drawing.Size(100, 20);
            this.mpRWaveBar.TabIndex = 39;
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(171, 78);
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(100, 20);
            this.trackBar1.TabIndex = 40;
            // 
            // PacemakerGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 544);
            this.Controls.Add(this.tableRightPacemakerGUI);
            this.Controls.Add(this.tableLeftPacemakerGUI);
            this.Name = "PacemakerGUI";
            this.Text = "Pacemaker GUI";
            this.groupBox3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.contentProgrammableParameters.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.tableRightPacemakerGUI.ResumeLayout(false);
            this.tableRightPacemakerGUI.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLeftPacemakerGUI.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ppBOMButton;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button ppVPulseAmplitudeButton;
        private System.Windows.Forms.MaskedTextBox ppVPulseAmplitudeValue;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button ppLRLButton;
        private System.Windows.Forms.MaskedTextBox ppAPulseWidthValue;
        private System.Windows.Forms.MaskedTextBox ppURLValue;
        private System.Windows.Forms.Button ppURLButton;
        private System.Windows.Forms.Button ppAPulseWidthButton;
        private System.Windows.Forms.MaskedTextBox ppVPulseWidthValue;
        private System.Windows.Forms.MaskedTextBox ppAPulseAmplitudeValue;
        private System.Windows.Forms.Button ppVPulseWidthButton;
        private System.Windows.Forms.Button ppAPulseAmplitudeButton;
        private System.Windows.Forms.MaskedTextBox ppFixedAVDelayValue;
        private System.Windows.Forms.Button ppFixedAVDelayButton;
        private System.Windows.Forms.ComboBox ppBOMValue;
        private System.Windows.Forms.MaskedTextBox ppLRLValue;
        private System.Windows.Forms.GroupBox contentProgrammableParameters;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button resetButton;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox mpBattStatusValue;
        private System.Windows.Forms.TableLayoutPanel tableRightPacemakerGUI;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button mpAtrialBeatsButton;
        private System.Windows.Forms.Button mpVentricularBeatsButton;
        private System.Windows.Forms.Button mpBattStatusButton;
        private System.Windows.Forms.TableLayoutPanel tableLeftPacemakerGUI;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label monPWaveValue;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.Label monRWaveValue;
        private System.Windows.Forms.Label monBPMValue;
        private System.Windows.Forms.Label monBattLevelValue;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label monAPulseWidth;
        private System.Windows.Forms.Label monStatus;
        private System.Windows.Forms.Label monURLValue;
        private System.Windows.Forms.Label monLRLValue;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label monAPAmpValue;
        private System.Windows.Forms.Label monVPAmpValue;
        private System.Windows.Forms.Label monFixAVDelayValue;
        private System.Windows.Forms.Label monBOMValue;
        private System.Windows.Forms.Label monVPWidthValue;
        private System.Windows.Forms.Label valueMarker;
        private System.Windows.Forms.Label monAPWidthValue;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label monTimeValue;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label mpPWaveValue;
        private System.Windows.Forms.Label mpRWaveValue;
        private System.Windows.Forms.Label mpPPMValue;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Label evVTypeValue;
        private System.Windows.Forms.Label evVWidthValue;
        private System.Windows.Forms.Label evVAmpValue;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label evVNoiseValue;
        private System.Windows.Forms.Label evVTimeValue;
        private System.Windows.Forms.Label pulso;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label evATValue;
        private System.Windows.Forms.Label evAWidthValue;
        private System.Windows.Forms.Label evAAmpValue;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label evANoiseValue;
        private System.Windows.Forms.Label evATimeValue;
        private System.Windows.Forms.MaskedTextBox mpPWaveBar;
        private System.Windows.Forms.MaskedTextBox mpRWaveBar;
        private System.Windows.Forms.MaskedTextBox trackBar1;
    }
}

