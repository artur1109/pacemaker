//***********************************************************************************************
//* File: 'Classes.cs'
//* THIS IS A GENERATED FILE: DO NOT EDIT. Please edit the Perfect Developer source file instead!
//*
//* Generated from: 'C:\Users\user\Dropbox\Academico\Pesquisa\pacemaker-project\pacemaker-pd\Classes.pd'
//* by Perfect Developer version 6.00.00.03 at 03:05:28 UTC on Sunday February 2nd 2014
//* Using command line options:
//* -z1 -el=3 -em=100 -gl=C# -gs=1 -gv=ISO -gw=100 -gdp=1 -gdo=0 -gdc=3 -gda=1 -gdA=0 -gdl=0 -gdr=0 -gdt=0 -gdi=1 -st=4 -sb=C:\Program Files\Escher Technologies\Verification Studio 6\Bin\builtin.pd -sr=C:\Program Files\Escher Technologies\Verification Studio 6\Bin\rubric.pd -q=0 -gk=Pacemaker -eM=0 -@=C:\Users\user\AppData\Local\Temp\etfF98.tmp
//***********************************************************************************************

// Namespaces imported
using System;

using Ertsys;
using Pacemaker;
#pragma warning disable 659 // disable warning "Foo overrides Object.Equals() but does not override Object.GetHashCode()"

namespace Pacemaker
{
    public sealed class _n5_quin < X, Y, Z, K, M > : _eAny where X : IComparable where Y :
        IComparable where Z : IComparable where K : IComparable where M : IComparable
    {
        public X x;
        public Y y;
        public Z z;
        public K k;
        public M m;
        public _eRank _lRank (_n5_quin < X, Y, Z, K, M > a)
        {
            if (this == a)
            {
                return _eRank.same;
            }
            _eRank _vLet_t1_25_16 = _eSystem._oRank (x, a.x);
            if ((_vLet_t1_25_16 == _eRank.same))
            {
                _eRank _vLet_t2_27_23 = _eSystem._oRank (y, a.y);
                if ((_vLet_t2_27_23 == _eRank.same))
                {
                    _eRank _vLet_t3_29_30 = _eSystem._oRank (z, a.z);
                    if ((_vLet_t3_29_30 == _eRank.same))
                    {
                        _eRank _vLet_t4_31_38 = _eSystem._oRank (k, a.k);
                        return ((_vLet_t4_31_38 == _eRank.same) ?
                        _eSystem._oRank (m, a.m) : _vLet_t4_31_38);
                    }
                    else
                    {
                        return _vLet_t3_29_30;
                    }
                }
                else
                {
                    return _vLet_t2_27_23;
                }
            }
            else
            {
                return _vLet_t1_25_16;
            }
        }
        public override _eRank _oRank (object _lArg)
        {
            if (_lArg is _n5_quin < X, Y, Z, K, M >)
            {
                return _lRank ((_n5_quin < X, Y, Z, K, M >) _lArg);
            }
            return base._oRank (_lArg);
        }
        public override int CompareTo (object _lArg)
        {
            return _eSystem.RankToInt (_oRank (_lArg));
        }
        public override _eSeq < char > toString ()
        {
            return _eSystem._ltoString (x).prepend ('(').append (',')._oPlusPlus (_eSystem.
                _ltoString (y).append (','))._oPlusPlus (_eSystem._ltoString (z).append (',')).
                _oPlusPlus (_eSystem._ltoString (k).append (','))._oPlusPlus (_eSystem._ltoString (m)
                .append (')'));
        }
        public _n5_quin (X _vx, Y _vy, Z _vz, K _vk, M _vm) : base ()
        {
            x = _vx;
            y = _vy;
            z = _vz;
            k = _vk;
            m = _vm;
        }
        public bool _lEqual (_n5_quin < X, Y, Z, K, M > _vArg_21_9)
        {
            if (this == _vArg_21_9)
            {
                return true;
            }
            return ((((_eSystem.GenericEquals (_vArg_21_9.x, x) && _eSystem.GenericEquals (
                _vArg_21_9.y, y)) && _eSystem.GenericEquals (_vArg_21_9.z, z)) && _eSystem.
                GenericEquals (_vArg_21_9.k, k)) && _eSystem.GenericEquals (_vArg_21_9.m, m));
        }
        public bool _oLess (_n5_quin < X, Y, Z, K, M > _vArg_21_9)
        {
            return (this._oRank (_vArg_21_9) == _eRank.below);
        }
        public bool _oLessOrEqual (_n5_quin < X, Y, Z, K, M > _vArg_21_9)
        {
            return (!(this._oRank (_vArg_21_9) == _eRank.above));
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (_n5_quin < X, Y, Z,
                K, M >) && _lEqual ((_n5_quin < X, Y, Z, K, M >) _lArg));
        }
    }

    public enum ACTIVITY_THRESHOLD
    {
        VERY_LOW = 0, LOW = 1, MED_LOW = 2, MED = 3, MED_HIGH = 4, HIGH = 5, VERY_HIGH = 6
    }

    public enum BATT_STATUS_LEVEL
    {
        BOL = 0, ERN = 1, ERT = 2, ERP = 3
    }

    public enum BRADYCARDIA_STATE
    {
        PERMANENT = 0, TEMPORARY = 1, PACE_NOW = 2
    }

    public enum LEAD_CHAMBER_TYPE
    {
        L_ATRIAL = 0, L_VENTRICULAR = 1
    }

    public enum POLARITY
    {
        UNIPOLAR = 0, BIPOLAR = 1
    }

    public enum MarkerABB
    {
        AS = 0, AP = 1, AT = 2, VS = 3, VP = 4, PVC = 5, TN = 6, SRef = 7, HyPc = 8, SRPc = 9, Sm_Up
            = 10, Sm_Down = 11, ATR_Dur = 12, ATR_FB = 13, ATR_End = 14, PVPext = 15
    }

    public enum SWITCH
    {
        ON = 0, OFF = 1
    }

    public enum CHAMBERS
    {
        C_NONE = 0, C_DUAL = 1, C_ATRIUM = 2, C_VENTRICLE = 3
    }

    public enum RESPONSE
    {
        R_NONE = 0, TRIGGERED = 1, INHIBITED = 2, TRACKED = 3
    }

    public sealed class BOM : _eAny
    {
        public SWITCH _rswitch;
        public CHAMBERS chambers_paced;
        public CHAMBERS chambers_sensed;
        public RESPONSE response_to_sensing;
        public bool rate_modulation;
        public bool inVOO ()
        {
            return (((((SWITCH.ON == _rswitch) && (CHAMBERS.C_VENTRICLE == chambers_paced)) && (
                CHAMBERS.C_NONE == chambers_sensed)) && (RESPONSE.R_NONE == response_to_sensing)) &&
                (false == rate_modulation));
        }
        public bool inAOO ()
        {
            return (((((SWITCH.ON == _rswitch) && (CHAMBERS.C_ATRIUM == chambers_paced)) && (
                CHAMBERS.C_NONE == chambers_sensed)) && (RESPONSE.R_NONE == response_to_sensing)) &&
                (false == rate_modulation));
        }
        public bool inDOO ()
        {
            return (((((SWITCH.ON == _rswitch) && (CHAMBERS.C_DUAL == chambers_paced)) && (CHAMBERS.
                C_NONE == chambers_sensed)) && (RESPONSE.R_NONE == response_to_sensing)) && (false
                == rate_modulation));
        }
        public bool inDDI ()
        {
            return (((((SWITCH.ON == _rswitch) && (CHAMBERS.C_DUAL == chambers_paced)) && (CHAMBERS.
                C_DUAL == chambers_sensed)) && (RESPONSE.INHIBITED == response_to_sensing)) && (
                false == rate_modulation));
        }
        public bool inVVI ()
        {
            return (((((SWITCH.ON == _rswitch) && (CHAMBERS.C_VENTRICLE == chambers_paced)) && (
                CHAMBERS.C_VENTRICLE == chambers_sensed)) && (RESPONSE.INHIBITED ==
                response_to_sensing)) && (false == rate_modulation));
        }
        public bool inVDD ()
        {
            return (((((SWITCH.ON == _rswitch) && (CHAMBERS.C_VENTRICLE == chambers_paced)) && (
                CHAMBERS.C_DUAL == chambers_sensed)) && (RESPONSE.TRACKED == response_to_sensing))
                && (false == rate_modulation));
        }
        public bool inAAI ()
        {
            return (((((SWITCH.ON == _rswitch) && (CHAMBERS.C_ATRIUM == chambers_paced)) && (
                CHAMBERS.C_ATRIUM == chambers_sensed)) && (RESPONSE.INHIBITED == response_to_sensing)
                ) && (false == rate_modulation));
        }
        public bool inVVT ()
        {
            return (((((SWITCH.ON == _rswitch) && (CHAMBERS.C_VENTRICLE == chambers_paced)) && (
                CHAMBERS.C_VENTRICLE == chambers_sensed)) && (RESPONSE.TRIGGERED ==
                response_to_sensing)) && (false == rate_modulation));
        }
        public bool inAAT ()
        {
            return (((((SWITCH.ON == _rswitch) && (CHAMBERS.C_ATRIUM == chambers_paced)) && (
                CHAMBERS.C_ATRIUM == chambers_sensed)) && (RESPONSE.TRIGGERED == response_to_sensing)
                ) && (false == rate_modulation));
        }
        public bool inAXXX ()
        {
            return ((SWITCH.ON == _rswitch) && (CHAMBERS.C_ATRIUM == chambers_paced));
        }
        public bool inVXXX ()
        {
            return ((SWITCH.ON == _rswitch) && (CHAMBERS.C_VENTRICLE == chambers_paced));
        }
        public bool inDXXX ()
        {
            return ((SWITCH.ON == _rswitch) && (CHAMBERS.C_DUAL == chambers_paced));
        }
        public bool inOXO ()
        {
            return (((SWITCH.ON == _rswitch) && (CHAMBERS.C_NONE == chambers_paced)) && (RESPONSE.
                R_NONE == response_to_sensing));
        }
        public BOM (SWITCH _vswitch, CHAMBERS _vchambers_paced, CHAMBERS _vchambers_sensed, RESPONSE
            _vresponse_to_sensing, bool _vrate_modulation) : base ()
        {
            _rswitch = _vswitch;
            chambers_paced = _vchambers_paced;
            chambers_sensed = _vchambers_sensed;
            response_to_sensing = _vresponse_to_sensing;
            rate_modulation = _vrate_modulation;
        }
        public bool _lEqual (BOM _vArg_86_13)
        {
            if (this == _vArg_86_13)
            {
                return true;
            }
            return (((((_vArg_86_13._rswitch == _rswitch) && (_vArg_86_13.chambers_paced ==
                chambers_paced)) && (_vArg_86_13.chambers_sensed == chambers_sensed)) && (
                _vArg_86_13.response_to_sensing == response_to_sensing)) && (_vArg_86_13.
                rate_modulation == rate_modulation));
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (BOM) && _lEqual ((
                BOM) _lArg));
        }
    }

    public sealed class TimeSt : _eAny
    {
        void _lc_TimeSt (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((0 <= time))) throw new _xClassInvariantItem ("Classes.pd:315,20", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_TimeSt (_lArg);
        }
        public int time;
        public int a_start_time;
        public int a_max;
        public int a_delay;
        public int v_start_time;
        public int v_max;
        public int v_delay;
        public int a_curr_measurement;
        public int v_curr_measurement;
        public TimeSt (int _vtime, int _va_start_time, int _va_max, int _va_delay, int
            _vv_start_time, int _vv_max, int _vv_delay, int _va_curr_measurement, int
            _vv_curr_measurement) : base ()
        {
            time = _vtime;
            a_start_time = _va_start_time;
            a_max = _va_max;
            a_delay = _va_delay;
            v_start_time = _vv_start_time;
            v_max = _vv_max;
            v_delay = _vv_delay;
            a_curr_measurement = _va_curr_measurement;
            v_curr_measurement = _vv_curr_measurement;
            _lc_TimeSt ("Classes.pd:322,5");
        }
        public void chg_time (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((0 <= x))) throw new _xPre ("Classes.pd:333,14");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            time = x;
            _lClassInvariantCheck ("Classes.pd:334,14");
        }
        public void chg_a_start_time (int x)
        {
            a_start_time = x;
            _lClassInvariantCheck ("Classes.pd:338,14");
        }
        public void chg_a_max (int x)
        {
            a_max = x;
            _lClassInvariantCheck ("Classes.pd:342,14");
        }
        public void chg_a_delay (int x)
        {
            a_delay = x;
            _lClassInvariantCheck ("Classes.pd:346,14");
        }
        public void chg_v_start_time (int x)
        {
            v_start_time = x;
            _lClassInvariantCheck ("Classes.pd:349,14");
        }
        public void chg_v_max (int x)
        {
            v_max = x;
            _lClassInvariantCheck ("Classes.pd:353,14");
        }
        public void chg_v_delay (int x)
        {
            v_delay = x;
            _lClassInvariantCheck ("Classes.pd:357,14");
        }
        public void chg_v_curr_measurement (int x)
        {
            v_curr_measurement = x;
            _lClassInvariantCheck ("Classes.pd:361,14");
        }
        public void chg_a_curr_measurement (int x)
        {
            a_curr_measurement = x;
            _lClassInvariantCheck ("Classes.pd:365,14");
        }
        public bool _lEqual (TimeSt _vArg_305_13)
        {
            if (this == _vArg_305_13)
            {
                return true;
            }
            return (((((((((_vArg_305_13.time == time) && (_vArg_305_13.a_start_time == a_start_time)
                ) && (_vArg_305_13.a_max == a_max)) && (_vArg_305_13.a_delay == a_delay)) && (
                _vArg_305_13.v_start_time == v_start_time)) && (_vArg_305_13.v_max == v_max)) && (
                _vArg_305_13.v_delay == v_delay)) && (_vArg_305_13.a_curr_measurement ==
                a_curr_measurement)) && (_vArg_305_13.v_curr_measurement == v_curr_measurement));
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (TimeSt) && _lEqual
                ((TimeSt) _lArg));
        }
    }

    public sealed class SensingPulse : _eAny
    {
        public int cardiac_cycles_length;
        public int a_sensing_threshold;
        public int v_sensing_threshold;
        public int sensed_a_pulse_width;
        public int sensed_v_pulse_width;
        public int sensed_a_pulse_amp;
        public int sensed_v_pulse_amp;
        public int last_s_a_pulse;
        public int last_s_v_pulse;
        public int current_rate;
        public SensingPulse (int _vcardiac_cycles_length, int _va_sensing_threshold, int
            _vv_sensing_threshold, int _vsensed_a_pulse_width, int _vsensed_v_pulse_width, int
            _vsensed_a_pulse_amp, int _vsensed_v_pulse_amp, int _vlast_s_a_pulse, int
            _vlast_s_v_pulse, int _vcurrent_rate) : base ()
        {
            cardiac_cycles_length = _vcardiac_cycles_length;
            a_sensing_threshold = _va_sensing_threshold;
            v_sensing_threshold = _vv_sensing_threshold;
            sensed_a_pulse_width = _vsensed_a_pulse_width;
            sensed_v_pulse_width = _vsensed_v_pulse_width;
            sensed_a_pulse_amp = _vsensed_a_pulse_amp;
            sensed_v_pulse_amp = _vsensed_v_pulse_amp;
            last_s_a_pulse = _vlast_s_a_pulse;
            last_s_v_pulse = _vlast_s_v_pulse;
            current_rate = _vcurrent_rate;
        }
        public void chg_sensed_atrial (int x, int y, int z)
        {
            sensed_a_pulse_width = x;
            sensed_a_pulse_amp = y;
            last_s_a_pulse = z;
        }
        public void chg_sensed_ventricular (int x, int y, int k, int z)
        {
            sensed_v_pulse_width = x;
            sensed_v_pulse_amp = y;
            last_s_v_pulse = k;
            cardiac_cycles_length = z;
        }
        public void chg_cardiac_cycles_length (int x)
        {
            cardiac_cycles_length = x;
        }
        public void chg_a_sensing_threshold (int x)
        {
            a_sensing_threshold = x;
        }
        public void chg_v_sensing_threshold (int x)
        {
            v_sensing_threshold = x;
        }
        public void chg_sensed_a_pulse_width (int x)
        {
            sensed_a_pulse_width = x;
        }
        public void chg_sensed_v_pulse_width (int x)
        {
            sensed_v_pulse_width = x;
        }
        public void chg_sensed_a_pulse_amp (int x)
        {
            sensed_a_pulse_amp = x;
        }
        public void chg_sensed_v_pulse_amp (int x)
        {
            sensed_v_pulse_amp = x;
        }
        public void chg_last_s_a_pulse (int x)
        {
            last_s_a_pulse = x;
        }
        public void chg_last_s_v_pulse (int x)
        {
            last_s_v_pulse = x;
        }
        public void chg_current_rate (int x)
        {
            current_rate = x;
        }
        public bool _lEqual (SensingPulse _vArg_371_13)
        {
            if (this == _vArg_371_13)
            {
                return true;
            }
            return ((((((((((_vArg_371_13.cardiac_cycles_length == cardiac_cycles_length) && (
                _vArg_371_13.a_sensing_threshold == a_sensing_threshold)) && (_vArg_371_13.
                v_sensing_threshold == v_sensing_threshold)) && (_vArg_371_13.sensed_a_pulse_width
                == sensed_a_pulse_width)) && (_vArg_371_13.sensed_v_pulse_width ==
                sensed_v_pulse_width)) && (_vArg_371_13.sensed_a_pulse_amp == sensed_a_pulse_amp))
                && (_vArg_371_13.sensed_v_pulse_amp == sensed_v_pulse_amp)) && (_vArg_371_13.
                last_s_a_pulse == last_s_a_pulse)) && (_vArg_371_13.last_s_v_pulse == last_s_v_pulse)
                ) && (_vArg_371_13.current_rate == current_rate));
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (SensingPulse) &&
                _lEqual ((SensingPulse) _lArg));
        }
    }

    public sealed class PacingPulse : _eAny
    {
        public int paced_a_pulse_width;
        public int paced_v_pulse_width;
        public int paced_a_pulse_amp;
        public int paced_v_pulse_amp;
        public int last_p_a_pulse;
        public int last_p_v_pulse;
        public PacingPulse (int _vpaced_a_pulse_width, int _vpaced_v_pulse_width, int
            _vpaced_a_pulse_amp, int _vpaced_v_pulse_amp, int _vlast_p_a_pulse, int _vlast_p_v_pulse)
            : base ()
        {
            paced_a_pulse_width = _vpaced_a_pulse_width;
            paced_v_pulse_width = _vpaced_v_pulse_width;
            paced_a_pulse_amp = _vpaced_a_pulse_amp;
            paced_v_pulse_amp = _vpaced_v_pulse_amp;
            last_p_a_pulse = _vlast_p_a_pulse;
            last_p_v_pulse = _vlast_p_v_pulse;
        }
        public void chg_paced_atrial (int x, int y, int z)
        {
            paced_a_pulse_width = x;
            paced_a_pulse_amp = y;
            last_p_a_pulse = z;
        }
        public void chg_paced_ventricular (int x, int y, int z)
        {
            paced_v_pulse_width = x;
            paced_v_pulse_amp = y;
            last_p_v_pulse = x;
        }
        public void chg_paced_dual (int x, int y, int z, int k, int m)
        {
            paced_a_pulse_width = x;
            paced_a_pulse_amp = y;
            last_p_a_pulse = m;
            paced_v_pulse_width = z;
            paced_v_pulse_amp = k;
            last_p_v_pulse = m;
        }
        public void chg_paced_a_pulse_width (int x)
        {
            paced_a_pulse_width = x;
        }
        public void chg_paced_v_pulse_width (int x)
        {
            paced_v_pulse_width = x;
        }
        public void chg_paced_a_pulse_amp (int x)
        {
            paced_a_pulse_amp = x;
        }
        public void chg_paced_v_pulse_amp (int x)
        {
            paced_v_pulse_amp = x;
        }
        public void chg_last_p_a_pulse (int x)
        {
            last_p_a_pulse = x;
        }
        public void chg_last_p_v_pulse (int x)
        {
            last_p_v_pulse = x;
        }
        public bool _lEqual (PacingPulse _vArg_449_13)
        {
            if (this == _vArg_449_13)
            {
                return true;
            }
            return ((((((_vArg_449_13.paced_a_pulse_width == paced_a_pulse_width) && (_vArg_449_13.
                paced_v_pulse_width == paced_v_pulse_width)) && (_vArg_449_13.paced_a_pulse_amp ==
                paced_a_pulse_amp)) && (_vArg_449_13.paced_v_pulse_amp == paced_v_pulse_amp)) && (
                _vArg_449_13.last_p_a_pulse == last_p_a_pulse)) && (_vArg_449_13.last_p_v_pulse ==
                last_p_v_pulse));
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (PacingPulse) &&
                _lEqual ((PacingPulse) _lArg));
        }
    }

    public sealed class BatteryStatus : _eAny
    {
        void _lc_BatteryStatus (string _lArg)
        {
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_BatteryStatus (_lArg);
        }
        public BATT_STATUS_LEVEL batt_status_level;
        public BatteryStatus (BATT_STATUS_LEVEL _vbatt_status_level) : base ()
        {
            batt_status_level = _vbatt_status_level;
            _lc_BatteryStatus ("Classes.pd:516,5");
        }
        public void chg_batt_status_level (BATT_STATUS_LEVEL x)
        {
            batt_status_level = x;
            _lClassInvariantCheck ("Classes.pd:519,14");
        }
        public bool _lEqual (BatteryStatus _vArg_508_13)
        {
            if (this == _vArg_508_13)
            {
                return true;
            }
            return (_vArg_508_13.batt_status_level == batt_status_level);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (BatteryStatus) &&
                _lEqual ((BatteryStatus) _lArg));
        }
    }

    public sealed class NoiseDetection : _eAny
    {
        void _lc_NoiseDetection (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((noise <= 1))) throw new _xClassInvariantItem ("Classes.pd:528,15", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_NoiseDetection (_lArg);
        }
        public int noise;
        public NoiseDetection (int _vnoise) : base ()
        {
            noise = _vnoise;
            _lc_NoiseDetection ("Classes.pd:533,5");
        }
        public void chg_noise (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((x <= 1))) throw new _xPre ("Classes.pd:536,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            noise = x;
            _lClassInvariantCheck ("Classes.pd:537,14");
        }
        public bool _lEqual (NoiseDetection _vArg_525_13)
        {
            if (this == _vArg_525_13)
            {
                return true;
            }
            return (_vArg_525_13.noise == noise);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (NoiseDetection) &&
                _lEqual ((NoiseDetection) _lArg));
        }
    }

    public sealed class EventMarkers : _eAny
    {
        void _lc_EventMarkers (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((0 <= atrial_marker._oHash ()))) throw new _xClassInvariantItem (
                        "Classes.pd:547,24", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((0 <= ventricular_marker._oHash ()))) throw new _xClassInvariantItem (
                        "Classes.pd:548,29", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((0 <= augmentation_marker._oHash ()))) throw new _xClassInvariantItem (
                        "Classes.pd:549,30", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_EventMarkers (_lArg);
        }
        public _eSeq < _n5_quin < MarkerABB, int, int, int, int > > atrial_marker;
        public _eSeq < _n5_quin < MarkerABB, int, int, int, int > > ventricular_marker;
        public _eSeq < _ePair < MarkerABB, int > > augmentation_marker;
        public EventMarkers (_eSeq < _n5_quin < MarkerABB, int, int, int, int > > _vatrial_marker,
            _eSeq < _n5_quin < MarkerABB, int, int, int, int > > _vventricular_marker, _eSeq <
            _ePair < MarkerABB, int > > _vaugmentation_marker) : base ()
        {
            atrial_marker = _vatrial_marker;
            ventricular_marker = _vventricular_marker;
            augmentation_marker = _vaugmentation_marker;
            _lc_EventMarkers ("Classes.pd:554,5");
        }
        public void chg_atrial_marker (MarkerABB m, int pw, int pa, int no, int ti)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((atrial_marker.empty () || (atrial_marker._oIndex ((atrial_marker._oHash ()
                        - 1)).m < ti)))) throw new _xPre ("Classes.pd:559,27");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            atrial_marker = atrial_marker._oPlusPlus (new _eSeq < _n5_quin < MarkerABB, int, int,
                int, int > > (new _n5_quin < MarkerABB, int, int, int, int > (m, pw, pa, no, ti)));
            _lClassInvariantCheck ("Classes.pd:561,14");
        }
        public void chg_ventricular_marker (MarkerABB m, int pw, int pa, int no, int ti)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((ventricular_marker.empty () || (ventricular_marker._oIndex ((
                        ventricular_marker._oHash () - 1)).m < ti)))) throw new _xPre (
                        "Classes.pd:566,32");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            ventricular_marker = ventricular_marker._oPlusPlus (new _eSeq < _n5_quin < MarkerABB,
                int, int, int, int > > (new _n5_quin < MarkerABB, int, int, int, int > (m, pw, pa,
                no, ti)));
            _lClassInvariantCheck ("Classes.pd:568,14");
        }
        public void chg_dual_marker (MarkerABB ma, int pwa, int paa, MarkerABB mv, int pwv, int pav,
            int no, int ti)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((atrial_marker.empty () || (atrial_marker._oIndex ((atrial_marker._oHash ()
                        - 1)).m < ti)))) throw new _xPre ("Classes.pd:573,27");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((ventricular_marker.empty () || (ventricular_marker._oIndex ((
                        ventricular_marker._oHash () - 1)).m < ti)))) throw new _xPre (
                        "Classes.pd:574,32");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            atrial_marker = atrial_marker._oPlusPlus (new _eSeq < _n5_quin < MarkerABB, int, int,
                int, int > > (new _n5_quin < MarkerABB, int, int, int, int > (ma, pwa, paa, no, ti)))
                ;
            ventricular_marker = ventricular_marker._oPlusPlus (new _eSeq < _n5_quin < MarkerABB,
                int, int, int, int > > (new _n5_quin < MarkerABB, int, int, int, int > (mv, pwv, pav,
                no, ti)));
            _lClassInvariantCheck ("Classes.pd:576,14");
        }
        public void chg_augmentation_marker (MarkerABB m, int ti)
        {
            augmentation_marker = augmentation_marker._oPlusPlus (new _eSeq < _ePair < MarkerABB,
                int > > (new _ePair < MarkerABB, int > (m, ti)));
            _lClassInvariantCheck ("Classes.pd:585,14");
        }
        public void unchg_dual_marker ()
        {
            atrial_marker = atrial_marker;
            ventricular_marker = ventricular_marker;
            _lClassInvariantCheck ("Classes.pd:591,14");
        }
        public void unchg_atrial_marker ()
        {
            atrial_marker = atrial_marker;
            _lClassInvariantCheck ("Classes.pd:596,14");
        }
        public void unchg_ventricular_marker ()
        {
            ventricular_marker = ventricular_marker;
            _lClassInvariantCheck ("Classes.pd:600,14");
        }
        public void unchg_augmentation_marker ()
        {
            augmentation_marker = augmentation_marker;
            _lClassInvariantCheck ("Classes.pd:604,14");
        }
        public bool _lEqual (EventMarkers _vArg_543_13)
        {
            if (this == _vArg_543_13)
            {
                return true;
            }
            return ((_vArg_543_13.atrial_marker._lEqual (atrial_marker) && _vArg_543_13.
                ventricular_marker._lEqual (ventricular_marker)) && _vArg_543_13.augmentation_marker
                ._lEqual (augmentation_marker));
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (EventMarkers) &&
                _lEqual ((EventMarkers) _lArg));
        }
    }

    public sealed class Accelerometer : _eAny
    {
        void _lc_Accelerometer (string _lArg)
        {
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_Accelerometer (_lArg);
        }
        public int accel;
        public Accelerometer (int _vaccel) : base ()
        {
            accel = _vaccel;
            _lc_Accelerometer ("Classes.pd:618,5");
        }
        public void chg_accel (int x)
        {
            accel = x;
            _lClassInvariantCheck ("Classes.pd:621,14");
        }
        public bool _lEqual (Accelerometer _vArg_610_13)
        {
            if (this == _vArg_610_13)
            {
                return true;
            }
            return (_vArg_610_13.accel == accel);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (Accelerometer) &&
                _lEqual ((Accelerometer) _lArg));
        }
    }

    public sealed class PWave : _eAny
    {
        public int p_wave;
        public PWave (int _vp_wave) : base ()
        {
            p_wave = _vp_wave;
        }
        public void chg_p_wave (int x)
        {
            p_wave = x;
        }
        public bool _lEqual (PWave _vArg_627_13)
        {
            if (this == _vArg_627_13)
            {
                return true;
            }
            return (_vArg_627_13.p_wave == p_wave);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (PWave) && _lEqual
                ((PWave) _lArg));
        }
    }

    public sealed class RWave : _eAny
    {
        public int r_wave;
        public RWave (int _vr_wave) : base ()
        {
            r_wave = _vr_wave;
        }
        public void chg_r_wave (int x)
        {
            r_wave = x;
        }
        public bool _lEqual (RWave _vArg_640_13)
        {
            if (this == _vArg_640_13)
            {
                return true;
            }
            return (_vArg_640_13.r_wave == r_wave);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (RWave) && _lEqual
                ((RWave) _lArg));
        }
    }

    public sealed class BatteryVoltage : _eAny
    {
        public int battery_voltage;
        public BatteryVoltage (int _vbattery_voltage) : base ()
        {
            battery_voltage = _vbattery_voltage;
        }
        public void chg_battery_voltage (int x)
        {
            battery_voltage = x;
        }
        public bool _lEqual (BatteryVoltage _vArg_654_13)
        {
            if (this == _vArg_654_13)
            {
                return true;
            }
            return (_vArg_654_13.battery_voltage == battery_voltage);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (BatteryVoltage) &&
                _lEqual ((BatteryVoltage) _lArg));
        }
    }

    public sealed class LeadImpedance : _eAny
    {
        void _lc_LeadImpedance (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (100, 2500)._ovIn (lead_impedance))) throw new
                        _xClassInvariantItem ("Classes.pd:670,24", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_LeadImpedance (_lArg);
        }
        public int lead_impedance;
        public LeadImpedance (int _vlead_impedance) : base ()
        {
            lead_impedance = _vlead_impedance;
            _lc_LeadImpedance ("Classes.pd:674,5");
        }
        public void chg_lead_impedance (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (100, 2500)._ovIn (x))) throw new _xPre (
                        "Classes.pd:677,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            lead_impedance = x;
            _lClassInvariantCheck ("Classes.pd:678,14");
        }
        public bool _lEqual (LeadImpedance _vArg_668_13)
        {
            if (this == _vArg_668_13)
            {
                return true;
            }
            return (_vArg_668_13.lead_impedance == lead_impedance);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (LeadImpedance) &&
                _lEqual ((LeadImpedance) _lArg));
        }
    }

    public sealed class MeasuredParameters : _eAny
    {
        public LeadImpedance c_LeadImpedance;
        public BatteryVoltage c_BatteryVoltage;
        public RWave c_RWave;
        public PWave c_PWave;
        public MeasuredParameters (LeadImpedance _vc_LeadImpedance, BatteryVoltage
            _vc_BatteryVoltage, RWave _vc_RWave, PWave _vc_PWave) : base ()
        {
            c_LeadImpedance = _vc_LeadImpedance;
            c_BatteryVoltage = _vc_BatteryVoltage;
            c_RWave = _vc_RWave;
            c_PWave = _vc_PWave;
        }
        public void set_p_wave (int x)
        {
            PWave _vUnshare_700_14 = ((PWave) c_PWave.Clone ());
            c_PWave = _vUnshare_700_14;
            _vUnshare_700_14.chg_p_wave (x);
        }
        public void set_r_wave (int x)
        {
            RWave _vUnshare_703_14 = ((RWave) c_RWave.Clone ());
            c_RWave = _vUnshare_703_14;
            _vUnshare_703_14.chg_r_wave (x);
        }
        public void set_lead_impedance (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (100, 2500)._ovIn (x))) throw new _xPre (
                        "Classes.pd:706,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            LeadImpedance _vUnshare_707_14 = ((LeadImpedance) c_LeadImpedance.Clone ());
            c_LeadImpedance = _vUnshare_707_14;
            _vUnshare_707_14.chg_lead_impedance (x);
        }
        public void set_battery_voltage (int x)
        {
            BatteryVoltage _vUnshare_710_14 = ((BatteryVoltage) c_BatteryVoltage.Clone ());
            c_BatteryVoltage = _vUnshare_710_14;
            _vUnshare_710_14.chg_battery_voltage (x);
        }
        public bool _lEqual (MeasuredParameters _vArg_684_13)
        {
            if (this == _vArg_684_13)
            {
                return true;
            }
            return (((_vArg_684_13.c_LeadImpedance._lEqual (c_LeadImpedance) && _vArg_684_13.
                c_BatteryVoltage._lEqual (c_BatteryVoltage)) && _vArg_684_13.c_RWave._lEqual (
                c_RWave)) && _vArg_684_13.c_PWave._lEqual (c_PWave));
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (MeasuredParameters)
                && _lEqual ((MeasuredParameters) _lArg));
        }
    }

    public sealed class BO_MODE : _eAny
    {
        public BOM bo_mode;
        public bool isVOO ()
        {
            return (true == bo_mode.inVOO ());
        }
        public bool isAOO ()
        {
            return (true == bo_mode.inAOO ());
        }
        public bool isDOO ()
        {
            return (true == bo_mode.inAOO ());
        }
        public bool isDDI ()
        {
            return (true == bo_mode.inDDI ());
        }
        public bool isVVI ()
        {
            return (true == bo_mode.inVVI ());
        }
        public bool isAAI ()
        {
            return (true == bo_mode.inAAI ());
        }
        public bool isVVT ()
        {
            return (true == bo_mode.inVVT ());
        }
        public bool isAAT ()
        {
            return (true == bo_mode.inAAT ());
        }
        public bool isVDD ()
        {
            return (true == bo_mode.inVDD ());
        }
        public BO_MODE (BOM _vbo_mode) : base ()
        {
            bo_mode = _vbo_mode;
        }
        public void chg_bo_mode (BOM x)
        {
            bo_mode = x;
        }
        public bool _lEqual (BO_MODE _vArg_720_13)
        {
            if (this == _vArg_720_13)
            {
                return true;
            }
            return _vArg_720_13.bo_mode._lEqual (bo_mode);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (BO_MODE) &&
                _lEqual ((BO_MODE) _lArg));
        }
    }

    public sealed class LRL : _eAny
    {
        void _lc_LRL (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (30, 175)._ovIn (lower_rate_limit))) throw new
                        _xClassInvariantItem ("Classes.pd:763,26", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_LRL (_lArg);
        }
        public int lower_rate_limit;
        public LRL (int _vlower_rate_limit) : base ()
        {
            lower_rate_limit = _vlower_rate_limit;
            _lc_LRL ("Classes.pd:768,5");
        }
        public void chg_lower_rate_limit (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (30, 175)._ovIn (x))) throw new _xPre (
                        "Classes.pd:771,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            lower_rate_limit = x;
            _lClassInvariantCheck ("Classes.pd:772,14");
        }
        public bool _lEqual (LRL _vArg_760_13)
        {
            if (this == _vArg_760_13)
            {
                return true;
            }
            return (_vArg_760_13.lower_rate_limit == lower_rate_limit);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (LRL) && _lEqual ((
                LRL) _lArg));
        }
    }

    public sealed class URL : _eAny
    {
        void _lc_URL (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (30, 175)._ovIn (upper_rate_limit))) throw new
                        _xClassInvariantItem ("Classes.pd:781,26", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_URL (_lArg);
        }
        public int upper_rate_limit;
        public URL (int _vupper_rate_limit) : base ()
        {
            upper_rate_limit = _vupper_rate_limit;
            _lc_URL ("Classes.pd:786,5");
        }
        public void chg_upper_rate_limit (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (30, 175)._ovIn (x))) throw new _xPre (
                        "Classes.pd:789,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            upper_rate_limit = x;
            _lClassInvariantCheck ("Classes.pd:790,14");
        }
        public bool _lEqual (URL _vArg_778_13)
        {
            if (this == _vArg_778_13)
            {
                return true;
            }
            return (_vArg_778_13.upper_rate_limit == upper_rate_limit);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (URL) && _lEqual ((
                URL) _lArg));
        }
    }

    public sealed class MaxSensorRate : _eAny
    {
        void _lc_MaxSensorRate (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (50, 175)._ovIn (max_sensor_rate))) throw new
                        _xClassInvariantItem ("Classes.pd:799,25", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_MaxSensorRate (_lArg);
        }
        public int max_sensor_rate;
        public MaxSensorRate (int _vmax_sensor_rate) : base ()
        {
            max_sensor_rate = _vmax_sensor_rate;
            _lc_MaxSensorRate ("Classes.pd:804,5");
        }
        public void chg_max_sensor_rate (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (50, 175)._ovIn (x))) throw new _xPre (
                        "Classes.pd:807,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            max_sensor_rate = x;
            _lClassInvariantCheck ("Classes.pd:808,14");
        }
        public bool _lEqual (MaxSensorRate _vArg_796_13)
        {
            if (this == _vArg_796_13)
            {
                return true;
            }
            return (_vArg_796_13.max_sensor_rate == max_sensor_rate);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (MaxSensorRate) &&
                _lEqual ((MaxSensorRate) _lArg));
        }
    }

    public sealed class FixedAVDelay : _eAny
    {
        void _lc_FixedAVDelay (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (70000, 300000)._ovIn (fixed_av_delay))) throw new
                        _xClassInvariantItem ("Classes.pd:817,24", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_FixedAVDelay (_lArg);
        }
        public int fixed_av_delay;
        public FixedAVDelay (int _vfixed_av_delay) : base ()
        {
            fixed_av_delay = _vfixed_av_delay;
            _lc_FixedAVDelay ("Classes.pd:822,5");
        }
        public void chg_fixed_av_delay (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (70000, 300000)._ovIn (x))) throw new _xPre (
                        "Classes.pd:825,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            fixed_av_delay = x;
            _lClassInvariantCheck ("Classes.pd:826,14");
        }
        public bool _lEqual (FixedAVDelay _vArg_814_13)
        {
            if (this == _vArg_814_13)
            {
                return true;
            }
            return (_vArg_814_13.fixed_av_delay == fixed_av_delay);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (FixedAVDelay) &&
                _lEqual ((FixedAVDelay) _lArg));
        }
    }

    public sealed class DynamicAVDelay : _eAny
    {
        public SWITCH dynamic_av_delay;
        public DynamicAVDelay (SWITCH _vdynamic_av_delay) : base ()
        {
            dynamic_av_delay = _vdynamic_av_delay;
        }
        public void chg_dynamic_av_delay (SWITCH x)
        {
            dynamic_av_delay = x;
        }
        public bool _lEqual (DynamicAVDelay _vArg_832_13)
        {
            if (this == _vArg_832_13)
            {
                return true;
            }
            return (_vArg_832_13.dynamic_av_delay == dynamic_av_delay);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (DynamicAVDelay) &&
                _lEqual ((DynamicAVDelay) _lArg));
        }
    }

    public sealed class MinDynamicAVDelay : _eAny
    {
        void _lc_MinDynamicAVDelay (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (30000, 100000)._ovIn (min_dyn_av_delay))) throw new
                        _xClassInvariantItem ("Classes.pd:848,26", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_MinDynamicAVDelay (_lArg);
        }
        public int min_dyn_av_delay;
        public MinDynamicAVDelay (int _vmin_dyn_av_delay) : base ()
        {
            min_dyn_av_delay = _vmin_dyn_av_delay;
            _lc_MinDynamicAVDelay ("Classes.pd:853,5");
        }
        public void chg_min_dyn_av_delay (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (30000, 100000)._ovIn (x))) throw new _xPre (
                        "Classes.pd:856,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            min_dyn_av_delay = x;
            _lClassInvariantCheck ("Classes.pd:857,14");
        }
        public bool _lEqual (MinDynamicAVDelay _vArg_846_13)
        {
            if (this == _vArg_846_13)
            {
                return true;
            }
            return (_vArg_846_13.min_dyn_av_delay == min_dyn_av_delay);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (MinDynamicAVDelay)
                && _lEqual ((MinDynamicAVDelay) _lArg));
        }
    }

    public sealed class SensedAVDelayOffset : _eAny
    {
        void _lc_SensedAVDelayOffset (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(((sensed_av_delay_offset._lEqual (new _ePair < SWITCH, int > (SWITCH.OFF,
                        0)) || ((SWITCH.OFF == sensed_av_delay_offset.x) && (((- 100000) <=
                        sensed_av_delay_offset.y) && (sensed_av_delay_offset.y <= (- 10000))))) || (
                        (SWITCH.ON == sensed_av_delay_offset.x) && (((- 100000) <=
                        sensed_av_delay_offset.y) && (sensed_av_delay_offset.y <= (- 10000)))))))
                        throw new _xClassInvariantItem ("Classes.pd:866,33", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_SensedAVDelayOffset (_lArg);
        }
        public _ePair < SWITCH, int > sensed_av_delay_offset;
        public SensedAVDelayOffset (_ePair < SWITCH, int > _vsensed_av_delay_offset) : base ()
        {
            sensed_av_delay_offset = _vsensed_av_delay_offset;
            _lc_SensedAVDelayOffset ("Classes.pd:874,5");
        }
        public void chg_sensed_av_delay_offset (_ePair < SWITCH, int > a)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((a._lEqual (new _ePair < SWITCH, int > (SWITCH.OFF, 0)) || ((SWITCH.ON ==
                        a.x) && (((- 100000) <= a.y) && (a.y <= (- 100000))))))) throw new _xPre (
                        "Classes.pd:877,16");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            sensed_av_delay_offset = a;
            _lClassInvariantCheck ("Classes.pd:880,14");
        }
        public bool _lEqual (SensedAVDelayOffset _vArg_863_13)
        {
            if (this == _vArg_863_13)
            {
                return true;
            }
            return _vArg_863_13.sensed_av_delay_offset._lEqual (sensed_av_delay_offset);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (
                SensedAVDelayOffset) && _lEqual ((SensedAVDelayOffset) _lArg));
        }
    }

    public sealed class APulseAmpRegulated : _eAny
    {
        void _lc_APulseAmpRegulated (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(((a_pulse_amp_regulated._lEqual (new _ePair < SWITCH, int > (SWITCH.OFF, 0)
                        ) || ((SWITCH.ON == a_pulse_amp_regulated.x) && ((500000 <=
                        a_pulse_amp_regulated.y) && (a_pulse_amp_regulated.y <= 3200000)))) || ((
                        SWITCH.ON == a_pulse_amp_regulated.x) && ((3500000 <= a_pulse_amp_regulated.
                        y) && (a_pulse_amp_regulated.y <= 7000000)))))) throw new
                        _xClassInvariantItem ("Classes.pd:889,32", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_APulseAmpRegulated (_lArg);
        }
        public _ePair < SWITCH, int > a_pulse_amp_regulated;
        public APulseAmpRegulated (_ePair < SWITCH, int > _va_pulse_amp_regulated) : base ()
        {
            a_pulse_amp_regulated = _va_pulse_amp_regulated;
            _lc_APulseAmpRegulated ("Classes.pd:898,5");
        }
        public void chg_a_pulse_amp_regulated (_ePair < SWITCH, int > a)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(((a._lEqual (new _ePair < SWITCH, int > (SWITCH.OFF, 0)) || ((SWITCH.ON ==
                        a.x) && ((500000 <= a.y) && (a.y <= 3200000)))) || ((SWITCH.ON == a.x) && ((
                        3500000 <= a.y) && (a.y <= 7000000)))))) throw new _xPre (
                        "Classes.pd:901,16");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            a_pulse_amp_regulated = a;
            _lClassInvariantCheck ("Classes.pd:906,14");
        }
        public bool _lEqual (APulseAmpRegulated _vArg_886_13)
        {
            if (this == _vArg_886_13)
            {
                return true;
            }
            return _vArg_886_13.a_pulse_amp_regulated._lEqual (a_pulse_amp_regulated);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (APulseAmpRegulated)
                && _lEqual ((APulseAmpRegulated) _lArg));
        }
    }

    public sealed class VPulseAmpRegulated : _eAny
    {
        void _lc_VPulseAmpRegulated (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(((v_pulse_amp_regulated._lEqual (new _ePair < SWITCH, int > (SWITCH.OFF, 0)
                        ) || ((SWITCH.ON == v_pulse_amp_regulated.x) && ((500000 <=
                        v_pulse_amp_regulated.y) && (v_pulse_amp_regulated.y <= 3200000)))) || ((
                        SWITCH.ON == v_pulse_amp_regulated.x) && ((3500000 <= v_pulse_amp_regulated.
                        y) && (v_pulse_amp_regulated.y <= 7000000)))))) throw new
                        _xClassInvariantItem ("Classes.pd:915,32", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_VPulseAmpRegulated (_lArg);
        }
        public _ePair < SWITCH, int > v_pulse_amp_regulated;
        public VPulseAmpRegulated (_ePair < SWITCH, int > _vv_pulse_amp_regulated) : base ()
        {
            v_pulse_amp_regulated = _vv_pulse_amp_regulated;
            _lc_VPulseAmpRegulated ("Classes.pd:922,5");
        }
        public void chg_v_pulse_amp_regulated (_ePair < SWITCH, int > a)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(((a._lEqual (new _ePair < SWITCH, int > (SWITCH.OFF, 0)) || ((SWITCH.ON ==
                        a.x) && ((500000 <= a.y) && (a.y <= 3200000)))) || ((SWITCH.ON == a.x) && ((
                        3500000 <= a.y) && (a.y <= 7000000)))))) throw new _xPre (
                        "Classes.pd:925,16");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            v_pulse_amp_regulated = a;
            _lClassInvariantCheck ("Classes.pd:928,14");
        }
        public bool _lEqual (VPulseAmpRegulated _vArg_912_13)
        {
            if (this == _vArg_912_13)
            {
                return true;
            }
            return _vArg_912_13.v_pulse_amp_regulated._lEqual (v_pulse_amp_regulated);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (VPulseAmpRegulated)
                && _lEqual ((VPulseAmpRegulated) _lArg));
        }
    }

    public sealed class APulseAmpUnregulated : _eAny
    {
        void _lc_APulseAmpUnregulated (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((a_pulse_amp_unregulated._lEqual (new _ePair < SWITCH, int > (SWITCH.OFF,
                        0)) || ((SWITCH.ON == a_pulse_amp_unregulated.x) && ((((1250000 ==
                        a_pulse_amp_unregulated.y) || (2500000 == a_pulse_amp_unregulated.y)) || (
                        3750000 == a_pulse_amp_unregulated.y)) || (5000000 ==
                        a_pulse_amp_unregulated.y)))))) throw new _xClassInvariantItem (
                        "Classes.pd:937,33", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_APulseAmpUnregulated (_lArg);
        }
        public _ePair < SWITCH, int > a_pulse_amp_unregulated;
        public APulseAmpUnregulated (_ePair < SWITCH, int > _va_pulse_amp_unregulated) : base ()
        {
            a_pulse_amp_unregulated = _va_pulse_amp_unregulated;
            _lc_APulseAmpUnregulated ("Classes.pd:946,5");
        }
        public void chg_a_pulse_amp_unregulated (_ePair < SWITCH, int > a)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((a._lEqual (new _ePair < SWITCH, int > (SWITCH.OFF, 0)) || ((SWITCH.ON ==
                        a.x) && ((((1250000 == a.y) || (2500000 == a.y)) || (3750000 == a.y)) || (
                        5000000 == a.y)))))) throw new _xPre ("Classes.pd:949,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            a_pulse_amp_unregulated = a;
            _lClassInvariantCheck ("Classes.pd:955,14");
        }
        public bool _lEqual (APulseAmpUnregulated _vArg_934_13)
        {
            if (this == _vArg_934_13)
            {
                return true;
            }
            return _vArg_934_13.a_pulse_amp_unregulated._lEqual (a_pulse_amp_unregulated);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (
                APulseAmpUnregulated) && _lEqual ((APulseAmpUnregulated) _lArg));
        }
    }

    public sealed class VPulseAmpUnregulated : _eAny
    {
        void _lc_VPulseAmpUnregulated (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((v_pulse_amp_unregulated._lEqual (new _ePair < SWITCH, int > (SWITCH.OFF,
                        0)) || ((SWITCH.ON == v_pulse_amp_unregulated.x) && ((((1250000 ==
                        v_pulse_amp_unregulated.y) || (2500000 == v_pulse_amp_unregulated.y)) || (
                        3750000 == v_pulse_amp_unregulated.y)) || (5000000 ==
                        v_pulse_amp_unregulated.y)))))) throw new _xClassInvariantItem (
                        "Classes.pd:964,33", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_VPulseAmpUnregulated (_lArg);
        }
        public _ePair < SWITCH, int > v_pulse_amp_unregulated;
        public VPulseAmpUnregulated (_ePair < SWITCH, int > _vv_pulse_amp_unregulated) : base ()
        {
            v_pulse_amp_unregulated = _vv_pulse_amp_unregulated;
            _lc_VPulseAmpUnregulated ("Classes.pd:973,5");
        }
        public void chg_v_pulse_amp_unregulated (_ePair < SWITCH, int > a)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((a._lEqual (new _ePair < SWITCH, int > (SWITCH.OFF, 0)) || ((SWITCH.ON ==
                        a.x) && ((((1250000 == a.y) || (2500000 == a.y)) || (3750000 == a.y)) || (
                        5000000 == a.y)))))) throw new _xPre ("Classes.pd:976,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            v_pulse_amp_unregulated = a;
            _lClassInvariantCheck ("Classes.pd:982,14");
        }
        public bool _lEqual (VPulseAmpUnregulated _vArg_961_13)
        {
            if (this == _vArg_961_13)
            {
                return true;
            }
            return _vArg_961_13.v_pulse_amp_unregulated._lEqual (v_pulse_amp_unregulated);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (
                VPulseAmpUnregulated) && _lEqual ((VPulseAmpUnregulated) _lArg));
        }
    }

    public sealed class APulseWidth : _eAny
    {
        void _lc_APulseWidth (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(((50 == a_pulse_width) || _eSystem._oRange (100, 1900)._ovIn (
                        a_pulse_width)))) throw new _xClassInvariantItem ("Classes.pd:992,24", _lArg)
                        ;
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_APulseWidth (_lArg);
        }
        public int a_pulse_width;
        public APulseWidth (int _va_pulse_width) : base ()
        {
            a_pulse_width = _va_pulse_width;
            _lc_APulseWidth ("Classes.pd:997,5");
        }
        public void chg_a_pulse_width (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(((50 == x) || _eSystem._oRange (100, 1900)._ovIn (x)))) throw new _xPre (
                        "Classes.pd:1000,16");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            a_pulse_width = x;
            _lClassInvariantCheck ("Classes.pd:1001,14");
        }
        public bool _lEqual (APulseWidth _vArg_989_13)
        {
            if (this == _vArg_989_13)
            {
                return true;
            }
            return (_vArg_989_13.a_pulse_width == a_pulse_width);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (APulseWidth) &&
                _lEqual ((APulseWidth) _lArg));
        }
    }

    public sealed class VPulseWidth : _eAny
    {
        void _lc_VPulseWidth (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(((50 == v_pulse_width) || _eSystem._oRange (100, 1900)._ovIn (
                        v_pulse_width)))) throw new _xClassInvariantItem ("Classes.pd:1010,24",
                        _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_VPulseWidth (_lArg);
        }
        public int v_pulse_width;
        public VPulseWidth (int _vv_pulse_width) : base ()
        {
            v_pulse_width = _vv_pulse_width;
            _lc_VPulseWidth ("Classes.pd:1015,5");
        }
        public void chg_v_pulse_width (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(((50 == x) || _eSystem._oRange (100, 1900)._ovIn (x)))) throw new _xPre (
                        "Classes.pd:1018,16");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            v_pulse_width = x;
            _lClassInvariantCheck ("Classes.pd:1019,14");
        }
        public bool _lEqual (VPulseWidth _vArg_1007_13)
        {
            if (this == _vArg_1007_13)
            {
                return true;
            }
            return (_vArg_1007_13.v_pulse_width == v_pulse_width);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (VPulseWidth) &&
                _lEqual ((VPulseWidth) _lArg));
        }
    }

    public sealed class ASensitivity : _eAny
    {
        void _lc_ASensitivity (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(((((250 == a_sensitivity) || (500 == a_sensitivity)) || (750 ==
                        a_sensitivity)) || _eSystem._oRange (1000, 10000)._ovIn (a_sensitivity))))
                        throw new _xClassInvariantItem ("Classes.pd:1028,24", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_ASensitivity (_lArg);
        }
        public int a_sensitivity;
        public ASensitivity (int _va_sensitivity) : base ()
        {
            a_sensitivity = _va_sensitivity;
            _lc_ASensitivity ("Classes.pd:1033,5");
        }
        public void chg_a_sensitivity (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(((((250 == x) || (500 == x)) || (750 == x)) || _eSystem._oRange (1000,
                        10000)._ovIn (x)))) throw new _xPre ("Classes.pd:1036,16");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            a_sensitivity = x;
            _lClassInvariantCheck ("Classes.pd:1038,14");
        }
        public bool _lEqual (ASensitivity _vArg_1025_13)
        {
            if (this == _vArg_1025_13)
            {
                return true;
            }
            return (_vArg_1025_13.a_sensitivity == a_sensitivity);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (ASensitivity) &&
                _lEqual ((ASensitivity) _lArg));
        }
    }

    public sealed class VSensitivity : _eAny
    {
        void _lc_VSensitivity (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(((((250 == v_sensitivity) || (500 == v_sensitivity)) || (750 ==
                        v_sensitivity)) || _eSystem._oRange (1000, 10000)._ovIn (v_sensitivity))))
                        throw new _xClassInvariantItem ("Classes.pd:1047,24", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_VSensitivity (_lArg);
        }
        public int v_sensitivity;
        public VSensitivity (int _vv_sensitivity) : base ()
        {
            v_sensitivity = _vv_sensitivity;
            _lc_VSensitivity ("Classes.pd:1053,5");
        }
        public void chg_v_sensitivity (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(((((250 == x) || (500 == x)) || (750 == x)) || _eSystem._oRange (1000,
                        10000)._ovIn (x)))) throw new _xPre ("Classes.pd:1056,16");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            v_sensitivity = x;
            _lClassInvariantCheck ("Classes.pd:1058,14");
        }
        public bool _lEqual (VSensitivity _vArg_1044_13)
        {
            if (this == _vArg_1044_13)
            {
                return true;
            }
            return (_vArg_1044_13.v_sensitivity == v_sensitivity);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (VSensitivity) &&
                _lEqual ((VSensitivity) _lArg));
        }
    }

    public sealed class VRefractoryPeriod : _eAny
    {
        void _lc_VRefractoryPeriod (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (150000, 500000)._ovIn (v_refract_period))) throw new
                        _xClassInvariantItem ("Classes.pd:1067,26", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_VRefractoryPeriod (_lArg);
        }
        public int v_refract_period;
        public VRefractoryPeriod (int _vv_refract_period) : base ()
        {
            v_refract_period = _vv_refract_period;
            _lc_VRefractoryPeriod ("Classes.pd:1071,5");
        }
        public void chg_v_refract_period (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (150000, 500000)._ovIn (x))) throw new _xPre (
                        "Classes.pd:1074,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            v_refract_period = x;
            _lClassInvariantCheck ("Classes.pd:1075,14");
        }
        public bool _lEqual (VRefractoryPeriod _vArg_1064_13)
        {
            if (this == _vArg_1064_13)
            {
                return true;
            }
            return (_vArg_1064_13.v_refract_period == v_refract_period);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (VRefractoryPeriod)
                && _lEqual ((VRefractoryPeriod) _lArg));
        }
    }

    public sealed class ARefractoryPeriod : _eAny
    {
        void _lc_ARefractoryPeriod (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (150000, 500000)._ovIn (a_refract_period))) throw new
                        _xClassInvariantItem ("Classes.pd:1084,26", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_ARefractoryPeriod (_lArg);
        }
        public int a_refract_period;
        public ARefractoryPeriod (int _va_refract_period) : base ()
        {
            a_refract_period = _va_refract_period;
            _lc_ARefractoryPeriod ("Classes.pd:1089,5");
        }
        public void chg_a_refract_period (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (150000, 500000)._ovIn (x))) throw new _xPre (
                        "Classes.pd:1092,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            a_refract_period = x;
            _lClassInvariantCheck ("Classes.pd:1093,14");
        }
        public bool _lEqual (ARefractoryPeriod _vArg_1081_13)
        {
            if (this == _vArg_1081_13)
            {
                return true;
            }
            return (_vArg_1081_13.a_refract_period == a_refract_period);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (ARefractoryPeriod)
                && _lEqual ((ARefractoryPeriod) _lArg));
        }
    }

    public sealed class PVARP : _eAny
    {
        void _lc_PVARP (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (150000, 500000)._ovIn (pvarp))) throw new
                        _xClassInvariantItem ("Classes.pd:1102,15", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_PVARP (_lArg);
        }
        public int pvarp;
        public PVARP (int _vpvarp) : base ()
        {
            pvarp = _vpvarp;
            _lc_PVARP ("Classes.pd:1107,5");
        }
        public void chg_pvarp (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (150000, 500000)._ovIn (x))) throw new _xPre (
                        "Classes.pd:1110,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            pvarp = x;
            _lClassInvariantCheck ("Classes.pd:1111,14");
        }
        public bool _lEqual (PVARP _vArg_1099_13)
        {
            if (this == _vArg_1099_13)
            {
                return true;
            }
            return (_vArg_1099_13.pvarp == pvarp);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (PVARP) && _lEqual
                ((PVARP) _lArg));
        }
    }

    public sealed class PVARP_Ext : _eAny
    {
        void _lc_PVARP_Ext (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((pvarp_ext._lEqual (new _ePair < SWITCH, int > (SWITCH.OFF, 0)) || ((
                        SWITCH.ON == pvarp_ext.x) && ((50000 <= pvarp_ext.y) && (pvarp_ext.y <=
                        400000)))))) throw new _xClassInvariantItem ("Classes.pd:1120,19", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_PVARP_Ext (_lArg);
        }
        public _ePair < SWITCH, int > pvarp_ext;
        public PVARP_Ext (_ePair < SWITCH, int > _vpvarp_ext) : base ()
        {
            pvarp_ext = _vpvarp_ext;
            _lc_PVARP_Ext ("Classes.pd:1126,5");
        }
        public void chg_pvarp_ext (_ePair < SWITCH, int > a)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((a._lEqual (new _ePair < SWITCH, int > (SWITCH.OFF, 0)) || ((SWITCH.ON ==
                        a.x) && ((50000 <= a.y) && (a.y <= 400000)))))) throw new _xPre (
                        "Classes.pd:1129,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            pvarp_ext = a;
            _lClassInvariantCheck ("Classes.pd:1131,14");
        }
        public bool _lEqual (PVARP_Ext _vArg_1117_13)
        {
            if (this == _vArg_1117_13)
            {
                return true;
            }
            return _vArg_1117_13.pvarp_ext._lEqual (pvarp_ext);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (PVARP_Ext) &&
                _lEqual ((PVARP_Ext) _lArg));
        }
    }

    public sealed class HysteresisRateLimit : _eAny
    {
        void _lc_HysteresisRateLimit (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((hysteresis_rate_limit._lEqual (new _ePair < SWITCH, int > (SWITCH.OFF, 0))
                        || ((SWITCH.ON == hysteresis_rate_limit.x) && ((30 <= hysteresis_rate_limit.
                        y) && (hysteresis_rate_limit.y <= 175)))))) throw new _xClassInvariantItem (
                        "Classes.pd:1140,31", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_HysteresisRateLimit (_lArg);
        }
        public _ePair < SWITCH, int > hysteresis_rate_limit;
        public HysteresisRateLimit (_ePair < SWITCH, int > _vhysteresis_rate_limit) : base ()
        {
            hysteresis_rate_limit = _vhysteresis_rate_limit;
            _lc_HysteresisRateLimit ("Classes.pd:1147,5");
        }
        public void chg_hysteresis_rate_limit (_ePair < SWITCH, int > a)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((a._lEqual (new _ePair < SWITCH, int > (SWITCH.OFF, 0)) || ((SWITCH.ON ==
                        a.x) && ((30 <= a.y) && (a.y <= 175)))))) throw new _xPre (
                        "Classes.pd:1150,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            hysteresis_rate_limit = a;
            _lClassInvariantCheck ("Classes.pd:1153,14");
        }
        public bool _lEqual (HysteresisRateLimit _vArg_1137_13)
        {
            if (this == _vArg_1137_13)
            {
                return true;
            }
            return _vArg_1137_13.hysteresis_rate_limit._lEqual (hysteresis_rate_limit);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (
                HysteresisRateLimit) && _lEqual ((HysteresisRateLimit) _lArg));
        }
    }

    public sealed class RateSmoothing : _eAny
    {
        void _lc_RateSmoothing (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((rate_smoothing._lEqual (new _ePair < SWITCH, int > (SWITCH.OFF, 0)) || ((
                        SWITCH.ON == rate_smoothing.x) && ((((((((3 == rate_smoothing.y) || (6 ==
                        rate_smoothing.y)) || (9 == rate_smoothing.y)) || (12 == rate_smoothing.y))
                        || (15 == rate_smoothing.y)) || (18 == rate_smoothing.y)) || (21 ==
                        rate_smoothing.y)) || (25 == rate_smoothing.y)))))) throw new
                        _xClassInvariantItem ("Classes.pd:1163,24", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_RateSmoothing (_lArg);
        }
        public _ePair < SWITCH, int > rate_smoothing;
        public RateSmoothing (_ePair < SWITCH, int > _vrate_smoothing) : base ()
        {
            rate_smoothing = _vrate_smoothing;
            _lc_RateSmoothing ("Classes.pd:1177,5");
        }
        public void chg_rate_smoothing (_ePair < SWITCH, int > a)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((a._lEqual (new _ePair < SWITCH, int > (SWITCH.OFF, 0)) || ((SWITCH.ON ==
                        a.x) && ((((((((3 == a.y) || (6 == a.y)) || (9 == a.y)) || (12 == a.y)) || (
                        15 == a.y)) || (18 == a.y)) || (21 == a.y)) || (25 == a.y)))))) throw new
                        _xPre ("Classes.pd:1180,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            rate_smoothing = a;
            _lClassInvariantCheck ("Classes.pd:1190,14");
        }
        public bool _lEqual (RateSmoothing _vArg_1160_13)
        {
            if (this == _vArg_1160_13)
            {
                return true;
            }
            return _vArg_1160_13.rate_smoothing._lEqual (rate_smoothing);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (RateSmoothing) &&
                _lEqual ((RateSmoothing) _lArg));
        }
    }

    public sealed class ATRMode : _eAny
    {
        public SWITCH atr_mode;
        public ATRMode (SWITCH _vatr_mode) : base ()
        {
            atr_mode = _vatr_mode;
        }
        public void chg_atr_mode (SWITCH x)
        {
            atr_mode = x;
        }
        public bool _lEqual (ATRMode _vArg_1196_13)
        {
            if (this == _vArg_1196_13)
            {
                return true;
            }
            return (_vArg_1196_13.atr_mode == atr_mode);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (ATRMode) &&
                _lEqual ((ATRMode) _lArg));
        }
    }

    public sealed class ATRDuration : _eAny
    {
        void _lc_ATRDuration (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((((10 == atr_duration) || _eSystem._oRange (20, 80)._ovIn (atr_duration))
                        || _eSystem._oRange (100, 2000)._ovIn (atr_duration)))) throw new
                        _xClassInvariantItem ("Classes.pd:1213,23", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_ATRDuration (_lArg);
        }
        public int atr_duration;
        public ATRDuration (int _vatr_duration) : base ()
        {
            atr_duration = _vatr_duration;
            _lc_ATRDuration ("Classes.pd:1218,5");
        }
        public void chg_atr_duration (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((((10 == x) || _eSystem._oRange (20, 80)._ovIn (x)) || _eSystem._oRange (
                        100, 2000)._ovIn (x)))) throw new _xPre ("Classes.pd:1221,16");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            atr_duration = x;
            _lClassInvariantCheck ("Classes.pd:1222,14");
        }
        public bool _lEqual (ATRDuration _vArg_1210_13)
        {
            if (this == _vArg_1210_13)
            {
                return true;
            }
            return (_vArg_1210_13.atr_duration == atr_duration);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (ATRDuration) &&
                _lEqual ((ATRDuration) _lArg));
        }
    }

    public sealed class ATRFallbackTime : _eAny
    {
        void _lc_ATRFallbackTime (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((((((1 == atr_fallback_time) || (2 == atr_fallback_time)) || (3 ==
                        atr_fallback_time)) || (4 == atr_fallback_time)) || (5 == atr_fallback_time))
                        )) throw new _xClassInvariantItem ("Classes.pd:1231,28", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_ATRFallbackTime (_lArg);
        }
        public int atr_fallback_time;
        public ATRFallbackTime (int _vatr_fallback_time) : base ()
        {
            atr_fallback_time = _vatr_fallback_time;
            _lc_ATRFallbackTime ("Classes.pd:1240,5");
        }
        public void chg_atr_fallback_time (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((((((1 == x) || (2 == x)) || (3 == x)) || (4 == x)) || (5 == x)))) throw
                        new _xPre ("Classes.pd:1243,16");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            atr_fallback_time = x;
            _lClassInvariantCheck ("Classes.pd:1248,14");
        }
        public bool _lEqual (ATRFallbackTime _vArg_1228_13)
        {
            if (this == _vArg_1228_13)
            {
                return true;
            }
            return (_vArg_1228_13.atr_fallback_time == atr_fallback_time);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (ATRFallbackTime)
                && _lEqual ((ATRFallbackTime) _lArg));
        }
    }

    public sealed class VBlanking : _eAny
    {
        void _lc_VBlanking (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (30000, 60000)._ovIn (v_blanking))) throw new
                        _xClassInvariantItem ("Classes.pd:1257,21", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_VBlanking (_lArg);
        }
        public int v_blanking;
        public VBlanking (int _vv_blanking) : base ()
        {
            v_blanking = _vv_blanking;
            _lc_VBlanking ("Classes.pd:1262,5");
        }
        public void chg_v_blanking (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (30000, 60000)._ovIn (x))) throw new _xPre (
                        "Classes.pd:1265,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            v_blanking = x;
            _lClassInvariantCheck ("Classes.pd:1266,14");
        }
        public bool _lEqual (VBlanking _vArg_1254_13)
        {
            if (this == _vArg_1254_13)
            {
                return true;
            }
            return (_vArg_1254_13.v_blanking == v_blanking);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (VBlanking) &&
                _lEqual ((VBlanking) _lArg));
        }
    }

    public sealed class ActivityThreshold : _eAny
    {
        public ACTIVITY_THRESHOLD act_threshold;
        public ActivityThreshold (ACTIVITY_THRESHOLD _vact_threshold) : base ()
        {
            act_threshold = _vact_threshold;
        }
        public void chg_act_threshold (ACTIVITY_THRESHOLD x)
        {
            act_threshold = x;
        }
        public bool _lEqual (ActivityThreshold _vArg_1272_13)
        {
            if (this == _vArg_1272_13)
            {
                return true;
            }
            return (_vArg_1272_13.act_threshold == act_threshold);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (ActivityThreshold)
                && _lEqual ((ActivityThreshold) _lArg));
        }
    }

    public sealed class ReactTime : _eAny
    {
        void _lc_ReactTime (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (10, 50)._ovIn (reaction_time))) throw new
                        _xClassInvariantItem ("Classes.pd:1289,23", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_ReactTime (_lArg);
        }
        public int reaction_time;
        public ReactTime (int _vreaction_time) : base ()
        {
            reaction_time = _vreaction_time;
            _lc_ReactTime ("Classes.pd:1294,5");
        }
        public void chg_reaction_time (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (10, 50)._ovIn (x))) throw new _xPre (
                        "Classes.pd:1297,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            reaction_time = x;
            _lClassInvariantCheck ("Classes.pd:1298,14");
        }
        public bool _lEqual (ReactTime _vArg_1286_13)
        {
            if (this == _vArg_1286_13)
            {
                return true;
            }
            return (_vArg_1286_13.reaction_time == reaction_time);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (ReactTime) &&
                _lEqual ((ReactTime) _lArg));
        }
    }

    public sealed class RespFactor : _eAny
    {
        void _lc_RespFactor (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (1, 16)._ovIn (response_factor))) throw new
                        _xClassInvariantItem ("Classes.pd:1307,25", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_RespFactor (_lArg);
        }
        public int response_factor;
        public RespFactor (int _vresponse_factor) : base ()
        {
            response_factor = _vresponse_factor;
            _lc_RespFactor ("Classes.pd:1312,5");
        }
        public void chg_response_factor (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (1, 16)._ovIn (x))) throw new _xPre ("Classes.pd:1315,15")
                        ;
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            response_factor = x;
            _lClassInvariantCheck ("Classes.pd:1316,14");
        }
        public bool _lEqual (RespFactor _vArg_1304_13)
        {
            if (this == _vArg_1304_13)
            {
                return true;
            }
            return (_vArg_1304_13.response_factor == response_factor);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (RespFactor) &&
                _lEqual ((RespFactor) _lArg));
        }
    }

    public sealed class RecoveryTime : _eAny
    {
        void _lc_RecoveryTime (string _lArg)
        {
            if (_eSystem.enableClassInvariantItem && _eSystem.currentCheckNesting <= _eSystem.
                maxCheckNesting && _eSystem.currentSuperNesting == 0)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (120, 960)._ovIn (recovery_time))) throw new
                        _xClassInvariantItem ("Classes.pd:1325,23", _lArg);
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
        }
        void _lClassInvariantCheck (string _lArg)
        {
            _lc_RecoveryTime (_lArg);
        }
        public int recovery_time;
        public RecoveryTime (int _vrecovery_time) : base ()
        {
            recovery_time = _vrecovery_time;
            _lc_RecoveryTime ("Classes.pd:1330,5");
        }
        public void chg_recovery_time (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (120, 960)._ovIn (x))) throw new _xPre (
                        "Classes.pd:1333,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            recovery_time = x;
            _lClassInvariantCheck ("Classes.pd:1334,14");
        }
        public bool _lEqual (RecoveryTime _vArg_1322_13)
        {
            if (this == _vArg_1322_13)
            {
                return true;
            }
            return (_vArg_1322_13.recovery_time == recovery_time);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (RecoveryTime) &&
                _lEqual ((RecoveryTime) _lArg));
        }
    }

    public sealed class ProgrammableParameters : _eAny
    {
        public LRL c_LRL;
        public URL c_URL;
        public MaxSensorRate c_MaxSensorRate;
        public FixedAVDelay c_FixedAVDelay;
        public DynamicAVDelay c_DynamicAVDelay;
        public MinDynamicAVDelay c_MinDynamicAVDelay;
        public SensedAVDelayOffset c_SensedAVDelayOffset;
        public APulseAmpRegulated c_APulseAmpRegulated;
        public VPulseAmpRegulated c_VPulseAmpRegulated;
        public APulseAmpUnregulated c_APulseAmpUnregulated;
        public VPulseAmpUnregulated c_VPulseAmpUnregulated;
        public APulseWidth c_APulseWidth;
        public VPulseWidth c_VPulseWidth;
        public ASensitivity c_ASensitivity;
        public VSensitivity c_VSensitivity;
        public VRefractoryPeriod c_VRefractoryPeriod;
        public ARefractoryPeriod c_ARefractoryPeriod;
        public PVARP c_PVARP;
        public PVARP_Ext c_PVARP_Ext;
        public HysteresisRateLimit c_HysteresisRateLimit;
        public RateSmoothing c_RateSmoothing;
        public ATRMode c_ATRMode;
        public ATRDuration c_ATRDuration;
        public ATRFallbackTime c_ATRFallbackTime;
        public VBlanking c_VBlanking;
        public ActivityThreshold c_ActivityThreshold;
        public ReactTime c_ReactTime;
        public RespFactor c_RespFactor;
        public RecoveryTime c_RecoveryTime;
        public ProgrammableParameters (LRL _vc_LRL, URL _vc_URL, MaxSensorRate _vc_MaxSensorRate,
            FixedAVDelay _vc_FixedAVDelay, DynamicAVDelay _vc_DynamicAVDelay, MinDynamicAVDelay
            _vc_MinDynamicAVDelay, SensedAVDelayOffset _vc_SensedAVDelayOffset, APulseAmpRegulated
            _vc_APulseAmpRegulated, VPulseAmpRegulated _vc_VPulseAmpRegulated, APulseAmpUnregulated
            _vc_APulseAmpUnregulated, VPulseAmpUnregulated _vc_VPulseAmpUnregulated, APulseWidth
            _vc_APulseWidth, VPulseWidth _vc_VPulseWidth, ASensitivity _vc_ASensitivity,
            VSensitivity _vc_VSensitivity, VRefractoryPeriod _vc_VRefractoryPeriod,
            ARefractoryPeriod _vc_ARefractoryPeriod, PVARP _vc_PVARP, PVARP_Ext _vc_PVARP_Ext,
            HysteresisRateLimit _vc_HysteresisRateLimit, RateSmoothing _vc_RateSmoothing, ATRMode
            _vc_ATRMode, ATRDuration _vc_ATRDuration, ATRFallbackTime _vc_ATRFallbackTime, VBlanking
            _vc_VBlanking, ActivityThreshold _vc_ActivityThreshold, ReactTime _vc_ReactTime,
            RespFactor _vc_RespFactor, RecoveryTime _vc_RecoveryTime) : base ()
        {
            c_LRL = _vc_LRL;
            c_URL = _vc_URL;
            c_MaxSensorRate = _vc_MaxSensorRate;
            c_FixedAVDelay = _vc_FixedAVDelay;
            c_DynamicAVDelay = _vc_DynamicAVDelay;
            c_MinDynamicAVDelay = _vc_MinDynamicAVDelay;
            c_SensedAVDelayOffset = _vc_SensedAVDelayOffset;
            c_APulseAmpRegulated = _vc_APulseAmpRegulated;
            c_VPulseAmpRegulated = _vc_VPulseAmpRegulated;
            c_APulseAmpUnregulated = _vc_APulseAmpUnregulated;
            c_VPulseAmpUnregulated = _vc_VPulseAmpUnregulated;
            c_APulseWidth = _vc_APulseWidth;
            c_VPulseWidth = _vc_VPulseWidth;
            c_ASensitivity = _vc_ASensitivity;
            c_VSensitivity = _vc_VSensitivity;
            c_VRefractoryPeriod = _vc_VRefractoryPeriod;
            c_ARefractoryPeriod = _vc_ARefractoryPeriod;
            c_PVARP = _vc_PVARP;
            c_PVARP_Ext = _vc_PVARP_Ext;
            c_HysteresisRateLimit = _vc_HysteresisRateLimit;
            c_RateSmoothing = _vc_RateSmoothing;
            c_ATRMode = _vc_ATRMode;
            c_ATRDuration = _vc_ATRDuration;
            c_ATRFallbackTime = _vc_ATRFallbackTime;
            c_VBlanking = _vc_VBlanking;
            c_ActivityThreshold = _vc_ActivityThreshold;
            c_ReactTime = _vc_ReactTime;
            c_RespFactor = _vc_RespFactor;
            c_RecoveryTime = _vc_RecoveryTime;
        }
        public void set_lower_rate_limit (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (30, 175)._ovIn (x))) throw new _xPre (
                        "Classes.pd:1432,13");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            LRL _vUnshare_1433_14 = ((LRL) c_LRL.Clone ());
            c_LRL = _vUnshare_1433_14;
            _vUnshare_1433_14.chg_lower_rate_limit (x);
        }
        public void set_upper_rate_limit (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (30, 175)._ovIn (x))) throw new _xPre (
                        "Classes.pd:1436,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            URL _vUnshare_1437_14 = ((URL) c_URL.Clone ());
            c_URL = _vUnshare_1437_14;
            _vUnshare_1437_14.chg_upper_rate_limit (x);
        }
        public void set_max_sensor_rate (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (50, 175)._ovIn (x))) throw new _xPre (
                        "Classes.pd:1440,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            MaxSensorRate _vUnshare_1441_14 = ((MaxSensorRate) c_MaxSensorRate.Clone ());
            c_MaxSensorRate = _vUnshare_1441_14;
            _vUnshare_1441_14.chg_max_sensor_rate (x);
        }
        public void set_fixed_av_delay (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (70000, 300000)._ovIn (x))) throw new _xPre (
                        "Classes.pd:1444,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            FixedAVDelay _vUnshare_1445_14 = ((FixedAVDelay) c_FixedAVDelay.Clone ());
            c_FixedAVDelay = _vUnshare_1445_14;
            _vUnshare_1445_14.chg_fixed_av_delay (x);
        }
        public void set_dynamic_av_delay (SWITCH x)
        {
            DynamicAVDelay _vUnshare_1448_14 = ((DynamicAVDelay) c_DynamicAVDelay.Clone ());
            c_DynamicAVDelay = _vUnshare_1448_14;
            _vUnshare_1448_14.chg_dynamic_av_delay (x);
        }
        public void set_min_dyn_av_delay (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (30000, 100000)._ovIn (x))) throw new _xPre (
                        "Classes.pd:1451,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            MinDynamicAVDelay _vUnshare_1452_14 = ((MinDynamicAVDelay) c_MinDynamicAVDelay.Clone ());
            c_MinDynamicAVDelay = _vUnshare_1452_14;
            _vUnshare_1452_14.chg_min_dyn_av_delay (x);
        }
        public void set_sensed_av_delay_offset (_ePair < SWITCH, int > a)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((a._lEqual (new _ePair < SWITCH, int > (SWITCH.OFF, 0)) || ((SWITCH.ON ==
                        a.x) && (((- 100000) <= a.y) && (a.y <= (- 100000))))))) throw new _xPre (
                        "Classes.pd:1455,16");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            SensedAVDelayOffset _vUnshare_1458_14 = ((SensedAVDelayOffset) c_SensedAVDelayOffset.
                Clone ());
            c_SensedAVDelayOffset = _vUnshare_1458_14;
            _vUnshare_1458_14.chg_sensed_av_delay_offset (a);
        }
        public void set_a_pulse_amp_regulated (_ePair < SWITCH, int > a)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(((a._lEqual (new _ePair < SWITCH, int > (SWITCH.OFF, 0)) || ((SWITCH.ON ==
                        a.x) && ((500000 <= a.y) && (a.y <= 3200000)))) || ((SWITCH.ON == a.x) && ((
                        3500000 <= a.y) && (a.y <= 7000000)))))) throw new _xPre (
                        "Classes.pd:1461,16");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            APulseAmpRegulated _vUnshare_1466_14 = ((APulseAmpRegulated) c_APulseAmpRegulated.Clone
                ());
            c_APulseAmpRegulated = _vUnshare_1466_14;
            _vUnshare_1466_14.chg_a_pulse_amp_regulated (a);
        }
        public void set_v_pulse_amp_regulated (_ePair < SWITCH, int > a)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(((a._lEqual (new _ePair < SWITCH, int > (SWITCH.OFF, 0)) || ((SWITCH.ON ==
                        a.x) && ((500000 <= a.y) && (a.y <= 3200000)))) || ((SWITCH.ON == a.x) && ((
                        3500000 <= a.y) && (a.y <= 7000000)))))) throw new _xPre (
                        "Classes.pd:1469,16");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            VPulseAmpRegulated _vUnshare_1472_14 = ((VPulseAmpRegulated) c_VPulseAmpRegulated.Clone
                ());
            c_VPulseAmpRegulated = _vUnshare_1472_14;
            _vUnshare_1472_14.chg_v_pulse_amp_regulated (a);
        }
        public void set_a_pulse_amp_unregulated (_ePair < SWITCH, int > a)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((a._lEqual (new _ePair < SWITCH, int > (SWITCH.OFF, 0)) || ((SWITCH.ON ==
                        a.x) && ((((1250000 == a.y) || (2500000 == a.y)) || (3750000 == a.y)) || (
                        5000000 == a.y)))))) throw new _xPre ("Classes.pd:1475,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            APulseAmpUnregulated _vUnshare_1481_14 = ((APulseAmpUnregulated) c_APulseAmpUnregulated.
                Clone ());
            c_APulseAmpUnregulated = _vUnshare_1481_14;
            _vUnshare_1481_14.chg_a_pulse_amp_unregulated (a);
        }
        public void set_v_pulse_amp_unregulated (_ePair < SWITCH, int > a)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((a._lEqual (new _ePair < SWITCH, int > (SWITCH.OFF, 0)) || ((SWITCH.ON ==
                        a.x) && ((((1250000 == a.y) || (2500000 == a.y)) || (3750000 == a.y)) || (
                        5000000 == a.y)))))) throw new _xPre ("Classes.pd:1484,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            VPulseAmpUnregulated _vUnshare_1490_14 = ((VPulseAmpUnregulated) c_VPulseAmpUnregulated.
                Clone ());
            c_VPulseAmpUnregulated = _vUnshare_1490_14;
            _vUnshare_1490_14.chg_v_pulse_amp_unregulated (a);
        }
        public void set_a_pulse_width (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(((50 == x) || _eSystem._oRange (100, 1900)._ovIn (x)))) throw new _xPre (
                        "Classes.pd:1493,16");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            APulseWidth _vUnshare_1494_14 = ((APulseWidth) c_APulseWidth.Clone ());
            c_APulseWidth = _vUnshare_1494_14;
            _vUnshare_1494_14.chg_a_pulse_width (x);
        }
        public void set_v_pulse_width (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(((50 == x) || _eSystem._oRange (100, 1900)._ovIn (x)))) throw new _xPre (
                        "Classes.pd:1497,16");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            VPulseWidth _vUnshare_1498_14 = ((VPulseWidth) c_VPulseWidth.Clone ());
            c_VPulseWidth = _vUnshare_1498_14;
            _vUnshare_1498_14.chg_v_pulse_width (x);
        }
        public void set_a_sensitivity (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(((((250 == x) || (500 == x)) || (750 == x)) || _eSystem._oRange (1000,
                        10000)._ovIn (x)))) throw new _xPre ("Classes.pd:1501,16");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            ASensitivity _vUnshare_1503_14 = ((ASensitivity) c_ASensitivity.Clone ());
            c_ASensitivity = _vUnshare_1503_14;
            _vUnshare_1503_14.chg_a_sensitivity (x);
        }
        public void set_v_sensitivity (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(((((250 == x) || (500 == x)) || (750 == x)) || _eSystem._oRange (1000,
                        10000)._ovIn (x)))) throw new _xPre ("Classes.pd:1506,16");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            VSensitivity _vUnshare_1508_14 = ((VSensitivity) c_VSensitivity.Clone ());
            c_VSensitivity = _vUnshare_1508_14;
            _vUnshare_1508_14.chg_v_sensitivity (x);
        }
        public void set_v_refract_period (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (150000, 500000)._ovIn (x))) throw new _xPre (
                        "Classes.pd:1511,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            VRefractoryPeriod _vUnshare_1512_14 = ((VRefractoryPeriod) c_VRefractoryPeriod.Clone ());
            c_VRefractoryPeriod = _vUnshare_1512_14;
            _vUnshare_1512_14.chg_v_refract_period (x);
        }
        public void set_a_refract_period (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (150000, 500000)._ovIn (x))) throw new _xPre (
                        "Classes.pd:1515,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            ARefractoryPeriod _vUnshare_1516_14 = ((ARefractoryPeriod) c_ARefractoryPeriod.Clone ());
            c_ARefractoryPeriod = _vUnshare_1516_14;
            _vUnshare_1516_14.chg_a_refract_period (x);
        }
        public void set_pvarp (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (150000, 500000)._ovIn (x))) throw new _xPre (
                        "Classes.pd:1519,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            PVARP _vUnshare_1520_14 = ((PVARP) c_PVARP.Clone ());
            c_PVARP = _vUnshare_1520_14;
            _vUnshare_1520_14.chg_pvarp (x);
        }
        public void set_pvarp_ext (_ePair < SWITCH, int > a)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((a._lEqual (new _ePair < SWITCH, int > (SWITCH.OFF, 0)) || ((SWITCH.ON ==
                        a.x) && ((50000 <= a.y) && (a.y <= 400000)))))) throw new _xPre (
                        "Classes.pd:1523,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            PVARP_Ext _vUnshare_1525_14 = ((PVARP_Ext) c_PVARP_Ext.Clone ());
            c_PVARP_Ext = _vUnshare_1525_14;
            _vUnshare_1525_14.chg_pvarp_ext (a);
        }
        public void set_hysteresis_rate_limit (_ePair < SWITCH, int > a)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((a._lEqual (new _ePair < SWITCH, int > (SWITCH.OFF, 0)) || ((SWITCH.ON ==
                        a.x) && ((30 <= a.y) && (a.y <= 175)))))) throw new _xPre (
                        "Classes.pd:1528,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            HysteresisRateLimit _vUnshare_1531_14 = ((HysteresisRateLimit) c_HysteresisRateLimit.
                Clone ());
            c_HysteresisRateLimit = _vUnshare_1531_14;
            _vUnshare_1531_14.chg_hysteresis_rate_limit (a);
        }
        public void set_rate_smoothing (_ePair < SWITCH, int > a)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((a._lEqual (new _ePair < SWITCH, int > (SWITCH.OFF, 0)) || ((SWITCH.ON ==
                        a.x) && ((((((((3 == a.y) || (6 == a.y)) || (9 == a.y)) || (12 == a.y)) || (
                        15 == a.y)) || (18 == a.y)) || (21 == a.y)) || (25 == a.y)))))) throw new
                        _xPre ("Classes.pd:1534,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            RateSmoothing _vUnshare_1544_14 = ((RateSmoothing) c_RateSmoothing.Clone ());
            c_RateSmoothing = _vUnshare_1544_14;
            _vUnshare_1544_14.chg_rate_smoothing (a);
        }
        public void set_atr_mode (SWITCH x)
        {
            ATRMode _vUnshare_1547_14 = ((ATRMode) c_ATRMode.Clone ());
            c_ATRMode = _vUnshare_1547_14;
            _vUnshare_1547_14.chg_atr_mode (x);
        }
        public void set_atr_duration (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((((10 == x) || _eSystem._oRange (20, 80)._ovIn (x)) || _eSystem._oRange (
                        100, 2000)._ovIn (x)))) throw new _xPre ("Classes.pd:1550,16");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            ATRDuration _vUnshare_1551_14 = ((ATRDuration) c_ATRDuration.Clone ());
            c_ATRDuration = _vUnshare_1551_14;
            _vUnshare_1551_14.chg_atr_duration (x);
        }
        public void set_atr_fallback_time (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!((((((1 == x) || (2 == x)) || (3 == x)) || (4 == x)) || (5 == x)))) throw
                        new _xPre ("Classes.pd:1554,16");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            ATRFallbackTime _vUnshare_1559_14 = ((ATRFallbackTime) c_ATRFallbackTime.Clone ());
            c_ATRFallbackTime = _vUnshare_1559_14;
            _vUnshare_1559_14.chg_atr_fallback_time (x);
        }
        public void set_v_blanking (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (30000, 60000)._ovIn (x))) throw new _xPre (
                        "Classes.pd:1562,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            VBlanking _vUnshare_1563_14 = ((VBlanking) c_VBlanking.Clone ());
            c_VBlanking = _vUnshare_1563_14;
            _vUnshare_1563_14.chg_v_blanking (x);
        }
        public void set_act_threshold (ACTIVITY_THRESHOLD x)
        {
            ActivityThreshold _vUnshare_1566_14 = ((ActivityThreshold) c_ActivityThreshold.Clone ());
            c_ActivityThreshold = _vUnshare_1566_14;
            _vUnshare_1566_14.chg_act_threshold (x);
        }
        public void set_reaction_time (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (10, 50)._ovIn (x))) throw new _xPre (
                        "Classes.pd:1569,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            ReactTime _vUnshare_1570_14 = ((ReactTime) c_ReactTime.Clone ());
            c_ReactTime = _vUnshare_1570_14;
            _vUnshare_1570_14.chg_reaction_time (x);
        }
        public void set_response_factor (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (1, 16)._ovIn (x))) throw new _xPre ("Classes.pd:1573,15")
                        ;
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            RespFactor _vUnshare_1574_14 = ((RespFactor) c_RespFactor.Clone ());
            c_RespFactor = _vUnshare_1574_14;
            _vUnshare_1574_14.chg_response_factor (x);
        }
        public void set_recovery_time (int x)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(_eSystem._oRange (120, 960)._ovIn (x))) throw new _xPre (
                        "Classes.pd:1577,15");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            RecoveryTime _vUnshare_1578_14 = ((RecoveryTime) c_RecoveryTime.Clone ());
            c_RecoveryTime = _vUnshare_1578_14;
            _vUnshare_1578_14.chg_recovery_time (x);
        }
        public bool _lEqual (ProgrammableParameters _vArg_1341_13)
        {
            if (this == _vArg_1341_13)
            {
                return true;
            }
            return ((((((((((((((((((((((((((((_vArg_1341_13.c_LRL._lEqual (c_LRL) && _vArg_1341_13.
                c_URL._lEqual (c_URL)) && _vArg_1341_13.c_MaxSensorRate._lEqual (c_MaxSensorRate))
                && _vArg_1341_13.c_FixedAVDelay._lEqual (c_FixedAVDelay)) && _vArg_1341_13.
                c_DynamicAVDelay._lEqual (c_DynamicAVDelay)) && _vArg_1341_13.c_MinDynamicAVDelay.
                _lEqual (c_MinDynamicAVDelay)) && _vArg_1341_13.c_SensedAVDelayOffset._lEqual (
                c_SensedAVDelayOffset)) && _vArg_1341_13.c_APulseAmpRegulated._lEqual (
                c_APulseAmpRegulated)) && _vArg_1341_13.c_VPulseAmpRegulated._lEqual (
                c_VPulseAmpRegulated)) && _vArg_1341_13.c_APulseAmpUnregulated._lEqual (
                c_APulseAmpUnregulated)) && _vArg_1341_13.c_VPulseAmpUnregulated._lEqual (
                c_VPulseAmpUnregulated)) && _vArg_1341_13.c_APulseWidth._lEqual (c_APulseWidth)) &&
                _vArg_1341_13.c_VPulseWidth._lEqual (c_VPulseWidth)) && _vArg_1341_13.c_ASensitivity
                ._lEqual (c_ASensitivity)) && _vArg_1341_13.c_VSensitivity._lEqual (c_VSensitivity))
                && _vArg_1341_13.c_VRefractoryPeriod._lEqual (c_VRefractoryPeriod)) && _vArg_1341_13
                .c_ARefractoryPeriod._lEqual (c_ARefractoryPeriod)) && _vArg_1341_13.c_PVARP._lEqual
                (c_PVARP)) && _vArg_1341_13.c_PVARP_Ext._lEqual (c_PVARP_Ext)) && _vArg_1341_13.
                c_HysteresisRateLimit._lEqual (c_HysteresisRateLimit)) && _vArg_1341_13.
                c_RateSmoothing._lEqual (c_RateSmoothing)) && _vArg_1341_13.c_ATRMode._lEqual (
                c_ATRMode)) && _vArg_1341_13.c_ATRDuration._lEqual (c_ATRDuration)) && _vArg_1341_13
                .c_ATRFallbackTime._lEqual (c_ATRFallbackTime)) && _vArg_1341_13.c_VBlanking._lEqual
                (c_VBlanking)) && _vArg_1341_13.c_ActivityThreshold._lEqual (c_ActivityThreshold))
                && _vArg_1341_13.c_ReactTime._lEqual (c_ReactTime)) && _vArg_1341_13.c_RespFactor.
                _lEqual (c_RespFactor)) && _vArg_1341_13.c_RecoveryTime._lEqual (c_RecoveryTime));
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (
                ProgrammableParameters) && _lEqual ((ProgrammableParameters) _lArg));
        }
    }

    public sealed class HistogramsSt : _eAny
    {
        public _eSeq < _eTriple < int, int, int > > atrial_paced;
        public _eSeq < _eTriple < int, int, int > > atrial_sensed;
        public _eSeq < _eTriple < int, int, int > > ventricular_paced;
        public _eSeq < _eTriple < int, int, int > > ventricular_sensed;
        public int pvc_event;
        public int atrial_tachy;
        public HistogramsSt (_eSeq < _eTriple < int, int, int > > _vatrial_paced, _eSeq < _eTriple <
            int, int, int > > _vatrial_sensed, _eSeq < _eTriple < int, int, int > >
            _vventricular_paced, _eSeq < _eTriple < int, int, int > > _vventricular_sensed, int
            _vpvc_event, int _vatrial_tachy) : base ()
        {
            atrial_paced = _vatrial_paced;
            atrial_sensed = _vatrial_sensed;
            ventricular_paced = _vventricular_paced;
            ventricular_sensed = _vventricular_sensed;
            pvc_event = _vpvc_event;
            atrial_tachy = _vatrial_tachy;
        }
        public void chg_atrial_paced (int min, int max, int qty)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(((0 < min) && (0 < max)))) throw new _xPre ("Classes.pd:1603,17");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            atrial_paced = atrial_paced._oPlusPlus (new _eSeq < _eTriple < int, int, int > > (new
                _eTriple < int, int, int > (min, max, qty)));
        }
        public void chg_atrial_sensed (int min, int max, int qty)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(((0 < min) && (0 < max)))) throw new _xPre ("Classes.pd:1607,17");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            atrial_sensed = atrial_sensed._oPlusPlus (new _eSeq < _eTriple < int, int, int > > (new
                _eTriple < int, int, int > (min, max, qty)));
        }
        public void chg_ventricular_paced (int min, int max, int qty)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(((0 < min) && (0 < max)))) throw new _xPre ("Classes.pd:1611,17");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            ventricular_paced = ventricular_paced._oPlusPlus (new _eSeq < _eTriple < int, int, int >
                > (new _eTriple < int, int, int > (min, max, qty)));
        }
        public void chg_ventricular_sensed (int min, int max, int qty)
        {
            if (_eSystem.enablePre && _eSystem.currentCheckNesting <= _eSystem.maxCheckNesting)
            {
                _eSystem.currentCheckNesting ++;
                try
                {
                    if (!(((0 < min) && (0 < max)))) throw new _xPre ("Classes.pd:1615,17");
                }
                catch (_xCannotEvaluate)
                {
                }
                _eSystem.currentCheckNesting --;
            }
            ventricular_sensed = ventricular_sensed._oPlusPlus (new _eSeq < _eTriple < int, int, int
                > > (new _eTriple < int, int, int > (min, max, qty)));
        }
        public void chg_pvc_event (int qty)
        {
            pvc_event = qty;
        }
        public void chg_atrial_tachy (int qty)
        {
            atrial_tachy = qty;
        }
        public bool _lEqual (HistogramsSt _vArg_1585_13)
        {
            if (this == _vArg_1585_13)
            {
                return true;
            }
            return (((((_vArg_1585_13.atrial_paced._lEqual (atrial_paced) && _vArg_1585_13.
                atrial_sensed._lEqual (atrial_sensed)) && _vArg_1585_13.ventricular_paced._lEqual (
                ventricular_paced)) && _vArg_1585_13.ventricular_sensed._lEqual (ventricular_sensed))
                && (_vArg_1585_13.pvc_event == pvc_event)) && (_vArg_1585_13.atrial_tachy ==
                atrial_tachy));
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (HistogramsSt) &&
                _lEqual ((HistogramsSt) _lArg));
        }
    }

    public sealed class BradycardiaSt : _eAny
    {
        public BRADYCARDIA_STATE bradycardia_state;
        public BradycardiaSt (BRADYCARDIA_STATE _vbradycardia_state) : base ()
        {
            bradycardia_state = _vbradycardia_state;
        }
        public void chg_bradycardia_state (BRADYCARDIA_STATE x)
        {
            bradycardia_state = x;
        }
        public bool _lEqual (BradycardiaSt _vArg_1628_13)
        {
            if (this == _vArg_1628_13)
            {
                return true;
            }
            return (_vArg_1628_13.bradycardia_state == bradycardia_state);
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (BradycardiaSt) &&
                _lEqual ((BradycardiaSt) _lArg));
        }
    }

    public sealed class AtrialBipolarLead : _eAny
    {
        public LEAD_CHAMBER_TYPE a_lead_chamber;
        public POLARITY a_lead_polarity;
        public AtrialBipolarLead (LEAD_CHAMBER_TYPE _va_lead_chamber, POLARITY _va_lead_polarity) :
            base ()
        {
            a_lead_chamber = _va_lead_chamber;
            a_lead_polarity = _va_lead_polarity;
        }
        public void chg_a_lead_chamber (LEAD_CHAMBER_TYPE x)
        {
            a_lead_chamber = x;
        }
        public void chg_a_lead_polarity (POLARITY x)
        {
            a_lead_polarity = x;
        }
        public bool _lEqual (AtrialBipolarLead _vArg_1643_13)
        {
            if (this == _vArg_1643_13)
            {
                return true;
            }
            return ((_vArg_1643_13.a_lead_chamber == a_lead_chamber) && (_vArg_1643_13.
                a_lead_polarity == a_lead_polarity));
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (AtrialBipolarLead)
                && _lEqual ((AtrialBipolarLead) _lArg));
        }
    }

    public sealed class VentricularBipolarLead : _eAny
    {
        public LEAD_CHAMBER_TYPE v_lead_chamber;
        public POLARITY v_lead_polarity;
        public VentricularBipolarLead (LEAD_CHAMBER_TYPE _vv_lead_chamber, POLARITY
            _vv_lead_polarity) : base ()
        {
            v_lead_chamber = _vv_lead_chamber;
            v_lead_polarity = _vv_lead_polarity;
        }
        public void chg_v_lead_chamber (LEAD_CHAMBER_TYPE x)
        {
            v_lead_chamber = x;
        }
        public void chg_v_lead_polarity (POLARITY x)
        {
            v_lead_polarity = x;
        }
        public bool _lEqual (VentricularBipolarLead _vArg_1660_13)
        {
            if (this == _vArg_1660_13)
            {
                return true;
            }
            return ((_vArg_1660_13.v_lead_chamber == v_lead_chamber) && (_vArg_1660_13.
                v_lead_polarity == v_lead_polarity));
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (
                VentricularBipolarLead) && _lEqual ((VentricularBipolarLead) _lArg));
        }
    }

    public sealed class LeadsSt : _eAny
    {
        public AtrialBipolarLead c_AtrialBipolarLead;
        public VentricularBipolarLead c_VentricularBipolarLead;
        public LeadsSt (AtrialBipolarLead _vc_AtrialBipolarLead, VentricularBipolarLead
            _vc_VentricularBipolarLead) : base ()
        {
            c_AtrialBipolarLead = _vc_AtrialBipolarLead;
            c_VentricularBipolarLead = _vc_VentricularBipolarLead;
        }
        public void chg_a_lead_chamber (LEAD_CHAMBER_TYPE x)
        {
            AtrialBipolarLead _vUnshare_1687_14 = ((AtrialBipolarLead) c_AtrialBipolarLead.Clone ());
            c_AtrialBipolarLead = _vUnshare_1687_14;
            _vUnshare_1687_14.chg_a_lead_chamber (x);
        }
        public void chg_a_lead_polarity (POLARITY x)
        {
            AtrialBipolarLead _vUnshare_1690_14 = ((AtrialBipolarLead) c_AtrialBipolarLead.Clone ());
            c_AtrialBipolarLead = _vUnshare_1690_14;
            _vUnshare_1690_14.chg_a_lead_polarity (x);
        }
        public void chg_v_lead_chamber (LEAD_CHAMBER_TYPE x)
        {
            VentricularBipolarLead _vUnshare_1693_14 = ((VentricularBipolarLead)
                c_VentricularBipolarLead.Clone ());
            c_VentricularBipolarLead = _vUnshare_1693_14;
            _vUnshare_1693_14.chg_v_lead_chamber (x);
        }
        public void chg_v_lead_polarity (POLARITY x)
        {
            VentricularBipolarLead _vUnshare_1696_14 = ((VentricularBipolarLead)
                c_VentricularBipolarLead.Clone ());
            c_VentricularBipolarLead = _vUnshare_1696_14;
            _vUnshare_1696_14.chg_v_lead_polarity (x);
        }
        public bool _lEqual (LeadsSt _vArg_1677_13)
        {
            if (this == _vArg_1677_13)
            {
                return true;
            }
            return (_vArg_1677_13.c_AtrialBipolarLead._lEqual (c_AtrialBipolarLead) && _vArg_1677_13
                .c_VentricularBipolarLead._lEqual (c_VentricularBipolarLead));
        }
        public override bool Equals (object _lArg)
        {
            return _lArg == this || (_lArg != null && _lArg.GetType () == typeof (LeadsSt) &&
                _lEqual ((LeadsSt) _lArg));
        }
    }

}


// End of file.
