//***********************************************************************************************
//* File: 'Classes.cpp'
//*
//* by Artur Oliveira Gomes - December 16th 2013
//***********************************************************************************************

//#include <iterator>
#include "stdlib.h"
//#include <StandardCplusplus.h>
//#include <serstream>
//#include <fstream>
//#include <vector>
//#include <pnew.cpp>
//#include <list>

// File inclusions for forward declarations

//#include "Classes0.h"

// File inclusions for full declarations

#include "Classes1.h"

// File inclusions for inline code

//#include "Classes2.h"
//using namespace std;
	
	bool checkRange(int i, int min, int max){
		if(min<= i && i <= max){return true;}
		else return false;
	}

	bool BOM :: inVOO () const
	{
		return (((((ON == _rswitch) 
		&& (C_VENTRICLE == chambers_paced)) 
		&& (C_NONE == chambers_sensed)) 
		&& (R_NONE == response_to_sensing)) 
		&& (false == rate_modulation));
	}
	bool BOM :: inAOO () const
	{
		return (((((ON == _rswitch) 
		&& (C_ATRIUM == chambers_paced)) 
		&& (C_NONE == chambers_sensed)) 
		&& (R_NONE == response_to_sensing)) 
		&& (false == rate_modulation));
	}
	bool BOM :: inDOO () const
	{
		return (((((ON == _rswitch) 
		&& (C_DUAL == chambers_paced)) 
		&& (C_NONE == chambers_sensed)) 
		&& (R_NONE == response_to_sensing)) 
		&& (false == rate_modulation));
	}
	bool BOM :: inDDI () const
	{
		return (((((ON == _rswitch) 
		&& (C_DUAL == chambers_paced)) 
		&& (C_DUAL == chambers_sensed)) 
		&& (INHIBITED == response_to_sensing) ) 
		&& (false == rate_modulation));
	}
	bool BOM :: inVVI () const
	{
		return (((((ON == _rswitch) 
		&& (C_VENTRICLE == chambers_paced)) 
		&& (C_VENTRICLE == chambers_sensed)) 
		&& (INHIBITED == response_to_sensing)) 
		&& (false == rate_modulation));
	}
	bool BOM :: inVDD () const
	{
		return (((((ON == _rswitch) 
		&& (C_VENTRICLE == chambers_paced)) 
		&& (C_DUAL == chambers_sensed)) 
		&& (TRACKED == response_to_sensing)) 
		&& (false == rate_modulation));
	}
	bool BOM :: inAAI () const
	{
		return (((((ON == _rswitch) 
		&& (C_ATRIUM == chambers_paced))
		&& (C_ATRIUM == chambers_sensed))
		&& (INHIBITED == response_to_sensing))
		&& (false == rate_modulation));
	}
	bool BOM :: inVVT () const
	{
		return (((((ON == _rswitch)
		&& (C_VENTRICLE == chambers_paced))
		&& (C_VENTRICLE == chambers_sensed))
		&& (TRIGGERED == response_to_sensing))
		&& (false == rate_modulation));
	}
	bool BOM :: inAAT () const
	{
		return (((((ON == _rswitch)
		&& (C_ATRIUM == chambers_paced))
		&& (C_ATRIUM == chambers_sensed))
		&& (TRIGGERED == response_to_sensing))
		&& (false == rate_modulation));
	}
	bool BOM :: inAXXX () const
	{
		return ((ON == _rswitch)
		&& (C_ATRIUM == chambers_paced));
	}
	bool BOM :: inVXXX () const
	{
		return ((ON == _rswitch)
		&& (C_VENTRICLE == chambers_paced));
	}
	bool BOM :: inDXXX() const
	{
		return ((ON == _rswitch)
		&& (C_DUAL == chambers_paced));
	}
	bool BOM :: inOXO() const
	{
		return (((ON == _rswitch)
		&& (C_NONE == chambers_paced))
		&& (R_NONE == response_to_sensing));
	}
	BOM :: BOM (const SWITCH _vswitch,
		const CHAMBERS _vchambers_paced,
		const CHAMBERS _vchambers_sensed,
		const RESPONSE _vresponse_to_sensing,
		const bool _vrate_modulation)
	{ _rswitch = _vswitch;
		chambers_paced = _vchambers_paced;
		chambers_sensed = _vchambers_sensed;
		response_to_sensing = _vresponse_to_sensing;
		rate_modulation = _vrate_modulation;
	}
	BOM :: BOM () {}

//Class
	
	TimeSt :: TimeSt (const int _vtime,
		const int _va_start_time,
		const int _va_max,
		const int _va_delay,
		const int _vv_start_time,
		const int _vv_max,
		const int _vv_delay, 
		const int _va_curr_measurement,
		const int _vv_curr_measurement)
	{
		time = _vtime;
		a_start_time = _va_start_time;
		a_max = _va_max; 
		a_delay = _va_delay;
		v_start_time = _vv_start_time;
		v_max = _vv_max;
		v_delay = _vv_delay;
		a_curr_measurement = _va_curr_measurement;
		v_curr_measurement = _vv_curr_measurement;
	}
	void TimeSt :: chg_time(const int x)
	{
		if(x>=0)
		time = x;
	}
	void TimeSt :: chg_a_start_time (const int x)
	{
		a_start_time = x;
	}
	void TimeSt :: chg_a_max (const int x)
	{
		a_max = x;
	}
	void TimeSt :: chg_a_delay (const int x)
	{
		a_delay = x;
	}
	void TimeSt :: chg_v_start_time (const int x)
	{
		v_start_time = x;
	}
	void TimeSt :: chg_v_max (const int x)
	{
		v_max = x;
	}
	void TimeSt :: chg_v_delay (const int x)
	{
		v_delay = x;
	}
	void TimeSt :: chg_v_curr_measurement (const int x)
	{
		v_curr_measurement = x;
	}
	void TimeSt :: chg_a_curr_measurement (const int x)
	{
		a_curr_measurement = x;
	}
	
	TimeSt :: TimeSt () {
		time = 0;
		a_start_time = 0;
		a_max = 0;
		a_delay = 0;
		v_start_time = 0;
		v_max = 0;
		v_delay = 0;
		a_curr_measurement = 0;
		v_curr_measurement = 0;
	}

//Class
	SensingPulse :: SensingPulse (const int _vcardiac_cycles_length,
		const int _va_sensing_threshold,
		const int _vv_sensing_threshold,
		const int _vsensed_a_pulse_width, 
		const int _vsensed_v_pulse_width,
		const int _vsensed_a_pulse_amp,
		const int _vsensed_v_pulse_amp,
		const int _vlast_s_a_pulse,
		const int _vlast_s_v_pulse,
		const int _vcurrent_rate)
	{
		cardiac_cycles_length = _vcardiac_cycles_length;
		a_sensing_threshold = _va_sensing_threshold;
		v_sensing_threshold = _vv_sensing_threshold;
		sensed_a_pulse_width = _vsensed_a_pulse_width;
		sensed_v_pulse_width = _vsensed_v_pulse_width;
		sensed_a_pulse_amp = _vsensed_a_pulse_amp;
		sensed_v_pulse_amp = _vsensed_v_pulse_amp;
		last_s_a_pulse = _vlast_s_a_pulse;
		last_s_v_pulse = _vlast_s_v_pulse;
		current_rate = _vcurrent_rate;
	}
	void SensingPulse :: chg_sensed_atrial (const int x,
		const int y,
		const int z)
	{
		sensed_a_pulse_width = x;
		sensed_a_pulse_amp = y;
		last_s_a_pulse = z;
	}
	void SensingPulse :: chg_sensed_ventricular (const int x,
		const int y,
		const int k,
		const int z)
	{
		sensed_v_pulse_width = x;
		sensed_v_pulse_amp = y;
		last_s_v_pulse = k;
		cardiac_cycles_length = z;
	}
	void SensingPulse :: chg_cardiac_cycles_length (const int x)
	{
		cardiac_cycles_length = x;
	}
	void SensingPulse :: chg_a_sensing_threshold (const int x)
	{
		a_sensing_threshold = x;
	}
	void SensingPulse :: chg_v_sensing_threshold (const int x)
	{
		v_sensing_threshold = x;
	}
	void SensingPulse :: chg_sensed_a_pulse_width (const int x)
	{
		sensed_a_pulse_width = x;
	}
	void SensingPulse :: chg_sensed_v_pulse_width (const int x)
	{
		sensed_v_pulse_width = x;
	}
	void SensingPulse :: chg_sensed_a_pulse_amp (const int x)
	{
		sensed_a_pulse_amp = x;
	}
	void SensingPulse :: chg_sensed_v_pulse_amp (const int x)
	{
		sensed_v_pulse_amp = x;
	}
	void SensingPulse :: chg_last_s_a_pulse (const int x)
	{
		last_s_a_pulse = x;
	}
	void SensingPulse :: chg_last_s_v_pulse (const int x)
	{
		last_s_v_pulse = x;
	}
	void SensingPulse :: chg_current_rate (const int x)
	{
		current_rate = x;
	}
	
	SensingPulse :: SensingPulse () {
		cardiac_cycles_length = 0;
		a_sensing_threshold = 0;
		v_sensing_threshold = 0;
		sensed_a_pulse_width = 0;
		sensed_v_pulse_width = 0;
		sensed_a_pulse_amp = 0;
		sensed_v_pulse_amp = 0;
		last_s_a_pulse = 0;
		last_s_v_pulse = 0;
		current_rate = 0;
	}

//Class
	PacingPulse :: PacingPulse (const int _vpaced_a_pulse_width,
		const int _vpaced_v_pulse_width, 
		const int _vpaced_a_pulse_amp,
		const int _vpaced_v_pulse_amp,
		const int _vlast_p_a_pulse,
		const int _vlast_p_v_pulse)
	{
		 paced_a_pulse_width = _vpaced_a_pulse_width;
		 paced_v_pulse_width = _vpaced_v_pulse_width;
		 paced_a_pulse_amp = _vpaced_a_pulse_amp;
		 paced_v_pulse_amp = _vpaced_v_pulse_amp;
		 last_p_a_pulse = _vlast_p_a_pulse;
		 last_p_v_pulse = _vlast_p_v_pulse;
	}
	void PacingPulse :: chg_paced_atrial (const int x,
		const int y,
		const int z)
	{
		paced_a_pulse_width = x;
		paced_a_pulse_amp = y;
		last_p_a_pulse = z;
	}
	void PacingPulse :: chg_paced_ventricular (const int x,
		const int y,
		const int z)
	{
		paced_v_pulse_width = x;
		paced_v_pulse_amp = y;
		last_p_v_pulse = x;
	}
	void PacingPulse :: chg_paced_dual (const int x,
		const int y,
		const int z,
		const int k, 
		const int m)
	{
		paced_a_pulse_width = x;
		paced_a_pulse_amp = y;
		last_p_a_pulse = m;
		paced_v_pulse_width = z;
		paced_v_pulse_amp = k;
		last_p_v_pulse = m;
	}
	void PacingPulse :: chg_paced_a_pulse_width (const int x)
	{
		paced_a_pulse_width = x;
	}
	void PacingPulse :: chg_paced_v_pulse_width (const int x)
	{
		paced_v_pulse_width = x;
	}
	void PacingPulse :: chg_paced_a_pulse_amp (const int x)
	{
		paced_a_pulse_amp = x;
	}
	void PacingPulse :: chg_paced_v_pulse_amp (const int x)
	{
		paced_v_pulse_amp = x;
	}
	void PacingPulse :: chg_last_p_a_pulse (const int x)
	{
		last_p_a_pulse = x;
	}
	void PacingPulse :: chg_last_p_v_pulse (const int x)
	{
		last_p_v_pulse = x;
	}
	
	PacingPulse :: PacingPulse () {
		paced_a_pulse_width = 0; 
		paced_v_pulse_width = 0;
		paced_a_pulse_amp = 0;
		paced_v_pulse_amp = 0;
		last_p_a_pulse = 0;
		last_p_v_pulse = 0;
	}

//Class
	BatteryStatus :: BatteryStatus (const BATT_STATUS_LEVEL _vbatt_status_level)
	{
		batt_status_level = _vbatt_status_level;
	}
	void BatteryStatus :: chg_batt_status_level (const BATT_STATUS_LEVEL x)
	{
		batt_status_level = x;
	}
	
	BatteryStatus :: BatteryStatus () {
		batt_status_level = BOL;

	}

//Class
	NoiseDetection :: NoiseDetection (const float _vnoise)
	{
		noise = _vnoise;
	}
	void NoiseDetection :: chg_noise (const float x)
	{
		noise = x;
	}
	NoiseDetection :: NoiseDetection () {noise = 0.0;}
//Class
	
	EventMarkers :: EventMarkers (const std::list<quintuple >_vatrial_marker,
		const std::list<quintuple >_vventricular_marker, 
		const std::list<pair<MarkerABB, int> >_vaugmentation_marker)
	{
		
		atrial_marker = _vatrial_marker;
		ventricular_marker = _vventricular_marker;
		augmentation_marker = _vaugmentation_marker;
	}
	void EventMarkers :: chg_atrial_marker (const MarkerABB m,
		const int pw,
		const int pa,
		const int no,
		const int ti)
	{
		atrial_marker.push_back(quintuple(m, pw, pa, no, ti));
	}
	void EventMarkers :: chg_ventricular_marker (const MarkerABB m,
		const int pw,
		const int pa,
		const int no,
		const int ti)
	{
		ventricular_marker.push_back(quintuple(m, pw, pa, no, ti));
	}

	void EventMarkers :: chg_dual_marker (const MarkerABB ma,
		const int pwa,
		const int paa,
		const MarkerABB mv,
		const int pwv,
		const int pav,
		const int no,
		const int ti)
	{
		atrial_marker.push_back(quintuple(ma, pwa, paa, no, ti));
		ventricular_marker.push_back(quintuple(mv, pwv, pav, no, ti)) ;
	}
	void EventMarkers :: chg_augmentation_marker (const MarkerABB m, const int ti)
	{
		augmentation_marker.push_back(pair<MarkerABB, int> (m, ti));
	}
	void EventMarkers :: unchg_dual_marker ()
	{
		atrial_marker = atrial_marker;
		ventricular_marker = ventricular_marker;
	}
	void EventMarkers :: unchg_atrial_marker ()
	{
		atrial_marker = atrial_marker;
	}
	void EventMarkers :: unchg_ventricular_marker ()
	{
		ventricular_marker = ventricular_marker;
	}
	void EventMarkers :: unchg_augmentation_marker ()
	{
		augmentation_marker = augmentation_marker;
	}
	EventMarkers :: EventMarkers () {
		atrial_marker.clear();
		ventricular_marker.clear();
		augmentation_marker.clear();
	}
//Class

	Accelerometer :: Accelerometer (const int _vaccel)
	{
		accel = _vaccel;
	}
	void Accelerometer :: chg_accel (const int x)
	{
		accel = x;
	}
	Accelerometer :: Accelerometer () {}
//Class

	PWave :: PWave (const int _vp_wave)
	{
		p_wave = _vp_wave;
	}
	void PWave :: chg_p_wave (const int x)
	{
		p_wave = x;
	}
	PWave :: PWave () {p_wave=0;}
//Class

	RWave :: RWave (const int _vr_wave)
	{
		r_wave = _vr_wave;
	}
	void RWave :: chg_r_wave (const int x)
	{
		r_wave = x;
	}
	RWave :: RWave () {r_wave=0;}
//Class

	BatteryVoltage :: BatteryVoltage (const int _vbattery_voltage)
	{
		battery_voltage = _vbattery_voltage; 
	
	}
	void BatteryVoltage :: chg_battery_voltage (const int x)
	{
		battery_voltage = x;
	}
	BatteryVoltage :: BatteryVoltage () {}
//Class

	LeadImpedance :: LeadImpedance (const int _vlead_impedance)
	{
		if(checkRange(_vlead_impedance, 100, 2500))
		lead_impedance = _vlead_impedance;
	}
	void LeadImpedance :: chg_lead_impedance (const int x)
	{
		if(checkRange(x, 100, 2500))
		lead_impedance = x;
	}
	LeadImpedance :: LeadImpedance () 
	{
		lead_impedance = 100;
	
	}
//Class

	MeasuredParameters :: MeasuredParameters (const LeadImpedance _vc_LeadImpedance,
		const BatteryVoltage _vc_BatteryVoltage,
		const RWave _vc_RWave,
		const PWave _vc_PWave)
	{
		c_LeadImpedance = _vc_LeadImpedance;
		c_BatteryVoltage = _vc_BatteryVoltage;
		c_RWave = _vc_RWave;
		c_PWave = _vc_PWave;
	}
	void MeasuredParameters :: set_p_wave (const int x)
	{
		c_PWave.chg_p_wave (x);
	}
	void MeasuredParameters :: set_r_wave (const int x)
	{
		c_RWave.chg_r_wave (x);
	}
	void MeasuredParameters :: set_lead_impedance (const int x)
	{
		if(checkRange(x, 100, 2500))	
			c_LeadImpedance.chg_lead_impedance (x);
	}
	void MeasuredParameters :: set_battery_voltage (const int x)
	{
		c_BatteryVoltage.chg_battery_voltage (x);
	}
	MeasuredParameters :: MeasuredParameters () {}
//Class

	bool BO_MODE :: isVOO () const
	{
		return (true == bo_mode.inVOO ());
	}
	bool BO_MODE :: isAOO () const
	{
		return (true == bo_mode.inAOO ());
	}
	bool BO_MODE :: isDOO () const
	{
		return (true == bo_mode.inAOO ());
	}
	bool BO_MODE :: isDDI () const
	{
		return (true == bo_mode.inDDI ());
	}
	bool BO_MODE :: isVVI () const
	{
		return (true == bo_mode.inVVI ());
	}
	bool BO_MODE :: isAAI () const
	{
		return (true == bo_mode.inAAI ());
	}
	bool BO_MODE :: isVVT () const
	{
		return (true == bo_mode.inVVT ());
	}
	bool BO_MODE :: isAAT () const
	{
		return (true == bo_mode.inAAT ());
	}
	bool BO_MODE :: isVDD () const
	{
		return (true == bo_mode.inVDD ());
	}
	BO_MODE :: BO_MODE (const BOM _vbo_mode)
	{
		bo_mode = _vbo_mode;
	}
	void BO_MODE :: chg_bo_mode (const BOM x)
	{
		bo_mode = x;
	}
	BO_MODE :: BO_MODE () {}
//Class

	LRL :: LRL (const int _vlower_rate_limit)
	{
		if(checkRange(_vlower_rate_limit, 30, 175))
		lower_rate_limit = _vlower_rate_limit;
	}
	void LRL :: chg_lower_rate_limit (const int x)
	{
		if(checkRange(x, 30, 175))
		lower_rate_limit = x;
	}
	LRL :: LRL () 
	{		
		lower_rate_limit = 60;
	}
//Class

	URL :: URL (const int _vupper_rate_limit)
	{
		if(checkRange(_vupper_rate_limit, 30, 175))
		upper_rate_limit = _vupper_rate_limit;
	}
	void URL :: chg_upper_rate_limit (const int x)
	{
		if(checkRange(x, 30, 175))
		upper_rate_limit = x;
	}
	URL :: URL () 
	{
		upper_rate_limit = 120;	
	}
//Class

	MaxSensorRate :: MaxSensorRate (const int _vmax_sensor_rate)
	{
		if(checkRange(_vmax_sensor_rate, 50, 175))
		max_sensor_rate = _vmax_sensor_rate;
	}
	void MaxSensorRate :: chg_max_sensor_rate (const int x)
	{
		if(checkRange(x, 50, 175))
		max_sensor_rate = x;
	}
	MaxSensorRate :: MaxSensorRate () 
	{
		max_sensor_rate = 120;
	}
//Class

	FixedAVDelay :: FixedAVDelay (const int _vfixed_av_delay)
	{
		if(checkRange(_vfixed_av_delay, 70000, 300000))
		fixed_av_delay = _vfixed_av_delay;
	}
	void FixedAVDelay :: chg_fixed_av_delay (const int x)
	{
		if(checkRange(x, 70000, 300000))
		fixed_av_delay = x;
	}
	FixedAVDelay :: FixedAVDelay () 
	{
		fixed_av_delay = 150000;	
	}
//Class

	DynamicAVDelay :: DynamicAVDelay (const SWITCH _vdynamic_av_delay)
	{
			dynamic_av_delay = _vdynamic_av_delay;
	}
	void DynamicAVDelay :: chg_dynamic_av_delay (const SWITCH x)
	{
		dynamic_av_delay = x;
	}
	DynamicAVDelay :: DynamicAVDelay () {}
//Class

	MinDynamicAVDelay :: MinDynamicAVDelay (const int _vmin_dyn_av_delay)
	{
		if(checkRange(_vmin_dyn_av_delay, 30000, 100000))
			min_dyn_av_delay = _vmin_dyn_av_delay;
	}
	void MinDynamicAVDelay :: chg_min_dyn_av_delay (const int x)
	{
		if(checkRange(x, 30000, 100000))
		min_dyn_av_delay = x;
	}
	MinDynamicAVDelay :: MinDynamicAVDelay () {		
		min_dyn_av_delay = 50000;
	}
//Class

	SensedAVDelayOffset :: SensedAVDelayOffset (const pair<SWITCH, int> _vsensed_av_delay_offset)
	{
	if((_vsensed_av_delay_offset.first==OFF && _vsensed_av_delay_offset.second==0)
		|| (_vsensed_av_delay_offset.first==OFF && 
				((-100000) <= _vsensed_av_delay_offset.second 
					&& _vsensed_av_delay_offset.second <= (-10000)))
		|| (_vsensed_av_delay_offset.first==ON && 
				((-100000) <= _vsensed_av_delay_offset.second 
					&& _vsensed_av_delay_offset.second <= (-10000))))
	{
		
		sensed_av_delay_offset = _vsensed_av_delay_offset;
	}
	}
	void SensedAVDelayOffset :: chg_sensed_av_delay_offset (const pair<SWITCH, int> a)
	{
	if((a.first==OFF && a.second==0)
		|| (a.first==OFF && 
				((-100000) <= a.second 
					&& a.second <= (-10000)))
		|| (a.first==ON && 
				((-100000) <= a.second 
					&& a.second <= (-10000))))
	{
		sensed_av_delay_offset = a;
	}
	}
	SensedAVDelayOffset :: SensedAVDelayOffset () 
	{
		sensed_av_delay_offset = make_pair<SWITCH, int>(OFF,0);
	}
//Class

	APulseAmpRegulated :: APulseAmpRegulated (const pair<SWITCH, int> _va_pulse_amp_regulated)
	{
		if((_va_pulse_amp_regulated.first==OFF && _va_pulse_amp_regulated.second==0)
			|| (_va_pulse_amp_regulated.first==ON && 
					(500000 <= _va_pulse_amp_regulated.second 
						&& _va_pulse_amp_regulated.second <= 3200000))
			|| (_va_pulse_amp_regulated.first==ON && 
					(3500000 <= _va_pulse_amp_regulated.second 
						&& _va_pulse_amp_regulated.second <= 7000000)))
	
		{
			a_pulse_amp_regulated = _va_pulse_amp_regulated;
		}
	}
	
	void APulseAmpRegulated :: chg_a_pulse_amp_regulated (const pair<SWITCH, int> a)
	{
		if((a.first==OFF && a.second==0)
			|| (a.first==ON && 
					(500000 <= a.second 
						&& a.second <= 3200000))
			|| (a.first==ON && 
					(3500000 <= a.second 
						&& a.second <= 7000000)))
	
		{
			a_pulse_amp_regulated = a;
		}
	}
	
	APulseAmpRegulated :: APulseAmpRegulated () 
	{
		a_pulse_amp_regulated = make_pair<SWITCH, int>(ON,3500000);
	}
//Class

	VPulseAmpRegulated :: VPulseAmpRegulated (const pair<SWITCH, int> _vv_pulse_amp_regulated)
	{
		if((_vv_pulse_amp_regulated.first==OFF && _vv_pulse_amp_regulated.second==0)
			|| (_vv_pulse_amp_regulated.first==ON && 
					(500000 <= _vv_pulse_amp_regulated.second 
						&& _vv_pulse_amp_regulated.second <= 3200000))
			|| (_vv_pulse_amp_regulated.first==ON && 
					(3500000 <= _vv_pulse_amp_regulated.second 
						&& _vv_pulse_amp_regulated.second <= 7000000)))
	
		{
		v_pulse_amp_regulated = _vv_pulse_amp_regulated;
		}
	}

	void VPulseAmpRegulated :: chg_v_pulse_amp_regulated (const pair<SWITCH, int> a)
	{
		if((a.first==OFF && a.second==0)
			|| (a.first==ON && 
					(500000 <= a.second 
						&& a.second <= 3200000))
			|| (a.first==ON && 
					(3500000 <= a.second 
						&& a.second <= 7000000)))
	
		{
		v_pulse_amp_regulated = a;
		}
	}
	VPulseAmpRegulated :: VPulseAmpRegulated () 
	{
		v_pulse_amp_regulated = make_pair<SWITCH, int>(ON,3500000);	
	}
//Class

	APulseAmpUnregulated :: APulseAmpUnregulated (const pair<SWITCH, int> _va_pulse_amp_unregulated)
	{
		if((_va_pulse_amp_unregulated.first==OFF && _va_pulse_amp_unregulated.second==0)
			|| (_va_pulse_amp_unregulated.first==ON && 
					(1250000 == _va_pulse_amp_unregulated.second 
					|| 250000 == _va_pulse_amp_unregulated.second 
					|| 375000 == _va_pulse_amp_unregulated.second 
					|| 500000 == _va_pulse_amp_unregulated.second )))
	
		{
		a_pulse_amp_unregulated = _va_pulse_amp_unregulated;
		}
	}
	void APulseAmpUnregulated :: chg_a_pulse_amp_unregulated (const pair<SWITCH, int> a)
	{
		if((a.first==OFF && a.second==0)
			|| (a.first==ON && 
					(1250000 == a.second 
					|| 250000 == a.second 
					|| 375000 == a.second 
					|| 500000 == a.second )))
	
		{
		a_pulse_amp_unregulated = a;
		}
	}
	APulseAmpUnregulated :: APulseAmpUnregulated () 
	{
		a_pulse_amp_unregulated = make_pair<SWITCH, int>(ON,3750000);	
	}
//Class

	VPulseAmpUnregulated :: VPulseAmpUnregulated (const pair<SWITCH, int> _vv_pulse_amp_unregulated)
	{
		if((v_pulse_amp_unregulated.first==OFF && v_pulse_amp_unregulated.second==0)
			|| (v_pulse_amp_unregulated.first==ON && 
					(1250000 == v_pulse_amp_unregulated.second 
					|| 250000 == v_pulse_amp_unregulated.second 
					|| 375000 == v_pulse_amp_unregulated.second 
					|| 500000 == v_pulse_amp_unregulated.second )))
	
		{
		v_pulse_amp_unregulated = _vv_pulse_amp_unregulated;
		}
	}
	void VPulseAmpUnregulated :: chg_v_pulse_amp_unregulated (const pair<SWITCH, int> a)
	{
		if((a.first==OFF && a.second==0)
			|| (a.first==ON && 
					(1250000 == a.second 
					|| 250000 == a.second 
					|| 375000 == a.second 
					|| 500000 == a.second )))
	
		{
		v_pulse_amp_unregulated = a;
		}
	}
	VPulseAmpUnregulated :: VPulseAmpUnregulated () 
	{
		v_pulse_amp_unregulated = make_pair<SWITCH, int>(ON,3750000);	
	}
//Class

	APulseWidth :: APulseWidth (const int _va_pulse_width)
	{
		if((_va_pulse_width==50)||checkRange(_va_pulse_width,100,1900))
			a_pulse_width = _va_pulse_width;
	}
	void APulseWidth :: chg_a_pulse_width (const int x)
	{
		if((x==50)||checkRange(x,100,1900))
			a_pulse_width = x;
	}
	APulseWidth :: APulseWidth () 
	{
			a_pulse_width = 400;	
	}
//Class

	VPulseWidth :: VPulseWidth (const int _vv_pulse_width)
	{
		if((_vv_pulse_width==50)||checkRange(_vv_pulse_width,100,1900))
		v_pulse_width = _vv_pulse_width;
	}
	void VPulseWidth :: chg_v_pulse_width (const int x)
	{
		if((x==50)||checkRange(x,100,1900))
		v_pulse_width = x;
	}
	VPulseWidth :: VPulseWidth () 
	{
			v_pulse_width = 400;		
	}

//Class
	ASensitivity :: ASensitivity (const int _va_sensitivity)
	{
		if((_va_sensitivity == 250) || (_va_sensitivity == 500) || (_va_sensitivity == 750) || checkRange(_va_sensitivity,1000,10000))
		a_sensitivity = _va_sensitivity;
	}
	void ASensitivity :: chg_a_sensitivity (const int x)
	{
		if(x == 250 || x == 500 
			|| x == 750  || checkRange(x,1000,10000))
		a_sensitivity = x;
	}
	ASensitivity :: ASensitivity () 
	{
		a_sensitivity = 750;	
	}

//Class
	VSensitivity :: VSensitivity (const int _vv_sensitivity)
	{
		if(_vv_sensitivity == 250 || _vv_sensitivity == 500 
			|| _vv_sensitivity == 750  || checkRange(_vv_sensitivity,1000,10000))
		v_sensitivity = _vv_sensitivity;
	}
	void VSensitivity :: chg_v_sensitivity (const int x)
	{
		if(x == 250 || x == 500 
			|| x == 750  || checkRange(x,1000,10000))
		v_sensitivity = x;
	}
	VSensitivity :: VSensitivity () 
	{
		v_sensitivity = 2500;
	}

//Class
	VRefractoryPeriod :: VRefractoryPeriod (const int _vv_refract_period)
	{
		if(checkRange(_vv_refract_period,150000,500000))
		v_refract_period = _vv_refract_period;
	}
	void VRefractoryPeriod :: chg_v_refract_period (const int x)
	{
		if(checkRange(x,150000,500000))
		v_refract_period = x;
	}
	VRefractoryPeriod :: VRefractoryPeriod () 
	{
		v_refract_period = 320000;
	}

//Class
	ARefractoryPeriod :: ARefractoryPeriod (const int _va_refract_period)
	{
		if(checkRange(_va_refract_period,150000,500000))
		a_refract_period = _va_refract_period;
	}
	void ARefractoryPeriod :: chg_a_refract_period (const int x)
	{
		if(checkRange(x,150000,500000))
		a_refract_period = x;
	}
	ARefractoryPeriod :: ARefractoryPeriod () 
	{
		a_refract_period = 250000;
	}

//Class
	PVARP :: PVARP (const int _vpvarp)
	{
		if(checkRange(_vpvarp,150000,500000))
		pvarp = _vpvarp;
	}
	void PVARP :: chg_pvarp (const int x)
	{		
		if(checkRange(x,150000,500000))
			pvarp = x;
	}
	PVARP :: PVARP () 
	{
		pvarp = 250000;
	}

//Class
	PVARP_Ext :: PVARP_Ext (const pair<SWITCH, int> _vpvarp_ext)
	{
		if((_vpvarp_ext.first==OFF && _vpvarp_ext.second==0)
			|| (_vpvarp_ext.first==ON && 
					(50000 <= _vpvarp_ext.second 
					&& _vpvarp_ext.second <= 400000)))
	
		{
		pvarp_ext = _vpvarp_ext;
		}
	}
	void PVARP_Ext :: chg_pvarp_ext (const pair<SWITCH, int> a)
	{
		if((a.first==OFF && a.second==0)
			|| (a.first==ON && 
					(50000 <= a.second 
					&& a.second <= 400000)))
	
		{
		pvarp_ext = a;
		}
	}
	PVARP_Ext :: PVARP_Ext () 
	{
		pvarp_ext = make_pair<SWITCH, int>(OFF,0);	
	}

//Class
	HysteresisRateLimit :: HysteresisRateLimit (const pair<SWITCH, int> _vhysteresis_rate_limit)
	{
		if((_vhysteresis_rate_limit.first==OFF && _vhysteresis_rate_limit.second==0)
			|| (_vhysteresis_rate_limit.first==ON && 
					(30 <= _vhysteresis_rate_limit.second 
					&& _vhysteresis_rate_limit.second <= 175)))
	
		{
		hysteresis_rate_limit = _vhysteresis_rate_limit;
		}
	}
	void HysteresisRateLimit :: chg_hysteresis_rate_limit (const pair<SWITCH, int> a)
	{
		if((a.first==OFF && a.second==0)
			|| (a.first==ON && 
					(30 <= a.second 
					&& a.second <= 175)))
	
		{
		hysteresis_rate_limit = a;
		}
	}
	HysteresisRateLimit :: HysteresisRateLimit () 
	{
		hysteresis_rate_limit = make_pair<SWITCH, int>(OFF,0);;	
	}

//Class
	RateSmoothing :: RateSmoothing (const pair<SWITCH, int> _vrate_smoothing)
	{
		if((_vrate_smoothing.first==OFF && _vrate_smoothing.second==0)
			|| (_vrate_smoothing.first==ON && 
					(_vrate_smoothing.second == 3
					|| _vrate_smoothing.second == 6
					|| _vrate_smoothing.second == 9
					|| _vrate_smoothing.second == 12
					|| _vrate_smoothing.second == 15
					|| _vrate_smoothing.second == 18
					|| _vrate_smoothing.second == 21
					|| _vrate_smoothing.second == 25)))
	
		{
		rate_smoothing = _vrate_smoothing;
		}
	}
	void RateSmoothing :: chg_rate_smoothing (const pair<SWITCH, int> a)
	{
		if((a.first==OFF && a.second==0)
			|| (a.first==ON && 
					(a.second == 3
					|| a.second == 6
					|| a.second == 9
					|| a.second == 12
					|| a.second == 15
					|| a.second == 18
					|| a.second == 21
					|| a.second == 25)))
	
		{
		rate_smoothing = a;
		}
	}
	RateSmoothing :: RateSmoothing () {}

//Class
	ATRMode :: ATRMode (const SWITCH _vatr_mode)
	{
		atr_mode = _vatr_mode;
	}
	void ATRMode :: chg_atr_mode (const SWITCH x)
	{
		atr_mode = x;
	}
	ATRMode :: ATRMode () 
	{
		atr_mode = OFF;
	}

//Class
	ATRDuration :: ATRDuration (const int _vatr_duration)
	{
		if(_vatr_duration == 10 ||
			checkRange(_vatr_duration,20,80) || 
			checkRange(_vatr_duration,100,2000))
		atr_duration = _vatr_duration;
	}
	void ATRDuration :: chg_atr_duration (const int x)
	{
		if(x == 10 ||
			checkRange(x,20,80) || 
			checkRange(x,100,2000))
		atr_duration = x;
	}
	ATRDuration :: ATRDuration () 
	{
		atr_duration = 20;	
	}

//Class
	ATRFallbackTime :: ATRFallbackTime (const int _vatr_fallback_time)
	{
		if(_vatr_fallback_time==1||
			_vatr_fallback_time==2||
			_vatr_fallback_time==3||
			_vatr_fallback_time==4||
			_vatr_fallback_time==5)
		atr_fallback_time = _vatr_fallback_time;
	}
	void ATRFallbackTime :: chg_atr_fallback_time (const int x)
	{
	if(x==1 || x==2 || x==3 || x==4 || x==5)
		atr_fallback_time = x;
	}
	ATRFallbackTime :: ATRFallbackTime () 
	{
		atr_fallback_time = 1;	
	}

//Class
	VBlanking :: VBlanking (const int _vv_blanking)
	{
		if(checkRange(_vv_blanking,30000,60000))
		v_blanking = _vv_blanking;
	}
	void VBlanking :: chg_v_blanking (const int x)
	{
		if(checkRange(x,30000,60000))
		v_blanking = x;
	}
	VBlanking :: VBlanking () 
	{
		v_blanking = 40000;	
	}

//Class
	ActivityThreshold :: ActivityThreshold (const ACTIVITY_THRESHOLD _vact_threshold)
	{
		act_threshold = _vact_threshold;
	}
	void ActivityThreshold :: chg_act_threshold (const ACTIVITY_THRESHOLD x)
	{
		act_threshold = x;
	}
	ActivityThreshold :: ActivityThreshold () 
	{
		act_threshold = MED;	
	}

//Class
	ReactTime :: ReactTime (const int _vreaction_time)
	{
		if(checkRange(_vreaction_time,10000,50000))

		reaction_time = _vreaction_time;
	}
	void ReactTime :: chg_reaction_time (const int x)
	{
		if(checkRange(x,10000,50000))
		reaction_time = x;
	}
	ReactTime :: ReactTime () 
	{
	reaction_time = 30000;
	}

//Class
	RespFactor :: RespFactor (const int _vresponse_factor)
	{
		if(checkRange(_vresponse_factor,1,16))
		response_factor = _vresponse_factor;
	}
	void RespFactor :: chg_response_factor (const int x)
	{
		if(checkRange(x,1,16))
		response_factor = x;
	}
	RespFactor :: RespFactor () 
	{
	response_factor = 8;
	}

//Class
	RecoveryTime :: RecoveryTime (const int _vrecovery_time)
	{
		if(checkRange(_vrecovery_time,120000,960000))

		recovery_time = _vrecovery_time;
	}
	void RecoveryTime :: chg_recovery_time (const int x)
	{
		if(checkRange(x,120000,960000))
		recovery_time = x;
	}
	RecoveryTime :: RecoveryTime () 
	{
		recovery_time = 300;
	}

//Class
	ProgrammableParameters :: ProgrammableParameters (const LRL _vc_LRL,
		const URL _vc_URL,
		const MaxSensorRate _vc_MaxSensorRate,
		const FixedAVDelay _vc_FixedAVDelay,
		const DynamicAVDelay _vc_DynamicAVDelay,
		const MinDynamicAVDelay _vc_MinDynamicAVDelay,
		const SensedAVDelayOffset _vc_SensedAVDelayOffset,
		const APulseAmpRegulated _vc_APulseAmpRegulated,
		const VPulseAmpRegulated _vc_VPulseAmpRegulated,
		const APulseAmpUnregulated _vc_APulseAmpUnregulated,
		const VPulseAmpUnregulated _vc_VPulseAmpUnregulated,
		const APulseWidth _vc_APulseWidth,
		const VPulseWidth _vc_VPulseWidth,
		const ASensitivity _vc_ASensitivity,
		const VSensitivity _vc_VSensitivity,
		const VRefractoryPeriod _vc_VRefractoryPeriod,
		const ARefractoryPeriod _vc_ARefractoryPeriod,
		const PVARP _vc_PVARP,
		const PVARP_Ext _vc_PVARP_Ext,
		const HysteresisRateLimit _vc_HysteresisRateLimit,
		const RateSmoothing _vc_RateSmoothing,
		const ATRMode _vc_ATRMode,
		const ATRDuration _vc_ATRDuration,
		const ATRFallbackTime _vc_ATRFallbackTime,
		const VBlanking _vc_VBlanking,
		const ActivityThreshold _vc_ActivityThreshold,
		const ReactTime _vc_ReactTime,
		const RespFactor _vc_RespFactor,
		const RecoveryTime _vc_RecoveryTime): 
		c_LRL(_vc_LRL), 
		c_URL(_vc_URL), 
		c_MaxSensorRate(_vc_MaxSensorRate),
		c_FixedAVDelay(_vc_FixedAVDelay),
		c_DynamicAVDelay(_vc_DynamicAVDelay),
		c_MinDynamicAVDelay(_vc_MinDynamicAVDelay),
		c_SensedAVDelayOffset(_vc_SensedAVDelayOffset), 
		c_APulseAmpRegulated(_vc_APulseAmpRegulated), 
		c_VPulseAmpRegulated(_vc_VPulseAmpRegulated),
		c_APulseAmpUnregulated(_vc_APulseAmpUnregulated),
		c_VPulseAmpUnregulated(_vc_VPulseAmpUnregulated),
		c_APulseWidth(_vc_APulseWidth),
		c_VPulseWidth(_vc_VPulseWidth),
		c_ASensitivity(_vc_ASensitivity),
		c_VSensitivity(_vc_VSensitivity),
		c_VRefractoryPeriod(_vc_VRefractoryPeriod),
		c_ARefractoryPeriod(_vc_ARefractoryPeriod),
		c_PVARP(_vc_PVARP),
		c_PVARP_Ext(_vc_PVARP_Ext),
		c_HysteresisRateLimit(_vc_HysteresisRateLimit),
		c_RateSmoothing(_vc_RateSmoothing),
		c_ATRMode(_vc_ATRMode),
		c_ATRDuration(_vc_ATRDuration),
		c_ATRFallbackTime(_vc_ATRFallbackTime),
		c_VBlanking(_vc_VBlanking),
		c_ActivityThreshold(_vc_ActivityThreshold),
		c_ReactTime(_vc_ReactTime),
		c_RespFactor(_vc_RespFactor),
		c_RecoveryTime(_vc_RecoveryTime)
	{
		
	}
	
	void ProgrammableParameters :: set_lower_rate_limit (const int x)
	{
		c_LRL.chg_lower_rate_limit (x);
	}
	void ProgrammableParameters :: set_upper_rate_limit (const int x)
	{
		c_URL.chg_upper_rate_limit (x);
	}
	void ProgrammableParameters :: set_max_sensor_rate (const int x)
	{
		c_MaxSensorRate.chg_max_sensor_rate (x);
	}
	void ProgrammableParameters :: set_fixed_av_delay (const int x)
	{
		c_FixedAVDelay.chg_fixed_av_delay (x);
	}
	void ProgrammableParameters :: set_dynamic_av_delay (const SWITCH x)
	{
		c_DynamicAVDelay.chg_dynamic_av_delay (x);
	}
	void ProgrammableParameters :: set_min_dyn_av_delay (const int x)
	{
		c_MinDynamicAVDelay.chg_min_dyn_av_delay (x);
	}
	void ProgrammableParameters :: set_sensed_av_delay_offset (const pair<SWITCH, int> a)
	{
		c_SensedAVDelayOffset.chg_sensed_av_delay_offset (a);
	}
	void ProgrammableParameters :: set_a_pulse_amp_regulated (const pair<SWITCH, int> a)
	{
		c_APulseAmpRegulated.chg_a_pulse_amp_regulated (a);
	}
	void ProgrammableParameters :: set_v_pulse_amp_regulated (const pair<SWITCH, int> a)
	{
		c_VPulseAmpRegulated.chg_v_pulse_amp_regulated (a);
	}
	void ProgrammableParameters :: set_a_pulse_amp_unregulated (const pair<SWITCH, int> a)
	{
		c_APulseAmpUnregulated.chg_a_pulse_amp_unregulated (a);
	}
	void ProgrammableParameters :: set_v_pulse_amp_unregulated (const pair<SWITCH, int> a)
	{
		c_VPulseAmpUnregulated.chg_v_pulse_amp_unregulated (a);
	}
	void ProgrammableParameters :: set_a_pulse_width (const int x)
	{
		c_APulseWidth.chg_a_pulse_width (x);
	}
	void ProgrammableParameters :: set_v_pulse_width (const int x)
	{
		c_VPulseWidth.chg_v_pulse_width (x);
	}
	void ProgrammableParameters :: set_a_sensitivity (const int x)
	{
		c_ASensitivity.chg_a_sensitivity (x);
	}
	void ProgrammableParameters :: set_v_sensitivity (const int x)
	{
		c_VSensitivity.chg_v_sensitivity (x);
	}
	void ProgrammableParameters :: set_v_refract_period (const int x)
	{
		c_VRefractoryPeriod.chg_v_refract_period (x);
	}
	void ProgrammableParameters :: set_a_refract_period (const int x)
	{
		c_ARefractoryPeriod.chg_a_refract_period (x);
	}
	void ProgrammableParameters :: set_pvarp (const int x)
	{
		c_PVARP.chg_pvarp (x);
	}
	void ProgrammableParameters :: set_pvarp_ext (const pair<SWITCH, int> a)
	{
		c_PVARP_Ext.chg_pvarp_ext (a);
	}
	void ProgrammableParameters :: set_hysteresis_rate_limit (const pair<SWITCH, int> a)
	{
		c_HysteresisRateLimit.chg_hysteresis_rate_limit (a);
	}
	void ProgrammableParameters :: set_rate_smoothing (const pair<SWITCH, int> a)
	{
		c_RateSmoothing.chg_rate_smoothing (a);
	}
	void ProgrammableParameters :: set_atr_mode (const SWITCH x)
	{
		c_ATRMode.chg_atr_mode (x);
	}
	void ProgrammableParameters :: set_atr_duration (const int x)
	{
		c_ATRDuration.chg_atr_duration (x);
	}
	void ProgrammableParameters :: set_atr_fallback_time (const int x)
	{
		c_ATRFallbackTime.chg_atr_fallback_time (x);
	}
	void ProgrammableParameters :: set_v_blanking (const int x)
	{
		c_VBlanking.chg_v_blanking (x);
	}
	void ProgrammableParameters :: set_act_threshold (const ACTIVITY_THRESHOLD x)
	{
		c_ActivityThreshold.chg_act_threshold (x);
	}
	void ProgrammableParameters :: set_reaction_time (const int x)
	{
		c_ReactTime.chg_reaction_time (x);
	}
	void ProgrammableParameters :: set_response_factor (const int x)
	{
		c_RespFactor.chg_response_factor (x);
	}
	void ProgrammableParameters :: set_recovery_time (const int x)
	{
		c_RecoveryTime.chg_recovery_time (x);
	}
		
	ProgrammableParameters :: ProgrammableParameters () {}

/*
	HistogramsSt :: HistogramsSt (const std::list<triple > _vatrial_paced,
		const std::list<triple > _vatrial_sensed,
		const std::list<triple > _vventricular_paced,
		const std::list<triple > _vventricular_sensed,
		const int _vpvc_event,
		const int _vatrial_tachy)
	{
		atrial_paced = _vatrial_paced;
		atrial_sensed = _vatrial_sensed;
		ventricular_paced = _vventricular_paced;
		ventricular_sensed = _vventricular_sensed;
		pvc_event = _vpvc_event;
		atrial_tachy = _vatrial_tachy;
	}
	void HistogramsSt :: chg_atrial_paced (const int _rmin, const int _rmax, const int qty)
	{
		if(_rmin > 0 && _rmax > 0)
		atrial_paced.push_back(triple(_rmin, _rmax, qty));
	}
	void HistogramsSt :: chg_atrial_sensed (const int _rmin,
		const int _rmax,
		const int qty)
	{
		if(_rmin > 0 && _rmax > 0)
		atrial_sensed.push_back(triple(_rmin, _rmax, qty));
	}
	void HistogramsSt :: chg_ventricular_paced (const int _rmin,
		const int _rmax,
		const int qty)
	{
		if(_rmin > 0 && _rmax > 0)
		ventricular_paced.push_back(triple(_rmin, _rmax, qty));
	}
	void HistogramsSt :: chg_ventricular_sensed (const int _rmin, const int _rmax, const int qty)
	{
		if(_rmin > 0 && _rmax > 0)
		ventricular_sensed.push_back(triple(_rmin, _rmax, qty));
	}
	void HistogramsSt :: chg_pvc_event (const int qty)
	{
		pvc_event = qty;
	}
	void HistogramsSt :: chg_atrial_tachy (const int qty)
	{
		atrial_tachy = qty;
	}
	
	HistogramsSt :: HistogramsSt () 
	{
	atrial_paced.clear();
	atrial_sensed.clear();
	ventricular_paced.clear();
	ventricular_sensed.clear();
		pvc_event = 0;
		atrial_tachy = 0;
	}*/

//Class
	BradycardiaSt :: BradycardiaSt (const BRADYCARDIA_STATE _vbradycardia_state)
	{
		bradycardia_state = _vbradycardia_state;
	}
	void BradycardiaSt :: chg_bradycardia_state (const BRADYCARDIA_STATE x)
	{
		bradycardia_state = x;
	}
	BradycardiaSt :: BradycardiaSt () 
	{
		bradycardia_state = PERMANENT;	
	}

//Class
	AtrialBipolarLead :: AtrialBipolarLead (const LEAD_CHAMBER_TYPE _va_lead_chamber, const POLARITY _va_lead_polarity)
	{
		a_lead_chamber = _va_lead_chamber;
		a_lead_polarity = _va_lead_polarity;
	}
	void AtrialBipolarLead :: chg_a_lead_chamber (const LEAD_CHAMBER_TYPE x)
	{
		a_lead_chamber = x;
	}
	void AtrialBipolarLead :: chg_a_lead_polarity (const POLARITY x)
	{
		a_lead_polarity = x;
	}
	
	AtrialBipolarLead :: AtrialBipolarLead () {
		a_lead_chamber = L_ATRIAL;
		a_lead_polarity	= BIPOLAR;
	}

//Class
	VentricularBipolarLead :: VentricularBipolarLead (const LEAD_CHAMBER_TYPE _vv_lead_chamber, const POLARITY _vv_lead_polarity)
	{
		v_lead_chamber = _vv_lead_chamber;
		v_lead_polarity = _vv_lead_polarity;
	}
	void VentricularBipolarLead :: chg_v_lead_chamber (const LEAD_CHAMBER_TYPE x)
	{
		v_lead_chamber = x;
	}
	void VentricularBipolarLead :: chg_v_lead_polarity (const POLARITY x)
	{
		v_lead_polarity = x;
	}
	
	VentricularBipolarLead :: VentricularBipolarLead () {
		v_lead_chamber = L_ATRIAL;
		v_lead_polarity	= BIPOLAR;	
	}

//Class
	LeadsSt :: LeadsSt (const AtrialBipolarLead _vc_AtrialBipolarLead, const VentricularBipolarLead _vc_VentricularBipolarLead)
	{
		c_AtrialBipolarLead = _vc_AtrialBipolarLead;
		c_VentricularBipolarLead = _vc_VentricularBipolarLead;
	}
	void LeadsSt :: chg_a_lead_chamber (const LEAD_CHAMBER_TYPE x)
	{
		c_AtrialBipolarLead.chg_a_lead_chamber (x);
	}
	void LeadsSt :: chg_a_lead_polarity (const POLARITY x)
	{
		c_AtrialBipolarLead.chg_a_lead_polarity (x);
	}
	void LeadsSt :: chg_v_lead_chamber (const LEAD_CHAMBER_TYPE x)
	{
		c_VentricularBipolarLead.chg_v_lead_chamber (x);
	}
	void LeadsSt :: chg_v_lead_polarity (const POLARITY x)
	{
		c_VentricularBipolarLead.chg_v_lead_polarity (x);
	}
	
	LeadsSt :: LeadsSt () {}

//int main(){return 0;}
// End of file.
