//***********************************************************************************************
//* File: 'Classes1.h'
//*
//* by Artur Oliveira Gomes - December 16th 2013
//***********************************************************************************************

#ifndef Classes1_hpp
#define Classes1_hpp

//#include <list> // INCLUDE LIST HERE
//using namespace std;

// Full declarations

	typedef enum {VERY_LOW, LOW, MED_LOW, MED, MED_HIGH, HIGH, VERY_HIGH} ACTIVITY_THRESHOLD;
	typedef enum {BOL, ERN, ERT, ERP} BATT_STATUS_LEVEL;
	typedef enum {PERMANENT, TEMPORARY, PACE_NOW} BRADYCARDIA_STATE;
	typedef enum {L_ATRIAL, L_VENTRICULAR} LEAD_CHAMBER_TYPE;
	typedef enum {UNIPOLAR, BIPOLAR} POLARITY;
	typedef enum {AS, AP, AT, VS, VP, PVC, TN, SRef, HyPc, SRPc, Sm_Up, Sm_Down, ATR_Dur, ATR_FB, ATR_End, PVPext} MarkerABB;
	typedef enum {ON, OFF} SWITCH;
	typedef enum {C_NONE, C_DUAL, C_ATRIUM, C_VENTRICLE} CHAMBERS;
	typedef enum {R_NONE, TRIGGERED, INHIBITED, TRACKED} RESPONSE;
 
 	class quintuple
	{
	public:
		quintuple(const MarkerABB t1, const int t2,const int t3,const int t4,const int t5):
			x(t1),
			y(t2),
			z(t3),
			k(t4),
			m(t5)
		{
		}
		MarkerABB x;
		int y;
		int z;
		int k;
		int m;
	};
 	
	class triple
	{
	public:
		triple(const int t1,const int t2,const int t3):
			x(t1),
			y(t2),
			z(t3)
		{
		}
		int x;
		int y;
		int z;
	};
	class pair
	{
	public:
		pair(const int t1,const int t2):
			x(t1),
			y(t2)
		{
		}
		int x;
		int y;
	};
	bool checkRange(int i, int min, int max);
	class ListPair
	{
	public:
		pair _list[100];
		int last;
	
		ListPair(){
		last = 0;
		}
		void insert(pair p){
			for(int i=99; i>=0; i--)
			{
			_list[i]=_list[i-1];
			}
			_list[0]=p;
		}
		pair getElement(int index){
		return _list[index];
		}	
	}
	class BOM{
	public:
		SWITCH _rswitch;
		CHAMBERS chambers_paced;
		CHAMBERS chambers_sensed;
		RESPONSE response_to_sensing;
		bool rate_modulation;
		bool inVOO () const;
		bool inAOO () const;
		bool inDOO () const;
		bool inDDI () const;
		bool inVVI () const;
		bool inVDD () const;
		bool inAAI () const;
		bool inVVT () const;
		bool inAAT () const;
		bool inAXXX () const;
		bool inVXXX () const;
		bool inDXXX () const;
		bool inOXO () const;
		BOM (const SWITCH, const CHAMBERS, const CHAMBERS, const RESPONSE, const bool);
		BOM();
	};
	class TimeSt{
	public:
		int time;
		int a_start_time;
		int a_max;
		int a_delay;
		int v_start_time;
		int v_max;
		int v_delay;
		int a_curr_measurement;
		int v_curr_measurement;
		TimeSt (const int, const int, const int, const int, const int, const int, const int, const int, const int);
		void chg_time (const int);
		void chg_a_start_time (const int);
		void chg_a_max (const int);
		void chg_a_delay (const int);
		void chg_v_start_time (const int);
		void chg_v_max (const int);
		void chg_v_delay (const int);
		void chg_v_curr_measurement (const int);
		void chg_a_curr_measurement (const int);
		TimeSt ();
	};
	class SensingPulse{
	public:
		int cardiac_cycles_length;
		int a_sensing_threshold;
		int v_sensing_threshold;
		int sensed_a_pulse_width;
		int sensed_v_pulse_width;
		int sensed_a_pulse_amp;
		int sensed_v_pulse_amp;
		int last_s_a_pulse;
		int last_s_v_pulse;
		int current_rate;
		SensingPulse (const int, const int, const int, const int, const int, const int,
			const int, const int, const int, const int);
		void chg_sensed_atrial (const int, const int, const int);
		void chg_sensed_ventricular (const int, const int, const int, const int);
		void chg_cardiac_cycles_length (const int);
		void chg_a_sensing_threshold (const int);
		void chg_v_sensing_threshold (const int);
		void chg_sensed_a_pulse_width (const int);
		void chg_sensed_v_pulse_width (const int);
		void chg_sensed_a_pulse_amp (const int);
		void chg_sensed_v_pulse_amp (const int);
		void chg_last_s_a_pulse (const int);
		void chg_last_s_v_pulse (const int);
		void chg_current_rate (const int);
		SensingPulse ();
	};
	class PacingPulse{
	public:
		int paced_a_pulse_width;
		int paced_v_pulse_width;
		int paced_a_pulse_amp;
		int paced_v_pulse_amp;
		int last_p_a_pulse;
		int last_p_v_pulse;
		PacingPulse (const int, const int, const int, const int, const int, const int);
		void chg_paced_atrial (const int, const int, const int);
		void chg_paced_ventricular (const int, const int, const int);
		void chg_paced_dual (const int, const int, const int, const int, const int);
		void chg_paced_a_pulse_width (const int);
		void chg_paced_v_pulse_width (const int);
		void chg_paced_a_pulse_amp (const int);
		void chg_paced_v_pulse_amp (const int);
		void chg_last_p_a_pulse (const int);
		void chg_last_p_v_pulse (const int);
		PacingPulse ();
	};
	class BatteryStatus{
	public:
		BATT_STATUS_LEVEL batt_status_level;
		explicit BatteryStatus (const BATT_STATUS_LEVEL);
		void chg_batt_status_level (const BATT_STATUS_LEVEL);
		BatteryStatus ();
	};
	class NoiseDetection{
	public:
		float noise;
		explicit NoiseDetection (const float);
		void chg_noise (const float);
		NoiseDetection ();
	};
	
	class EventMarkers{
	public:
		list<quintuple>  atrial_marker;
		std::list<quintuple >  ventricular_marker;
		std::list<std::pair< MarkerABB, int> >  augmentation_marker;
		EventMarkers (const std::list<quintuple >, 
		const std::list<quintuple >, 
		const std::list<pair< MarkerABB, int> > );
		void chg_atrial_marker (const MarkerABB, const int, const int, const int, const int);
		void chg_ventricular_marker (const MarkerABB, const int, const int, const int, const int);
		void chg_dual_marker (const MarkerABB, const int, const int, const MarkerABB, const int, const int, const int, const int);
		void chg_augmentation_marker (const MarkerABB, const int);
		void unchg_dual_marker ();
		void unchg_atrial_marker ();
		void unchg_ventricular_marker ();
		void unchg_augmentation_marker ();
		EventMarkers ();
	};
	class Accelerometer{
	public:
		int accel;
		explicit Accelerometer (const int);
		void chg_accel (const int);
		Accelerometer ();
	};
	class PWave{
	public:
		int p_wave;
		explicit PWave (const int);
		void chg_p_wave (const int);
		PWave ();
	};
	class RWave{
	public:
		int r_wave;
		explicit RWave (const int);
		void chg_r_wave (const int);
		RWave ();
	};
	class BatteryVoltage{
	public:
		int battery_voltage;
		explicit BatteryVoltage (const int);
		void chg_battery_voltage (const int);
		BatteryVoltage ();
	};
	class LeadImpedance{
	public:
		int lead_impedance;
		explicit LeadImpedance (const int);
		void chg_lead_impedance (const int);
		LeadImpedance ();
	};
	class MeasuredParameters{
	public:
		LeadImpedance c_LeadImpedance;
		BatteryVoltage c_BatteryVoltage;
		RWave c_RWave;
		PWave c_PWave;
		MeasuredParameters (const LeadImpedance, const BatteryVoltage, const RWave, const PWave);
		void set_p_wave (const int);
		void set_r_wave (const int);
		void set_lead_impedance (const int);
		void set_battery_voltage (const int);
		MeasuredParameters ();
	};
	class BO_MODE{
	public:
		BOM bo_mode;
		bool isVOO () const;
		bool isAOO () const;
		bool isDOO () const;
		bool isDDI () const;
		bool isVVI () const;
		bool isAAI () const;
		bool isVVT () const;
		bool isAAT () const;
		bool isVDD () const;
		explicit BO_MODE (const BOM);
		void chg_bo_mode (const BOM);
		BO_MODE ();
	};
	class LRL{
	public:
		int lower_rate_limit;
		explicit LRL (const int);
		void chg_lower_rate_limit (const int);
		LRL ();
	};
	class URL{
	public:
		int upper_rate_limit;
		explicit URL (const int);
		void chg_upper_rate_limit (const int);
		URL ();
	};
	class MaxSensorRate{
	public:
		int max_sensor_rate;
		explicit MaxSensorRate (const int);
		void chg_max_sensor_rate (const int);
		MaxSensorRate ();
	};
	class FixedAVDelay{
	public:
		int fixed_av_delay;
		explicit FixedAVDelay (const int);
		void chg_fixed_av_delay (const int);
		FixedAVDelay ();
	};
	class DynamicAVDelay{
	public:
		SWITCH dynamic_av_delay;
		explicit DynamicAVDelay (const SWITCH);
		void chg_dynamic_av_delay (const SWITCH);
		DynamicAVDelay ();
	};
	class MinDynamicAVDelay{
	public:
		int min_dyn_av_delay;
		explicit MinDynamicAVDelay (const int);
		void chg_min_dyn_av_delay (const int);
		MinDynamicAVDelay ();
	};
	class SensedAVDelayOffset{
	public:
		pair< SWITCH, int>sensed_av_delay_offset;
		explicit SensedAVDelayOffset (const pair< SWITCH, int >);
		void chg_sensed_av_delay_offset (const pair< SWITCH, int >);
		SensedAVDelayOffset ();
	};
	class APulseAmpRegulated{
	public:
		pair< SWITCH, int>a_pulse_amp_regulated;
		explicit APulseAmpRegulated (const pair< SWITCH, int >);
		void chg_a_pulse_amp_regulated (const pair< SWITCH, int >);
		APulseAmpRegulated ();
	};
	class VPulseAmpRegulated{
	public:
		pair< SWITCH, int>v_pulse_amp_regulated;
		explicit VPulseAmpRegulated (const pair< SWITCH, int >);
		void chg_v_pulse_amp_regulated (const pair< SWITCH, int >);
		VPulseAmpRegulated ();
	};
	class APulseAmpUnregulated{
	public:
		pair< SWITCH, int>a_pulse_amp_unregulated;
		explicit APulseAmpUnregulated (const pair< SWITCH, int >);
		void chg_a_pulse_amp_unregulated (const pair< SWITCH, int >);
		APulseAmpUnregulated ();
	};
	class VPulseAmpUnregulated{
	public:
		pair< SWITCH, int>v_pulse_amp_unregulated;
		explicit VPulseAmpUnregulated (const pair< SWITCH, int >);
		void chg_v_pulse_amp_unregulated (const pair< SWITCH, int >);
		VPulseAmpUnregulated ();
	};
	class APulseWidth{
	public:
		int a_pulse_width;
		explicit APulseWidth (const int);
		void chg_a_pulse_width (const int);
		APulseWidth ();
	};
	class VPulseWidth{
	public:
		int v_pulse_width;
		explicit VPulseWidth (const int);
		void chg_v_pulse_width (const int);
		VPulseWidth ();
	};
	class ASensitivity{
	public:
		int a_sensitivity;
		explicit ASensitivity (const int);
		void chg_a_sensitivity (const int);
		ASensitivity ();
	};
	class VSensitivity{
	public:
		int v_sensitivity;
		explicit VSensitivity (const int);
		void chg_v_sensitivity (const int);
		VSensitivity ();
	};
	class VRefractoryPeriod{
	public:
		int v_refract_period;
		explicit VRefractoryPeriod (const int);
		void chg_v_refract_period (const int);
		VRefractoryPeriod ();
	};
	class ARefractoryPeriod{
	public:
		int a_refract_period;
		explicit ARefractoryPeriod (const int);
		void chg_a_refract_period (const int);
		ARefractoryPeriod ();
	};
	class PVARP{
	public:
		int pvarp;
		explicit PVARP (const int);
		void chg_pvarp (const int);
		PVARP ();
	};
	class PVARP_Ext{
	public:
		pair< SWITCH, int>pvarp_ext;
		explicit PVARP_Ext (const pair< SWITCH, int >);
		void chg_pvarp_ext (const pair< SWITCH, int >);
		PVARP_Ext ();
	};
	class HysteresisRateLimit{
	public:
		pair< SWITCH, int>hysteresis_rate_limit;
		explicit HysteresisRateLimit (const pair< SWITCH, int >);
		void chg_hysteresis_rate_limit (const pair< SWITCH, int >);
		HysteresisRateLimit ();
	};
	class RateSmoothing{
	public:
		pair< SWITCH, int>rate_smoothing;
		explicit RateSmoothing (const pair< SWITCH, int >);
		void chg_rate_smoothing (const pair< SWITCH, int >);
		RateSmoothing ();
	};
	class ATRMode{
	public:
		SWITCH atr_mode;
		explicit ATRMode (const SWITCH);
		void chg_atr_mode (const SWITCH);
		ATRMode ();
	};
	class ATRDuration{
	public:
		int atr_duration;
		explicit ATRDuration (const int);
		void chg_atr_duration (const int);
		ATRDuration ();
	};
	class ATRFallbackTime{
	public:
		int atr_fallback_time;
		explicit ATRFallbackTime (const int);
		void chg_atr_fallback_time (const int);
		ATRFallbackTime ();
	};
	class VBlanking{
	public:
		int v_blanking;
		explicit VBlanking (const int);
		void chg_v_blanking (const int);
		VBlanking ();
	};
	class ActivityThreshold{
	public:
		ACTIVITY_THRESHOLD act_threshold;
		explicit ActivityThreshold (const ACTIVITY_THRESHOLD);
		void chg_act_threshold (const ACTIVITY_THRESHOLD);
		ActivityThreshold ();
	};
	class ReactTime{
	public:
		int reaction_time;
		explicit ReactTime (const int);
		void chg_reaction_time (const int);
		ReactTime ();
	};
	class RespFactor{
	public:
		int response_factor;
		explicit RespFactor (const int);
		void chg_response_factor (const int);
		RespFactor ();
	};
	class RecoveryTime{
	public:
		int recovery_time;
		explicit RecoveryTime (const int);
		void chg_recovery_time (const int);
		RecoveryTime ();
	};
	class ProgrammableParameters{
	public:
		LRL c_LRL;
		URL c_URL;
		MaxSensorRate c_MaxSensorRate;
		FixedAVDelay c_FixedAVDelay;
		DynamicAVDelay c_DynamicAVDelay;
		MinDynamicAVDelay c_MinDynamicAVDelay;
		SensedAVDelayOffset c_SensedAVDelayOffset;
		APulseAmpRegulated c_APulseAmpRegulated;
		VPulseAmpRegulated c_VPulseAmpRegulated;
		APulseAmpUnregulated c_APulseAmpUnregulated;
		VPulseAmpUnregulated c_VPulseAmpUnregulated;
		APulseWidth c_APulseWidth;
		VPulseWidth c_VPulseWidth;
		ASensitivity c_ASensitivity;
		VSensitivity c_VSensitivity;
		VRefractoryPeriod c_VRefractoryPeriod;
		ARefractoryPeriod c_ARefractoryPeriod;
		PVARP c_PVARP;
		PVARP_Ext c_PVARP_Ext;
		HysteresisRateLimit c_HysteresisRateLimit;
		RateSmoothing c_RateSmoothing;
		ATRMode c_ATRMode;
		ATRDuration c_ATRDuration;
		ATRFallbackTime c_ATRFallbackTime;
		VBlanking c_VBlanking;
		ActivityThreshold c_ActivityThreshold;
		ReactTime c_ReactTime;
		RespFactor c_RespFactor;
		RecoveryTime c_RecoveryTime;
		explicit ProgrammableParameters (const LRL, 
			const URL, 
			const MaxSensorRate, 
			const FixedAVDelay, 
			const DynamicAVDelay, 
			const MinDynamicAVDelay, 
			const SensedAVDelayOffset, 
			const APulseAmpRegulated, 
			const VPulseAmpRegulated, 
			const APulseAmpUnregulated, 
			const VPulseAmpUnregulated, 
			const APulseWidth, 
			const VPulseWidth, 
			const ASensitivity, 
			const VSensitivity, 
			const VRefractoryPeriod, 
			const ARefractoryPeriod, 
			const PVARP, 
			const PVARP_Ext, 
			const HysteresisRateLimit, 
			const RateSmoothing, 
			const ATRMode, 
			const ATRDuration, 
			const ATRFallbackTime, 
			const VBlanking, 
			const ActivityThreshold, 
			const ReactTime, 
			const RespFactor, 
			const RecoveryTime);
		void set_lower_rate_limit (const int);
		void set_upper_rate_limit (const int);
		void set_max_sensor_rate (const int);
		void set_fixed_av_delay (const int);
		void set_dynamic_av_delay (const SWITCH);
		void set_min_dyn_av_delay (const int);
		void set_sensed_av_delay_offset (const pair< SWITCH, int >);
		void set_a_pulse_amp_regulated (const pair< SWITCH, int >);
		void set_v_pulse_amp_regulated (const pair< SWITCH, int >);
		void set_a_pulse_amp_unregulated (const pair< SWITCH, int >);
		void set_v_pulse_amp_unregulated (const pair< SWITCH, int >);
		void set_a_pulse_width (const int);
		void set_v_pulse_width (const int);
		void set_a_sensitivity (const int);
		void set_v_sensitivity (const int);
		void set_v_refract_period (const int);
		void set_a_refract_period (const int);
		void set_pvarp (const int);
		void set_pvarp_ext (const pair< SWITCH, int >);
		void set_hysteresis_rate_limit (const pair< SWITCH, int >);
		void set_rate_smoothing (const pair< SWITCH, int >);
		void set_atr_mode (const SWITCH);
		void set_atr_duration (const int);
		void set_atr_fallback_time (const int);
		void set_v_blanking (const int);
		void set_act_threshold (const ACTIVITY_THRESHOLD);
		void set_reaction_time (const int);
		void set_response_factor (const int);
		void set_recovery_time (const int);
		ProgrammableParameters ();
	};
	
	class HistogramsSt{
	public:
		std::list<triple >  atrial_paced;
		std::list<triple >  atrial_sensed;
		std::list<triple >  ventricular_paced;
		std::list<triple >  ventricular_sensed;
		int pvc_event;
		int atrial_tachy;
		HistogramsSt (const std::list<triple >, 
			const std::list<triple >, 
			const std::list<triple >, 
			const std::list<triple >, 
			const int, 
			const int);
		void chg_atrial_paced (const int, const int, const int);
		void chg_atrial_sensed (const int, const int, const int);
		void chg_ventricular_paced (const int, const int, const int);
		void chg_ventricular_sensed (const int, const int, const int);
		void chg_pvc_event (const int);
		void chg_atrial_tachy (const int);
		HistogramsSt ();
	};
	class BradycardiaSt{
	public:
		BRADYCARDIA_STATE bradycardia_state;
		explicit BradycardiaSt (const BRADYCARDIA_STATE);
		void chg_bradycardia_state (const BRADYCARDIA_STATE);
		BradycardiaSt ();
	};
	class AtrialBipolarLead{
	public:
		LEAD_CHAMBER_TYPE a_lead_chamber;
		POLARITY a_lead_polarity;
		AtrialBipolarLead (const LEAD_CHAMBER_TYPE, const POLARITY);
		void chg_a_lead_chamber (const LEAD_CHAMBER_TYPE);
		void chg_a_lead_polarity (const POLARITY);
		AtrialBipolarLead ();
	};
	class VentricularBipolarLead{
	public:
		LEAD_CHAMBER_TYPE v_lead_chamber;
		POLARITY v_lead_polarity;
		VentricularBipolarLead (const LEAD_CHAMBER_TYPE, const POLARITY);
		void chg_v_lead_chamber (const LEAD_CHAMBER_TYPE);
		void chg_v_lead_polarity (const POLARITY);
		VentricularBipolarLead ();
	};
	class LeadsSt{
	public:
		AtrialBipolarLead c_AtrialBipolarLead;
		VentricularBipolarLead c_VentricularBipolarLead;
		LeadsSt (const AtrialBipolarLead, const VentricularBipolarLead);
		void chg_a_lead_chamber (const LEAD_CHAMBER_TYPE);
		void chg_a_lead_polarity (const POLARITY);
		void chg_v_lead_chamber (const LEAD_CHAMBER_TYPE);
		void chg_v_lead_polarity (const POLARITY);
		LeadsSt ();
	};

#endif

// End of file.
