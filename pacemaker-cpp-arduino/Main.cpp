//***********************************************************************************************
//* File: 'Main.cpp'
//*
//* by Artur Oliveira Gomes - December 16th 2013
//***********************************************************************************************



//#include <iterator>
//#include <pnew.cpp>
//#include <list>
#include "stdlib.h"
// File inclusions for forward declarations

//#include "Main0.h"

// File inclusions for full declarations

#include "Main1.h"

// File inclusions for inline code
//#include "Classes0.h"
//#include "Classes1.h"

//#include "Main2.h"

//using namespace std;
		int PulseGenerator :: _nz_PulseWidth (const int x) const
		{
			if(0<x) return 60000/x;
			else return 0;
		}
		int PulseGenerator :: _nz_PulsePerMinute (const int x) const
		{
			if(0<x) return 60000/x;
			else return 0;
		}
		//function countAP(min: int, max: int): nat
		//^= #(those i::c_EventMarkers.atrial_marker :- (MarkerABB AP = i.x & i.y > 0 &
		//	min <= PulsePerMinute(i.y) & max >= PulsePerMinute(i.y)));
		
		int PulseGenerator :: countAP (const int _rmin, const int _rmax) const
		{
			int counter = 0;
			for (list<quintuple >::const_iterator i=c_EventMarkers.ventricular_marker.begin(); i != c_EventMarkers.ventricular_marker.end(); ++i) {
    			//cout << *iterator;
    			if((*i).x==AP 
    				&& (*i).y > 0 
    				&& _rmin <= _nz_PulsePerMinute((*i).y) 
    				&& _rmax >= _nz_PulsePerMinute((*i).y))
    				counter++;
			}
			return counter;
		}
		int PulseGenerator :: countAS (const int _rmin, const int _rmax) const
		{
			int counter = 0;
			for (list<quintuple >::const_iterator i=c_EventMarkers.ventricular_marker.begin(); i != c_EventMarkers.ventricular_marker.end(); ++i) {
    			//cout << *iterator;
    			if((*i).x==AS
    				&& (*i).y > 0 
    				&& _rmin <= _nz_PulsePerMinute((*i).y) 
    				&& _rmax >= _nz_PulsePerMinute((*i).y))
    				counter++;
			}
			return counter;
		}
		int PulseGenerator :: countVP (const int _rmin, const int _rmax) const
		{
			int counter = 0;
			for (list<quintuple >::const_iterator i=c_EventMarkers.ventricular_marker.begin(); i != c_EventMarkers.ventricular_marker.end(); ++i) {
    			//cout << *iterator;
    			if((*i).x==VP
    				&& (*i).y > 0 
    				&& _rmin <= _nz_PulsePerMinute((*i).y) 
    				&& _rmax >= _nz_PulsePerMinute((*i).y))
    				counter++;
			}
			return counter;
		}
		int PulseGenerator :: countVS (const int _rmin, const int _rmax) const
		{
			int counter = 0;
			for (list<quintuple >::const_iterator i=c_EventMarkers.ventricular_marker.begin(); i != c_EventMarkers.ventricular_marker.end(); ++i) {
    			//cout << *iterator;
    			if((*i).x==VS
    				&& (*i).y > 0 
    				&& _rmin <= _nz_PulsePerMinute((*i).y) 
    				&& _rmax >= _nz_PulsePerMinute((*i).y))
    				counter++;
			}
			return counter;
		}
		int PulseGenerator :: countPVC () const
		{
				int counter = 0;
			for (list<quintuple >::const_iterator i=c_EventMarkers.ventricular_marker.begin(); i != c_EventMarkers.ventricular_marker.end(); ++i) {
    			//cout << *iterator;
    			if((*i).x==PVC)
    				counter++;
			}
			return counter;
		}
		int PulseGenerator :: countAT () const
		{
				int counter = 0;
			for (list<quintuple >::const_iterator i=c_EventMarkers.atrial_marker.begin(); i != c_EventMarkers.atrial_marker.end(); ++i) {
    			//cout << *iterator;
    			if((*i).x==AT)
    				counter++;
			}
			return counter;
		}
		PulseGenerator :: PulseGenerator (const ProgrammableParameters _vc_ProgrammableParameters,
			const MeasuredParameters _vc_MeasuredParameters,
			const BO_MODE _vc_BO_MODE,
			const TimeSt _vc_TimeSt,
			const SensingPulse _vc_SensingPulse,
			const PacingPulse _vc_PacingPulse,
			const BatteryStatus _vc_BatteryStatus,
			const NoiseDetection _vc_NoiseDetection,
			const EventMarkers _vc_EventMarkers,
			const Accelerometer _vc_Accelerometer,
			const HistogramsSt _vc_HistogramsSt) : 
				c_ProgrammableParameters (_vc_ProgrammableParameters), 
				c_MeasuredParameters (_vc_MeasuredParameters), 
				c_BO_MODE (_vc_BO_MODE), 
				c_TimeSt (_vc_TimeSt), 
				c_SensingPulse (_vc_SensingPulse), 
				c_PacingPulse (_vc_PacingPulse), 
				c_BatteryStatus (_vc_BatteryStatus),
				c_NoiseDetection (_vc_NoiseDetection), 
				c_EventMarkers (_vc_EventMarkers), 
				c_Accelerometer ( _vc_Accelerometer), 
				c_HistogramsSt (_vc_HistogramsSt)
		{
		}
		bool PulseGenerator :: preSetVOO () const
		{
			list<quintuple >::const_iterator i = c_EventMarkers.atrial_marker.begin();
				advance(i, (c_EventMarkers.atrial_marker.size () - 1));
				
				return (((((((true == c_BO_MODE.isVOO ()) 
						&& (0 < c_ProgrammableParameters.c_URL.upper_rate_limit)) 
						&& (0 < c_ProgrammableParameters.c_LRL.lower_rate_limit))				
						&& ((((BOL == c_BatteryStatus.batt_status_level) 
						|| ( ERN == c_BatteryStatus.batt_status_level)) 
						|| (ERT == c_BatteryStatus.batt_status_level)) 
						|| (ERP == c_BatteryStatus.batt_status_level))) 
						&& ((c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time)) 
						&& (_nz_PulseWidth (c_ProgrammableParameters.c_LRL.lower_rate_limit) <= (c_TimeSt.time - c_PacingPulse.last_p_v_pulse))) 
						&& (c_EventMarkers.ventricular_marker.empty () 
						|| ((*i).m < c_TimeSt.time)));
		}
		void PulseGenerator :: _nz_SetVOO ()
		{
			if(preSetVOO()){
				c_PacingPulse.chg_paced_ventricular (c_ProgrammableParameters.c_VPulseAmpRegulated.v_pulse_amp_regulated.second, c_ProgrammableParameters.c_VPulseWidth.v_pulse_width, c_TimeSt.time);
				c_EventMarkers.chg_ventricular_marker (VP, c_ProgrammableParameters.c_VPulseAmpRegulated.v_pulse_amp_regulated.second, c_ProgrammableParameters.c_VPulseWidth.v_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
			}
		}
		bool PulseGenerator :: preSetAOO () const
		{
			list<quintuple >::const_iterator i = c_EventMarkers.atrial_marker.begin();
				advance(i, (c_EventMarkers.atrial_marker.size () - 1));
		
				return (((((((true == c_BO_MODE.isAOO ()) 
						&& (0 < c_ProgrammableParameters.c_URL.upper_rate_limit)) 
						&& (0 < c_ProgrammableParameters.c_LRL.lower_rate_limit))
						&& ((((BOL == c_BatteryStatus.batt_status_level) 
						|| ( ERN == c_BatteryStatus.batt_status_level)) 
						|| (ERT == c_BatteryStatus.batt_status_level)) 
						|| (ERP == c_BatteryStatus.batt_status_level))) 
						&& ((c_PacingPulse.last_p_a_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time)) 
						&& (_nz_PulseWidth (c_ProgrammableParameters.c_LRL.lower_rate_limit) <= (c_TimeSt.time - c_PacingPulse.last_p_a_pulse))) 
						&& (c_EventMarkers.atrial_marker.empty () 
						|| ((*i).m < c_TimeSt.time)));
		}
		void PulseGenerator :: _nz_SetAOO ()
		{
			if(preSetAOO()){
				c_PacingPulse.chg_paced_atrial (c_ProgrammableParameters.c_APulseAmpRegulated.a_pulse_amp_regulated.second, c_ProgrammableParameters.c_APulseWidth.a_pulse_width, c_TimeSt.time);
				c_EventMarkers.chg_atrial_marker (AP, c_ProgrammableParameters.c_APulseAmpRegulated.a_pulse_amp_regulated.second, c_ProgrammableParameters.c_APulseWidth.a_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
			}
		}
		bool PulseGenerator :: preSetDOO () const
		{
			list<quintuple >::const_iterator a = c_EventMarkers.atrial_marker.begin();
				advance(a, (c_EventMarkers.atrial_marker.size () - 1));
		
			list<quintuple >::const_iterator v = c_EventMarkers.ventricular_marker.begin();
				advance(v, (c_EventMarkers.ventricular_marker.size () - 1));
		
			
				return (((((((true == c_BO_MODE.isDOO ()) 
						&& (0 < c_ProgrammableParameters.c_URL.upper_rate_limit)) 
						&& (0 < c_ProgrammableParameters.c_LRL.lower_rate_limit))
						
						&& ((BOL == c_BatteryStatus.batt_status_level) 
						|| ( ERN == c_BatteryStatus.batt_status_level))) 
						&& (((c_PacingPulse.last_p_a_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time) 
						&& (_nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit) <= ( c_TimeSt.time - c_PacingPulse.last_p_a_pulse)))) 
						&& (c_EventMarkers.ventricular_marker.empty () 
						|| ((*v).m < c_TimeSt.time))) 
						&& (c_EventMarkers.atrial_marker.empty () 
						|| ((*a).m < c_TimeSt.time)));
		}
		void PulseGenerator :: _nz_SetDOO ()
		{
			if(preSetDOO()){
				c_PacingPulse.chg_paced_dual (c_ProgrammableParameters.c_APulseWidth.a_pulse_width, c_ProgrammableParameters.c_APulseAmpRegulated.a_pulse_amp_regulated.second, c_ProgrammableParameters.c_VPulseWidth.v_pulse_width, c_ProgrammableParameters.c_VPulseAmpRegulated.v_pulse_amp_regulated.second, c_TimeSt.time);
				c_EventMarkers.chg_dual_marker (AP, c_ProgrammableParameters.c_APulseAmpRegulated.a_pulse_amp_regulated.second, c_ProgrammableParameters.c_APulseWidth.a_pulse_width, VP, c_ProgrammableParameters.c_VPulseAmpRegulated.v_pulse_amp_regulated.second, c_ProgrammableParameters.c_VPulseWidth.v_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
			}
		}
		bool PulseGenerator :: preSetDDI () const
		{
			list<quintuple >::const_iterator a = c_EventMarkers.atrial_marker.begin();
				advance(a, (c_EventMarkers.atrial_marker.size () - 1));
		
			list<quintuple >::const_iterator v = c_EventMarkers.ventricular_marker.begin();
				advance(v, (c_EventMarkers.ventricular_marker.size () - 1));
		
				return (((((true == c_BO_MODE.isDDI ()) 
						&& ((BOL == c_BatteryStatus.batt_status_level) 
						|| (ERN == c_BatteryStatus.batt_status_level)))					
						&& (((((((_nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit) - c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) + c_PacingPulse.last_p_v_pulse) == (c_TimeSt.time - c_PacingPulse.last_p_v_pulse)) 
						|| (((c_PacingPulse.last_p_v_pulse + c_ProgrammableParameters.c_PVARP.pvarp) < c_SensingPulse.last_s_a_pulse) 
						&& ( c_SensingPulse.last_s_a_pulse < ((_nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit) - c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) + c_PacingPulse.last_p_v_pulse)))) 
						|| ((((c_PacingPulse.last_p_v_pulse + c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) == c_PacingPulse.last_p_a_pulse)			
						&& (c_PacingPulse.last_p_v_pulse < c_SensingPulse.last_s_v_pulse)) 
						&& (c_SensingPulse.last_s_v_pulse < (c_PacingPulse.last_p_v_pulse + _nz_PulseWidth ( c_ProgrammableParameters.c_URL.upper_rate_limit))))) 
						|| (((((_nz_PulseWidth ( c_ProgrammableParameters.c_URL.upper_rate_limit) - c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) + c_PacingPulse.last_p_v_pulse) == c_PacingPulse.last_p_a_pulse) 
						&& (c_SensingPulse.last_s_v_pulse < c_PacingPulse.last_p_v_pulse)) 
						&& (( c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time))) 
						|| (((((((c_PacingPulse.last_p_v_pulse + c_ProgrammableParameters.c_PVARP.pvarp) < c_SensingPulse.last_s_a_pulse) 
						&& ( c_SensingPulse.last_s_a_pulse < ((_nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit) - c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) + c_PacingPulse.last_p_v_pulse))) 
						&& (c_PacingPulse.last_p_v_pulse < c_SensingPulse.last_s_v_pulse)) 
						&& (c_SensingPulse.last_s_v_pulse < (c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)))) 
						&& (0 < c_ProgrammableParameters.c_URL.upper_rate_limit)) 
						&& (0 < c_ProgrammableParameters.c_LRL.lower_rate_limit)))) 
						&& (c_EventMarkers.ventricular_marker.empty () 
						|| ((*v).m < c_TimeSt.time))) 
						&& (c_EventMarkers.atrial_marker.empty () 
						|| ((*a).m < c_TimeSt.time)));
		}
		void PulseGenerator :: _nz_SetDDI ()
		{
			if(preSetDDI()){
				if ((((_nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit) - c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) + c_PacingPulse.last_p_v_pulse) == (c_TimeSt.time - c_PacingPulse.last_p_v_pulse)))
				{ 
					c_PacingPulse.chg_paced_atrial (c_ProgrammableParameters.c_APulseWidth.a_pulse_width, c_ProgrammableParameters.c_APulseAmpRegulated.a_pulse_amp_regulated.second, c_TimeSt.time); 
					c_EventMarkers.chg_atrial_marker (AP, c_ProgrammableParameters.c_APulseAmpRegulated.a_pulse_amp_regulated.second, c_ProgrammableParameters.c_APulseWidth.a_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
				}
				else if (((((c_PacingPulse.last_p_v_pulse + c_ProgrammableParameters.c_PVARP.pvarp) < c_SensingPulse.last_s_a_pulse) 
						&& (c_SensingPulse.last_s_a_pulse < ((_nz_PulseWidth ( c_ProgrammableParameters.c_URL.upper_rate_limit) - c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) + c_PacingPulse.last_p_v_pulse))) 
						&& (0 < c_ProgrammableParameters.c_URL.upper_rate_limit)))
				{ 
					c_EventMarkers.unchg_atrial_marker ();
				}
				else if ((((((c_PacingPulse.last_p_v_pulse + c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) == c_PacingPulse.last_p_a_pulse) 
						&& (c_PacingPulse.last_p_v_pulse < c_SensingPulse.last_s_v_pulse)) 
						&& (c_SensingPulse.last_s_v_pulse < (c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)))) 
						&& (0 < c_ProgrammableParameters.c_URL.upper_rate_limit)))
				{ 
					c_EventMarkers.unchg_ventricular_marker ();
				}
				else if ((((((((c_PacingPulse.last_p_v_pulse + c_ProgrammableParameters.c_PVARP.pvarp) < c_SensingPulse.last_s_a_pulse) 
						&& (c_SensingPulse.last_s_a_pulse < ((_nz_PulseWidth ( c_ProgrammableParameters.c_URL.upper_rate_limit) - c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) + c_PacingPulse.last_p_v_pulse))) 
						&& (c_PacingPulse.last_p_v_pulse < c_SensingPulse.last_s_v_pulse)) 
						&& (0 < c_ProgrammableParameters.c_URL.upper_rate_limit)) 
						&& (0 < c_ProgrammableParameters.c_LRL.lower_rate_limit)) 
						&& (c_SensingPulse.last_s_v_pulse < ( c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)))))
				{ c_EventMarkers.unchg_ventricular_marker ();
				}
				else if (((((((_nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit) - c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) + c_PacingPulse.last_p_v_pulse) == c_PacingPulse.last_p_a_pulse) 
						&& (c_SensingPulse.last_s_v_pulse < c_PacingPulse.last_p_v_pulse)) 
						&& ((c_PacingPulse.last_p_v_pulse + _nz_PulseWidth ( c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time)) 
						&& (0 < c_ProgrammableParameters.c_URL.upper_rate_limit)))
				{ c_PacingPulse.chg_paced_ventricular (c_ProgrammableParameters.c_VPulseWidth.v_pulse_width, c_ProgrammableParameters.c_VPulseAmpRegulated.v_pulse_amp_regulated.second, c_TimeSt.time); c_EventMarkers.chg_ventricular_marker (VP, c_ProgrammableParameters.c_VPulseAmpRegulated.v_pulse_amp_regulated.second, c_ProgrammableParameters.c_VPulseWidth.v_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
				}
				else
				{
				}
			}
		}
		bool PulseGenerator :: preSetVDD () const
		{
			list<quintuple >::const_iterator a = c_EventMarkers.atrial_marker.begin();
				advance(a, (c_EventMarkers.atrial_marker.size () - 1));
		
			list<quintuple >::const_iterator v = c_EventMarkers.ventricular_marker.begin();
				advance(v, (c_EventMarkers.ventricular_marker.size () - 1));
		
				return ((((((true == c_BO_MODE.isVDD ()) 
						&& (0 < c_ProgrammableParameters.c_URL.upper_rate_limit)) 
						&& ((BOL == c_BatteryStatus.batt_status_level)
						
						|| (ERN == c_BatteryStatus.batt_status_level))) 
						&& (((((((( c_SensingPulse.last_s_a_pulse < c_PacingPulse.last_p_v_pulse) 
						&& ((c_SensingPulse.last_s_a_pulse + c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) == c_TimeSt.time)) 
						&& (c_SensingPulse.last_s_v_pulse < c_TimeSt.time)) 
						|| (((c_SensingPulse.last_s_a_pulse < c_SensingPulse.last_s_v_pulse) 
						&& ((c_SensingPulse.last_s_a_pulse + c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay) == c_TimeSt.time)) 
						&& ( c_SensingPulse.last_s_v_pulse < c_TimeSt.time))) 
						|| ((c_SensingPulse.last_s_v_pulse < c_SensingPulse.last_s_a_pulse) 
						&& ((c_PacingPulse.last_p_v_pulse + _nz_PulseWidth ( c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time))) 
						|| ((c_PacingPulse.last_p_v_pulse < c_SensingPulse.last_s_a_pulse) 
						&& ((c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time))) 
						|| (((c_SensingPulse.last_s_a_pulse < c_PacingPulse.last_p_v_pulse) 
						&& ((c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time)) 
						&& (c_TimeSt.time < (c_PacingPulse.last_p_v_pulse + c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay)))) 
						|| (((c_SensingPulse.last_s_a_pulse < c_SensingPulse.last_s_v_pulse) 
						&& ((c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time)) 
						&& (c_TimeSt.time < (c_PacingPulse.last_p_v_pulse + c_ProgrammableParameters.c_FixedAVDelay.fixed_av_delay))))) 
						&& (c_EventMarkers.ventricular_marker.empty () 
						|| ((*v).m < c_TimeSt.time))) 
						&& (c_EventMarkers.atrial_marker.empty () 
						|| ((*a).m < c_TimeSt.time)));
		}
		void PulseGenerator :: _nz_SetVDD ()
		{
			if(preSetVDD()){
				c_PacingPulse.chg_paced_ventricular (c_ProgrammableParameters.c_VPulseWidth.v_pulse_width, c_ProgrammableParameters.c_VPulseAmpRegulated.v_pulse_amp_regulated.second, c_TimeSt.time);
				c_EventMarkers.chg_ventricular_marker (VP, c_ProgrammableParameters.c_VPulseAmpRegulated.v_pulse_amp_regulated.second, c_ProgrammableParameters.c_VPulseWidth.v_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
			}
		}
		bool PulseGenerator :: preSetVVI () const
		{
		list<quintuple >::const_iterator v = c_EventMarkers.ventricular_marker.begin();
				advance(v, (c_EventMarkers.ventricular_marker.size () - 1));
		
				return ((((((true == c_BO_MODE.isVVI ()) 
						&& (0 < c_ProgrammableParameters.c_URL.upper_rate_limit)) 
						&& (0 < c_ProgrammableParameters.c_HysteresisRateLimit.hysteresis_rate_limit.second)) 
						&& ((BOL == c_BatteryStatus.batt_status_level) 
						|| (ERN == c_BatteryStatus.batt_status_level)))				
						&& ((((((c_TimeSt.time - c_PacingPulse.last_p_v_pulse) < (c_TimeSt.time - c_SensingPulse.last_s_v_pulse)) 
						&& ((c_PacingPulse.last_p_v_pulse + _nz_PulseWidth ( c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time)) 
						&& ( c_ProgrammableParameters.c_VRefractoryPeriod.v_refract_period <= (c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)))) 
						&& (_nz_PulseWidth (c_ProgrammableParameters.c_LRL.lower_rate_limit) <= (c_TimeSt.time - c_PacingPulse.last_p_v_pulse))) 
						|| (((((c_TimeSt.time - c_SensingPulse.last_s_v_pulse) < (c_TimeSt.time - c_PacingPulse.last_p_v_pulse)) 
						&& ((c_SensingPulse.last_s_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_HysteresisRateLimit.hysteresis_rate_limit.second)) == c_TimeSt.time)) 
						&& (c_ProgrammableParameters.c_VRefractoryPeriod.v_refract_period <= (c_SensingPulse.last_s_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_HysteresisRateLimit.hysteresis_rate_limit.second)))) 
						&& (c_ProgrammableParameters.c_URL.upper_rate_limit <= (c_TimeSt.time - c_SensingPulse.last_s_v_pulse))))) 
						&& ( c_EventMarkers.ventricular_marker.empty () 
						|| ((*v).m < c_TimeSt.time)));
		}
		void PulseGenerator :: _nz_SetVVI ()
		{
			if(preSetVVI()){
				c_PacingPulse.chg_paced_ventricular (c_ProgrammableParameters.c_VPulseWidth.v_pulse_width, c_ProgrammableParameters.c_VPulseAmpRegulated.v_pulse_amp_regulated.second, c_TimeSt.time);
				c_EventMarkers.chg_ventricular_marker (VP, c_ProgrammableParameters.c_VPulseAmpRegulated.v_pulse_amp_regulated.second, c_ProgrammableParameters.c_VPulseWidth.v_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
			}
		}
		bool PulseGenerator :: preSetAAI () const
		{
				list<quintuple >::const_iterator a = c_EventMarkers.atrial_marker.begin();
				advance(a, (c_EventMarkers.atrial_marker.size () - 1));
		
				return (((((((true == c_BO_MODE.isAAI ()) 
						&& (0 < c_ProgrammableParameters.c_URL.upper_rate_limit)) 
						&& (0 < c_ProgrammableParameters.c_HysteresisRateLimit.hysteresis_rate_limit.second)) 
						&& ((((BOL == c_BatteryStatus.batt_status_level) 
						|| (ERN == c_BatteryStatus.batt_status_level))					
						|| (ERT == c_BatteryStatus.batt_status_level)) 
						|| ( ERP == c_BatteryStatus.batt_status_level))) 
						&& ((((((c_TimeSt.time
										- c_PacingPulse.last_p_a_pulse) < (c_TimeSt.time - c_SensingPulse.last_s_a_pulse)) 
						&& (( c_PacingPulse.last_p_a_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time)) 
						&& (c_ProgrammableParameters.c_ARefractoryPeriod.a_refract_period <= (c_PacingPulse.last_p_a_pulse + _nz_PulseWidth ( c_ProgrammableParameters.c_URL.upper_rate_limit)))) 
						&& (_nz_PulseWidth ( c_ProgrammableParameters.c_LRL.lower_rate_limit) <= (c_TimeSt.time - c_PacingPulse.last_p_a_pulse))) 
						|| ((((c_TimeSt.time - c_SensingPulse.last_s_a_pulse) < (c_TimeSt.time
										- c_PacingPulse.last_p_a_pulse)) 
						&& ((c_SensingPulse.last_s_a_pulse + _nz_PulseWidth ( c_ProgrammableParameters.c_HysteresisRateLimit.hysteresis_rate_limit.second)) == c_TimeSt.time)) 
						&& (c_ProgrammableParameters.c_ARefractoryPeriod.a_refract_period <= ( c_PacingPulse.last_p_a_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_HysteresisRateLimit.hysteresis_rate_limit.second)))))) 
						&& ( ON == c_ProgrammableParameters.c_HysteresisRateLimit.hysteresis_rate_limit.first)) 
						&& ( c_EventMarkers.atrial_marker.empty () 
						|| ((*a).m < c_TimeSt.time)));
		}
		void PulseGenerator :: _nz_SetAAI ()
		{
			if(preSetAAI()){
				c_PacingPulse.chg_paced_atrial (c_ProgrammableParameters.c_APulseWidth.a_pulse_width, c_ProgrammableParameters.c_APulseAmpRegulated.a_pulse_amp_regulated.second, c_TimeSt.time);
				c_EventMarkers.chg_atrial_marker (AP, c_ProgrammableParameters.c_APulseAmpRegulated.a_pulse_amp_regulated.second, c_ProgrammableParameters.c_APulseWidth.a_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
			}
		}
		bool PulseGenerator :: preSetVVT () const
		{
				list<quintuple >::const_iterator v = c_EventMarkers.ventricular_marker.begin();
				advance(v, (c_EventMarkers.ventricular_marker.size () - 1));
				return (((((true == c_BO_MODE.isVVT ()) 
						&& (0 < c_ProgrammableParameters.c_URL.upper_rate_limit)) 
						&& ((((BOL == c_BatteryStatus.batt_status_level)
						|| (ERN == c_BatteryStatus.batt_status_level)) 
						|| ( ERT == c_BatteryStatus.batt_status_level)) 
						|| (ERP == c_BatteryStatus.batt_status_level))) 
						&& ((((((c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time) 
						&& ( c_ProgrammableParameters.c_VRefractoryPeriod.v_refract_period <= (c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)))) 
						&& (_nz_PulseWidth (c_ProgrammableParameters.c_LRL.lower_rate_limit) <= (c_TimeSt.time - c_PacingPulse.last_p_v_pulse))) 
						|| (((c_TimeSt.time < (c_SensingPulse.last_s_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit))) 
						&& ( c_ProgrammableParameters.c_VRefractoryPeriod.v_refract_period <= (c_SensingPulse.last_s_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)))) 
						&& (_nz_PulseWidth (c_ProgrammableParameters.c_LRL.lower_rate_limit) <= (c_SensingPulse.last_s_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit))))) 
						|| ((c_TimeSt.time == c_SensingPulse.last_s_v_pulse) 
						&& (c_ProgrammableParameters.c_VRefractoryPeriod.v_refract_period <= (c_SensingPulse.last_s_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)))))) 
						&& (c_EventMarkers.ventricular_marker.empty () 
						|| ((*v).m < c_TimeSt.time)));
		}
		void PulseGenerator :: _nz_SetVVT ()
		{
			if(preSetVVT()){
				c_PacingPulse.chg_paced_ventricular (c_ProgrammableParameters.c_VPulseAmpRegulated.v_pulse_amp_regulated.second, c_ProgrammableParameters.c_VPulseWidth.v_pulse_width, c_TimeSt.time);
				c_EventMarkers.chg_ventricular_marker (VP, c_ProgrammableParameters.c_VPulseAmpRegulated.v_pulse_amp_regulated.second, c_ProgrammableParameters.c_VPulseWidth.v_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
			}
		}
		bool PulseGenerator :: preSetAAT () const
		{
				list<quintuple >::const_iterator a = c_EventMarkers.atrial_marker.begin();
				advance(a, (c_EventMarkers.atrial_marker.size () - 1));
		
				return ((((((true == c_BO_MODE.isAAT ()) 
						&& (0 < c_ProgrammableParameters.c_URL.upper_rate_limit)) 
						&& (0 < c_ProgrammableParameters.c_LRL.lower_rate_limit))
						
						&& ((((BOL == c_BatteryStatus.batt_status_level) 
						|| ( ERN == c_BatteryStatus.batt_status_level)) 
						|| (ERT == c_BatteryStatus.batt_status_level)) 
						|| (ERP == c_BatteryStatus.batt_status_level))) 
						&& ((((((c_PacingPulse.last_p_a_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)) == c_TimeSt.time) 
						&& ( c_ProgrammableParameters.c_ARefractoryPeriod.a_refract_period <= (c_PacingPulse.last_p_a_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)))) 
						&& (_nz_PulseWidth (c_ProgrammableParameters.c_LRL.lower_rate_limit) <= (c_PacingPulse.last_p_v_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)))) 
						|| ((c_TimeSt.time < (c_SensingPulse.last_s_a_pulse + _nz_PulseWidth ( c_ProgrammableParameters.c_URL.upper_rate_limit))) 
						&& (c_ProgrammableParameters.c_ARefractoryPeriod.a_refract_period <= (c_SensingPulse.last_s_a_pulse + _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit))))) 
						|| ((c_TimeSt.time == c_SensingPulse.last_s_a_pulse) 
						&& (c_ProgrammableParameters.c_ARefractoryPeriod.a_refract_period <= (c_SensingPulse.last_s_a_pulse + _nz_PulseWidth ( c_ProgrammableParameters.c_URL.upper_rate_limit)))))) 
						&& (c_EventMarkers.atrial_marker.empty () 
						|| ((*a).m < c_TimeSt.time)));
		}
		void PulseGenerator :: _nz_SetAAT ()
		{
			if(preSetAAT()){
				c_PacingPulse.chg_paced_atrial (c_ProgrammableParameters.c_APulseWidth.a_pulse_width, c_ProgrammableParameters.c_APulseAmpRegulated.a_pulse_amp_regulated.second, c_TimeSt.time);
				c_EventMarkers.chg_atrial_marker (AP, c_ProgrammableParameters.c_APulseAmpRegulated.a_pulse_amp_regulated.second, c_ProgrammableParameters.c_APulseWidth.a_pulse_width, c_NoiseDetection.noise, c_TimeSt.time);
			}
		}
		bool PulseGenerator :: preAtriumStartTime () const
		{
				return (((C_DUAL == c_BO_MODE.bo_mode.chambers_sensed) 
						|| (C_ATRIUM == c_BO_MODE.bo_mode.chambers_sensed)) 
						&& ((c_MeasuredParameters.c_PWave.p_wave < c_TimeSt.a_curr_measurement) 
						&& (0 == c_MeasuredParameters.c_PWave.p_wave)));
		}
		void PulseGenerator :: _nz_AtriumStartTime (){
		if(preAtriumStartTime())
		{
				c_MeasuredParameters.set_p_wave (c_TimeSt.a_curr_measurement);
				c_TimeSt.chg_a_start_time (c_TimeSt.time);
		}
		}
		bool PulseGenerator :: preAtriumMax () const
		{
				return (((C_DUAL == c_BO_MODE.bo_mode.chambers_sensed) 
						|| (C_ATRIUM == c_BO_MODE.bo_mode.chambers_sensed)) 
						&& ((c_MeasuredParameters.c_PWave.p_wave < c_TimeSt.a_curr_measurement) 
						&& (0 < c_MeasuredParameters.c_PWave.p_wave)));
		}
		void PulseGenerator :: _nz_AtriumMax (){
		if(preAtriumMax())
		{
				c_MeasuredParameters.set_p_wave (c_TimeSt.a_curr_measurement);
				c_TimeSt.chg_a_max (c_TimeSt.a_curr_measurement);
		}}
		bool PulseGenerator :: preAtriumEndTime () const
		{
				return (((C_DUAL == c_BO_MODE.bo_mode.chambers_sensed) 
						|| (C_ATRIUM == c_BO_MODE.bo_mode.chambers_sensed)) 
						&& ((0 == c_TimeSt.a_curr_measurement)
						&& (0 < c_MeasuredParameters.c_PWave.p_wave)));
		}
		void PulseGenerator :: _nz_AtriumEndTime (){
		if(preAtriumEndTime())
		{

				c_MeasuredParameters.set_p_wave (0);
				c_TimeSt.chg_a_delay (c_TimeSt.time);
		}
		}
		bool PulseGenerator :: preAtrialMeasurement () const
		{
				return ((preAtriumStartTime () 
						|| preAtriumMax ()) 
						|| preAtriumEndTime ());
		}
		void PulseGenerator :: _nz_AtrialMeasurement ()
		{
				if (preAtriumStartTime ())
				{ _nz_AtriumStartTime ();
				}
				else if (preAtriumMax ())
				{ _nz_AtriumMax ();
				}
				else if (preAtriumEndTime ())
				{ _nz_AtriumEndTime ();
				}
				else
				{
				}
		}
		bool PulseGenerator :: preVentricularStartTime () const
		{
				return (((C_DUAL == c_BO_MODE.bo_mode.chambers_sensed) 
						|| (C_VENTRICLE == c_BO_MODE.bo_mode.chambers_sensed)) 
						&& ((c_MeasuredParameters.c_RWave.r_wave < c_TimeSt.v_curr_measurement) 
						&& (0 == c_MeasuredParameters.c_RWave.r_wave)));
		}
		void PulseGenerator :: _nz_VentricularStartTime (){
		if(preVentricularStartTime())
		{
				c_MeasuredParameters.set_r_wave (c_TimeSt.v_curr_measurement);
				c_TimeSt.chg_v_start_time (c_TimeSt.time);
		}}
		bool PulseGenerator :: preVentricularMax () const
		{
				return (((C_DUAL == c_BO_MODE.bo_mode.chambers_sensed) 
						|| (C_VENTRICLE == c_BO_MODE.bo_mode.chambers_sensed)) 
						&& ((c_MeasuredParameters.c_RWave.r_wave < c_TimeSt.v_curr_measurement) 
						&& (0 < c_MeasuredParameters.c_RWave.r_wave)));
		}
		void PulseGenerator :: _nz_VentricularMax (){
		if(preVentricularMax())
		{
				c_MeasuredParameters.set_r_wave (c_TimeSt.v_curr_measurement);
				c_TimeSt.chg_v_max (c_TimeSt.v_curr_measurement);
		}}
		bool PulseGenerator :: preVentricularEndTime () const
		{
				return (((C_DUAL == c_BO_MODE.bo_mode.chambers_sensed) 
						|| (C_VENTRICLE == c_BO_MODE.bo_mode.chambers_sensed)) 
						&& ((0 == c_TimeSt.v_curr_measurement) 
						&& (0 < c_MeasuredParameters.c_RWave.r_wave)));
		}
		void PulseGenerator :: _nz_VentricularEndTime (){
		if(preVentricularEndTime())
		{
				c_MeasuredParameters.set_r_wave (0);
				c_TimeSt.chg_v_delay (c_TimeSt.time);
		}}
		bool PulseGenerator :: preVentricularMeasurement () const
		{
				return ((preVentricularStartTime () 
							|| preVentricularMax ()) 
							|| preVentricularEndTime ());
		}
		void PulseGenerator :: _nz_VentricularMeasurement ()
		{
				if (preVentricularStartTime ())
				{ _nz_VentricularStartTime ();
				}
				else if (preVentricularMax ())
				{ _nz_VentricularMax ();
				}
				else if (preVentricularEndTime ())
				{ _nz_VentricularEndTime ();
				}
				else
				{
				}
		}
		bool PulseGenerator :: preSensingModule () const
		{
				return (preAtrialMeasurement () 
		|| preVentricularMeasurement ());
		}
		bool PulseGenerator :: preAtrialSensedMarkerA () const
		{
				list<quintuple >::const_iterator i = c_EventMarkers.atrial_marker.begin();
				advance(i, (c_EventMarkers.atrial_marker.size () - 1));
				return (((1 < c_EventMarkers.atrial_marker.size()) 
							&& ((*i).m < c_TimeSt.time)) 
							&& ((c_TimeSt.a_start_time - (*i).m) < _nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit)));
		}
		bool PulseGenerator :: preAtrialSensedMarkerB () const
		{
				list<quintuple >::const_iterator i = c_EventMarkers.atrial_marker.begin();
				advance(i, (c_EventMarkers.atrial_marker.size () - 1));
				return (((1 < c_EventMarkers.atrial_marker.size()) 
							&& ((*i).m < c_TimeSt.time)) 
							&& (_nz_PulseWidth (c_ProgrammableParameters.c_URL.upper_rate_limit) < (c_TimeSt.a_start_time - (*i).m)));
		}
		bool PulseGenerator :: preAtrialSensedMarker () const
		{
				return (preAtrialSensedMarkerA () || preAtrialSensedMarkerB ());
		}
		void PulseGenerator :: _nz_AtrialSensedMarker (){
		if(preAtrialSensedMarker())
		{
				if (c_EventMarkers.atrial_marker.empty ())
				{ 
					c_EventMarkers.chg_atrial_marker (AS, (c_TimeSt.a_delay - c_TimeSt.a_start_time), c_TimeSt.a_max, c_NoiseDetection.noise, c_TimeSt.time); 
					c_SensingPulse.chg_sensed_atrial ((c_TimeSt.a_delay - c_TimeSt.a_start_time), c_TimeSt.a_max, c_TimeSt.time);
				}
				else if (preAtrialSensedMarkerA ())
				{ 
					c_EventMarkers.chg_atrial_marker (AS, (c_TimeSt.a_delay - c_TimeSt.a_start_time), c_TimeSt.a_max, c_NoiseDetection.noise, c_TimeSt.time); 
					c_SensingPulse.chg_sensed_atrial ((c_TimeSt.a_delay - c_TimeSt.a_start_time), c_TimeSt.a_max, c_TimeSt.time);
				}
				else if (preAtrialSensedMarkerB ())
				{ 
					c_EventMarkers.chg_atrial_marker (AT, (c_TimeSt.a_delay - c_TimeSt.a_start_time), c_TimeSt.a_max, c_NoiseDetection.noise, c_TimeSt.time); 
					c_SensingPulse.chg_sensed_atrial ((c_TimeSt.a_delay - c_TimeSt.a_start_time), c_TimeSt.a_max, c_TimeSt.time);
				}
				else
				{
				}
		}}
		bool PulseGenerator :: preVentricularSensedMarker () const
		{
				list<quintuple >::const_iterator i = c_EventMarkers.ventricular_marker.begin();
				advance(i, (c_EventMarkers.ventricular_marker.size () - 1));
				return (((c_TimeSt.v_start_time <= c_TimeSt.v_delay) 
					&& (c_EventMarkers.ventricular_marker.empty () 
					|| ((*i).m < c_TimeSt.v_start_time))) 
					&& ((C_VENTRICLE == c_BO_MODE.bo_mode.chambers_sensed) 
					|| (C_DUAL == c_BO_MODE.bo_mode.chambers_sensed)));
		}
		



		void PulseGenerator :: _nz_VentricularSensedMarker (){
		if(preVentricularSensedMarker())
		{
				list<quintuple >::const_iterator v = c_EventMarkers.ventricular_marker.begin();
				advance(v, (c_EventMarkers.ventricular_marker.size () - 1));
				
				if(c_EventMarkers.ventricular_marker.empty() && c_EventMarkers.atrial_marker.empty())
				{
					c_SensingPulse.chg_sensed_ventricular((c_TimeSt.v_delay - c_TimeSt.v_start_time),
										c_TimeSt.v_max, c_TimeSt.v_start_time, 0);
					c_EventMarkers.chg_ventricular_marker(VS, c_TimeSt.v_max,
							(c_TimeSt.v_delay - c_TimeSt.v_start_time),
							c_NoiseDetection.noise, c_TimeSt.v_start_time);
				}
				else if(c_EventMarkers.ventricular_marker.size() > 1 
							&& !c_EventMarkers.atrial_marker.empty() 
							&& (c_EventMarkers.ventricular_marker.back().m 
									< c_EventMarkers.atrial_marker.back().m 
									< c_TimeSt.v_start_time)){
					 c_SensingPulse.chg_sensed_ventricular((c_TimeSt.v_delay - c_TimeSt.v_start_time),
										c_TimeSt.v_max, c_TimeSt.v_start_time,
										(c_EventMarkers.ventricular_marker.back().m - (*v).m));
					 c_EventMarkers.chg_ventricular_marker(VS, c_TimeSt.v_max,
							(c_TimeSt.v_delay - c_TimeSt.v_start_time),
							c_NoiseDetection.noise, c_TimeSt.v_start_time);
				}
				else if(c_EventMarkers.ventricular_marker.size() > 1 
							&& !c_EventMarkers.ventricular_marker.empty()
							&& !c_EventMarkers.atrial_marker.empty ()
							&& !(c_EventMarkers.ventricular_marker.back().m 
										< c_EventMarkers.atrial_marker.back().m 
										< c_TimeSt.v_start_time)){
					c_SensingPulse.chg_sensed_ventricular((c_TimeSt.v_delay - c_TimeSt.v_start_time), c_TimeSt.v_max,
										c_TimeSt.v_start_time,
										(c_EventMarkers.ventricular_marker.back().m - (*v).m));
					c_EventMarkers.chg_ventricular_marker(PVC, c_TimeSt.v_max,
							(c_TimeSt.v_delay - c_TimeSt.v_start_time),
							c_NoiseDetection.noise, c_TimeSt.v_start_time);
				}
			}
			else {}
		}

		void PulseGenerator :: _nz_AugmentationMarker ()
		{
				list<quintuple >::const_iterator a = c_EventMarkers.atrial_marker.begin();
				advance(a, (c_EventMarkers.atrial_marker.size () - 1));
				list<quintuple >::const_iterator v = c_EventMarkers.ventricular_marker.begin();
				std::advance(v, (c_EventMarkers.ventricular_marker.size () - 1));
				if ((((!c_EventMarkers.atrial_marker.empty()) 
							&& (c_EventMarkers.atrial_marker.size() > 0)) 
							&& (((*a).x == AS) 
							&& ( c_EventMarkers.atrial_marker.back().x == AT))))
				{ 
					c_EventMarkers.chg_augmentation_marker (ATR_Dur, c_EventMarkers.atrial_marker.back().m);
				}
				else if ((((!c_EventMarkers.atrial_marker.empty ()) 
							&& (c_EventMarkers.atrial_marker.size() > 0)) 
							&& (((*a).x == AT)				
							&& (c_EventMarkers.atrial_marker.back ().x == AT))))
				{ 
					c_EventMarkers.chg_augmentation_marker (ATR_FB, c_EventMarkers.atrial_marker.back().m);
				}
				else if ((((!c_EventMarkers.atrial_marker.empty ()) 
							&& (c_EventMarkers.atrial_marker.size() > 0)) 
							&& (((*a).x == AT)
							&& (c_EventMarkers.atrial_marker.back ().x == AS))))
				{ 
					c_EventMarkers.chg_augmentation_marker (ATR_End, c_EventMarkers.atrial_marker.back().m);
				}
				else if ((((!c_EventMarkers.ventricular_marker.empty ()) 
							&& (c_EventMarkers.atrial_marker.size() > 0)) 
							&& (((*v).x == VS) 
							&& (c_EventMarkers.ventricular_marker.back ().x == PVC))))
				{ 
					c_EventMarkers.chg_augmentation_marker (PVPext, c_EventMarkers.ventricular_marker.back().m);
				}
				else
				{
				}
		}
		void PulseGenerator :: _nz_SetMode ()
		{
				if (preSetVOO ())
				{ _nz_SetVOO ();
				}
				else if (preSetAOO ())
				{ _nz_SetAOO ();
				}
				else if (preSetDOO ())
				{ _nz_SetDOO ();
				}
				else if (preSetDDI ())
				{ _nz_SetDDI ();
				}
				else if (preSetVDD ())
				{ _nz_SetVDD ();
				}
				else if (preSetVVI ())
				{ _nz_SetVVI ();
				}
				else if (preSetAAI ())
				{ _nz_SetAAI ();
				}
				else if (preSetVVT ())
				{ _nz_SetVVT ();
				}
				else if (preSetAAT ())
				{ _nz_SetAAT ();
				}
				else
				{
				}
		}
		bool PulseGenerator :: preSensingMarkers () const
		{
				return (preAtrialSensedMarker () || preVentricularSensedMarker ());
		}
		void PulseGenerator :: _nz_SensingMarkers ()
		{
				if (preAtrialSensedMarker ())
				{ 
					_nz_AtrialSensedMarker ();
				}
				else if (preVentricularSensedMarker ())
				{ 
					_nz_VentricularSensedMarker ();
				}
				else if (true)
				{ 
					_nz_AugmentationMarker ();
				}
				else
				{
				}
		}
		void PulseGenerator :: _nz_SensingModule ()
		{
				if (preVentricularMeasurement ())
				{ 
					_nz_VentricularMeasurement ();
				}
				else if (preAtrialMeasurement ())
				{ 
					_nz_AtrialMeasurement ();
				}
				else
				{
				}
		}
		void PulseGenerator :: _nz_SetTimer ()
		{
				c_TimeSt.chg_time((1 + c_TimeSt.time));
		}
		void PulseGenerator :: _nz_BradyTherapy ()
		{
				_nz_SetTimer ();
				_nz_SensingModule ();
				_nz_SensingMarkers ();
				_nz_SetMode ();
		}
		void PulseGenerator :: _nz_GetPWave (const int device_p_wave)
		{
				c_MeasuredParameters.set_p_wave (device_p_wave);
		}
		void PulseGenerator :: _nz_GetRWave (const int device_r_wave)
		{
				c_MeasuredParameters.set_r_wave (device_r_wave);
		}
		void PulseGenerator :: _nz_GetBatteryVoltage (const int device_battery_voltage)
		{
				c_MeasuredParameters.set_battery_voltage (device_battery_voltage);
		}
		void PulseGenerator :: _nz_GetLeadImpedance (const int device_lead_impedance)
		{
			if(checkRange(device_lead_impedance,100,2500)){
				c_MeasuredParameters.set_lead_impedance (device_lead_impedance);
			}
		}
		void PulseGenerator :: _nz_AtrialPacedEventHistogram ()
		{
				c_HistogramsSt.chg_atrial_paced (30, 40, countAP (30, 40));
				c_HistogramsSt.chg_atrial_paced (40, 50, countAP (40, 50));
				c_HistogramsSt.chg_atrial_paced (50, 60, countAP (50, 60));
				c_HistogramsSt.chg_atrial_paced (60, 70, countAP (60, 70));
				c_HistogramsSt.chg_atrial_paced (70, 80, countAP (70, 80));
				c_HistogramsSt.chg_atrial_paced (80, 90, countAP (80, 90));
				c_HistogramsSt.chg_atrial_paced (90, 100, countAP (90, 100));
				c_HistogramsSt.chg_atrial_paced (100, 110, countAP (100, 110));
				c_HistogramsSt.chg_atrial_paced (110, 120, countAP (110, 120));
				c_HistogramsSt.chg_atrial_paced (120, 130, countAP (120, 130));
				c_HistogramsSt.chg_atrial_paced (130, 140, countAP (130, 140));
				c_HistogramsSt.chg_atrial_paced (140, 150, countAP (140, 150));
				c_HistogramsSt.chg_atrial_paced (150, 160, countAP (150, 160));
				c_HistogramsSt.chg_atrial_paced (170, 175, countAP (170, 175));
		}
		void PulseGenerator :: _nz_AtrialSensedEventHistogram ()
		{
				c_HistogramsSt.chg_atrial_sensed (30, 40, countAS (30, 40));
				c_HistogramsSt.chg_atrial_sensed (40, 50, countAS (40, 50));
				c_HistogramsSt.chg_atrial_sensed (50, 60, countAS (50, 60));
				c_HistogramsSt.chg_atrial_sensed (60, 70, countAS (60, 70));
				c_HistogramsSt.chg_atrial_sensed (70, 80, countAS (70, 80));
				c_HistogramsSt.chg_atrial_sensed (80, 90, countAS (80, 90));
				c_HistogramsSt.chg_atrial_sensed (90, 100, countAS (90, 100));
				c_HistogramsSt.chg_atrial_sensed (100, 110, countAS (100, 110));
				c_HistogramsSt.chg_atrial_sensed (110, 120, countAS (110, 120));
				c_HistogramsSt.chg_atrial_sensed (120, 130, countAS (120, 130));
				c_HistogramsSt.chg_atrial_sensed (130, 140, countAS (130, 140));
				c_HistogramsSt.chg_atrial_sensed (140, 150, countAS (140, 150));
				c_HistogramsSt.chg_atrial_sensed (150, 160, countAS (150, 160));
				c_HistogramsSt.chg_atrial_sensed (170, 175, countAS (170, 175));
		}
		void PulseGenerator :: _nz_VentricularSensedEventHistogram ()
		{
				c_HistogramsSt.chg_ventricular_sensed (30, 40, countVS (30, 40));
				c_HistogramsSt.chg_ventricular_sensed (40, 50, countVS (40, 50));
				c_HistogramsSt.chg_ventricular_sensed (50, 60, countVS (50, 60));
				c_HistogramsSt.chg_ventricular_sensed (60, 70, countVS (60, 70));
				c_HistogramsSt.chg_ventricular_sensed (70, 80, countVS (70, 80));
				c_HistogramsSt.chg_ventricular_sensed (80, 90, countVS (80, 90));
				c_HistogramsSt.chg_ventricular_sensed (90, 100, countVS (90, 100));
				c_HistogramsSt.chg_ventricular_sensed (100, 110, countVS (100, 110));
				c_HistogramsSt.chg_ventricular_sensed (110, 120, countVS (110, 120));
				c_HistogramsSt.chg_ventricular_sensed (120, 130, countVS (120, 130));
				c_HistogramsSt.chg_ventricular_sensed (130, 140, countVS (130, 140));
				c_HistogramsSt.chg_ventricular_sensed (140, 150, countVS (140, 150));
				c_HistogramsSt.chg_ventricular_sensed (150, 160, countVS (150, 160));
				c_HistogramsSt.chg_ventricular_sensed (170, 175, countVS (170, 175));
		}
		void PulseGenerator :: _nz_VentricularPacedEventHistogram ()
		{
				c_HistogramsSt.chg_ventricular_paced (30, 40, countVP (30,  40));
				c_HistogramsSt.chg_ventricular_paced (40, 50, countVP (40,  50));
				c_HistogramsSt.chg_ventricular_paced (50, 60, countVP (50,  60));
				c_HistogramsSt.chg_ventricular_paced (60, 70, countVP (60,  70));
				c_HistogramsSt.chg_ventricular_paced (70, 80, countVP (70,  80));
				c_HistogramsSt.chg_ventricular_paced (80, 90, countVP (80,  90));
				c_HistogramsSt.chg_ventricular_paced (90, 100, countVP (90, 100));
				c_HistogramsSt.chg_ventricular_paced (100, 110, countVP (100, 110));
				c_HistogramsSt.chg_ventricular_paced (110, 120, countVP (110, 120));
				c_HistogramsSt.chg_ventricular_paced (120, 130, countVP (120, 130));
				c_HistogramsSt.chg_ventricular_paced (130, 140, countVP (130, 140));
				c_HistogramsSt.chg_ventricular_paced (140, 150, countVP (140, 150));
				c_HistogramsSt.chg_ventricular_paced (150, 160, countVP (150, 160));
				c_HistogramsSt.chg_ventricular_paced (170, 175, countVP (170, 175));
		}
		void PulseGenerator :: _nz_PVCEventHistogram ()
		{
				c_HistogramsSt.chg_pvc_event (countPVC ());
		}
		void PulseGenerator :: _nz_AtrialTachycardiaEventHistogram ()
		{
				c_HistogramsSt.chg_atrial_tachy (countAT ());
		}
		void PulseGenerator :: _nz_SetBradycardiaOperationMode (const BOM bradycardia_op_mode)
		{
				c_BO_MODE.chg_bo_mode (bradycardia_op_mode);
		}
		PulseGenerator :: PulseGenerator ()
		{
		}
//int main(){return 0;}


// End of file.
