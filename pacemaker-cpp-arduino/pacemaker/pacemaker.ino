
//***********************************************************************************************
//* File: 'pacemaker.ino'
//*
//* by Artur Oliveira Gomes - December 16th 2013
//***********************************************************************************************
#include <StandardCplusplus.h>
#include <serstream>
#include <iterator>

#include "Main1.h"

//init pacemaker object instantiation

BOM *_bom = new BOM(OFF, C_VENTRICLE, C_NONE, R_NONE, false);
BO_MODE *bm = new BO_MODE(*_bom);
LRL *_lrl = new LRL();
URL *_url = new URL();
MaxSensorRate *_msr = new MaxSensorRate();
FixedAVDelay *_favd = new FixedAVDelay();
DynamicAVDelay *_davd = new DynamicAVDelay();
MinDynamicAVDelay *_mdavd = new MinDynamicAVDelay();
SensedAVDelayOffset *_savdo = new SensedAVDelayOffset();
APulseAmpRegulated *_apar = new APulseAmpRegulated();
VPulseAmpRegulated *_vpar = new VPulseAmpRegulated();
APulseAmpUnregulated *_apau = new APulseAmpUnregulated();
VPulseAmpUnregulated *_vpau = new VPulseAmpUnregulated();
APulseWidth *_apw = new APulseWidth();
VPulseWidth *_vpw = new VPulseWidth();
ASensitivity *_asen = new ASensitivity();
VSensitivity *_vsen = new VSensitivity();
VRefractoryPeriod *_vrp = new VRefractoryPeriod();
ARefractoryPeriod *_arp = new ARefractoryPeriod();
PVARP *_pvarp = new PVARP();
PVARP_Ext *_pvarpe = new PVARP_Ext();
HysteresisRateLimit *_hrl = new HysteresisRateLimit();
RateSmoothing *_rs = new RateSmoothing();
ATRMode *_atrm = new ATRMode();
ATRDuration *_atrd = new ATRDuration();
ATRFallbackTime *_atrf = new ATRFallbackTime();
VBlanking *_vb = new VBlanking();
ActivityThreshold *_actt = new ActivityThreshold();
ReactTime *_react = new ReactTime();
RespFactor *_respf = new RespFactor();
RecoveryTime *_rect = new RecoveryTime();
ProgrammableParameters *prp =  new ProgrammableParameters(*_lrl, *_url, *_msr, *_favd, *_davd, *_mdavd, *_savdo, *_apar, *_vpar, *_apau, *_vpau, *_apw, *_vpw, *_asen, *_vsen, *_vrp, *_arp, *_pvarp, *_pvarpe, *_hrl, *_rs, *_atrm, *_atrd, *_atrf, *_vb, *_actt, *_react, *_respf, *_rect);

LeadImpedance *_li = new LeadImpedance();
BatteryVoltage *_bv = new BatteryVoltage();
RWave *_rw = new RWave();
PWave *_pw = new PWave();
MeasuredParameters *mp = new MeasuredParameters(*_li, *_bv, *_rw, *_pw);


TimeSt *tst = new TimeSt();

SensingPulse *sp = new SensingPulse();

PacingPulse *pap = new PacingPulse();

BatteryStatus *bs = new BatteryStatus();

NoiseDetection *nd = new NoiseDetection();


EventMarkers *em = new EventMarkers();

Accelerometer *acc = new Accelerometer(0);

//HistogramsSt *hst = new HistogramsSt();

PulseGenerator *pg = new PulseGenerator(*prp, *mp, *bm, *tst, *sp, *pap, *bs, *nd, *em);
unsigned long time0, time1, inc = 0;

void setup() {
  Serial.begin(115200);
}

// The loop routine runs over and over again forever:
void loop() {
  Serial.print("Iteration n:");
  Serial.println(inc);
/*
  time0 = millis();
 // pg->_nz_SetTimer (millis());
  time1 = millis();

  Serial.print("SetTimer: ");
  Serial.println(time1 - time0); //timer
  time0=time1=0;
  time0 = millis();
 // pg->_nz_SensingModule ();
  time1 = millis();

  Serial.print("SensingModule: ");
  Serial.println(time1 - time0); //sensing
  time0=time1=0;
  time0 = millis();
  //pg->_nz_SensingMarkers ();
  time1 = millis();

  Serial.print("SensingMarkers: ");
  Serial.println(time1 - time0); //registering
  time0=time1=0;
*/
  time0 = millis();
 // pg->_nz_SetMode ();
 pg->_nz_BradyTherapy(millis());
  time1 = millis();

  Serial.print("BradyTherapy: ");
  Serial.println(time1 - time0); //pacing
  
  Serial.println(); //
  //inc++;
//delay(1);
}
