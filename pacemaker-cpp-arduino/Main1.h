//***********************************************************************************************
//* File: 'Main1.h'
//*
//* by Artur Oliveira Gomes - December 16th 2013
//***********************************************************************************************

#ifndef Main1_hpp
#define Main1_hpp

// Full declarations
//#include <list> // INCLUDE LIST HERE

#include "Classes1.h"

using namespace std;
  class PulseGenerator
    {
    private:
        ProgrammableParameters c_ProgrammableParameters;
        MeasuredParameters c_MeasuredParameters;
        BO_MODE c_BO_MODE;
        TimeSt c_TimeSt;
        SensingPulse c_SensingPulse;
        PacingPulse c_PacingPulse;
        BatteryStatus c_BatteryStatus;
        NoiseDetection c_NoiseDetection;
        EventMarkers c_EventMarkers;
        Accelerometer c_Accelerometer;
        /*HistogramsSt c_HistogramsSt;*/
    public:
        int _nz_PulseWidth (const int) const;
        int _nz_PulsePerMinute (const int) const;
        int countAP (const int, const int) const;
        int countAS (const int, const int) const;
        int countVP (const int, const int) const;
        int countVS (const int, const int) const;
        int countPVC () const;
        int countAT () const;
        PulseGenerator (const ProgrammableParameters, 
        	const MeasuredParameters, 
        	const BO_MODE, 
        	const TimeSt, 
        	const SensingPulse, 
        	const PacingPulse, 
        	const BatteryStatus, 
        	const NoiseDetection, 
        	const EventMarkers, 
        	const Accelerometer 
        	//const HistogramsSt
        	);
        bool preSetVOO () const;
        void _nz_SetVOO ();
        bool preSetAOO () const;
        void _nz_SetAOO ();
        bool preSetDOO () const;
        void _nz_SetDOO ();
        bool preSetDDI () const;
        void _nz_SetDDI ();
        bool preSetVDD () const;
        void _nz_SetVDD ();
        bool preSetVVI () const;
        void _nz_SetVVI ();
        bool preSetAAI () const;
        void _nz_SetAAI ();
        bool preSetVVT () const;
        void _nz_SetVVT ();
        bool preSetAAT () const;
        void _nz_SetAAT ();
        bool preAtriumStartTime () const;
        void _nz_AtriumStartTime ();
        bool preAtriumMax () const;
        void _nz_AtriumMax ();
        bool preAtriumEndTime () const;
        void _nz_AtriumEndTime ();
        bool preAtrialMeasurement () const;
        void _nz_AtrialMeasurement ();
        bool preVentricularStartTime () const;
        void _nz_VentricularStartTime ();
        bool preVentricularMax () const;
        void _nz_VentricularMax ();
        bool preVentricularEndTime () const;
        void _nz_VentricularEndTime ();
        bool preVentricularMeasurement () const;
        void _nz_VentricularMeasurement ();
        bool preSensingModule () const;
        bool preAtrialSensedMarkerA () const;
        bool preAtrialSensedMarkerB () const;
        bool preAtrialSensedMarker () const;
        void _nz_AtrialSensedMarker ();
        bool preVentricularSensedMarker () const;
        void _nz_VentricularSensedMarker ();
        void _nz_AugmentationMarker ();
        void _nz_SetMode ();
        bool preSensingMarkers () const;
        void _nz_SensingMarkers ();
        void _nz_SensingModule ();
        void _nz_SetTimer ();
        void _nz_BradyTherapy ();
        void _nz_GetPWave (const int);
        void _nz_GetRWave (const int);
        void _nz_GetBatteryVoltage (const int);
        void _nz_GetLeadImpedance (const int);
        void _nz_AtrialPacedEventHistogram ();
        void _nz_AtrialSensedEventHistogram ();
        void _nz_VentricularSensedEventHistogram ();
        void _nz_VentricularPacedEventHistogram ();
        void _nz_PVCEventHistogram ();
        void _nz_AtrialTachycardiaEventHistogram ();
        void _nz_SetBradycardiaOperationMode (const BOM);
        PulseGenerator ();
    };


#endif

// End of file.
