//***********************************************************************************************
//* File: 'Classes0.h'
//*
//* by Artur Oliveira Gomes - December 16th 2013
//***********************************************************************************************
//***********************************************************************************************


#ifndef Classes0_hpp
#define Classes0_hpp
//#include "ecv.h"
class quintuple;
class triple;
class BOM;
class TimeSt;
class SensingPulse;
class PacingPulse;
class BatteryStatus;
class NoiseDetection;
class EventMarkers;
class Accelerometer;
class PWave;
class RWave;
class BatteryVoltage;
class LeadImpedance;
class MeasuredParameters;
class BO_MODE;
class LRL;
class URL;
class MaxSensorRate;
class FixedAVDelay;
class DynamicAVDelay;
class MinDynamicAVDelay;
class SensedAVDelayOffset;
class APulseAmpRegulated;
class VPulseAmpRegulated;
class APulseAmpUnregulated;
class VPulseAmpUnregulated;
class APulseWidth;
class VPulseWidth;
class ASensitivity;
class VSensitivity;
class VRefractoryPeriod;
class ARefractoryPeriod;
class PVARP;
class PVARP_Ext;
class HysteresisRateLimit;
class RateSmoothing;
class ATRMode;
class ATRDuration;
class ATRFallbackTime;
class VBlanking;
class ActivityThreshold;
class ReactTime;
class RespFactor;
class RecoveryTime;
class ProgrammableParameters;
class HistogramsSt;
class BradycardiaSt;
class AtrialBipolarLead;
class VentricularBipolarLead;
class LeadsSt;

#endif

// End of file.
